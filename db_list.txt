id,rank,is_visible

/*財務資訊*/
finance_info
[year(varchar),month(varchar),month_income(varchar),lastym_rate(varchar),addtionrate(varchar),lastym_rate(varchar),headincome(varchar),lasty_income(varchar),yearrate(varchar)]

/*股東專區*/
dividen_history 
[year(varchar),title(varchar),file(varchar)]

/*公司治理*/
policy_director
[year(varchar),fulldate(varchar),comment[text]]

policy_auditing
[title(varchar),comment[text]]***

policy_procedures
[fulldate(varchar),title(varchar),file(varchar)]

policy_photo
[title(varchar),image_pc(varchar),image_mobile(varchar)]

/*活動訊息*/
activity_conference
[year(varchar),title(varchar),file(varchar),type(varchar)[annouce,conference]]

/*服務窗口*/
service_information
[position(varchar),fullname(varchar),phone(varchar),email(varchar)]

service_questions
[title,answer]

/*CSR*/
csr_contact
[title(varchar),comment(text)]

csr_communacation
[]

csr_identification
[people(varchar),concern(text),communicationway(text)]






