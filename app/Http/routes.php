<?php
use App\Http\Controllers\Fantasy\BackendController as myBackEnd;
use ItemMaker as Items;
use Illuminate\Http\Request;


//轉到預設語系
Route::get('/',function(){
	return redirect( Items::url('/'.App::getLocale()) );
});

//轉到預設語系
Route::get('/Fantasy',function(){
	return redirect( Items::url('/'.App::getLocale().'/Fantasy') );
});

//轉到預設語系
Route::get('/home',function(){
	return redirect( Items::url('/'.App::getLocale().'/Fantasy') );
});



/*===後台===*/

Route::group(['prefix'=>'/{locale?}'],function(){

	Route::group(['prefix'=>'/Fantasy','middleware'=> 'auth'],function(){
		Route::group(['prefix'=>'/schema'],function(){
			Route::controller('/','schemaController');
		});
		Route::group(['prefix'=>'Member'],function(){
				Route::controller('/List','Fantasy\Member\MemberController');
				Route::controller('/Service','Fantasy\Member\MemberServiceController');
				Route::controller('/Bonus','Fantasy\Member\BonusController');
			});

		Route::controller('SEO管理', 'SeoController');
		Route::controller('帳號管理/帳號列表', 'Fantasy\AccountController');
		Route::controller('帳號管理/權限', 'Fantasy\RoleController');

		Route::group(['prefix'=>'Product'],function(){
			Route::controller('產品設定', 'Fantasy\Product\ProductSetController');
			Route::controller('產品類別管理', 'Fantasy\Product\ProductCategoryController');
			Route::controller('產品次類別管理', 'Fantasy\Product\ProductSubCategoryController');
			Route::controller('產品管理', 'Fantasy\Product\ProductController');
			Route::controller('ProductContent', 'Fantasy\Product\ProductContentController');
			Route::controller('行銷類別管理', 'Fantasy\Product\MarketingController');
			Route::controller('搜尋類別管理', 'Fantasy\Product\SearchingController');
			Route::controller('ProductDetails', 'Fantasy\Product\ProductDetailsController');
			Route::controller('SizeDetails', 'Fantasy\Product\SizeDetailsController');
			Route::controller('珠寶搜尋項目管理', 'Fantasy\Product\JewelryController');
			Route::controller('商品材質搜尋項目管理', 'Fantasy\Product\MatirialController');
			
			
			
		});

		Route::group(['prefix'=>'Happiness'],function(){
			Route::controller('幸福好評設定', 'Fantasy\Happiness\HappinessSetController');
			Route::controller('幸福好評類別管理', 'Fantasy\Happiness\HappinessCategoryController');	
			Route::controller('幸福好評管理', 'Fantasy\Happiness\HappinessController');
		});

		Route::group(['prefix'=>'Size'],function(){
			Route::controller('商品尺寸設定', 'Fantasy\Size\SizeSetController');
			Route::controller('商品尺寸管理', 'Fantasy\Size\SizeController');			
		});

		Route::group(['prefix'=>'Location'],function(){
			Route::controller('門市據點設定', 'Fantasy\Location\LocationSetController');
			Route::controller('門市據點管理', 'Fantasy\Location\LocationController');			
		});

		Route::group(['prefix'=>'News'],function(){
			Route::controller('最新消息設定', 'Fantasy\News\NewsSetController');
			Route::controller('最新消息管理', 'Fantasy\News\NewsController');
			Route::controller('NewsDetails', 'Fantasy\News\NewsContentController');
		});

		Route::group(['prefix'=>'Reservation'],function(){
			Route::controller('預約鑑賞設定', 'Fantasy\Reservation\ReservationSetController');
			Route::controller('預約鑑賞資料顯示', 'Fantasy\Reservation\ReservationController');
			
		});

		Route::group(['prefix'=>'About'],function(){
			Route::controller('品牌故事設定', 'Fantasy\About\AboutController');
			 Route::controller('設計師照片管理', 'Fantasy\About\DesignerController');
			 Route::controller('售後服務管理', 'Fantasy\About\AfterSellController');
			 Route::controller('國際證書管理', 'Fantasy\About\CertificateController');
			
		});


		Route::group(['prefix'=>'DisCount'],function(){
			Route::controller('折扣基本設定', 'Fantasy\DisCount\DisCountSetController');
			Route::controller('全館折扣', 'Fantasy\DisCount\DisCountPercentController');
			 Route::controller('滿額折扣設定', 'Fantasy\DisCount\DisCountFullController');
			 Route::controller('折扣碼設定', 'Fantasy\DisCount\DisCountCodeController');
			});


		Route::group(['prefix'=>'Contact'],function(){
			Route::controller('聯絡我們設定', 'Fantasy\Contact\ContactSetController');
			 Route::controller('聯絡我們', 'Fantasy\Contact\ContactController');
			
			
		});


		Route::group(['prefix'=>'OrderList'],function(){
			Route::controller('訂單管理', 'Fantasy\OrderList\OrderListController');
		});

		Route::group(['prefix'=>'Home'],function(){
			Route::controller('首頁管理', 'Fantasy\Home\HomeController');
		});

		Route::group(['prefix'=>'ShoppingQa'],function(){
			Route::controller('問與答基本設定', 'Fantasy\Shopping_qa\ShoppingQaSetController');
			Route::controller('問與答內容管理', 'Fantasy\Shopping_qa\ShoppingQaController');
		});
		
		Route::controller('/網站基本設定', 'Fantasy\SettingController');
		Route::get('/','Fantasy\FantasyController@index');
				//{addRoute}
	});

	/*===前台================================================*/
	Route::group(['prefix'=>'/'], function(){
		Route::get('/', 'HomeController@index');
		//產品
		
		Route::get('/productcategory/{id?}', 'front\ProductCategoryController@index');
		
		Route::get('/productsubcategory/{id?}', 'front\ProductSubCategoryController@index');
		
		Route::get('/product/{id?}/{title?}', 'front\ProductController@index');
		
		Route::get('/changemenu/{id?}/{Separate_id?}', 'front\ProductController@changemenu');
		
		Route::get('/marketing/{id?}/{title?}', 'front\ProductController@marketing');
		
		Route::get('/productorder/{order?}/{matirial?}/{jewelry?}', 'front\ProductController@order');
		
		Route::get('/addsession/{productid?}/{separate_id?}/{count?}/{size?}', 'front\SessionController@add');
		
		Route::get('/session/{id?}', 'front\SessionController@session_edit');
		
		Route::get('/sessiondel/{id?}', 'front\SessionController@remove_session');
		
		Route::get('/tocar/{id?}', 'front\SessionController@tocar');
		
		Route::get('/order_list/{id?}', 'front\ProductController@to_order_list');
		
		Route::get('/updatenum/{key?}/{count?}','front\SessionController@updatenum');
		
		Route::get('/updateDisCountCode/{code?}', 'front\SessionController@updateDisCountCode');
		Route::get('/updateDisCountBonus/{bonus?}', 'front\SessionController@updateDisCountBonus');
		Route::get('/updateFriendInvite/{friendid?}', 'front\SessionController@updateFriendInvite');

		Route::get('/send_list/{type?}/{id?}', 'front\PayController@index');

		Route::get('/member_bar/{id?}/{speaker?}/{content?}', 'front\ProductController@orderdetailTalkbar');

		Route::get('/backmember_bar/{id?}/{speaker?}/{content?}', 'front\ProductController@backorderdetailTalkbar');

		Route::get('member/return', 'front\ProductController@backchangerecord');
		Route::get('addfavorite/{id?}', 'front\ProductController@addfavorite');

		Route::get('member/favorite', 'front\ProductSubCategoryController@favorite');

		Route::get('addbonus', 'front\ProductController@addbonus');

		

		//首頁
		Route::get('/home', 'front\HomeController@index');
		Route::get('/', 'front\HomeController@index');

		//幸福見證
		Route::get('/witness', 'front\HappinessController@index');
		Route::get('/witness/{id}/{title?}', 'front\HappinessController@detail');
		//門市據點
		Route::get('/location/{id?}', 'front\LocationController@index');
		Route::get('/locationdetail/{id?}', 'front\LocationController@detail');
		//最新消息
		Route::get('/news', 'front\NewsController@index');
		Route::get('/newsdetails/{id?}/{title?}', 'front\NewsController@details');
		//預約鑑賞
		Route::get('/reservation', 'front\ReservationController@index');
		Route::get('/reservationpost', 'front\ReservationController@detail');

		//品牌故事
		Route::get('/about', 'front\AboutController@index');
		Route::get('/about/story', 'front\AboutController@story');
		Route::get('/about/design', 'front\AboutController@design');
		Route::get('/about/quality', 'front\AboutController@quality');
		Route::get('/about/charity', 'front\AboutController@charity');
		Route::get('/about/service', 'front\AboutController@service');

		//QA
		Route::get('/shopping_qa', 'front\QandAController@index');

		//聯絡我們
		Route::get('/contact', 'front\ContactController@index');
		Route::post('/getpostdata', 'front\ContactController@getdata');

		//金流
		Route::get('/topay/{type?}/{id?}', 'front\PayController@index');
		Route::get('/looklook', 'front\PayController@looklook');
		Route::post('/feedback/{id?}', 'front\PayController@feedback');
		Route::get('/creatorderlist', 'front\PayController@creatorderlist');

		//隱私權		

		Route::get('/privacy', function () {
    		return view( 'zh-tw.include.privacy');
		});


		//前台
Route::group(['prefix'=>'/member'], function(){
		Route::get('/', 'MemberController@index');							//主頁面
		Route::get('/register', 'MemberController@register');				//註冊主頁
		Route::get('/register/timeUp', 'MemberController@delsession');		//時間到刪驗證碼
		Route::get('/forget/timeUp', 'MemberController@delsession');		//時間到刪驗證碼
		Route::get('/register_submit', 'MemberController@registerSubmit');	//送出註冊資料
		Route::get('/chk_code', 'MemberController@codeSubmit');				//檢查驗證碼
		Route::get('/code_submit', 'MemberController@psaawordSubmit');		//驗證完成
		Route::get('/success', 'MemberController@success');					//註冊成功
		Route::get('/getsession', 'MemberController@getsession');
		Route::get('/forget', 'MemberController@forgetPass');				//忘記密碼畫面
		Route::get('/forget_submit', 'MemberController@forgetSubmit');
		Route::post('/login', 'MemberController@login')->name('login');
		Route::get('/order', 'front\ProductController@orderhistory');	
		Route::get('/cancel_list/{id?}', 'front\ProductController@cancel');	
		Route::get('/orderdetail/{id?}', 'front\ProductController@orderdetail');
		Route::post('/ajaxnewaddress', 'front\ProductController@ajaxnewaddress');
		Route::post('/changesession', 'front\ProductController@changesession');
		Route::post('/change_ticket', 'front\ProductController@change_ticket');
		//先放這邊以後要進member 裡面

	});

		Route::group(['prefix'=>'/member', 'middleware' => 'Member'], function(){
			Route::get('/guide', 'MemberController@guide');
			Route::get('/account', 'MemberController@account');
			Route::get('/save', 'MemberController@save');
			Route::get('/change/{type}', 'MemberController@ChangeAcc');
			Route::get('/change_submit', 'MemberController@ChangeSubmit');
			Route::get('/change_pass', 'MemberController@ChangePass');
			Route::post('/photo_submit', 'MemberController@ChangePhoto');
			Route::get('/photo_del', 'MemberController@DelPhoto');
			Route::get('/logout', 'MemberController@logout');
			Route::get('/bonus', 'MbController@bonus');
			Route::get('/address', 'MbController@address');
			Route::get('/new_submit', 'MbController@NewSubmit');
			Route::get('/ajax', 'MbController@ajax');
			Route::get('/edit_submit', 'MbController@EditSubmit');
	  		Route::get('/del_submit', 'MbController@DelSubmit');
		});
	});

	/*===前台==*/

	Route::controllers([
		'auth' => 'Auth\AuthController',
		'password' => 'Auth\PasswordController',
	]);

});


