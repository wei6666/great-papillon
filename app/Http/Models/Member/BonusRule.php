<?php

namespace App\Http\Models\Member;

use Config;

use Illuminate\Database\Eloquent\Model;

class BonusRule extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."bonus_rule");
        }else{
            $this->setTable("bonus_rule");
        }
    }
}