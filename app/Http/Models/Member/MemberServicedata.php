<?php
namespace App\Http\Models\Member;

use Config;

use Illuminate\Database\Eloquent\Model;

class MemberServicedata extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."mb_servicedata");
        }else{
            $this->setTable("mb_servicedata");
        }
    }
}