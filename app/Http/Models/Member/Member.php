<?php

namespace App\Http\Models\Member;

use Config;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."member");
        }else{
            $this->setTable("member");
        }
    }

    /*public function Point()
    {
    	return $this->hasMany('App\Http\Models\Point','mb_id','id')
                    ->where('is_visible',1)
                    ->orderBy('rank','asc');
    }*/

}