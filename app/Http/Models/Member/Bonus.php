<?php

namespace App\Http\Models\Member;

use Config;

use Illuminate\Database\Eloquent\Model;

class Bonus extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."bonus");
        }else{
            $this->setTable("bonus");
        }
    }
}