<?php

namespace App\Http\Models\Member;

use Config;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."address");
        }else{
            $this->setTable("address");
        }
    }
    public function City()
    {
    	return $this->belongsTo('App\Http\Models\Member\City','city');
    }
}