<?php

namespace App\Http\Models\Member;

use Config;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."city");
        }else{
            $this->setTable("city");
        }
    }
    public function Address()
    {
    	return $this->hasMany('App\Http\Models\Member\Address','city','id');
    }

}