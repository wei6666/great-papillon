<?php

namespace App\Http\Models\Location;

use Config;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."location");
        }else{
            $this->setTable("location");
        }
    }

}
