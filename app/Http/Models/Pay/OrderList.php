<?php

namespace App\Http\Models\Pay;

use Config;

use Illuminate\Database\Eloquent\Model;

class OrderList extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."order_list");
        }else{
            $this->setTable("order_list");
        }
    }

}
