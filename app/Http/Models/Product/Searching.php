<?php

namespace App\Http\Models\Product;

use Config;

use Illuminate\Database\Eloquent\Model;

class Searching extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."searching_category");
        }else{
            $this->setTable("searching_category");
        }
    }

}
