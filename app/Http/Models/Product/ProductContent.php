<?php

namespace App\Http\Models\Product;

use Config;

use Illuminate\Database\Eloquent\Model;

class ProductContent extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."product_content");
        }else{
            $this->setTable("product_content");
        }
    }

}
