<?php

namespace App\Http\Models\Product;

use Config;

use Illuminate\Database\Eloquent\Model;

class ProductSize extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."product_size");
        }else{
            $this->setTable("product_size");
        }
    }

}
