<?php

namespace App\Http\Models\Home;

use Config;

use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."home");
        }else{
            $this->setTable("home");
        }
    }

}
