<?php

namespace App\Http\Models\Shopping_qa;

use Config;

use Illuminate\Database\Eloquent\Model;

class Shopping_qa extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."qa");
        }else{
            $this->setTable("qa");
        }
    }

}
