<?php

namespace App\Http\Models\Shopping_qa;

use Config;

use Illuminate\Database\Eloquent\Model;

class Shopping_qa_category extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."qa_category");
        }else{
            $this->setTable("qa_category");
        }
    }

}
