<?php

namespace App\Http\Models\Reservation;

use Config;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."reservation");
        }else{
            $this->setTable("reservation");
        }
    }

}
