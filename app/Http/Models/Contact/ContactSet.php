<?php

namespace App\Http\Models\Contact;

use Config;

use Illuminate\Database\Eloquent\Model;

class ContactSet extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."contact_set");
        }else{
            $this->setTable("contact_set");
        }
    }

}
