<?php

namespace App\Http\Models\Contact;

use Config;

use Illuminate\Database\Eloquent\Model;

class ContactOption extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."contact_option");
        }else{
            $this->setTable("contact_option");
        }
    }

}
