<?php

namespace App\Http\Models\Contact;

use Config;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."contact");
        }else{
            $this->setTable("contact");
        }
    }

}
