<?php

namespace App\Http\Models\Happiness;

use Config;

use Illuminate\Database\Eloquent\Model;

class HappinessSet extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."happiness_set");
        }else{
            $this->setTable("happiness_set");
        }
    }

}
