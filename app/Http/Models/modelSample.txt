<?php

namespace App\Http\Models\{namespace};

use Config;

use Illuminate\Database\Eloquent\Model;

class {className} extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."{SQLname}");
        }else{
            $this->setTable("{SQLname}");
        }
    }


}
