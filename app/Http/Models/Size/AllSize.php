<?php

namespace App\Http\Models\Size;

use Config;

use Illuminate\Database\Eloquent\Model;

class AllSize extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."all_size");
        }else{
            $this->setTable("all_size");
        }
    }

}
