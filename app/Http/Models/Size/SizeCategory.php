<?php

namespace App\Http\Models\Size;

use Config;

use Illuminate\Database\Eloquent\Model;

class SizeCategory extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."size_category");
        }else{
            $this->setTable("size_category");
        }
    }

}
