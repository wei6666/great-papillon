<?php

namespace App\Http\Models\News;

use Config;

use Illuminate\Database\Eloquent\Model;

class NewsPhoto extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."news_photo");
        }else{
            $this->setTable("news_photo");
        }
    }

}
