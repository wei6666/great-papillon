<?php

namespace App\Http\Models\News;

use Config;

use Illuminate\Database\Eloquent\Model;

class NewsContent extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."news_content");
        }else{
            $this->setTable("news_content");
        }
    }

}
