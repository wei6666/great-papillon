<?php

namespace App\Http\Models\News;

use Config;

use Illuminate\Database\Eloquent\Model;

class NewsSet extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."news_set");
        }else{
            $this->setTable("news_set");
        }
    }

}
