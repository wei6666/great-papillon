<?php

namespace App\Http\Models\Discount;

use Config;

use Illuminate\Database\Eloquent\Model;

class DiscountPercent extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."discount_percent");
        }else{
            $this->setTable("discount_percent");
        }
    }

}
