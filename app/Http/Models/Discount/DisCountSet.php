<?php

namespace App\Http\Models\Discount;

use Config;

use Illuminate\Database\Eloquent\Model;

class DisCountSet extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."discount_set");
        }else{
            $this->setTable("discount_set");
        }
    }

}
