<?php

namespace App\Http\Models\Discount;

use Config;

use Illuminate\Database\Eloquent\Model;

class DiscountFull extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."discount_full");
        }else{
            $this->setTable("discount_full");
        }
    }

}
