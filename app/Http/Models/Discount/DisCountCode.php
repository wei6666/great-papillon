<?php

namespace App\Http\Models\Discount;

use Config;

use Illuminate\Database\Eloquent\Model;

class DisCountCode extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."discount_code");
        }else{
            $this->setTable("discount_code");
        }
    }

}
