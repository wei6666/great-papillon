<?php

namespace App\Http\Models\About;

use Config;

use Illuminate\Database\Eloquent\Model;

class DesignerPhoto extends Model
{
    public function __construct()
    {

        if(!empty(Config::get('app.dataBasePrefix')) )
        {
            $this->setTable(Config::get('app.dataBasePrefix')."about_designer_photo");
        }else{
            $this->setTable("about_designer_photo");
        }
    }

}
