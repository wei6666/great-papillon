<?php
namespace App\Http\Controllers;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use ItemMaker;
use Cache;
use Excel;
use Storage;


/*create Fantasy class*/
use App\Http\Controllers\Fantasy\CreateFantasy;


/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\Fantasy\MakeTableController;
use App\Http\Controllers\Fantasy\test;

/*相關的Controller*/
use App\Http\Controllers\Product\FunctionController as ProductFunction;

/*Models*/
use App\Http\Models\Product\ProductItem;
use App\Http\Models\Banner\Banner;
use App\Http\Models\Banner\Category;

class HomeController extends BackendController {

	public $createTable ;
	public $tablearray ;
	/*首頁========================================================*/
	public function index($locale)
	{

		$Banner =  Banner::where('is_visible',1)
                         ->where('title','Home')
                         ->first();

        $BannerImage = Category::where('is_visible',1)
                               ->where('banner_id',$Banner->id)
                               ->orderby('rank','asc')
                               ->get();

        $ProductItem = ProductItem::where('is_visible',1)
        						  ->where('is_show_in_home',1)
        						  ->orderby('rank','asc')
        						  ->get();
		
		return View::make( $locale.'.home.index',
			[
				'Banner' => $Banner,
				'BannerImage' => $BannerImage,
				'ProductItem' => $ProductItem,

			]);
	}

	public function test(){
		$CreateFantasy = new CreateFantasy();
		$setdb = array(
							'en_Csr_policydirector' => array(
						/*year(varchar),fulldate(varchar),comment[text]*/
										'id' => 'increments',
										'rank' => 'Integer',
										'is_visible' => 'Integer',
										'year' =>'string',
										'fulldate' =>'string',
										'comment' =>'text',
										 ),
							'en_Csr_policyaudit' => array(
						/*[title(varchar),comment(text)]*/
										'id' => 'increments',
										'rank' => 'Integer',
										'is_visible' => 'Integer',
										'title' =>'string',
										'comment' =>'text',
										 ),

							'en_Csr_policystep' => array(
						/*fulldate(varchar),title(varchar),file(varchar)*/
										'id' => 'increments',
										'rank' => 'Integer',
										'is_visible' => 'Integer',
										'title' =>'string',
										'fulldate' =>'text',
										'file' =>'text',
										 ),

							'en_Csr_policyphoto' => array(
						/*title(varchar),image_pc(varchar),image_mobile(varchar)*/
										'id' => 'increments',
										'rank' => 'Integer',
										'is_visible' => 'Integer',
										'title' =>'string',
										'comment' =>'text',
										'image_pc' =>'string',
										'image_mobile' => 'string'
										 ),
							'en_Csr_activeconf' => array(
						/*year(varchar),title(varchar),file(varchar),type(varchar)[annouce,conference]*/
										'id' => 'increments',
										'rank' => 'Integer',
										'is_visible' => 'Integer',
										'title' =>'string',
										'year' =>'string',
										'file' =>'string',
										'type' =>'string',

										 ),
							'en_Csr_servieinfo' => array(
						/*position(varchar),fullname(varchar),phone(varchar),email(varchar)*/
										'id' => 'increments',
										'rank' => 'Integer',
										'is_visible' => 'Integer',
										'position' =>'string',
										'fullname' =>'string',
										'phone' =>'string',
										'email' =>'string',
										 ),
							'en_Csr_servieques' => array(
						/*title,answer*/
										'id' => 'increments',
										'rank' => 'Integer',
										'is_visible' => 'Integer',
										'title' =>'string',
										'answer' =>'string',
										 ),

							'en_Csr_contact' => array(
						/*title,answer*/
										'id' => 'increments',
										'rank' => 'Integer',
										'is_visible' => 'Integer',
										'title' =>'string',
										'comment' =>'text',
										 ),
							'en_Csr_identification' => array(
						/*[people(varchar),concern(text),communicationway(text)]*/
										'id' => 'increments',
										'rank' => 'Integer',
										'is_visible' => 'Integer',
										'title' =>'string',
										'concern' =>'text',
										'communicationway' =>'text',
										 ),
							'en_Csr_communacation' => array(
						
										'id' => 'increments',
										'rank' => 'Integer',
										'is_visible' => 'Integer',

										 ),
						  );
		
		$CreateFantasy->tabletest($setdb);

	}

	
}
