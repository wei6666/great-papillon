<?php

namespace App\Http\Controllers;

/**原生函式**/
use Illuminate\Http\Request;
use View;
use Session;
use Mail;
use Config;
use App;

/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;

class LangController extends BackendController
{

    public function __construct()
    {
        parent::__construct();
        echo "LangController __construct.";

    }    

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public static function index()
    {   
        if($_SERVER['REQUEST_URI'] == '/' )
        {
            echo "link is / ";
            return redirect()->to("/".App::getLocale());
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        echo "create";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        echo "store";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        echo $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        echo "edit : ".$id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
