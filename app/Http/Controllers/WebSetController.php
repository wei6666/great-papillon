<?php



namespace App\Http\Controllers;

/**原生函式**/
use Illuminate\Http\Request;
use View;
use Session;
use Mail;
use Config;
use App\Http\Controllers\Fantasy\MakeItemV2;
use Cache;

/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;



/***相關Model*****/
use App\Http\Models\webset;

class WebSetController extends BackendController
{

    public function __construct()
    {
        parent::__construct();

        //系統訊息
        if(!empty(Session::get('Message')))
        {
            View::share('Message',Session::get('Message'));
        }else{
            View::share('Message','');
        }
        if(!empty(Session::get('Message_type')))
        {
            View::share('Message_type',Session::get('Message_type'));
        }else{
            View::share('Message_type','');
        }
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        //檢查
        //LangController::checkLang($lang);
        $webset = webset::findOrFail(1);
        //$webset = webset::findOrFail(1)->toarray();
        //print_r(json_decode($webset->content,true));

        return view('Fantasy.WebSet.index', [
            "data" => json_decode($webset->content, true),
            "seo" => json_decode($webset->seo, true)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function postUpdate(Request $request)
    {
        $data = $request->input('webset');

        //更新Cache
        Cache::forever('globalsSet', $data);

        $find = webset::find(1);
        $find->content = json_encode($data, JSON_UNESCAPED_UNICODE);

        if ($find->save()) {
            return redirect( MakeItemV2::url('Fantasy/網站設定') )->with('Message','儲存成功')->with('Message_type','success');
        }
        else{
            return redirect( MakeItemV2::url('Fantasy/網站設定') )->with('Message','儲存失敗')->with('Message_type','error');

        }
    }
}




?>
