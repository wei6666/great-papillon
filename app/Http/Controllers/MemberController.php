<?php
namespace App\Http\Controllers;
/**原生函式**/
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use View;
use ItemMaker;
use Cache;
use Excel;
use Redirect;
use DateTime;
use Validator;
use Mail;
use DB;
use Hash;

/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;

use App\Http\Controllers\front\ProductController as ProductController;

/**Models**/
use App\Http\Controllers\Controller;
use App\Http\Controllers\SMSHttpSend;
use App\Http\Models\Member\Member;
use App\Http\Models\Member\City;
use App\Http\Models\Member\MemberService;
use App\Http\Models\Member\MemberServicedata;
use App\Http\Models\Member\Bonus;
use App\Http\Models\News\News;
use App\Http\Models\Product\Product;
use App\Http\Models\Product\ProductPhoto;
use App\Http\Models\Pay\OrderList;

class MemberController extends BackendController
{
    /*登入-畫面*/
    public function index($locale){
        if(Session()->has('Member')){
            return Redirect::to($locale.'/member/guide');
        } else {
            return View::make( $locale.'.member.index',
            [
                
            ]);
        }
        
    }
    /*會員-畫面*/
    public function guide($locale){

        ProductController::NewBonusData($locale);
        
        $mb=Member::where('id',Session::get('mb_id'))
                ->first();
        $news=News::where('is_visible',1)
                ->orderBy('date','desc')
                ->first();
        $newPro=Product::where('is_visible',1)
                ->orderBy('created_at','desc')
                ->first();

        $Bonus=Bonus::where('mb_id',Session::get('mb_id'))
                    ->where('is_add',1)
                    ->orWhere(function($query)
                    {
                        $query->where('is_add',0)
                        ->where('sel_type',3);
                    })
                    ->orderBy('created_at','desc')
                    ->get();

        $tempusedbonuscount=0;
        foreach($Bonus as $value)
        {
            $tempusedbonuscount+=$value->bonus;
            
        }

        $orderlastdate='';

        $Member_OrderList=OrderList::where('member_sql_id',Session::get('mb_id'))->where("order_list_type",7)->orderBy("updated_at","desc")->first();
        if(!empty($Member_OrderList->update_at))
        {
            $orderlastdate=date($Member_OrderList->updated_at);
        }
        else
        {
            
            $orderlastdate=date($mb->created_at);
        }

        //沒指定找第一章 沒第一章就沒圖
        $newPro_img= !empty(ProductPhoto::where('product_id',$newPro['id'])->where('is_out_img',1)->first()) ? ProductPhoto::where('product_id',$newPro['id'])->where('is_out_img',1)->first() : !empty(ProductPhoto::where('product_id',$newPro['id'])->first()) ? ProductPhoto::where('product_id',$newPro['id'])->first() : "";
       
       return View::make( $locale.'.member.guide',
            [
                'mb'=>$mb,
                'newPro'=>$newPro,
                'news'=>$news,
                'newPro_img'=>$newPro_img,
                'tempusedbonuscount'=>$tempusedbonuscount,
                'orderlastdate'=>$orderlastdate,
            ]);
    }
    /*註冊-畫面*/
    public function register($locale,Request $request){

        $city = City::where('is_visible',1)
                    ->get();
        $service = MemberService::where('is_visible',1)
                    ->orderBy('rank','asc')
                    ->get();
        $list=[];

        foreach ($service as $key => $value) {
            $list[$key]=MemberServicedata::where('service_id',$value['id'])
                        ->where('is_visible',1)
                        ->orderBy('rank','asc')
                        ->get();
        }
       

        return View::make( $locale.'.member.register',
            [
                'city'=>$city,
                'service'=>$service,
                'list'=>$list
            ]);
    }
    /*忘記密碼-畫面*/
    public function forgetPass($locale){
        
        return View::make( $locale.'.member.password',
            [
                
            ]);
    }
    /*註冊-送驗證碼*/
    public function registerSubmit($locale,Request $request){
        $sms = new SMSHttpSend();
        $userID="papillon";   //發送帳號
        $password="weddingring1985"; //發送密碼
        $subject = "法蝶珠寶-註冊驗證碼通知信";  //簡訊主旨

        $resend = $request->input('resend');
        if(!empty($resend)&&$resend=='true'){   //重送簡訊
            $BID=Session::get('batchID');
            Session::forget('code');
            Session::put('code', rand(10000,99999)); //重新產生驗證碼
            $data['name']=Session::get('name');
            $data['code']=Session::get('code');
            $content = "親愛的".$data['name']."您好,感謝您在法蝶珠寶註冊,以下是您的驗證碼:".$data['code'].",請在5分鐘內完成驗證。"; //簡訊內容
            $mobile = Session::get('mobile'); //接收人之手機號碼
            $sendTime= "20180520000000";//簡訊預定發送時間

            //dd($userID,$password,$subject,$content,$mobile,$sendTime);
            if($sms->sendSMS($userID,$password,$subject,$content,$mobile,$sendTime)){
                $Message = "傳送簡訊成功,請收取手機驗證碼";
                //$Message .= "餘額為：" . $sms->credit . "，此次簡訊批號為：" . $sms->batchID ;
                Session::put('batchID',$sms->batchID);
            } else {
                //dd($userID,$password,$subject,$content,$mobile,$sendTime);
                //重送一次
                if($sms->sendSMS($userID,$password,$subject,$content,$mobile,$sendTime)){
                    $Message = "傳送簡訊成功,請收取手機驗證碼";
                    //$Message .= "餘額為：" . $sms->credit . "，此次簡訊批號為：" . $sms->batchID ;
                    Session::put('batchID',$sms->batchID);
                } else {
                    $Message = "傳送簡訊失敗" /*. $sms->processMsg*/;
                    Session::put('batchID',$sms->batchID);
                }
            }
            //檢查狀態
            /*if($sms->getStatus($userID,$password,$BID)){
                if($sms->status!="100\n"&&$sms->status!="0\n"){//若沒有成功發送
                    //傳送簡訊
                    if($sms->sendSMS($userID,$password,$subject,$content,$mobile,$sendTime)){
                        $Message = "傳送簡訊成功,請收取手機驗證碼";
                        //$Message .= "餘額為：" . $sms->credit . "，此次簡訊批號為：" . $sms->batchID ;
                        Session::put('batchID',$sms->batchID);
                    } else {
                        $Message = "傳送簡訊失敗" /*. $sms->processMsg;
                    }
                }else{
                    $Message='此手機已成功發送驗證碼,請重新確認!';
                }
            } else {
                $Message = "取得狀態失敗，" . $sms->processMsg . "<br />";
            }*/

            //寄信
            /*$user = [];
            $user['sendName']  = "法蝶珠寶";
            $user['fromMail'] = "Jane@wddgroup.com";

            //發信
            if(!empty($data['email'])){
                Mail::send( $locale.'.mail.register', ['data'=>$data], function ($m) use ( $user , $data ) {
                    $m->from( $user['fromMail'] , $user['sendName'] );
                    $m->to( Session::get('email') , Session::get('name') )->subject( "法蝶珠寶-註冊驗證碼通知信" );
                });
            }*/

            Session::put('submitTime', date('Y-m-d H:i:s'));
            Session::put('getcode', 'true');
            return Redirect::to($locale.'/member/register')->with('Message',$Message)->with('Message_type','success');
        }else{
        $rules = [
            'captcha' => 'required|captcha'
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            
            echo '<script>'."alert('驗證碼錯誤！請重新輸入'); window.history.back();".'</script>';
            /*驗證碼錯誤*/
            die;
            $message = ( $locale == 'zh-tw' ) ? '驗證碼錯誤！請重新輸入。' : 'Please Fill In The Correct Validation Code.';

            $data = [
                'message' => $message,
                'formPass' => false
            ];
            $data = json_encode($data, JSON_UNESCAPED_UNICODE);

            return $data;
        }else{
            $data = $request->input('Register');
            $warning='';
            if(empty($data['mobile'])){
                $warning.="手機 ";
            }else{
                $chk_mobile=Member::select('id')
                            ->where('mobile',$data['mobile'])
                            ->first();
                if(!empty($chk_mobile)){
                    echo '<script>'."alert('此手機已申請帳號,請重新確認!'); window.history.back();".'</script>';
                    die;
                }
            }
            if(empty($data['email'])){
                $warning.="E-mail ";
            }else{
                $chk_mail=Member::select('id')
                            ->where('email',$data['email'])
                            ->first();
                if(!empty($chk_mail)){
                    echo '<script>'."alert('此電子郵件已申請帳號,請重新確認!'); window.history.back();".'</script>';
                    die;
                }
            }
            if(empty($data['pass'])){
                $warning.="密碼 ";
            }else{
                if(strlen($data['pass']) < 8){
                    echo '<script>'."alert('請輸入英數組合8字元之密碼,請再試一次!'); window.history.back();".'</script>';
                    die;
                }else{
                    $data['pass']=bcrypt($data['pass']);
                }
            }
            if($warning!="")
            {
                if($locale=='zh-tw')
                    {
                        echo '<script>'."alert('以下欄位未填寫: ".$warning." '); window.history.back();".'</script>';
                        die;
                    }
                    else
                    {
                        echo '<script>'."alert('please enter".$warning."'); window.history.back();".'</script>';
                        die;
                    }
            }
            Session::put('name', $data['name']);
            Session::put('mobile', $data['mobile']);
            Session::put('email', $data['email']);
            Session::put('pass', $data['pass']);
            Session::put('gender', $data['gender']);
            Session::put('city', $data['city']);
            Session::put('code', rand(10000,99999));
            $data['code']=Session::get('code');

    
            /*============================================================================================PHP fsockopen */
            $content = "親愛的".$data['name']."您好,感謝您在法蝶珠寶註冊,以下是您的驗證碼:".$data['code'].",請在5分鐘內完成驗證。"; //簡訊內容

            $mobile = $data["mobile"]; //接收人之手機號碼
            $sendTime= "20180520000000";//簡訊預定發送時間

            //取餘額
            /*if($sms->getCredit($userID,$password)){
                $Message1 = "取得餘額成功，餘額為：" . $sms->credit . "<br />";
            } else {
                $Message1 = "取得餘額失敗，" . $sms->processMsg . "<br />";
            }*/
            //傳送簡訊
            if($sms->sendSMS($userID,$password,$subject,$content,$mobile,$sendTime)){
                $Message = "傳送簡訊成功,請收取手機驗證碼";
                //$Message .= "餘額為：" . $sms->credit . "，此次簡訊批號為：" . $sms->batchID ;
                Session::put('batchID',$sms->batchID);
            } else {
                //重送一次
                if($sms->sendSMS($userID,$password,$subject,$content,$mobile,$sendTime)){
                    $Message = "傳送簡訊成功,請收取手機驗證碼";
                    //$Message .= "餘額為：" . $sms->credit . "，此次簡訊批號為：" . $sms->batchID ;
                    Session::put('batchID',$sms->batchID);
                } else {
                    $Message = "傳送簡訊失敗" /*. $sms->processMsg*/;
                    Session::put('batchID',$sms->batchID);
                }
            }
            /*
            //寄信
            $user = [];
            $user['sendName']  = "法蝶珠寶";
            $user['fromMail'] = "Jane@wddgroup.com";
            //發信
            if(!empty($data['email'])){
                Mail::send( $locale.'.mail.register', ['data'=>$data], function ($m) use ( $user , $data ) {
                    $m->from( $user['fromMail'] , $user['sendName'] );
                    $m->to( $data['email'] , $data['name'] )->subject( "法蝶珠寶-註冊驗證碼通知信" );
                });
            }*/
            $now=date('Y-m-d H:i:s');
            Session::put('submitTime', $now);
            Session::put('test', '1234');
            Session::put('getcode', 'true');
            return Redirect::to($locale.'/member/register')->with('Message',$Message)->with('Message_type','success');
        }
        }
    }
    /*忘記密碼-送驗證碼*/
    public function forgetSubmit($locale,Request $request){
        $sms = new SMSHttpSend();
        $userID="papillon";   //發送帳號
        $password="weddingring1985"; //發送密碼
        $subject = "法蝶珠寶-忘記密碼通知信";  //簡訊主旨

        $resend = $request->input('resend');
        if(!empty($resend)&&$resend=='true'){   //重送簡訊
            Session::forget('forget_code');
            Session::put('forget_code', rand(10000,99999)); //重新產生驗證碼
            $data['name']=Session::get('name');
            $data['code']=Session::get('code');
            $content = "親愛的會員您好,以下是您忘記密碼的驗證碼:".$data['code'].",請在5分鐘內完成驗證。"; //簡訊內容
            $mobile = Session::get('mobile'); //接收人之手機號碼
            $sendTime= "20180520000000";//簡訊預定發送時間
            $msg="親愛的會員您好,以下是您忘記密碼的驗證碼:".Session::get('forget_code').",請在5分鐘內完成驗證。";
            //簡訊
            if($sms->sendSMS($userID,$password,$subject,$content,$mobile,$sendTime)){
                $Message = "傳送簡訊成功,請收取手機驗證碼";
                //$Message .= "餘額為：" . $sms->credit . "，此次簡訊批號為：" . $sms->batchID ;
                Session::put('batchID',$sms->batchID);
            } else {
                //重送一次
                if($sms->sendSMS($userID,$password,$subject,$content,$mobile,$sendTime)){
                    $Message = "傳送簡訊成功,請收取手機驗證碼";
                    //$Message .= "餘額為：" . $sms->credit . "，此次簡訊批號為：" . $sms->batchID ;
                    Session::put('batchID',$sms->batchID);
                } else {
                    $Message = "傳送簡訊失敗" /*. $sms->processMsg*/;
                    Session::put('batchID',$sms->batchID);
                }
            }
            /*//寄信
            $user = [];
            $user['sendName']  = "法蝶珠寶";
            $user['fromMail'] = "Jane@wddgroup.com";

            //發信
            if(!empty($data['email'])){
                Mail::send( $locale.'.mail.forgetPass', ['data'=>$data], function ($m) use ( $user , $data ) {
                    $m->from( $user['fromMail'] , $user['sendName'] );
                    $m->to( Session::get('email') , Session::get('name') )->subject( "法蝶珠寶-忘記密碼驗證碼通知信" );
                });
            }*/
            Session::put('submitTime', date('Y-m-d H:i:s'));
            Session::put('getcode', 'true');
            return Redirect::to($locale.'/member/forget')->with('Message',$Message)->with('Message_type','success');
        }else{

        $rules = [
            'captcha' => 'required|captcha'
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            
            echo '<script>'."alert('驗證碼錯誤！請重新輸入'); window.history.back();".'</script>';
            /*驗證碼錯誤*/
            die;
            $message = ( $locale == 'zh-tw' ) ? '驗證碼錯誤！請重新輸入。' : 'Please Fill In The Correct Validation Code.';

            $data = [
                'message' => $message,
                'formPass' => false
            ];
            $data = json_encode($data, JSON_UNESCAPED_UNICODE);

            return $data;
        }else{
            $data = $request->input('Forget');
            $warning='';
            if(empty($data['mobile'])||empty($data['email'])){
                echo '<script>'."alert('手機或電子郵件未填!'); window.history.back();".'</script>';
                die;
            }else{
                $chk=Member::select('id','name')
                            ->where('mobile',$data['mobile'])
                            ->where('email',$data['email'])
                            ->first();
                if(empty($chk)){
                    echo '<script>'."alert('手機或電子郵件錯誤,請重新確認!'); window.history.back();".'</script>';
                    die;
                }else{
                    Session::put('mobile', $data['mobile']);
                    Session::put('email', $data['email']);
                    Session::put('forgetmb_id', $chk['id']);
                    Session::put('name', $chk['name']);
                    Session::put('forget_code', rand(10000,99999));
                    $data['code']=Session::get('forget_code');
                    $data['name']=$chk['name'];
                    $content = "親愛的會員您好,以下是您忘記密碼的驗證碼:".$data['code'].",請在5分鐘內完成驗證。"; //簡訊內容

                    $mobile = $data["mobile"]; //接收人之手機號碼
                    $sendTime= "20180520000000";//簡訊預定發送時間

                    //傳送簡訊
                    if($sms->sendSMS($userID,$password,$subject,$content,$mobile,$sendTime)){
                        $Message = "傳送簡訊成功,請收取手機驗證碼";
                        //$Message .= "餘額為：" . $sms->credit . "，此次簡訊批號為：" . $sms->batchID ;
                        Session::put('batchID',$sms->batchID);
                    } else {
                        //重送一次
                        if($sms->sendSMS($userID,$password,$subject,$content,$mobile,$sendTime)){
                            $Message = "傳送簡訊成功,請收取手機驗證碼";
                            //$Message .= "餘額為：" . $sms->credit . "，此次簡訊批號為：" . $sms->batchID ;
                            Session::put('batchID',$sms->batchID);
                        } else {
                            $Message = "傳送簡訊失敗" /*. $sms->processMsg*/;
                            Session::put('batchID',$sms->batchID);
                        }
                    }
                    //寄信
                    /*$user = [];
                    $user['sendName']  = "法蝶珠寶";
                    $user['fromMail'] = "Jane@wddgroup.com";
                    //發信
                    if(!empty($data['email'])){
                        Mail::send( $locale.'.mail.forgetPass', ['data'=>$data], function ($m) use ( $user , $data ) {
                            $m->from( $user['fromMail'] , $user['sendName'] );
                            $m->to( $data['email'] , $data['name'] )->subject( "法蝶珠寶-忘記密碼驗證碼通知信" );
                        });
                    }*/

                    Session::put('submitTime', date('Y-m-d H:i:s'));
                    Session::put('getcode', 'true');
                    return Redirect::to($locale.'/member/forget')->with('Message',$Message)->with('Message_type','success');
                }
            }
        }
        }
    }
    /*會員-更改手機或電子郵件*/
    public function ChangeSubmit($locale,Request $request){
        $sms = new SMSHttpSend();
        $userID="papillon";   //發送帳號
        $password="weddingring1985"; //發送密碼
        $subject = "法蝶珠寶-忘記密碼通知信";  //簡訊主旨

        $resend = $request->input('resend');
        if(!empty($resend)&&$resend=='true'){   //重送簡訊
            Session::forget('chg_code');
            Session::put('chg_code', rand(10000,99999)); //重新產生驗證碼
            $data['name']=Session::get('name');
            $data['chg_code']=Session::get('chg_code');
            $content = "親愛的會員您好,以下是您修改帳號資料的驗證碼:".$data['chg_code'].",請在5分鐘內完成驗證。"; //簡訊內容
            $mobile = Session::get('chg_mobile'); //接收人之手機號碼
            $sendTime= "20180520000000";//簡訊預定發送時間
            //簡訊
            if($sms->sendSMS($userID,$password,$subject,$content,$mobile,$sendTime)){
                $Message = "傳送簡訊成功,請收取手機驗證碼";
                //$Message .= "餘額為：" . $sms->credit . "，此次簡訊批號為：" . $sms->batchID ;
                Session::put('batchID',$sms->batchID);
            } else {
                //重送一次
                if($sms->sendSMS($userID,$password,$subject,$content,$mobile,$sendTime)){
                    $Message = "傳送簡訊成功,請收取手機驗證碼";
                    //$Message .= "餘額為：" . $sms->credit . "，此次簡訊批號為：" . $sms->batchID ;
                    Session::put('batchID',$sms->batchID);
                } else {
                    $Message = "傳送簡訊失敗" /*. $sms->processMsg*/;
                    Session::put('batchID',$sms->batchID);
                }
            }
            
            /*//寄信
            $user = [];
            $user['sendName']  = "法蝶珠寶";
            $user['fromMail'] = "Jane@wddgroup.com";

            //發信
            if(!empty(Session::get('chg_email'))){
                Mail::send( $locale.'.mail.chgAcc', ['data'=>$data], function ($m) use ( $user , $data ) {
                    $m->from( $user['fromMail'] , $user['sendName'] );
                    $m->to( Session::get('chg_email') , Session::get('name') )->subject( "法蝶珠寶-更改個資驗證碼通知信" );
                });
            }*/
            Session::forget('submitTime');
            Session::put('submitTime', date('Y-m-d H:i:s'));
            Session::put('getcode', 'true');
            return Redirect::to($locale.'/member/change/1')->with('Message',$Message)->with('Message_type','success');
        }else{
            $rules = [
            'captcha' => 'required|captcha'
            ];
            $validator = Validator::make($request->all(), $rules);
            if($validator->fails()){
                
                echo '<script>'."alert('驗證碼錯誤！請重新輸入'); window.history.back();".'</script>';
                die;
            }else{
            $data = $request->input('Change');
            $warning='';
            if($data['mobile']==Session::get('mobile')&&$data['email']==Session::get('email')){
                echo '<script>'."alert('請至少輸入一組新的手機號碼或電子郵件'); window.history.back();".'</script>';
                die;
            }
            if(empty($data['email'])){
                $warning.="電子郵件 ";

            }else{
                $chk_email=Member::select('id')
                            ->where('email',$data['email'])
                            ->where('id', '!=', Session::get('mb_id'))
                            ->first();
                if(!empty($chk_email)){
                    echo '<script>'."alert('電子郵件重複,請重新確認!'); window.history.back();".'</script>';
                    die;
                }
            }
            if(empty($data['mobile'])){
                $warning.="手機 ";
            }else{
                $chk_mobile=Member::select('id')
                            ->where('mobile',$data['mobile'])
                            ->where('id', '!=', Session::get('mb_id'))
                            ->first();
                if(!empty($chk_mobile)){
                    echo '<script>'."alert('手機號碼重複,請重新確認!'); window.history.back();".'</script>';
                    die;
                }
            }
            
            if($warning!="")
            {
                if($locale=='zh-tw')
                    {
                        echo '<script>'."alert('以下欄位未填寫: ".$warning." '); window.history.back();".'</script>';
                        die;
                    }
                    else
                    {
                        echo '<script>'."alert('please enter".$warning."'); window.history.back();".'</script>';
                        die;
                    }
            }
            Session::put('chg_mobile', $data['mobile']);
            Session::put('chg_email', $data['email']);
            Session::put('chg_code', rand(10000,99999));
            $data['chg_code']=Session::get('chg_code');
            $content = "親愛的會員您好,以下是您修改帳號資料的驗證碼:".$data['chg_code'].",請在5分鐘內完成驗證。"; //簡訊內容

            $mobile = $data["mobile"]; //接收人之手機號碼
            $sendTime= "20180520000000";//簡訊預定發送時間

            //傳送簡訊
            if($sms->sendSMS($userID,$password,$subject,$content,$mobile,$sendTime)){
                $Message = "傳送簡訊成功,請收取手機驗證碼";
                //$Message .= "餘額為：" . $sms->credit . "，此次簡訊批號為：" . $sms->batchID ;
                Session::put('batchID',$sms->batchID);
            } else {
                //重送一次
                if($sms->sendSMS($userID,$password,$subject,$content,$mobile,$sendTime)){
                    $Message = "傳送簡訊成功,請收取手機驗證碼";
                    //$Message .= "餘額為：" . $sms->credit . "，此次簡訊批號為：" . $sms->batchID ;
                    Session::put('batchID',$sms->batchID);
                } else {
                    $Message = "傳送簡訊失敗" /*. $sms->processMsg*/;
                    Session::put('batchID',$sms->batchID);
                }
            }
            //寄信
            /*$user = [];
            $user['sendName']  = "法蝶珠寶";
            $user['fromMail'] = "Jane@wddgroup.com";
            //發信
            if(!empty($data['email'])){
                Mail::send( $locale.'.mail.chdAcc', ['data'=>$data], function ($m) use ( $user , $data ) {
                    $m->from( $user['fromMail'] , $user['sendName'] );
                    $m->to( $data['email'] , $data['name'] )->subject( "法蝶珠寶-更改個資驗證碼通知信" );
                });
            }*/
            Session::put('submitTime', date('Y-m-d H:i:s'));
            Session::put('getcode', 'true');
            return Redirect::to($locale.'/member/change/1')->with('Message',$Message)->with('Message_type','success');
            }
        }
    }



    /*會員-更改密碼*/
    public function ChangePass($locale,Request $request){
        $sms = new SMSHttpSend();
        $userID="papillon";   //發送帳號
        $password="weddingring1985"; //發送密碼
        $subject = "法蝶珠寶-更改密碼通知信";  //簡訊主旨

        $resend = $request->input('resend');
        if(!empty($resend)&&$resend=='true'){   //重送簡訊
            Session::forget('pass_code');
            Session::put('pass_code', rand(10000,99999)); //重新產生驗證碼
            $data['pass_code']=Session::get('pass_code');
            $data['mobile']=Session::get('mobile');
            $data['email']=Session::get('email');
            $data['name']=Session::get('name');
            $content = "親愛的會員您好,以下是您修改帳號資料的驗證碼:".$data['pass_code'].",請在5分鐘內完成驗證。"; //簡訊內容
            $mobile = Session::get('mobile'); //接收人之手機號碼
            $sendTime= "20180520000000";//簡訊預定發送時間
            //簡訊
            if($sms->sendSMS($userID,$password,$subject,$content,$mobile,$sendTime)){
                $Message = "傳送簡訊成功,請收取手機驗證碼";
                //$Message .= "餘額為：" . $sms->credit . "，此次簡訊批號為：" . $sms->batchID ;
                Session::put('batchID',$sms->batchID);
            } else {
                //重送一次
                if($sms->sendSMS($userID,$password,$subject,$content,$mobile,$sendTime)){
                    $Message = "傳送簡訊成功,請收取手機驗證碼";
                    //$Message .= "餘額為：" . $sms->credit . "，此次簡訊批號為：" . $sms->batchID ;
                    Session::put('batchID',$sms->batchID);
                } else {
                    $Message = "傳送簡訊失敗" /*. $sms->processMsg*/;
                    Session::put('batchID',$sms->batchID);
                }
            }
            
            /*//寄信
            $user = [];
            $user['sendName']  = "法蝶珠寶";
            $user['fromMail'] = "Jane@wddgroup.com";

            //發信
            if(!empty(Session::get('chg_email'))){
                Mail::send( $locale.'.mail.chgPass', ['data'=>$data], function ($m) use ( $user , $data ) {
                    $m->from( $user['fromMail'] , $user['sendName'] );
                    $m->to( Session::get('chg_email') , Session::get('name') )->subject( "法蝶珠寶-更改密碼驗證碼通知信" );
                });
            }*/
            Session::forget('submitTime');
            Session::put('submitTime', date('Y-m-d H:i:s'));
            Session::put('getcode', 'true');
            return Redirect::to($locale.'/member/change/2')->with('Message',$Message)->with('Message_type','success');
        }else{
            $rules = [
                'captcha' => 'required|captcha'
            ];
            $validator = Validator::make($request->all(), $rules);
            if($validator->fails()){
                
                echo '<script>'."alert('驗證碼錯誤！請重新輸入'); window.history.back();".'</script>';
                /*驗證碼錯誤*/
                die;
            }else{
                $data = $request->input('Pass');
                $member=Member::select('id','password')
                                ->where('id',Session::get('mb_id'))
                                ->where('is_visible',1)
                                ->first();
                if(!(Hash::check($data['now'],$member['password']))){
                    return Redirect::to($locale.'/member/change/2')->with('Message','舊密碼錯誤,請再試一次!')->with('Message_type','error');
                }else if($data['new']!=$data['new1']){
                    return Redirect::to($locale.'/member/change/2')->with('Message','新密碼與確認密碼不相同,請再試一次!')->with('Message_type','error');
                }else if(strlen($data['new'])!=8){
                    return Redirect::to($locale.'/member/change/2')->with('Message','請輸入英數組合8字元之密碼,請再試一次!')->with('Message_type','error');
                }else{
                    Session::put('new_pass', bcrypt($data['new']));
                    Session::put('pass_code', rand(10000,99999));
                    $data['pass_code']=Session::get('pass_code');
                    $data['mobile']=Session::get('mobile');
                    $data['email']=Session::get('email');
                    $data['name']=Session::get('name');
                    $content = "親愛的會員您好,以下是您修改密碼的驗證碼:".$data['pass_code'].",請在5分鐘內完成驗證。"; //簡訊內容

                    $mobile = $data["mobile"]; //接收人之手機號碼
                    $sendTime= "20180520000000";//簡訊預定發送時間

                    //傳送簡訊
                    if($sms->sendSMS($userID,$password,$subject,$content,$mobile,$sendTime)){
                        $Message = "傳送簡訊成功,請收取手機驗證碼";
                        //$Message .= "餘額為：" . $sms->credit . "，此次簡訊批號為：" . $sms->batchID ;
                        Session::put('batchID',$sms->batchID);
                    } else {
                        //重送一次
                        if($sms->sendSMS($userID,$password,$subject,$content,$mobile,$sendTime)){
                            $Message = "傳送簡訊成功,請收取手機驗證碼";
                            //$Message .= "餘額為：" . $sms->credit . "，此次簡訊批號為：" . $sms->batchID ;
                            Session::put('batchID',$sms->batchID);
                        } else {
                            $Message = "傳送簡訊失敗" /*. $sms->processMsg*/;
                            Session::put('batchID',$sms->batchID);
                        }
                    }
                    //寄信
                    /*$user = [];
                    $user['sendName']  = "法蝶珠寶";
                    $user['fromMail'] = "Jane@wddgroup.com";
                    //發信
                    if(!empty($data['email'])){
                        Mail::send( $locale.'.mail.chgPass', ['data'=>$data], function ($m) use ( $user , $data ) {
                            $m->from( $user['fromMail'] , $user['sendName'] );
                            $m->to( $data['email'] , $data['name'] )->subject( "法蝶珠寶-更改密碼驗證碼通知信" );
                        });
                    }*/
                    Session::put('submitTime', date('Y-m-d H:i:s'));
                    Session::put('getcode', 'true');
                    return Redirect::to($locale.'/member/change/2')->with('Message',$Message)->with('Message_type','success');
                }
            }
        }
    }
    /*判斷驗證碼*/
    public function codeSubmit($locale,Request $request){
        $myCode = '';
        $myCode=$request->input('Vcode1').$request->input('Vcode2').$request->input('Vcode3').$request->input('Vcode4').$request->input('Vcode5');
        
        if(Session()->has('code')){//註冊
            $sessinCode=Session::get('code');
        }elseif(Session()->has('chg_code')){//修改手機或email
            $sessinCode=Session::get('chg_code');
        }elseif(Session()->has('pass_code')){//修改密碼
            $sessinCode=Session::get('pass_code');
        }elseif(Session()->has('forget_code')){//忘記密碼
            $sessinCode=Session::get('forget_code');
        }
        if($myCode==$sessinCode){
            $msg='true';
            return $msg;
        }else{
            $msg='false';
            return $msg;
        }
    }
    /*驗證成功*/
    public function psaawordSubmit($locale,Request $request){      
        if(Session()->has('Member')&&Session()->has('chg_code')){ //修改手機或email
            $id=Session::get('mb_id');
            $Member=Member::find($id);
            $Member->mobile=Session::get('chg_mobile');
            $Member->email=Session::get('chg_email');
            if ($Member->save()) {
                Session::forget('mobile');
                Session::forget('email');
                Session::put('mobile', Session::get('chg_mobile'));
                Session::put('email', Session::get('chg_email'));
                Session::pull('chg_mobile','default');
                Session::pull('chg_email','default');
                Session::pull('chg_code', 'default');
                Session::put('success', 'true');
                return Redirect::to($locale.'/member/change/1')->with('Message','修改完成')->with('Message_type','success');
            }else{
                echo '<script>'."alert('發生錯誤,請稍後再試!'); window.history.back();".'</script>';
                die;
            }
        }elseif(Session()->has('Member')&&Session()->has('pass_code')){ //修改密碼
            $id=Session::get('mb_id');
            $Member=Member::find($id);
            $Member->password=Session::get('new_pass');
            if ($Member->save()) {
                Session::pull('new_pass','default');
                Session::pull('pass_code','default');
                Session::put('success', 'true');
                return Redirect::to($locale.'/member/change/2')->with('Message','修改完成')->with('Message_type','success');
            }else{
                echo '<script>'."alert('發生錯誤,請稍後再試!'); window.history.back();".'</script>';
                die;
            }
        }elseif(Session()->has('forget_code')){ //忘記密碼
            $sms = new SMSHttpSend();
            $userID="papillon";   //發送帳號
            $password="weddingring1985"; //發送密碼
            $subject = "法蝶珠寶-密碼通知信";  //簡訊主旨

            $passlen=8;
            $new_pass='';
            $passlist="abcdefghijklmnopqrstuvwxyz1234567890";
            for ($i=0; $i<$passlen ; $i++) {
                $new_pass .= $passlist{rand(0,35)};
            }
            $id=Session::get('forgetmb_id');
            $Member=Member::find($id);
            $Member->password=bcrypt($new_pass);
            if ($Member->save()) {
                $content = "親愛的會員您好,以下是您的新密碼:".$new_pass.",建議您登入後修改密碼。"; //簡訊內容

                $mobile = Session::get('mobile'); //接收人之手機號碼
                $sendTime= "20180520000000";//簡訊預定發送時間

                //傳送簡訊
                if($sms->sendSMS($userID,$password,$subject,$content,$mobile,$sendTime)){
                    $Message = "傳送簡訊成功,請收取手機簡訊";
                    //$Message .= "餘額為：" . $sms->credit . "，此次簡訊批號為：" . $sms->batchID ;
                    Session::put('batchID',$sms->batchID);
                } else {
                    //重送一次
                    if($sms->sendSMS($userID,$password,$subject,$content,$mobile,$sendTime)){
                        $Message = "傳送簡訊成功,請收取手機簡訊";
                        //$Message .= "餘額為：" . $sms->credit . "，此次簡訊批號為：" . $sms->batchID ;
                        Session::put('batchID',$sms->batchID);
                    } else {
                        $Message = "傳送簡訊失敗" /*. $sms->processMsg*/;
                        Session::put('batchID',$sms->batchID);
                    }
                }

                //寄信
                /*$user = [];
                $user['sendName']  = "法蝶珠寶";
                $user['fromMail'] = "Jane@wddgroup.com";
                //發信
                if(!empty($data['email'])){
                    Mail::send( $locale.'.mail.complete', ['data'=>$data], function ($m) use ( $user , $data ) {
                        $m->from( $user['fromMail'] , $user['sendName'] );
                        $m->to( $data['email'] , $data['name'] )->subject( "法蝶珠寶-忘記密碼通知信" );
                    });
                }*/
                Session::pull('forget_code','default');
                Session::pull('pass_code','default');
                Session::pull('mobile', 'default');
                Session::pull('email', 'default');
                Session::pull('forgetmb_id', 'default');
                Session::pull('name', 'default');
                Session::pull('getcode', 'default');

                Session::put('success', 'true');
                return Redirect::to($locale.'/member/forget')->with('Message',$Message)->with('Message_type','success');
            }else{
                echo '<script>'."alert('發生錯誤,請稍後再試!'); window.history.back();".'</script>';
                die;
            }
        }else{
            $city=City::where('id',Session::get('city'))
                        ->first();
            $myCode=$city['code'].Session::get('gender');
            $now_bonus=500; //註冊贈點

            dd( DB::select('CALL `insert_mb_tw`(?, ?, ?, ?, ?, ?, ?, ?)',[Session::get('name'),Session::get('mobile'),Session::get('email'),Session::get('pass'),Session::get('gender'),Session::get('city'),$myCode,$now_bonus]));
            $result = DB::select('CALL `insert_mb_tw`(?, ?, ?, ?, ?, ?, ?, ?)',[Session::get('name'),Session::get('mobile'),Session::get('email'),Session::get('pass'),Session::get('gender'),Session::get('city'),$myCode,$now_bonus]);
            $result->execute();
            Session::pull('getcode', 'default');
            Session::pull('name', 'default');
            Session::pull('email', 'default');
            Session::pull('mobile','default');
            Session::pull('code', 'default');
            Session::pull('forget_code', 'default');
            Session::pull('submitTime','default');
            Session::pull('pass','default');
            Session::pull('gender','default');
            Session::pull('city','default');
            Session::pull('batchID','default');

            Session::put('success', 'true');

            return Redirect::to($locale.'/member/register');
            
        }
        
    }
    /*查看SESSION*/
    public function getsession($locale,Request $request){
        dd(Session::all());
    }
    /*過期刪除SESSION*/
    public function delsession($locale,Request $request){
        Session::pull('name','default');
        Session::pull('mobile','default');
        Session::pull('email','default');
        Session::pull('code','default');
        Session::pull('pass','default');
        Session::pull('gender','default');
        Session::pull('city','default');
        Session::pull('batchID','default');
        Session::pull('getcode','default');
        Session::pull('submitTime','default');
        Session::pull('forgetmb_id','default');
        Session::pull('forget_code','default');
        $msg='true';
        return $msg; 
    }
    /*註冊成功刪除session*/
    public function success($locale, Request $request){
        Session::pull('success','default');
        Session::pull('submitTime','default');

        Session::pull('Member', 'default');
        Session::pull('mb_id', 'default');
        Session::pull('name', 'default');
        Session::pull('email', 'default');
        Session::pull('mobile', 'default');
        Session::pull('birthday', 'default');
        Session::pull('mbid_city', 'default');
        Session::pull('mbid_no', 'default');
        Session::pull('getcode', 'default');
        return 'success';
    }
    /*會員-登入*/
    public function login($locale,Request $request){
        $login=$request->input('Login');

        $member=Member::where('email',$login['account'])
                    ->first();

        if(!empty($member)){
            if(Hash::check($login['password'],$member['password'])){
                if($member['is_visible']==0){
                    return Redirect::to($locale.'/member')->with('Message','帳號已停用,請聯絡管理員!')->with('Message_type','error');
                }else{
                    Session::put('Member', 'login');
                    Session::put('mb_id', $member['id']);
                    Session::put('name', $member['name']);
                    Session::put('email', $member['email']);
                    Session::put('mobile', $member['mobile']);
                    Session::put('birthday', $member['birthday']);
                    Session::put('mbid_city', $member['mbid_city']);
                    Session::put('mbid_no', $member['mbid_no']);

                    /*檢查上次獲取點數時間,若超過3年則新增一筆歸零的資料*/
                    $bonus_date=date('Y-m-d', strtotime('+3 year',strtotime($member['last_get'])));
                    $today=date('Y-m-d');
                    if($bonus_date<$today){
                        $zero=0;
                        $chkId=Bonus::where('mb_id',$member['id'])
                                ->where('sel_type',7)
                                ->max('id');
                        if(!empty($chkId)){
                            $chk=Bonus::where('id',$chkId)
                                    ->first();
                            if($chk['created_at']<$bonus_date){
                                $zero=1;
                            }else{
                                $zero=0;
                            }
                        }else{
                            $zero=1;
                        }

                        if($zero==1){
                            $Bonus=new Bonus();
                            $Bonus->is_visible=1;
                            $Bonus->is_add=1;
                            $Bonus->mb_id=$member['id'];
                            $Bonus->sel_type=7;
                            $Bonus->bonus=0;
                            $Bonus->detail='歸零';
                            $Bonus->created_at=date('Y-m-d', strtotime('+1 day',strtotime($bonus_date)));
                            $Bonus->save();

                            $mb_new_bonus=Member::Find($member['id']);
                            $mb_new_bonus->bonus=0;
                            $mb_new_bonus->save();
                        }
                    }
                    /*如果今天生日,新增一筆生日禮金*/
                    if(date("m-d", strtotime($member['birthday']))==date("m-d")){
                        $birth=0;
                        $chkBirth=Bonus::where('mb_id',$member['id'])
                                ->where('sel_type',1)
                                ->orderBy('created_at','desc')
                                ->max('id');
                        if(!empty($chkBirth)){
                            $bir=Bonus::where('id',$chkBirth)->first();
                            if(date("Y", strtotime($bir['created_at']))==date("Y")){
                                $birth=0;
                            }else{
                                $birth=1;
                            }
                        }else{
                            $birth=1;
                        }
                        if($birth==1){
                            $Bonus=new Bonus();
                            $Bonus->is_visible=1;
                            $Bonus->is_add=1;
                            $Bonus->mb_id=$member['id'];
                            $Bonus->sel_type=1;
                            $Bonus->bonus=1500;//生日禮金
                            $Bonus->detail='法蝶珠寶祝您生日快樂';
                            $Bonus->save();

                            $new_bonus=$member['bonus']+1500;
                            $mb_new_bonus=Member::Find($member['id']);
                            $mb_new_bonus->bonus=$new_bonus;
                            $mb_new_bonus->save();
                        }
                    }
                    return Redirect::to($locale.'/member/guide');
                }
            }else{
                return Redirect::to($locale.'/member')->with('Message','密碼錯誤!')->with('Message_type','error');
            }
                
        }else{
            return Redirect::to($locale.'/member')->with('Message','查無此帳號!')->with('Message_type','error');
        }

    }
    /*會員-登出*/
    public function logout($locale){
        if(Session::get('Member')=='login'){
            Session::pull('Member', 'default');
            Session::pull('mb_id', 'default');
            Session::pull('name', 'default');
            Session::pull('email', 'default');
            Session::pull('mobile', 'default');
            Session::pull('birthday', 'default');
            Session::pull('mbid_city', 'default');
            Session::pull('mbid_no', 'default');
            return Redirect::to($locale.'/member')->with('Message','登出成功!')->with('Message_type','success');
        }else{
            return Redirect::to($locale.'/member');
        }
    }
    /*會員-會員資料畫面*/
    public function account($locale){
        $memberData=Member::where('id',Session::get('mb_id'))
                        ->first();
        $miss=($memberData->gender==2)?'active':'';
        $mr=($memberData->gender==1)?'active':'';
        $city=City::where('is_visible',1)
                    ->orderBy('rank','asc')
                    ->get();
        return View::make( $locale.'.member.account',
            [
                'memberData'=>$memberData,
                'city'=>$city,
                'miss'=>$miss,
                'mr'=>$mr
            ]);
    }
    /*會員-修改個資*/
    public function save($locale,Request $request){
        $data=$request->input();
        $id=Session::get('mb_id');
        $Member=Member::find($id);
        $Member->name=(!empty($data['name']))?$data['name']:'';
        $Member->birthday=(!empty($data['birthday']))?$data['birthday']:'';
        $Member->gender=(!empty($data['gender']))?$data['gender']:'';
        $Member->city=(!empty($data['city']))?$data['city']:'';
        $Member->address=(!empty($data['address']))?$data['address']:'';
        $Member->zip=(!empty($data['zip']))?$data['zip']:'';

        if ($Member->save()) {
            return Redirect::to($locale.'/member/account')->with('Message','修改成功!')->with('Message_type','success');
        }
        else{
            return Redirect::to($locale.'/member/account')->with('Message','儲存失敗,請稍後再試!')->with('Message_type','success');
        }
    }
    /*會員-修改大頭貼*/
    public function ChangePhoto($locale,Request $request){
        if($_FILES['photo']['error']>0){
            return "圖片上傳錯誤,".$_FILES['photo']['error'];
        }else{
            $fileName=Session::get('mbid_city').Session::get('mbid_no').'_'.$_FILES['photo']['name'];
            $fileDir='upload/member/photo/';
            move_uploaded_file($_FILES['photo']['tmp_name'], $fileDir.$fileName);

            $id=Session::get('mb_id');
            $Member=Member::find($id);
            $Member->image=$fileDir.$fileName;

            if ($Member->save()) {
                return Redirect::to($locale.'/member/account')->with('Message','修改成功!')->with('Message_type','success');
            }
            else{
                return Redirect::to($locale.'/member/account')->with('Message','儲存失敗,請稍後再試!')->with('Message_type','success');
            }
        }
        
    }
    /*會員-刪除大頭貼*/
    public function DelPhoto($locale,Request $request){
        $myphoto=$request->input('photo');
        if(file_exists($myphoto)){
            unlink($myphoto);
            $id=Session::get('mb_id');
            $Member=Member::find($id);
            $Member->image='';
            if ($Member->save()) {
                return 'success';
            }
            else{
                return '儲存失敗,請稍後再試!';
            }
        }else{
            return '查無檔案,請重新確認!';
        }
        
    }
    /*會員-修改帳密畫面*/
    public function ChangeAcc($locale,$type){
        return View::make( $locale.'.member.change',
        [
            'type'=>$type
        ]);
    }
    
    
}