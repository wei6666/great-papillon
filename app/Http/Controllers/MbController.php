<?php
namespace App\Http\Controllers;
/**原生函式**/
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use View;
use ItemMaker;
use Cache;
use Excel;
use Redirect;
use DateTime;
use Validator;
use Mail;
use DB;

/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\front\ProductController as ProductController;

/**Models**/
use App\Http\Controllers\Controller;
use App\Http\Controllers\SMSHttpSend;
use App\Http\Models\Member\Member;
use App\Http\Models\Member\Bonus;
use App\Http\Models\Member\BonusRule;
use App\Http\Models\Member\Address;
use App\Http\Models\Member\City;

class MbController extends BackendController
{
    /*會員點數-畫面*/
    public function bonus($locale){

        ProductController::ifTimeOutChangeOrderListType($locale);

        $BonusRule=BonusRule::where('is_visible',1)
                    ->orderBy('rank','asc')
                    ->get();


        //撈出所有紀錄 或者還沒寫入的使用的紅利
        $Bonus=Bonus::where('mb_id',Session::get('mb_id'))
                    ->where('is_add',1)
                    ->orWhere(function($query)
                    {
                        $query->where('is_add',0)
                        ->where('sel_type',3);
                    })
                    ->orderBy('created_at','desc')
                    ->get();


                    




        $Member=Member::select('bonus','last_get')
                    ->where('id',Session::get('mb_id'))
                    ->where('is_visible',1)
                    ->first();

        $tempusedbonus=Bonus::where('mb_id',Session::get('mb_id'))
                    ->where('is_add',0)
                    ->where('sel_type',3)                    
                    ->get();

        $tempusedbonuscount=0;
        foreach($Bonus as $value)
        {
            $tempusedbonuscount+=$value->bonus;
            
        }
        
        $bonusType=[
            1=>'會員生日禮',2=>'購買回饋',3=>'訂單折抵',4=>'好友推薦',5=>'首次消費',6=>'會員註冊',7=>'使用期限到期'
        ];
        return View::make( $locale.'.member.bonus',
            [
                'BonusRule'=>$BonusRule,
                'Bonus'=>$Bonus,
                'bonusType'=>$bonusType,
                'Member'=>$Member,
                'tempusedbonuscount'=>$tempusedbonuscount,
            ]);
    }
    /*地址簿*/
    public function address($locale){
        $add=Address::where('mb_id',Session::get('mb_id'))
                    ->with('City')
                    ->get();
        $city=City::orderBy('rank','asc')
                    ->get();
        return View::make( $locale.'.member.address',
            [
                'add'=>$add,
                'city'=>$city
            ]);
    }
    /*地址簿內頁*/
    public function ajax($locale,Request $request){
        $id=$request->input('id');
        $thisAddr=Address::where('id',$id)
                    ->first();
        $myCity=City::select('title','id')
                    ->where('id',$thisAddr['city'])
                    ->first();
        $data = [
            'title' => $thisAddr['title'],
            'mobile' => $thisAddr['mobile'],
            'name' => $thisAddr['name'],
            'zip' => $thisAddr['zip'],
            'address' => $thisAddr['address'],
            'city' => $myCity['title'],
            'city_id'=>$myCity['id'],
        ];
        $data = json_encode($data, JSON_UNESCAPED_UNICODE);

        return $data;
    }
    /*新增地址簿*/
    public function NewSubmit($locale,Request $request){
        $data=$request->input('New');
        $id=Session::get('mb_id');
        $Address=new Address;
        $Address->name=(!empty($data['name']))?$data['name']:'';
        $Address->mobile=(!empty($data['mobile']))?$data['mobile']:'';
        $Address->title=(!empty($data['title']))?$data['title']:'';
        $Address->city=(!empty($data['city']))?$data['city']:'';
        $Address->address=(!empty($data['address']))?$data['address']:'';
        $Address->zip=(!empty($data['zip']))?$data['zip']:'';
        $Address->mb_id=$id;

        if ($Address->save()) {
            return Redirect::to($locale.'/member/address')->with('Message','修改成功!')->with('Message_type','success');
        }
        else{
            return Redirect::to($locale.'/member/address')->with('Message','儲存失敗,請稍後再試!')->with('Message_type','success');
        }
    }
    /*修改地址簿*/
    public function EditSubmit($locale,Request $request){
        $data=$request->input('Edit');
        $id=$data['id'];
        $Address=Address::Find($id);
        $Address->name=(!empty($data['name']))?$data['name']:'';
        $Address->mobile=(!empty($data['mobile']))?$data['mobile']:'';
        $Address->title=(!empty($data['title']))?$data['title']:'';
        $Address->city=(!empty($data['city']))?$data['city']:'';
        $Address->address=(!empty($data['address']))?$data['address']:'';
        $Address->zip=(!empty($data['zip']))?$data['zip']:'';

        if ($Address->save()) {
            return Redirect::to($locale.'/member/address')->with('Message','修改成功!')->with('Message_type','success');
        }
        else{
            return Redirect::to($locale.'/member/address')->with('Message','儲存失敗,請稍後再試!')->with('Message_type','success');
        }
    }
    /*刪除地址簿*/
    public function DelSubmit($locale,Request $request){
        $id=$request->input('Del_id');
        $Address=Address::Find($id);
        if ($Address->delete()) {
            return Redirect::to($locale.'/member/address')->with('Message','刪除成功!')->with('Message_type','success');
        }
        else{
            return Redirect::to($locale.'/member/address')->with('Message','刪除失敗,請稍後再試!')->with('Message_type','success');
        }
    }
    
}