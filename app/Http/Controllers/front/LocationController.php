<?php
namespace App\Http\Controllers\front;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use ItemMaker;
use Cache;
use Excel;
use Storage;
use Validator;
use Session;


/*create Fantasy class*/
use App\Http\Controllers\Fantasy\CreateFantasy;


/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\Fantasy\MakeTableController;


/*相關的Controller*/

use App\Http\Models\Location\LocationCategory;
use App\Http\Models\Location\Location;
use App\Http\Models\Product\ProductPhoto;
use App\Http\Models\Location\LocationSet;


// use App\Http\Models\Location\SearchPlace;
// use App\Http\Models\Seo;


class LocationController extends BackendController {

	
	/*首頁========================================================*/
	public function index($locale,$id="")
	{

	//先撈這業的類別
	$LocationCategory=LocationCategory::where("is_visible",1)->get();
	
	//把所有產品撈出來
    $Location=Location::where('is_visible',1)     				  
                      ->OrderBy('rank','asc')
                      ->get();

	$og_data=[];
	$_GET['lo']=$id;
    if(!empty($_GET['lo']))
    {
    	$GetLocation=Location::where('is_visible',1)
    				  ->where('id',$_GET['lo']) 				  
                      ->OrderBy('rank','asc')
                      ->first();

		$og_data['web_title']=$GetLocation->title;
	    $og_data['meta_description']=$GetLocation->address;
	    $og_data['og_image']=$GetLocation->in_photo;
	    

    }
    //dd($_GET['lo'],$id);
    
    $LocationSet=LocationSet::first();

	$pageBodyClass="index";

	$pageBodyId="storehold";

	$CategoryWithData=$LocationCategory;

	foreach($LocationCategory as $key => $value)
	{
		$CategoryWithData[$key]["location"]=Location::where('is_visible',1)				  ->where('category_id',$value->id)		
                      ->OrderBy('rank','asc')
                      ->get();
	}



		return View::make( $locale.'.location.index',
			[
				"Location"=>$Location,
				"CategoryWithData"=>$CategoryWithData,
				"LocationSet"=>$LocationSet,
				"LocationCategory"=>$LocationCategory,
				"pageBodyClass"=>$pageBodyClass,
				"pageBodyId"=>$pageBodyId,
				"og_data"=>$og_data,
				
			
			]);
	}


	public function detail($locale,$id="")
	{

		$LocationSet=LocationSet::first();

		$Location=Location::where('is_visible',1)
				 	  ->where('id',$id)
                      ->OrderBy('rank','asc')
                      ->first();


        	
        $LocationCategory=LocationCategory::where("is_visible",1)->get();

        $forchange=[];
        foreach ($LocationCategory as $key => $value) {
        	$forchange[$value->id]=$value->title;
        }

		return View::make( $locale.'.location.detail',
			[
				"Location"=>$Location,
				"forchange"=>$forchange,
				"LocationSet"=>$LocationSet,
			
				
			]);

	}
	



}
	