<?php
namespace App\Http\Controllers\front;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use ItemMaker;
use Cache;
use Excel;
use Storage;
use Validator;
use Session;


/*create Fantasy class*/
use App\Http\Controllers\Fantasy\CreateFantasy;


/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\Fantasy\MakeTableController;


/*相關的Controller*/
use App\Http\Controllers\News\FunctionController as NewsFunction;
// use App\Http\Models\News\News;
// use App\Http\Models\News\NewsPhoto;
// use App\Http\Models\News\NewsSet;
// use App\Http\Models\News\NewsBig;

use App\Http\Models\Product\ProductSet;
use App\Http\Models\Product\ProductCategory;
use App\Http\Models\Location\Location;
use App\Http\Models\Happiness\HappinessSet;
use App\Http\Models\Happiness\Happiness;
use App\Http\Models\About\DesignerPhoto;
use App\Http\Models\About\CertificatePhoto;
use App\Http\Models\About\AfterSell;


// use App\Http\Models\News\SearchPlace;
// use App\Http\Models\Seo;


class AboutController extends BackendController {

	
	/*首頁========================================================*/
	public function index($locale)
	{
		

		$pageBodyId='about';
		$pageBodyClass='index';

		


		return View::make( $locale.'.about.index',
			[

				"pageBodyId"=>$pageBodyId,
				"pageBodyClass"=>$pageBodyClass,

            ]);
	}


    public function story($locale)
    {
        

        $pageBodyId='about';
        $pageBodyClass='detail story';
        $Happiness=Happiness::where('is_visible',1)                       
                      ->OrderBy('rank','asc')
                      ->get();
        $HappinessSet=HappinessSet::first();
        


        return View::make( $locale.'.about.story',
            [

                "pageBodyId"=>$pageBodyId,
                "pageBodyClass"=>$pageBodyClass,
                "Happiness"=>$Happiness,
                "HappinessSet"=>$HappinessSet,

            ]);
    }



    public function design($locale)
    {
        

        $pageBodyId='about';
        $pageBodyClass='detail design';
        $DesignerPhoto=DesignerPhoto::where('is_visible',1)->OrderBy('rank','asc')->get();
        


        return View::make( $locale.'.about.design',
            [

                "pageBodyId"=>$pageBodyId,
                "DesignerPhoto"=>$DesignerPhoto,
                "pageBodyClass"=>$pageBodyClass,

            ]);
    }

    public function quality($locale)
    {
        

        $pageBodyId='about';
        $pageBodyClass='detail quality';
        
        $CertificatePhoto=CertificatePhoto::where('is_visible',1)->OrderBy('rank','asc')->get();
        
        

        return View::make( $locale.'.about.quality',
            [

                "pageBodyId"=>$pageBodyId,
                "pageBodyClass"=>$pageBodyClass,
                "CertificatePhoto"=>$CertificatePhoto,

            ]);
    }


    public function charity($locale)
    {
        

        $pageBodyId='about';
        $pageBodyClass='detail charity';

        


        return View::make( $locale.'.about.charity',
            [

                "pageBodyId"=>$pageBodyId,
                "pageBodyClass"=>$pageBodyClass,

            ]);
    }


    public function service($locale)
    {
        

        $pageBodyId='about';
        $pageBodyClass='detail service';
        $Service=AfterSell::where('is_visible',1)->OrderBy("id",'desc')->get();
        


        return View::make( $locale.'.about.service',
            [
                "pageBodyId"=>$pageBodyId,
                "pageBodyClass"=>$pageBodyClass,
                "Service"=>$Service,
            ]);
    }



}
	