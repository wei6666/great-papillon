<?php
namespace App\Http\Controllers\front;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use ItemMaker;
use Cache;
use Excel;
use Storage;
use Validator;
use Session;


/*create Fantasy class*/
use App\Http\Controllers\Fantasy\CreateFantasy;


/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\Fantasy\MakeTableController;


/*相關的Controller*/
use App\Http\Controllers\News\FunctionController as NewsFunction;
// use App\Http\Models\News\News;
// use App\Http\Models\News\NewsPhoto;
// use App\Http\Models\News\NewsSet;
// use App\Http\Models\News\NewsBig;

use App\Http\Models\Product\ProductSet;
use App\Http\Models\Product\ProductCategory;
use App\Http\Models\Location\Location;


// use App\Http\Models\News\SearchPlace;
// use App\Http\Models\Seo;


class ReservationController extends BackendController {

	
	/*首頁========================================================*/
	public function index($locale)
	{
		

		$pageBodyId='reservation';
		$pageBodyClass='index';

		$ReservationSet=ProductSet::first();
		
		$Location=Location::where('is_visible',1)->OrderBy("rank",'asc')->get();

		$ProductCategory=ProductCategory::where('is_visible',1)->OrderBy("rank",'asc')->get();
		


		return View::make( $locale.'.reservation.index',
			[

				"pageBodyId"=>$pageBodyId,
				"pageBodyClass"=>$pageBodyClass,
				"ReservationSet"=>$ReservationSet,
				"Location"=>$Location,
				"ProductCategory"=>$ProductCategory	,

			
			]);
	}


	public function detail($locale , Request $request)
	{

		
		$data=$request->all()['data'];
		$Reservation =  "App\Http\Models\Reservation\Reservation";

            $new = new $Reservation;

            foreach( $data as $key => $value )
            {
                $new->$key = $value;
            }
       
            if($new->save())
            {
                    $message = ( $locale == 'zh-tw' ) ? '聯絡表單送出成功！' : 'Your form is submited successfully!';
                    $data = [
                        'message' => '資料送出',
                        'formPass' => true
                    ];
            } 
            else
            {
                 $message = ( $locale == 'zh-tw' ) ? '聯絡表單送出失敗！請再填寫一次' : 'Your form is submited failed! please try again';
                    $data = [
                        'message' => '再填寫一次',
                        'formPass' => true
                    ];
            }

               echo '<script>'."window.history.back();".'</script>';  
                die;
               
                $set = [];
                $set['view'] = 'mail.contact';
                $globalsSet = Cache::get('globalsSet');
                $globalrecieveMails = explode(";" , $globalsSet['sendmail']);
                $set['to'] =$globalrecieveMails; //收件者

                //$set['to'] ='victor@wddgroup.com'; //收件者
                $set['to_name'] = '黃山石 - '.$new['name'];
                $set['subject'] = '聯絡我們通知信 - 黃山石 - '.$new['name'];
                
                $is_success=Mail::send( $set['view'], ['data' => $new], function ($m) use ( $set ) {
                        $m->from( 'service@maychen.com.tw' , $set['to_name'] );
                        $m->to( $set['to'])->subject( $set['subject'] );
                    });



                if( $is_success > 0 )
                {
                    $new->is_mail=1;
                    $new->save();

                }
                else
                {
                  $new->is_mail=0;
                  $new->save();
                }    
                
                $data = json_encode($data, JSON_UNESCAPED_UNICODE);
                return $data;
		dd($form_data);
	}






}
	