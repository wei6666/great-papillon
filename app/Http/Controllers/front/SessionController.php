<?php
namespace App\Http\Controllers\front;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use ItemMaker;
use Cache;
use Excel;
use Storage;
use Validator;
use Session;


/*create Fantasy class*/
use App\Http\Controllers\Fantasy\CreateFantasy;


/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\Fantasy\MakeTableController;
use App\Http\Models\Product\Product;
use App\Http\Models\Product\ProductPhoto;
use App\Http\Models\Product\ProductSeparate;
use App\Http\Models\DisCount\DisCountCode;
use App\Http\Models\DisCount\DisCountFull;
use App\Http\Models\DisCount\DisCountPercent;
use App\Http\Models\Member\Member;
use App\Http\Models\Pay\OrderList;
use App\Http\Models\Member\Bonus;
use App\Http\Models\Setting;

/*相關的Controller*/
use App\Http\Controllers\Product\FunctionController as ProductFunction;


class SessionController extends BackendController {

	
	/*首頁========================================================*/


	
	public function add($locale,$productid='',$separate_id='',$count='',$size='')
	{
		
		$sessiondata=Session::get('product_data');
		if(empty($sessiondata))
		{
			$sessiondata=[];
		}
		$temparr=[];
		$temparr['productid']= $productid;
		$temparr['separate_id']= $separate_id;
		$temparr['count']= $count;
		$temparr['size']= $size;
		array_push($sessiondata, $temparr);
		Session::forget('product_data');
		Session::put('product_data',$sessiondata);
		$numbercount=count($sessiondata);
		echo $numbercount;

	}
	public static  function session_edit($locale,$id='')
	{
		
		
		$sessiondata=Session::get('product_data');

		if($id=='show')
		{
			dd(dd( Session::all() ), Session::get('product_data') );
			die;
		}
		
		if(count($sessiondata) == 0)
		{
			
			$sessiondata=[];
		}

	
		//dd(Session::get('product_data'));
		if( !in_array($id, $sessiondata))
		{
			
			array_push($sessiondata, $id);
		}
		else
		{
			
			if(count($sessiondata) >0)
			{
				foreach($sessiondata as $key => $value)
				{
					if($value == $id)
					{
						unset($sessiondata[$key]);
					}
				}
			}
			
		}
		
		
		Session::forget('product_data');
		Session::put('product_data',$sessiondata);
		
		if($id=='ff')
		{
			Session::forget('product_data');
			// Session::forget('city');
			// Session::forget('gender');
			// Session::forget('pass');
			// Session::forget('email');
			// Session::forget('mobile');
			// Session::forget('name');
		
			return redirect( $locale."/member/guide" );
		}
		
		//dd(Session::get('product_data'));
		return Session::get('product_data');;
	}


	public function remove_session($locale,$id='')
	{
		$sessiondata=Session::get('product_data');
		unset($sessiondata[$id]);	
		Session::forget('product_data');
		Session::put('product_data',$sessiondata);

	}

	public function tocar($locale,$id='')
	{

		$Product=Product::get();
		$ProductSeparate=ProductSeparate::get();

		$ProductSeparateArr=[];
		$ProductArr=[];
		$car_data=[];
		$total=0;

		foreach ($ProductSeparate as $key => $value) {
			$ProductSeparateArr[$value->id]=$value;
		}

		foreach ($Product as $key => $value) {
			$ProductArr[$value->id]=$value;
		}

		//dd($ProductSeparateArr);
		$sessiondata=Session::get('product_data');
		if(!empty($sessiondata))
		{
			foreach($sessiondata as $key => $value)
			{

				$car_data[$key]['price']=0;
				$productid=$value['productid'];
				$separate_id=$value['separate_id'];
				$count=$value['count'];
				$size=$value['size'];
				// if separate_id

				if($separate_id !="-")
				{
					$car_data[$key]['separate']=$ProductSeparateArr[$separate_id];
					if(!empty($ProductSeparateArr[$separate_id]->price_discount))
					{
						$car_data[$key]['price']=$ProductSeparateArr[$separate_id]->price_discount;
					}
					else {
						$car_data[$key]['price']=$ProductSeparateArr[$separate_id]->price_original;
					}
				}
				// if no separate_id
				else
				{
					
					if(!empty($ProductArr[$productid]->price_discount))
					{
						$car_data[$key]['price']=$ProductArr[$productid]->price_discount;
					}
					else {
						$car_data[$key]['price']=$ProductArr[$productid]->price_original;
					}
				}


				

					
						$car_data[$key]['out_img']= !empty(ProductPhoto::where('product_id',$productid)->where('is_out_img',1)->first()->image) ? ProductPhoto::where('product_id',$productid)->where('is_out_img',1)->first()->image : !empty(ProductPhoto::where('product_id',$productid)->first()->image) ? ProductPhoto::where('product_id',$productid)->first()->image : "";
					
					

				$car_data[$key]['product']=$ProductArr[$productid];			
				$car_data[$key]['count']=$count;
				$car_data[$key]['size']=$size;
				$total+=($car_data[$key]['price']/1)*($count/1);

			}
		}
		else
		{
				$car_data="";
				$total="";
				$size="";
				$count="";
		}
		

		return View::make( $locale.'.product.car',
			[
				"car_data"=>$car_data,
				"total"=>$total,
				"size"=>$size,
				"count"=>$count,


			]);

	}


	

	public function updateDisCountCode($locale,$code="")
	{
		
		if($code =="")
		{
			Session::forget('code');
			Session::forget('message');
		}
		else
		{
			$DisCountCode=DisCountCode::where("code",$code)->OrderBy("id","desc")->first();
			$message=Session::get('message');
			Session::forget('message');
			Session::forget('code');
			//這邊要加上重複以及總數的計算
			if(count($DisCountCode) > 0)
			{
				if($DisCountCode->is_repeat ==1)
				{
					//如果折扣碼有總數限制

					if(!empty($DisCountCode->order_list_limit) )
					{
						//取得所有用這個折扣碼的單子
						$OrderList=OrderList::where("is_cancel",0)->get();

						$_thisCodeUsedCount=0;

						foreach ($OrderList as $key => $value) {
							if(!empty($value->DisCountCodeRecord))
							{
								if(json_decode($value->DisCountCodeRecord)->code == $code)
								{
									$_thisCodeUsedCount++;
								}
							}
							
						}
						//還沒超過限制總數
						if($_thisCodeUsedCount < $DisCountCode->order_list_limit)
						{
							Session::put('code',$code);
						}
						else
						{
							//echo "<script>alert('太多人用過ㄌ') </script>";
						}


					}
					else
					{
						Session::put('code',$code);
					}
				
					
				}
				//如果不能重複使用
				else
				{


					$OrderList=OrderList::where("member_sql_id",Session::get("mb_id"))->where("is_cancel",0)->get();

					if(count($OrderList) < 1)
					{


						//如果折扣碼有總數限制
						if(!empty($DisCountCode->order_list_limit) )
						{
							//取得所有用這個折扣碼的單子
							$OrderList=OrderList::where("is_cancel",0)->get();

							$_thisCodeUsedCount=0;

							foreach ($OrderList as $key => $value) {
								if(!empty($value->DisCountCodeRecord))
								{
									if(json_decode($value->DisCountCodeRecord)->code == $code)
									{
										$_thisCodeUsedCount++;
									}
								}
								
							}
							//還沒超過限制總數
							if($_thisCodeUsedCount < $DisCountCode->order_list_limit)
							{
								Session::put('code',$code);
							}


						}
						else
						{
							Session::put('code',$code);
						}
					}
					else
					{
						//echo "<script>alert('用過ㄌ') </script>";
					}
					
				}
				
			}
			else
			{
				$message['CodeMsg']="你的折扣碼有問題喔";
			}
			Session::put('message',$message);
		}



		
		return 	SessionController::to_discount(SessionController::gettotal());
	}

	public static function updateDisCountBonus($locale,$bonus)
	{
		
		$member=Member::where("id",Session::get('mb_id'))->first();
		Session::forget('bonus');
		$message=Session::get('message');
		Session::forget('message');
	


		//還沒付錢的訂單有紀錄 所以先從記錄搜出來先扣掉
		$bonusdata=Bonus::where('mb_id',Session::get('mb_id'))->get();


		$allusedbonus=0;

		//所有未付錢 但是有使用紅利的訂單
		foreach ($bonusdata as $key => $value) {

			if($value->is_add ==1 || $value->is_add==0 && $value->sel_type ==3 )
			{
				$allusedbonus+=$value->bonus;
			}
			
		}

		



		$memberbonus=$allusedbonus;

		
		
		
		if($bonus > 0 )
		{
			if($bonus > $memberbonus)
			{
				if($bonus - SessionController::to_discount(SessionController::gettotal() )['total'] > 5)
				{
					$bonus = SessionController::to_discount(SessionController::gettotal())['total'] - 5;
					$message["BonusMsg"]="以折扣至0元 所以只使用".$bonus."點";
				}
				else
				{
					$bonus = $memberbonus;
					$message["BonusMsg"]="會員點數只剩下".$bonus."點 已全部使用";					
				}
			}
			else
			{
				if($bonus - SessionController::to_discount(SessionController::gettotal())['total'] > 5)
				{
					$bonus = SessionController::to_discount(SessionController::gettotal())['total']-5;
					$message["BonusMsg"]="以折扣至0元 所以只使用".$bonus."點";
				}
				else
				{
					
				}
			}
			
		}
		else
		{
			$bonus = 0;
		}


		//這邊要加上重複以及總數的計算
		
		
		Session::put('bonus',$bonus);
		Session::put('message',$message);
		
		return 	SessionController::to_discount(SessionController::gettotal());

		
	}

	public static function  gettotal()
	{
		$Product=Product::get();
		$ProductSeparate=ProductSeparate::get();

		$ProductSeparateArr=[];
		$ProductArr=[];
		$car_data=[];
		$total=0;

		foreach ($ProductSeparate as $key => $value) {
			$ProductSeparateArr[$value->id]=$value;
		}

		foreach ($Product as $key => $value) {
			$ProductArr[$value->id]=$value;
		}

		//dd($ProductSeparateArr);
		$sessiondata=Session::get('product_data');
		if(!empty($sessiondata))
		{
			foreach($sessiondata as $key => $value)
			{

				$car_data[$key]['price']=0;
				$productid=$value['productid'];
				$separate_id=$value['separate_id'];
				$count=$value['count'];
				$size=$value['size'];
				// if separate_id

				if($separate_id !="-")
				{
					$car_data[$key]['separate']=$ProductSeparateArr[$separate_id];
					if(!empty($ProductSeparateArr[$separate_id]->price_discount))
					{
						$car_data[$key]['price']=$ProductSeparateArr[$separate_id]->price_discount;
					}
					else {
						$car_data[$key]['price']=$ProductSeparateArr[$separate_id]->price_original;
					}
				}
				// if no separate_id
				else
				{
					
					if(!empty($ProductArr[$productid]->price_discount))
					{
						$car_data[$key]['price']=$ProductArr[$productid]->price_discount;
					}
					else {
						$car_data[$key]['price']=$ProductArr[$productid]->price_original;
					}
				}


				

					
						$car_data[$key]['out_img']= !empty(ProductPhoto::where('product_id',$productid)->where('is_out_img',1)->first()->image) ? ProductPhoto::where('product_id',$productid)->where('is_out_img',1)->first()->image : !empty(ProductPhoto::where('product_id',$productid)->first()->image) ? ProductPhoto::where('product_id',$productid)->first()->image : "";
					
					

				$car_data[$key]['product']=$ProductArr[$productid];			
				$car_data[$key]['count']=$count;
				$car_data[$key]['size']=$size;
				$total+=($car_data[$key]['price']/1)*($count/1);

			}
		}
		else
		{
				$car_data="";
				$total="";
				$size="";
				$count="";
		}

		return $total;

	}

	public function updatenum($locale,$id='',$count)
	{
		$sessiondata=Session::get('product_data');
		$sessiondata[$id]['count']=$count;
		Session::forget('product_data');
		Session::put('product_data',$sessiondata);

		
		return 	SessionController::to_discount(SessionController::gettotal());
	}


	//算折扣 上面判斷 折扣 OK的話就加session裡面 然後這個function 直接抓算
	public static function to_discount($total)
	{


		$Setting=Setting::first();
		$PercentDiscount=0;
		$FullDiscount=0;
		
		
		
		$DisCountData=[];

		$DisCountPercent=DisCountPercent::where('is_visible',1)
                                      ->where('st_date',"<=",Date("Y-m-d"))
                                      ->where('ed_date',">=",Date("Y-m-d"))
                                      ->OrderBy('id','desc')
                                      ->first();

		$DisCountFull=DisCountFull::where('is_visible',1)
                                      ->where('st_date',"<=",Date("Y-m-d"))
                                      ->where('ed_date',">=",Date("Y-m-d"))
                                      ->OrderBy('id','desc')
                                      ->first();


		

        $DisCountData["PercentDisCount"]=0;
        $DisCountData["FullDisCount"]=0;
        $DisCountData["CodeDisCount"]=0;
        $DisCountData["BonusDisCount"]=0;
        $DisCountData["Code"]="";


        if(count($DisCountPercent) > 0)                                   	
        {

        	$PercentDiscount=ceil((1-$DisCountPercent->percent) * $total);
        	$total=ceil($total * $DisCountPercent->percent);
        	$DisCountData["PercentDisCount"]=$PercentDiscount;
        }


        if(count($DisCountFull) > 0)                                   	
        {
        	$FullDisCount=intval($total / $DisCountFull->full) * $DisCountFull->discount ;

        	$total=$total-$FullDisCount;
        	$DisCountData["FullDisCount"]=$FullDisCount;
        }

       
        
        if(!empty(Session::get('code')))
    	{
    		
    		$codedata=DisCountCode::where('code',Session::get('code'))->first();
    		$DisCountData["CodeDisCount"]=$codedata->money_discount;

    		$DisCountData["Code"]=$codedata->code;
    		
    		$total = $total - $DisCountData["CodeDisCount"];
    		
       	}


       	if(Session::get('bonus') >0)
    	{
    		
    		
    		$DisCountData["BonusDisCount"]=Session::get('bonus');

    		$total =$total - $DisCountData["BonusDisCount"];
       	}
       	else
       	{
       		$DisCountData["BonusDisCount"]=Session::get('bonus');
       	}

       	if($total <= 0)
       	{
       		$total =5;
       	}
        
        $message=Session::get('message');

        if(!empty(Session::get('shop_receiver')['city']))
    	{
    		
	        if(!empty($Setting->shipping_cost_full))
	        {	
	        	$out_sea_arr=["連江縣","金門縣","澎湖縣"];
        
		        $shipping_cost=0;
		        if(!empty($Setting->shipping_cost_all))
		        {
		        	$shipping_cost=$Setting->shipping_cost_all;
		        }
		        else
		        {
		        	if(in_array(Session::get('shop_receiver')['city'], $out_sea_arr))
		        	{

		        		$shipping_cost=$Setting->shipping_cost_out_sea;
		        	}
		        	else
		        	{
		        		$shipping_cost=$Setting->shipping_cost_in_sea;	
		        	}
		        }


	        	if($total < $Setting->shipping_cost_full)
	        	{
	        		$DisCountData["ShippingCost"]=$shipping_cost;
	        		$total=$total+$shipping_cost;
	        	}
	        	else
	        	{
	        		$DisCountData["ShippingCost"]="免運費";
	        	}
	        }

    	}
    	
    	

        $DisCountData["BonusMsg"]=!empty($message["BonusMsg"]) ? $message["BonusMsg"]: "";	
        $DisCountData["CodeMsg"]=!empty($message["CodeMsg"]) ? $message["CodeMsg"]: "";	
        $DisCountData["total"]=$total;


        
        if(Session::get('friendfeedback') !=NULL)
    	{
    		$DisCountData["friendfeedback"]= ceil($total/100 * $Setting->friend_feedback );
    	}
    	else
    	{
    		$DisCountData["friendfeedback"]=0;
    	}
        


        $DisCountData["feedback"]= ceil($total /100 * $Setting->feedback );
        
        Session::forget("DisCountData");

        Session::put("DisCountData",$DisCountData);


        

		return $DisCountData;
	}

	public  function updateFriendInvite($locale,$friendid)
	{

		
		$city_code=substr($friendid, 0,2);
		$membernum=substr($friendid, 2);
		$member=Member::where("mbid_city",$city_code)->where("mbid_no",$membernum)->first();

		Session::forget('friendfeedback');
		//自己不行
		if(count($member) >0 && $member->id != Session::get('mb_id'))
		{
			Session::put('friendfeedback',$member->id);
			return "icon-check"; 
		}
		else
		{
			return "icon-x";
		}
		
		


		$ss=substr($friendid, 0,2)."--".substr($friendid, 2);
		return $ss; 
	}



	

	
	

	
}
