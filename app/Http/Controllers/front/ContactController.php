<?php
namespace App\Http\Controllers\front;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use ItemMaker;
use Cache;
use Excel;
use Storage;
use Validator;
use Session;


/*create Fantasy class*/
use App\Http\Controllers\Fantasy\CreateFantasy;


/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\Fantasy\MakeTableController;


/*相關的Controller*/
use App\Http\Controllers\News\FunctionController as NewsFunction;
// use App\Http\Models\News\News;
// use App\Http\Models\News\NewsPhoto;
// use App\Http\Models\News\NewsSet;
// use App\Http\Models\News\NewsBig;

use App\Http\Models\Contact\ContactSet;
use App\Http\Models\Contact\Contact;
use App\Http\Models\Contact\ContactOption;



// use App\Http\Models\News\SearchPlace;
// use App\Http\Models\Seo;


class ContactController extends BackendController {

	
	/*首頁========================================================*/
	public function index($locale)
	{
		

		$pageBodyId='contact';
		$pageBodyClass='member contact';
        $ContactSet=ContactSet::first();
        $ContactOption=ContactOption::where('is_visible',1)->OrderBy("rank","asc")->get();
		


		return View::make( $locale.'.contact.index',
			[

				"pageBodyId"=>$pageBodyId,
				"pageBodyClass"=>$pageBodyClass,
                "ContactSet"=>$ContactSet,
                "ContactOption"=>$ContactOption,

            ]);
	}
	public function getdata()
	{
		$Contactdata=New Contact;
		
		unset($_POST['_token']);

		foreach($_POST as $key => $value)
		{
			$Contactdata->$key=$value;
		}

		$Contactdata->save();

		if($locale =="zh-tw")
		{
			echo "<script>alert('送出成功')</script>";
		}
		else
		{
			echo "<script>alert('Success')</script>";
		}
		
		echo "<script>window.history.back();</script>";
	}






}
	