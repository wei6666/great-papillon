<?php
namespace App\Http\Controllers\front;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use ItemMaker;
use Cache;
use Excel;
use Storage;
use Validator;
use Session;


/*create Fantasy class*/
use App\Http\Controllers\Fantasy\CreateFantasy;


/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\Fantasy\MakeTableController;


/*相關的Controller*/
use App\Http\Controllers\News\FunctionController as NewsFunction;
// use App\Http\Models\News\News;
// use App\Http\Models\News\NewsPhoto;
// use App\Http\Models\News\NewsSet;
// use App\Http\Models\News\NewsBig;
use App\Http\Models\News\NewsCategory;
use App\Http\Models\News\News;
use App\Http\Models\News\NewsPhoto;
use App\Http\Models\News\NewsContent;
use App\Http\Models\News\NewsSet;

// use App\Http\Models\News\SearchPlace;
// use App\Http\Models\Seo;


class NewsController extends BackendController {

	
	/*首頁========================================================*/
	public function index($locale)
	{
		$News=News::where('is_visible',1)->OrderBy("date",'desc')->get();

		$NewsNews=$News;
		foreach($News as $key => $value)
		{
				
			if(!empty(getimagesize(str_replace($locale,"",ItemMaker::url("").$value->out_image))))
			{
			$width=getimagesize(str_replace($locale,"",ItemMaker::url("").$value->out_image))[0];
			$height=getimagesize(str_replace($locale,"",ItemMaker::url("").$value->out_image))[1];
			}

			if($width>$height)
			{
				$NewsNews[$key]['sz']=rand(60,100);
			}
			else
			{
				$NewsNews[$key]['sz']=rand(40,80);	
			}
			
		}

		$NewsCategory=NewsCategory::where('is_visible',1)->OrderBy("rank",'asc')->get();

		$pageBodyId='news';
		$pageBodyClass='index';
		
		$NewsSet=NewsSet::first();
		return View::make( $locale.'.news.index',
			[
				"NewsNews"=>$NewsNews,	
				"NewsSet"=>$NewsSet,	
				"NewsCategory"=>$NewsCategory,
				"pageBodyId"=>$pageBodyId,
				"pageBodyClass"=>$pageBodyClass,

			
			]);
	}


	public function details($locale,$id="")
	{
			$NewsContent=NewsContent::where("news_id",$id)->where("is_visible",1)->get();

			$NewsCategory=NewsCategory::where("is_visible",1)->get();

			$NewsCategorydata=[];
			foreach ($NewsCategory as $key => $value) {
				$NewsCategorydata[$value->id]=$value->title;
			}

			$NewsPhoto=NewsPhoto::where("news_id",$id)->where("is_visible",1)->get();

			$News=News::where('is_visible',1)->where("id",$id)->OrderBy("rank",'asc')->first();

			$pageBodyId='news';
			$pageBodyClass='detail';

			
			$og_data=[];
			$og_data['web_title']=$News->title;
		    $og_data['meta_description']=$News->out_content;
		    $og_data['og_image']=$News->out_image;

		    
		  
			return View::make( $locale.'.news.details',
			[
				"NewsContent"=>$NewsContent,
				"News"=>$News,
				"NewsPhoto"=>$NewsPhoto,
				"pageBodyId"=>$pageBodyId,
				"pageBodyClass"=>$pageBodyClass,
				"NewsCategorydata"=>$NewsCategorydata,
				"og_data"=>$og_data,

			
			]);

	}



}
	