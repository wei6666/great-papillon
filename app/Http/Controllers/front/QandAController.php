<?php
namespace App\Http\Controllers\front;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use ItemMaker;
use Cache;
use Excel;
use Storage;
use Validator;
use Session;


/*create Fantasy class*/
use App\Http\Controllers\Fantasy\CreateFantasy;


/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\Fantasy\MakeTableController;


/*相關的Controller*/
use App\Http\Controllers\News\FunctionController as NewsFunction;
// use App\Http\Models\News\News;
// use App\Http\Models\News\NewsPhoto;
// use App\Http\Models\News\NewsSet;
// use App\Http\Models\News\NewsBig;

use App\Http\Models\Shopping_qa\Shopping_qa;
use App\Http\Models\Shopping_qa\Shopping_qa_set;
use App\Http\Models\Shopping_qa\Shopping_qa_category;
use App\Http\Models\Shopping_qa\Shopping_qa_content;


// use App\Http\Models\News\SearchPlace;
// use App\Http\Models\Seo;


class QandAController extends BackendController {

	
	/*首頁========================================================*/
	public function index($locale)
	{
		

		$pageBodyId='shopping_qa';
		$pageBodyClass='shopping_qa';
        $SetData=Shopping_qa_set::first();
        $Category=Shopping_qa_category::get();
        $qadata=Shopping_qa::where('is_visible',1)->get();

        foreach ($Category as $key => $value) {
            $Category[$key]['qa_title']=Shopping_qa::where("category_id",$value->id)->where('is_visible',1)->get();

            foreach ($Category[$key]['qa_title'] as $key1 => $value1) {
            $Category[$key]['qa_title'][$key1]['content']=Shopping_qa_content::where("qa_id",$value1->id)->where('is_visible',1)->OrderBy('rank','asc')->get();
            }

        }

        

        


		return View::make( $locale.'.qanda.index',
			[

				"pageBodyId"=>$pageBodyId,
				"pageBodyClass"=>$pageBodyClass,
                "qadata"=>$qadata,
                "SetData"=>$SetData,
                "Category"=>$Category,

            ]);
	}





}
	