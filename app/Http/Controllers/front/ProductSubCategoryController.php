<?php
namespace App\Http\Controllers\front;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use ItemMaker;
use Cache;
use Excel;
use Storage;
use Validator;
use Session;


/*create Fantasy class*/
use App\Http\Controllers\Fantasy\CreateFantasy;


/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\Fantasy\MakeTableController;


/*相關的Controller*/
use App\Http\Controllers\Product\FunctionController as ProductFunction;
// use App\Http\Models\Product\Product;
// use App\Http\Models\Product\ProductPhoto;
// use App\Http\Models\Product\ProductSet;
// use App\Http\Models\Product\ProductBig;
use App\Http\Models\Product\ProductCategory;
use App\Http\Models\Product\ProductSubCategory;
use App\Http\Models\Product\Product;
use App\Http\Models\Product\ProductPhoto;
use App\Http\Models\Product\Matirial;
use App\Http\Models\Product\Jewelry;
use App\Http\Models\Product\ProductSeparate;
use App\Http\Models\Member\Member;

// use App\Http\Models\Product\SearchPlace;
// use App\Http\Models\Seo;


class ProductSubCategoryController extends BackendController {

	
	/*首頁========================================================*/
	public function index($locale,$id)
	{

		$Matirial=Matirial::where('is_visible',1)     				  
	                      ->OrderBy('rank','asc')
	                      ->get();

	  	$Jewelry=Jewelry::where('is_visible',1)     				  
	                      ->OrderBy('rank','asc')
	                      ->get();


		//先撈這業的類別
		$ProductSubCategory=ProductSubCategory::where("id",$id)->first();
			$ProductData=[];
		//把所有產品撈出來
		if(!empty($_GET['Marketing']))
		{

			$id=$_GET['Marketing'];
			$ProductData=Product::where('is_visible',1) 
	    				  ->where("market_id",'like','%'.$id.'%')                   
	                      ->OrderBy('rank','asc')
	                      ->get();

	                      

		}
		elseif (!empty($_GET['Searching'])) {

			$id=$_GET['Searching'];
			$ProductData=Product::where('is_visible',1) 
	    				  ->where("search_id",'like','%'.$id.'%')                   
	                      ->OrderBy('rank','asc')
	                      ->get();
	     	
		}
		else
		{
			$ProductData=Product::where('is_visible',1) 
	    				  ->where("category_id",$id)                     
	                      ->OrderBy('rank','asc')
	                      ->get();
		}

		foreach ($ProductData as $key => $value) {
			$ProductSeparate=ProductSeparate::where("product_id",$value->id)->where('is_visible',1)->OrderBy('rank','asc')->first();

			if(!empty($ProductSeparate))
			{
				$ProductData[$key]->price_discount=$ProductSeparate->price_discount;
				$ProductData[$key]->price_original=$ProductSeparate->price_original;
			}
		}
    
    
    	$bigcate=ProductCategory::where('id',$ProductSubCategory->category_id)->first();
		$pageBodyClass="productSeries";
		$pageBodyId="product";
		//把它該選的照片組給他
		foreach($ProductData as $key => $value)
		{
			
			$value['out_img']=[];
			//沒指定找第一章 沒第一章就沒圖
			$value['out_img']= !empty(ProductPhoto::where('product_id',$value->id)->where('is_out_img',1)->first()->image) ? ProductPhoto::where('product_id',$value->id)->where('is_out_img',1)->first()->image : !empty(ProductPhoto::where('product_id',$value->id)->first()->image) ? ProductPhoto::where('product_id',$value->id)->first()->image : "";

			if(!empty($value))
			{
				$value= $value->toArray();
			}
			$ProductData[$key]= $value;


		}

		
		return View::make( $locale.'.product.productsubcategory',
			[
				"ProductData"=>$ProductData,
				"ProductSubCategory"=>$ProductSubCategory,
				"pageBodyClass"=>$pageBodyClass,
				"pageBodyId"=>$pageBodyId,
				"bigcate"=>$bigcate,
				"Jewelry"=>$Jewelry,
				"Matirial"=>$Matirial,
			
			]);
	}

	public function favorite($locale)
	{

		
	    $member=Member::where('id',Session::get('mb_id'))->first();

		$favorite=explode(",", $member->favorite);

		$favoritearr=[];
		foreach ($favorite as $key => $value) {
			array_push($favoritearr, $value);
		}
		//先撈這業的類別

		$ProductData=Product::where('is_visible',1) 
    				  ->whereIn("id",$favoritearr)                     
                      ->OrderBy('rank','asc')
                      ->get();
        foreach ($ProductData as $key => $value) {
			$ProductSeparate=ProductSeparate::where("product_id",$value->id)->where('is_visible',1)->OrderBy('rank','asc')->first();

			if(!empty($ProductSeparate))
			{
				$ProductData[$key]->price_discount=$ProductSeparate->price_discount;
				$ProductData[$key]->price_original=$ProductSeparate->price_original;
				if(!empty($ProductSeparate->price_original))
				{
					$ProductData[$key]['price']=$ProductSeparate->price_original;
				}

				if(!empty($ProductSeparate->price_discount))
				{
					$ProductData[$key]['price']=$ProductSeparate->price_discount;
				}


				
			}
			else
			{

				if(!empty($value->price_original))
				{
					$ProductData[$key]['price']=$value->price_original;
				}

				if(!empty($value->price_discount))
				{
					$ProductData[$key]['price']=$value->price_discount;
				}

			}
		}
    	
		$pageBodyClass="member member_favorite";
		$pageBodyId="member";
		//把它該選的照片組給他
		foreach($ProductData as $key => $value)
		{
			
			$value['out_img']=[];
			//沒指定找第一章 沒第一章就沒圖
			$value['out_img']= !empty(ProductPhoto::where('product_id',$value->id)->where('is_out_img',1)->first()->image) ? ProductPhoto::where('product_id',$value->id)->where('is_out_img',1)->first()->image : !empty(ProductPhoto::where('product_id',$value->id)->first()->image) ? ProductPhoto::where('product_id',$value->id)->first()->image : "";

			if(!empty($value))
			{
				$value= $value->toArray();
			}
			$ProductData[$key]= $value;


		}

		
		return View::make( $locale.'.member.favoriteview',
			[
				"ProductData"=>$ProductData,				
				"pageBodyClass"=>$pageBodyClass,
				"pageBodyId"=>$pageBodyId,
				
			]);
	}



}
	