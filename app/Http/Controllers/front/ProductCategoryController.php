<?php
namespace App\Http\Controllers\front;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use ItemMaker;
use Cache;
use Excel;
use Storage;
use Validator;
use Session;


/*create Fantasy class*/
use App\Http\Controllers\Fantasy\CreateFantasy;


/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\Fantasy\MakeTableController;


/*相關的Controller*/
use App\Http\Controllers\Product\FunctionController as ProductFunction;
// use App\Http\Models\Product\Product;
// use App\Http\Models\Product\ProductPhoto;
// use App\Http\Models\Product\ProductSet;
// use App\Http\Models\Product\ProductBig;
use App\Http\Models\Product\ProductCategory;
use App\Http\Models\Product\ProductCategoryAreadata;
// use App\Http\Models\Product\SearchPlace;
// use App\Http\Models\Seo;


class ProductCategoryController extends BackendController {

	
	/*首頁========================================================*/
	public function index($locale,$id)
	{

		 $position=[];
		 $position[1]='left_top';
		 $position[2]='left_down';
		 $position[3]='right_top';
		 $position[4]='right_down';
        

		
		//$ProductSet=ProductSet::first();

		//$allbig=ProductBig::where('is_visible' , 1)->OrderBy('rank',"asc")->get();
		$pageBodyId='product';
		$pageBodyClass='index';
		$thiscategory=ProductCategory::where('is_visible' , 1)->OrderBy('rank',"asc")->where('id',$id)->first();

		$thiscategoryareadata=ProductCategoryAreadata::where('is_visible' , 1)->OrderBy('rank',"asc")->where('category_id',$thiscategory->id)->get();
		//$seo=Seo::where('id',3)->first();
		
		return View::make( $locale.'.product.productcategory',
			[
				//"allbig"=>$allbig,
				"thiscategory"=>$thiscategory,
				"pageBodyId"=>$pageBodyId,
				"thiscategoryareadata"=>$thiscategoryareadata,
				"pageBodyClass"=>$pageBodyClass,
				"position"=>$position,
				//"ProductSet"=>$ProductSet,
				//'seo'=>$seo, 
			]);
	}

	
}
	