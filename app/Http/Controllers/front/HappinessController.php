<?php
namespace App\Http\Controllers\front;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use ItemMaker;
use Cache;
use Excel;
use Storage;
use Validator;
use Session;


/*create Fantasy class*/
use App\Http\Controllers\Fantasy\CreateFantasy;


/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\Fantasy\MakeTableController;


/*相關的Controller*/

use App\Http\Models\Happiness\HappinessCategory;
use App\Http\Models\Happiness\Happiness;
use App\Http\Models\Product\ProductSubCategory;
use App\Http\Models\Product\ProductCategory;
use App\Http\Models\Product\Product;
use App\Http\Models\Product\ProductPhoto;
use App\Http\Models\Happiness\HappinessSet;
use App\Http\Models\Location\Location;


// use App\Http\Models\Happiness\SearchPlace;
// use App\Http\Models\Seo;


class HappinessController extends BackendController {

	
	/*首頁========================================================*/
	public function index($locale)
	{

	//先撈這業的類別
	$HappinessCategory=HappinessCategory::where("is_visible",1)->get();
	
	//把所有產品撈出來
    $Happiness=Happiness::where('is_visible',1)     				  
                      ->OrderBy('id','DESC')
                      ->get();
    $HappinessSet=HappinessSet::first();

	$pageBodyClass="index";

	$pageBodyId="witness";

	if(!empty($Happiness))
	{	

		$Happiness->toArray();



		foreach ($Happiness as $key => $value) {

			if(!empty($value['product_id']))
			{

				$Product=Product::where("id",$value->product_id)->first();
				$ProductSubCategory=ProductSubCategory::where("id",$Product->category_id)->first();
				
		        

				

			}
		
			$ProductCategoryTitle=ProductCategory::where('id',$value->product_category_id)->first();
			if(empty($value->product_category_id))
			{
				$Happiness[$key]['probig']="-";
			}
			else {
				$Happiness[$key]['probig']=$ProductCategoryTitle->title;
			}


			

		}
	}

	
		return View::make( $locale.'.happiness.index',
			[
				"Happiness"=>$Happiness,
				"HappinessSet"=>$HappinessSet,
				"HappinessCategory"=>$HappinessCategory,
				"pageBodyClass"=>$pageBodyClass,
				"pageBodyId"=>$pageBodyId,				
				
			
			]);
	}


	public function detail($locale,$id="")
	{

		$Happiness=Happiness::where('is_visible',1)
				 	  ->where('id',$id)
                      ->OrderBy('rank','asc')
                      ->first();
                      
        $HappinessSet=HappinessSet::first();
        
        $Product=Product::where("id",$Happiness->product_id)->first();

        $ProductPhoto=ProductPhoto::where('product_id',$Happiness->product_id)->where('is_happinese_img',1)->first();

        if(empty($ProductPhoto))
        {
			$ProductPhoto=ProductPhoto::first();        	
        }

        $ProductSubCategory="";
        $ProductCategory="";
        if(!empty($Product->category_id))
        {
        	$ProductSubCategory=ProductSubCategory::where("id",$Product->category_id)->first();

        }

        if(!empty($Happiness->product_category_id))
        {

        	$ProductCategory=ProductCategory::where("id",$Happiness->product_category_id)->first();

        	$Happiness['probig']=$ProductCategory->title;

        }

        $Location="";
        
		if(!empty($Happiness->location_id))
		{
			$Location=Location::where("id",$Happiness->location_id)->first();
		}	    	

		$og_data['web_title']=$Happiness->title;
	    $og_data['meta_description']=$Happiness->content;
	    $og_data['og_image']=$Happiness->image;

	    $pageBodyClass="detail";

		$pageBodyId="witness";


		return View::make( $locale.'.happiness.detail',
			[
				"Happiness"=>$Happiness,
				"HappinessSet"=>$HappinessSet,
				"Product"=>$Product,
				// "HappinessSet"=>$HappinessSet,
				// "HappinessCategory"=>$HappinessCategory,
				"pageBodyClass"=>$pageBodyClass,
				"pageBodyId"=>$pageBodyId,
				"ProductPhoto"=>$ProductPhoto,
				"Location"=>$Location,
				"og_data"=>$og_data,
				
			]);

	}
	



}
	