<?php
namespace App\Http\Controllers\front;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use ItemMaker;
use Cache;
use Excel;
use Storage;
use Validator;
use Session;
use DateTime;


/*create Fantasy class*/
use App\Http\Controllers\Fantasy\CreateFantasy;


/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\Fantasy\MakeTableController;


/*相關的Controller*/
use App\Http\Controllers\News\FunctionController as NewsFunction;
// use App\Http\Models\News\News;
// use App\Http\Models\News\NewsPhoto;
// use App\Http\Models\News\NewsSet;
// use App\Http\Models\News\NewsBig;

use App\Http\Models\Home\Home;
use App\Http\Models\Product\Product;
use App\Http\Models\News\News;
use App\Http\Models\Product\ProductCategory;
use App\Http\Models\Product\ProductSubCategory;
use App\Http\Models\Product\ProductPhoto;
use App\Http\Models\Product\ProductContent;
use App\Http\Models\Happiness\HappinessSet;
use App\Http\Models\Happiness\Happiness;



// use App\Http\Models\News\SearchPlace;
// use App\Http\Models\Seo;


class HomeController extends BackendController {

	
	/*首頁========================================================*/
	public function index($locale)
	{
		
        
		$pageBodyId='home';
		$pageBodyClass='home has_video';
        $Home=Home::first();
        $Product=Product::where('is_home',1)->OrderBy('rank','asc')->limit(4)->get();
        $ProductCategory=ProductCategory::where('is_visible',1)->OrderBy('rank','asc')->get();
        $News=News::where('is_home',1)->where('is_visible',1)->OrderBy('rank','asc')->limit(6)->get();

        $HappinessSet=HappinessSet::first();

        $Happiness=Happiness::where('is_visible',1)->where('is_home',1)->get();

        $Happinesscount=Happiness::where('is_visible',1)->get();

        $pro_cateid_to_title=[];

        foreach ($News as $key => $value) {
            if(!empty($value->date))
            {
                $date= new DateTime($value->date);
                $News[$key]['day']=$date->format('Y M d');
            }
        }

        foreach ($ProductCategory as $key => $value) {
            $pro_cateid_to_title[$value->id]=$value->title;
        }

        foreach ($Product as $key => $value) {
            $Product[$key]['img1']=ProductPhoto::where('product_id',$value->id)->where('is_home1_img',1)->first();

            $Product[$key]['img2']=ProductPhoto::where('product_id',$value->id)->where('is_home2_img',1)->first();

            $ProductSubCate=ProductSubCategory::where('id',$value->category_id)->first();

            $ProductCate=ProductCategory::where('id',$ProductSubCate->category_id)->first();


            $Product[$key]['cate_title']=$ProductCate->title;

            $Product[$key]['cate_en_title']=$ProductCate->en_title;

            $Product[$key]['content']=ProductContent::where('product_id',$value->id)->first();


        }
       // dd($Product);
		
        $logindata=Session::get("name");
        $carcount=count(Session::get("product_data"));
		return View::make( $locale.'.home.index',
			[

				"pageBodyId"=>$pageBodyId,
				"pageBodyClass"=>$pageBodyClass,
                "Home"=>$Home,
                "Product"=>$Product,
                "ProductCategory"=>$ProductCategory,
                "News"=>$News,
                "carcount"=>$carcount,
                "logindata"=>$logindata,
                "HappinessSet"=>$HappinessSet,
                "Happiness"=>$Happiness,
                "pro_cateid_to_title"=>$pro_cateid_to_title,
                "Happinesscount"=>$Happinesscount,

            ]);
	}


 
}
	