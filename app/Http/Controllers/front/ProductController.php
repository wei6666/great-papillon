<?php
namespace App\Http\Controllers\front;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use ItemMaker;
use Cache;
use Excel;
use Storage;
use Validator;
use Session;


/*create Fantasy class*/
use App\Http\Controllers\Fantasy\CreateFantasy;


/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\front\SessionController as DealSession;


/*相關的Controller*/
use App\Http\Controllers\Product\FunctionController as ProductFunction;
use App\Http\Controllers\Fantasy\PermissionController as Permission;
// use App\Http\Models\Product\Product;
// use App\Http\Models\Product\ProductPhoto;
// use App\Http\Models\Product\ProductSet;
// use App\Http\Models\Product\ProductBig;
use App\Http\Models\Product\ProductCategory;
use App\Http\Models\Product\ProductSubCategory;
use App\Http\Models\Product\Product;
use App\Http\Models\Product\ProductPhoto;
use App\Http\Models\Product\ProductContent;
use App\Http\Models\Size\SizeCategory;
use App\Http\Models\Product\ProductSeparate;
use App\Http\Models\Product\ProductSize;
use App\Http\Models\Product\ProductSet;
use App\Http\Models\Size\AllSize;
use App\Http\Models\Happiness\Happiness;
use App\Http\Models\Pay\OrderList;
use App\Http\Models\Member\Address;
use App\Http\Models\Member\City;
use App\Http\Models\Member\Member;
use App\Http\Models\DisCount\DisCountSet;
use App\Http\Models\DisCount\DisCountPercent;
use App\Http\Models\DisCount\DisCountFull;
use App\Http\Models\Setting;
use App\Http\Models\Member\Bonus;


// use App\Http\Models\Product\SearchPlace;
// use App\Http\Models\Seo;


class ProductController extends BackendController {

	
	/*首頁========================================================*/
	public static function DealReturnOrderList($locale,$MerchantTradeNo="")
	{


		$ReturnOrderList=OrderList::where("order_list_type",12)->get();
		
		foreach ($ReturnOrderList as $key => $value) {
			ProductController::cancel($locale,$value->id);
			
		}
	}
	
	public static function NewBonusData($locale,$MerchantTradeNo="")
	{

		//$MerchantTradeNo="MerchantTradeNo";

		if(!empty($MerchantTradeNo))
		{
			$OrderList=OrderList::where("MerchantTradeNo",$MerchantTradeNo)->get();
		}
		else
		{
			$addbonusarr=[2,7,4];
			$OrderList=OrderList::whereIn("order_list_type",$addbonusarr)->where('is_count',"no")->get();
			
		}
		 
        //訂單成立 把它用的折扣碼納入計算 並把紅利寫入BONUS表 但還不加入
        foreach($OrderList as $value)
        {
        	$forlistcount=OrderList::where("member_sql_id",$value->member_sql_id)->get();

        	$Member=Member::where("id",$value->member_sql_id)->first();


            $feedback=json_decode($value->feedback);
            $friendfeedback=json_decode($value->friendfeedback);



            
            
            //購買回饋贈點
            if(!empty($feedback))
            {
                $bonus=New Bonus;
                $bonus->mb_id=$value->member_sql_id;
                $bonus->order_list_id=$value->id;
                $bonus->bonus=$feedback->feedbackBP;
                $bonus->is_add=0;
                $bonus->sel_type=2;
                $bonus->detail="購買回饋贈點 詳情請查詢訂單編號".$value->num_for_user;
                $bonus->save();
            }


            //有填朋友贈點
            if(!empty($friendfeedback->friend_id))
            {
                //朋友的點數
                $bonus=New Bonus;
                $bonus->mb_id=$friendfeedback->friend_id;                
                $forfriendname=Member::where("id",$friendfeedback->friend_id)->first();
                $bonus->order_list_id=$value->id;          
                $bonus->bonus=$friendfeedback->friendBP;
                $bonus->is_add=0;
                $bonus->sel_type=4;
                $bonus->detail="好友".$forfriendname->name."購買回饋贈點";
                $bonus->save();


                //會員的點數
                $bonus=New Bonus;
                $bonus->mb_id=$value->member_sql_id;
                $bonus->order_list_id=$value->id;
                $bonus->bonus=$friendfeedback->friendBP;
                $bonus->is_add=0;
                $bonus->sel_type=4;
                // $bonus->detail="邀請好友回饋贈點";
                // $bonus->save();
            }

            //首次購買贈點
            if(count($forlistcount) ==1)
            {
                $bonus=New Bonus;
                $bonus->mb_id=$value->member_sql_id;
                $bonus->order_list_id=$value->id;
                //這邊要開欄位讓他可以改喔
                $bonus->bonus=500;
                $bonus->is_add=0;
                $bonus->sel_type=5;
                $bonus->detail="首次購買贈點";
                $bonus->save();
                // $Member->Bonus=$Member->Bonus+500;
                // $Member->save();
            }

            if(!empty($value->DisCountBonusRecord))
            {
                
                $bonusdata=Bonus::where('order_list_id',$value->id)->where('is_add',0)->first();
                if(count($bonusdata) > 0)
                {
                    $bonusdata->is_add=1;
                    $bonusdata->order_list_id=$value->id;
                    $bonusdata->save();
                    // $Member->bonus+=$bonusdata->bonus;

                    // $Member->save();
                }
            }

            $value->is_count='yes';
           	$value->save();
            
            
        }
        ProductController::addbonus($locale);
	}
	public static function addbonus($locale,$id="",$check="")
	{
		$OrderList=OrderList::where('is_add_bonus',0)->where("order_list_type",4)->get();


		

		foreach ($OrderList as $key => $value) 
		{

			//超過 7天 把紅利加進去

			if(date('Y-m-d H:i:s', strtotime($value->order_list_3)+604800) < date("Y-m-d H:i:s"))
			{
				
				$bonus=Bonus::where('order_list_id',$value->id)->get();

				if(count($bonus) > 0)
				{
					
					foreach ($bonus as $keybonus => $bonusvalue) 
					{
						
							$bonusvalue->is_add=1;
							$bonusvalue->save();
						
						
					}
				}

			$value->is_add_bonus=1;
			$value->order_list_type=7;
			$value->save();
			}
		}

		$OrderListNo7Day=OrderList::where('is_add_bonus',0)->where("order_list_type",7)->get();

		foreach ($OrderListNo7Day as $key => $value) 
		{

			
				
				$bonus=Bonus::where('order_list_id',$value->id)->get();

				if(count($bonus) > 0)
				{
					
					foreach ($bonus as $keybonus => $bonusvalue) 
					{
						
							$bonusvalue->is_add=1;
							$bonusvalue->save();
						
						
					}
				}

			$value->is_add_bonus=1;
			$value->order_list_type=7;
			$value->save();
			
		}


		
	}
	public function addfavorite($locale,$id="",$check="")
	{


		$member=Member::where('id',Session::get('mb_id'))->first();

		if(!empty($member->favorite))
		{
			$favoritearr=explode(",", $member->favorite);
		}
		else
		{
			$favoritearr=[];
		}
		

		if($check!="")
		{
			if(in_array($id, $favoritearr))
			{
				return "in";
			}
			else
			{
				return "noin";
			}
		}

		if(count($favoritearr) < 1)
		{
			$favoritearr=[];
		}


		if(in_array($id, $favoritearr))
		{
			
			unset($favoritearr[array_search($id, $favoritearr)]);
			
			$fastr='';
			foreach($favoritearr as $vv)
			{
				if($vv !="")
				{
					$fastr.=$vv.",";
				}
			}
			$member->favorite=$fastr;

			$member->save();

			if(!empty($_GET['R']))
			{
				return redirect($locale.'/member/favorite');
				
			}

			return 'canadd';
		}
		else
		{
			array_push($favoritearr, $id);

			$fastr='';
			foreach($favoritearr as $vv)
			{
				if($vv !="")
				{
					$fastr.=$vv.",";
				}
			}
			$member->favorite=$fastr;

			$member->save();

			if(!empty($_GET['R']))
			{
				return redirect($locale.'/member/favorite');
				
			}

			return 'cannotadd';

		}
		

		

	}

	public function change_ticket($locale)
	{
		$arr=[];

		array_push($arr, $_POST['ticketnum']);
		array_push($arr, $_POST['ticket_title']);
		array_push($arr, $_POST['tickettype']);
		Session::forget('ticket');        	
		Session::put('ticket',$arr);

	}

	public function changesession($locale)
	{
		$str="";
		
        if($_POST['type']=="ticket_receiver")
        {
        	
			Session::forget('ticket_receiver');
        	// unset($_POST['_token']);
        	// unset($_POST['type']);
			Session::put('ticket_receiver',$_POST);
        }

        if($_POST['type']=="shop_receiver")
        {
        	Session::forget('shop_receiver');
        	// unset($_POST['_token']);
        	// unset($_POST['type']);
			Session::put('shop_receiver',$_POST);

        }



                 $address="<p><span>收件人姓名 : </span><span>".$_POST['name']."</span></p><p><span>收件人住址 : </span><span>".$_POST['zip']." ".$_POST['city']." ".$_POST['address']."</span></p><p><span>收件人電話 : </span><span>".$_POST['mobile']."</span></p>";
                 /*<p><span>收件人手機 : </span><span>0912345678</span></p>*/
               

        

		return $_POST['type']."--".$address;
	}
	public function ajaxnewaddress($locale)
	{
		$str="";
		$arr=$_POST;
        foreach($arr as $key => $value)
        {
                $str.=' key: '.$key."val:".$arr[$key];
        }
        $add =new Address ;
        $add->title=$_POST['title'];
        $add->mobile=$_POST['mobile'];
        $add->name=$_POST['name'];
        $add->city=$_POST['city'];
        $add->zip=$_POST['zip'];
        $add->address=$_POST['address'];

        $add->save();
        
        if($_POST['type']=="ticket_receiver")
        {
        	
			Session::forget('ticket_receiver');
        	// unset($_POST['_token']);
        	// unset($_POST['type']);
			Session::put('ticket_receiver',$_POST);
        }

        if($_POST['type']=="shop_receiver")
        {
        	Session::forget('shop_receiver');
        	// unset($_POST['_token']);
        	// unset($_POST['type']);
			Session::put('shop_receiver',$_POST);

        }

         $address="<p><span>收件人姓名 : </span><span>".$_POST['name']."</span></p><p><span>收件人住址 : </span><span>".$_POST['zip']." ".$_POST['address']."</span></p><p><span>收件人電話 : </span><span>".$_POST['mobile']."</span></p>";
                 /*<p><span>收件人手機 : </span><span>0912345678</span></p>*/
               

        

		return $_POST['type']."--".$address;
	}

	public function orderdetailTalkbar($locale,$id="",$speaker="",$content="")
	{

		$OrderList=OrderList::where("id",$id)->first();

		if( !empty($speaker ))
		{
			$talk=$OrderList->talkbar;
			$talk_data=json_decode($talk);
			
			$input_data=[];
			$input_data['speaker']=$speaker;
			$input_data['content']=$content;
			
			$input_data['time']=Date("Y/m/d h:i:s a");
			if(empty($talk_data))
			{
				$talk_data=[];
			}
			array_push($talk_data, $input_data);
			$OrderList->talkbar=json_encode($talk_data);
			

			$OrderList->save();


			$OrderList=OrderList::where("id",$id)->first();
			$talkdata=json_decode($OrderList->talkbar);
			$talkdata=array_reverse($talkdata);
			return View::make( $locale.'.member.talkbar',
			[
				"talkdata"=>$talkdata,
				
				
			]);

		}
		else
		{

			$OrderList=OrderList::where("id",$id)->first();			
			$talkdata=json_decode($OrderList->talkbar);
			$talkdata=array_reverse($talkdata);
			return View::make( $locale.'.member.talkbar',
			[
				"talkdata"=>$talkdata,
				
				
			]);
		}

	}


	public function backorderdetailTalkbar($locale,$id="",$speaker="",$content="")
	{

		$OrderList=OrderList::where("id",$id)->first();

		if( !empty($speaker ))
		{
			$talk=$OrderList->talkbar;
			$talk_data=json_decode($talk);
			
			$input_data=[];
			$input_data['speaker']=$speaker;
			$input_data['content']=$content;
			
			$input_data['time']=Date("Y/m/d h:i:s a");
			if(empty($talk_data))
			{
				$talk_data=[];
			}
			array_push($talk_data, $input_data);
			$OrderList->talkbar=json_encode($talk_data);
			

			$OrderList->save();


			$OrderList=OrderList::where("id",$id)->first();
			$talkdata=json_decode($OrderList->talkbar);
			$talkdata=array_reverse($talkdata);
			return View::make( $locale.'.member.back_talkbar',
			[
				"talkdata"=>$talkdata,
				
				
			]);

		}
		else
		{


			$OrderList=OrderList::where("id",$id)->first();				
			$talkdata=json_decode($OrderList->talkbar);
			$talkdata=array_reverse($talkdata);
			return View::make( $locale.'.member.back_talkbar',
			[
				"talkdata"=>$talkdata,
				
				
			]);
		}

	}

	



	public function orderdetail($locale,$id="")
	{
		
		$pageBodyClass="member orderDetail_frame";
		$pageBodyId="member";
		$Member_OrderList=OrderList::where("id",$id)->first();

		$productdata=json_decode($Member_OrderList->buy_stuff_json);

		$addressdata=Address::where('mb_id',Session::get('mb_id'))
                    ->with('City')
                    ->get();



        $City=City::get();
        $Citydata=[];
        foreach ($City as $key => $value) {

        	$Citydata[$value->id]=$value->title;
        }
		return View::make( $locale.'.member.orderlistdetail',
			[
				"Member_OrderList"=>$Member_OrderList,
				"pageBodyId"=>$pageBodyId,
				"pageBodyClass"=>$pageBodyClass,
				"productdata"=>$productdata,
				"addressdata"=>$addressdata,
				"Citydata"=>$Citydata,
				
			]);
	}
	public static function cancel($locale,$id="",$nogo="")
	{


		$Member_OrderList=OrderList::where("id",$id)->first();
		$Member_OrderList->is_cancel=1;
		if($Member_OrderList->order_list_type != 12)
		{
			$Member_OrderList->order_list_type=5;
		}
		
		$Member_OrderList->save();
		$bonus=Bonus::where('order_list_id',$id)->get();

		if(count($bonus) > 0)
		{
			foreach ($bonus as $key => $value) 
			{
				$value->delete();
			}
		
			
		 
		}

		if(!empty($nogo))
		{
			return redirect($locale.'/member/order');
		}

	}

	public function backchangerecord($locale)
	{
		
		$order_type_arr=["未完成付款","未完成付款","訂單成立","已出貨","已到貨","取消訂單","退換貨審核中","完成訂單","退貨進行中","換貨進行中","已換貨","退換貨審核中","已退貨"];

		$pageBodyClass="orderReturn_frame";
		$pageBodyId="member";

		ProductController::ifTimeOutChangeOrderListType($locale);

		$member_id=Session::get("mb_id");
        $backchangerecordarr=[6,8,9,10,11,12];

        $Member_OrderList=OrderList::/*where('is_cancel',0)->*/where("member_sql_id",$member_id)->whereIn('order_list_type',$backchangerecordarr)->OrderBy("created_at","desc")->get();


        //dd($Member_OrderList);
        $arrivedate=[2,3,4,6,7,9,10];

        $cancancelarr=[1,2,3,4];

		return View::make( $locale.'.member.returnhistory',
			[
				"Member_OrderList"=>$Member_OrderList,
				"pageBodyId"=>$pageBodyId,
				"pageBodyClass"=>$pageBodyClass,
				"order_type_arr"=>$order_type_arr,
				"arrivedate"=>$arrivedate,
				"cancancelarr"=>$cancancelarr,
				
			]);

	}

	public function orderhistory($locale)
	{

		$order_type_arr=["未完成付款","未完成付款","訂單成立","已出貨","已到貨","取消訂單","退換貨審核中","完成訂單","退貨進行中","換貨進行中","已換貨","退換貨審核中","已退貨"];

		$pageBodyClass="orderReturn_frame";
		$pageBodyId="member";

		ProductController::ifTimeOutChangeOrderListType($locale);

		$member_id=Session::get("mb_id");

		$Member_OrderList=OrderList::/*where('is_cancel',0)->*/where("member_sql_id",$member_id)->OrderBy("created_at","desc")->get();
		  
          
        $arrivedate=[2,3,4,6,7,9,10];

        $cancancelarr=[1,2,3,4];

		return View::make( $locale.'.member.orderhistory',
			[
				"Member_OrderList"=>$Member_OrderList,
				"pageBodyId"=>$pageBodyId,
				"pageBodyClass"=>$pageBodyClass,
				"order_type_arr"=>$order_type_arr,
				"arrivedate"=>$arrivedate,
				"cancancelarr"=>$cancancelarr,
				
			]);
		

	}

	public static function ifTimeOutChangeOrderListType($locale,$is_all="")
	{
		if(!empty($is_all))
		{
			$Member_OrderList=OrderList::where('is_cancel',0)->where("order_list_type",1)->OrderBy("created_at","asc")->get();
		}
		else
		{


			$member_id=Session::get("mb_id");

			$Member_OrderList=OrderList::where('is_cancel',0)->where("member_sql_id",$member_id)->where("order_list_type",1)->OrderBy("created_at","asc")->get();
		}

		
		foreach ($Member_OrderList as $key => $value) {
			

			if(date('Y-m-d H:i:s', strtotime($value->created_at)+3600) < date("Y-m-d H:i:s") && $value->order_list_type == 1)
			{
				
				//更新訂單狀態
				ProductController::cancel($locale,$value->id,"go");
				
			}
			$Member_OrderList[$key]['pro_data']=json_decode($value->buy_stuff_json);
		}
	}


	public function index($locale,$id)
	{
		$ProductData=Product::where('is_visible',1)->where("id",$id)->OrderBy("rank","asc")->first();


		$ProductSeparate=ProductSeparate::where("product_id",$id)->where('is_visible',1)->OrderBy('rank','asc')->get();

		$ProductContent=ProductContent::where("product_id",$ProductData->id)->where('is_visible',1)->get();

		$ProductSubCategory=ProductSubCategory::where("id",$ProductData->category_id)->first();

		$ProductPhoto=ProductPhoto::where('product_id',$ProductData->id)->where('is_visible',1)->get();

		$ProductMayLike=Product::where('is_visible',1)->where("en_num",$ProductData->en_num)->OrderBy("rank","asc")->get()->shuffle();


		$ProductHappiness=Happiness::where('is_visible',1)->where("product_id",$id)->OrderBy("rank","asc")->get()->shuffle();

		

		foreach ($ProductMayLike as $key => $value) {

			if(!empty($value) )
			{
				$provalue=$value->toArray();

				$img=ProductPhoto::where('product_id',$provalue['id'])->where('is_may_like',1)->first();

				if(!empty($img->image))
				{
					$ProductMayLike[$key]['out_img']=$img->image;
				}

			}
			if($value->id == $id)
			{
				unset($ProductMayLike[$key]);
			}
		}





		foreach ($ProductHappiness as $key => $value) {

			if(!empty($value) )
			{
				$provalue=$value->toArray();

				$img=ProductPhoto::where('product_id',$id)->where('is_happinese_img',1)->first();

				if(!empty($img->image))
				{
					$ProductHappiness[$key]['out_img']=$img->image;
				}

			}
			
		}

		

		$pageBodyClass="productInf ";
		$pageBodyId="product";

		if(!empty($ProductSeparate->toArray()) && $ProductData->is_sell_out != 0)
		{
			$pageBodyClass.=" hasSize";
		}
		else
		{

			$pageBodyClass.="justajax";
		}

		if($ProductData->is_sell_out ==0)
		{
			$pageBodyClass.=" out";
		}


		$bigcate=ProductCategory::where("id",$ProductSubCategory->category_id)->first();

		
		$og_data=[];
		$og_data['web_title']=$ProductData->title;
	    $og_data['meta_description']=$ProductData->out_content;
	    $og_data['og_image']="";
	    $og_data['meta_description']="";
	    if(count($ProductContent) > 0)
	    {

	    	$og_data['meta_description']=$ProductContent[0]->content;
	    }
	    if(count($ProductPhoto) > 0)
	    {

	    	$og_data['og_image']=$ProductPhoto[0]->image;
	    }
	    


		/*foreach($ProductData as $key => $value)
		{
			
			$value['out_img']=[];
			$value['out_img']= !empty(ProductPhoto::where('product_id',$value->id)->where('is_out_img',1)->first()->image) ? ProductPhoto::where('product_id',$value->id)->where('is_out_img',1)->first()->image : "";
			if(!empty($value))
			{
				$value= $value->toArray();
			}
			$ProductData[$key]= $value;


		}*/


		$is_in_favorite=ProductController::addfavorite($locale,$id,"check");
		
		return View::make( $locale.'.product.productdetails',
			[
				"ProductData"=>$ProductData,
				"ProductSeparate"=>$ProductSeparate,
				"ProductSubCategory"=>$ProductSubCategory,				
				"ProductPhoto"=>$ProductPhoto,
				"pageBodyClass"=>$pageBodyClass,
				"pageBodyId"=>$pageBodyId,
				"ProductContent"=>$ProductContent,
				"ProductMayLike"=>$ProductMayLike, 
				"bigcate"=>$bigcate,
				"ProductHappiness"=>$ProductHappiness,
				"is_in_favorite"=>$is_in_favorite,
				"og_data"=>$og_data,

			
			]);
	}

	public function changemenu($locale,$id='',$Separate_id='')
	{
			$Product=Product::where("id",$id)->first();
			$Productnum=$Product->id;
			$ProductSet=ProductSet::first();
			$ProductTitle="";
			if(!empty($Separate_id))
			{
				$ProductSeparate=ProductSeparate::where("id",$Separate_id)->where('is_visible',1)->OrderBy('rank','asc')->first();
				$ProductTitle=$ProductSeparate->id;
			}
			else
			{
				$ProductSeparate=[];
			}



			//有次產品的時候 抓次產品
			if(!empty($ProductSeparate))
			{
			
					$SizeCategory=SizeCategory::where("id",$ProductSeparate->size_category)->first();
					$correct_size=$ProductSeparate;
			}
			//沒有的時候
			else{				
				$correct_size=$Product;			
			}

			if(!empty($correct_size))
			{
				$correct_size=$correct_size->toArray();
			}
			//抓類別
			$ProductSubCategory=ProductSubCategory::where("id",$Product->category_id)->first();

			//從類別找尺寸類別
			$SizeCategory=SizeCategory::where("id",$ProductSubCategory->size_category)->first();
			//從尺寸類別找尺寸
			$ProductSizetitle=AllSize::where("category_id",$ProductSubCategory->size_category)->OrderBy('rank','asc')->get();
			

			return View::make( $locale.'.product.productmenulist',
			[
				"correct_size"=>$correct_size,
				"SizeCategory"=>$SizeCategory,
				"ProductSet"=>$ProductSet,
				"ProductSizetitle"=>$ProductSizetitle,
				"Productnum"=>$Productnum,
				"ProductTitle"=>$ProductTitle,
				
			]);


	}

	public function order	($locale,$order='',$matirial='',$jewelry='')
	{


			$ProductData=Product::where("is_visible",1);
			
			
			
				
			if($jewelry !='*')
			{
				$ProductData=$ProductData->where('search_jewelry',"like",'%'.$jewelry.'%');
			}

			if($matirial !='*')
			{
				$ProductData=$ProductData->where('search_matirial',"like",'%'.$matirial.'%');
			}

				$ProductData=$ProductData->get();
		

			foreach ($ProductData as $key => $value) {
				$ProductSeparate=ProductSeparate::where("product_id",$value->id)->where('is_visible',1)->OrderBy('rank','asc')->first();

				if(!empty($ProductSeparate))
				{
					$ProductData[$key]->price_discount=$ProductSeparate->price_discount;
					$ProductData[$key]->price_original=$ProductSeparate->price_original;
				}
			}

			foreach($ProductData as $key => $value)
			{
				
				$value['out_img']=[];
				//沒指定找第一章 沒第一章就沒圖
				$value['out_img']= !empty(ProductPhoto::where('product_id',$value->id)->where('is_out_img',1)->first()->image) ? ProductPhoto::where('product_id',$value->id)->where('is_out_img',1)->first()->image : !empty(ProductPhoto::where('product_id',$value->id)->first()->image) ? ProductPhoto::where('product_id',$value->id)->first()->image : "";

				if(!empty($value))
				{
					$value= $value->toArray();
				}
				$ProductData[$key]= $value;


			}
			return View::make( $locale.'.product.order_ajax',
			[
				"ProductData"=>$ProductData,
				
			
			]);

	}

	public function to_order_list($locale,$id='')
	{



		$addressdata=Address::where('mb_id',Session::get('mb_id'))
                    ->with('City')
                    ->get();


         
        $City=City::get();
        
        $Citydata=[];

        foreach ($City as $key => $value) {

        	$Citydata[$value->id]=$value->title;
        }

        //dd($addressdata);

		$pageBodyClass="member shopping_cart";
		$pageBodyId="member";
		$Product=Product::get();
		$ProductSeparate=ProductSeparate::get();

		$ProductSeparateArr=[];
		$ProductArr=[];
		$car_data=[];
		$total=0;

		foreach ($ProductSeparate as $key => $value) {
			$ProductSeparateArr[$value->id]=$value;
		}

		foreach ($Product as $key => $value) {
			$ProductArr[$value->id]=$value;
		}

		//dd($ProductSeparateArr);
		$sessiondata=Session::get('product_data');
		if(!empty($sessiondata))
		{
			foreach($sessiondata as $key => $value)
			{

				$car_data[$key]['price']=0;
				$productid=$value['productid'];
				$separate_id=$value['separate_id'];
				$count=$value['count'];
				$size=$value['size'];
				// if separate_id

				if($separate_id !="-")
				{
					$car_data[$key]['separate']=$ProductSeparateArr[$separate_id];
					if(!empty($ProductSeparateArr[$separate_id]->price_discount))
					{
						$car_data[$key]['price']=$ProductSeparateArr[$separate_id]->price_discount;
					}
					else {
						$car_data[$key]['price']=$ProductSeparateArr[$separate_id]->price_original;
					}
					//串完整產品ID
					$car_data[$key]['complete_num']=$ProductArr[$productid].$ProductSeparateArr[$separate_id]->code.$count;
				}
				// if no separate_id
				else
				{
					
					if(!empty($ProductArr[$productid]->price_discount))
					{
						$car_data[$key]['price']=$ProductArr[$productid]->price_discount;
					}
					else {
						$car_data[$key]['price']=$ProductArr[$productid]->price_original;
					}
					//串完整產品ID
					$car_data[$key]['complete_num']=$ProductArr[$productid].$count;

				}


				

					
				$car_data[$key]['out_img']= !empty(ProductPhoto::where('product_id',$productid)->where('is_out_img',1)->first()->image) ? ProductPhoto::where('product_id',$productid)->where('is_out_img',1)->first()->image : !empty(ProductPhoto::where('product_id',$productid)->first()->image) ? ProductPhoto::where('product_id',$productid)->first()->image : "";
					
					

				$car_data[$key]['product']=$ProductArr[$productid];
				$car_data[$key]['count']=$count;
				$car_data[$key]['size']=$size;				
				$total+=($car_data[$key]['price']/1)*($count/1);

				$DisCountData=DealSession::to_discount($total);
				$total=$DisCountData['total'];
				$Setting=Setting::first();

				$feedback=ceil($total/100 * $Setting->feedback);

				$friend_feedback=ceil($total/100 * $Setting->friend_feedback);
				



			}
		}
		else
		{
				$car_data="";
				$total="";
				$size="";
				$count="";
		}


		$DisCountPercent=DisCountPercent::where('is_visible',1)
                                      ->where('st_date',"<=",Date("Y-m-d"))
                                      ->where('ed_date',">=",Date("Y-m-d"))
                                      ->OrderBy('id','desc')
                                      ->first();

		$DisCountFull=DisCountFull::where('is_visible',1)
                                      ->where('st_date',"<=",Date("Y-m-d"))
                                      ->where('ed_date',">=",Date("Y-m-d"))
                                      ->OrderBy('id','desc')
                                      ->first();
		
		
		
		$DisCountSet=DisCountSet::first();
		$MemberData=Member::where("id",Session::get('mb_id'))->first();

		
		return View::make( $locale.'.product.order_list',
			[
				"car_data"=>$car_data,
				"total"=>$total,
				"size"=>$size,
				"count"=>$count,
				"pageBodyId"=>$pageBodyId,
				"pageBodyClass"=>$pageBodyClass,
				"sessiondata"=>$sessiondata,
				"addressdata"=>$addressdata,
				"Citydata"=>$Citydata,
				"MemberData"=>$MemberData,
				"City"=>$City,
				"DisCountData"=>$DisCountData,
				"DisCountSet"=>$DisCountSet,
				"feedback"=>$feedback,
				"friend_feedback"=>$friend_feedback,
				"DisCountPercent"=>$DisCountPercent,
				"DisCountFull"=>$DisCountFull,
			]);

	}


}
	