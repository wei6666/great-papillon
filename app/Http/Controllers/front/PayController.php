<?php
namespace App\Http\Controllers\front;
/**原生函式**/
use Illuminate\Http\Request;
use View;
use ItemMaker;
use Cache;
use Excel;
use Storage;
use Validator;
use Session;


/*create Fantasy class*/
use App\Http\Controllers\SMSHttpSend;
use App\Http\Controllers\Fantasy\CreateFantasy;
use App\Http\Models\User;
use App\Http\Models\Pay\OrderList;
use App\Http\Models\Product\Product;
use App\Http\Models\Product\ProductPhoto;
use App\Http\Models\Product\ProductSeparate;
use App\Http\Models\DisCount\DisCountPercent;
use App\Http\Models\DisCount\DisCountFull;
use App\Http\Models\DisCount\DisCountCode;
use App\Http\Models\DisCount\DisCountSet;
use App\Http\Models\Setting;
use App\Http\Models\Member\Bonus;
use App\Http\Models\Member\Member;

/**相關Controller**/
use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Controllers\Fantasy\MakeTableController;
use App\Http\Controllers\front\PayCCC as ALLapi;
use App\Http\Controllers\front\SessionController as DealSession;
use App\Http\Controllers\front\ProductController as ProductController;

class AllInOne {

    public $ServiceURL = 'ServiceURL';
    public $ServiceMethod = 'ServiceMethod';
    public $HashKey = 'HashKey';
    public $HashIV = 'HashIV';
    public $MerchantID = 'MerchantID';
    public $PaymentType = 'PaymentType';
    public $Send = 'Send';
    public $SendExtend = 'SendExtend';
    public $Query = 'Query';
    public $Action = 'Action';
    public $ChargeBack = 'ChargeBack';
    public $EncryptType = EncryptType::ENC_MD5;

    function __construct() {

        $this->PaymentType = 'aio';
        $this->Send = array(
            "ReturnURL"         => '',
            "ClientBackURL"     => '',
            "OrderResultURL"    => '',
            "MerchantTradeNo"   => '',
            "MerchantTradeDate" => '',
            "PaymentType"       => 'aio',
            "TotalAmount"       => '',
            "TradeDesc"         => '',
            "ChoosePayment"     => PaymentMethod::ALL,
            "Remark"            => '',
            "ChooseSubPayment"  => PaymentMethodItem::None,
            "NeedExtraPaidInfo" => ExtraPaymentInfo::No,
            "DeviceSource"      => '',
            "IgnorePayment"     => '',
            "PlatformID"        => '',
            "InvoiceMark"       => InvoiceState::No,
            "Items"             => array(),
            "UseRedeem"         => UseRedeem::No,
            "HoldTradeAMT"      => 0,
            "StoreID"           => '1171858'
        );

        $this->SendExtend = array();

        $this->Query = array(
            'MerchantTradeNo' => '',
            'TimeStamp' => ''
        );
        $this->Action = array(
            'MerchantTradeNo' => '',
            'TradeNo' => '',
            'Action' => ActionType::C,
            'TotalAmount' => 0
        );
        $this->ChargeBack = array(
            'MerchantTradeNo' => '',
            'TradeNo' => '',
            'ChargeBackTotalAmount' => 0,
            'Remark' => ''
        );
        $this->Capture = array(
            'MerchantTradeNo' => '',
            'CaptureAMT' => 0,
            'UserRefundAMT' => 0,
            'PlatformID' => ''
        );

        $this->TradeNo = array(
            'DateType' => '',
            'BeginDate' => '',
            'EndDate' => '',
            'MediaFormated' => ''
        );

        $this->Trade = array(
            'CreditRefundId' => '',
            'CreditAmount' => '',
            'CreditCheckCode' => ''
        );

        $this->Funding = array(
            "PayDateType" => '',
            "StartDate" => '',
            "EndDate" => ''
        );

    }

    //產生訂單
    function CheckOut($target = "_self") {
        $arParameters = array_merge( array('MerchantID' => $this->MerchantID, 'EncryptType' => $this->EncryptType) ,$this->Send);
        /*dd($target,$arParameters,$this->SendExtend,$this->HashKey,$this->HashIV,$this->ServiceURL);*/
        Send::CheckOut($target,$arParameters,$this->SendExtend,$this->HashKey,$this->HashIV,$this->ServiceURL);
    }

    //產生訂單html code
    function CheckOutString($paymentButton = null, $target = "_self") {
        $arParameters = array_merge( array('MerchantID' => $this->MerchantID, 'EncryptType' => $this->EncryptType) ,$this->Send);
        return Send::CheckOutString($paymentButton,$target = "_self",$arParameters,$this->SendExtend,$this->HashKey,$this->HashIV,$this->ServiceURL);
    }

    //取得付款結果通知的方法
    function CheckOutFeedback() {
        return $arFeedback = CheckOutFeedback::CheckOut(array_merge($_POST, array('EncryptType' => $this->EncryptType)),$this->HashKey,$this->HashIV,0);   
    }

    //訂單查詢作業
    function QueryTradeInfo() {
         //dd(array_merge($this->Query,array("MerchantID" => $this->MerchantID, 'EncryptType' => $this->EncryptType)) ,$this->HashKey ,$this->HashIV ,$this->ServiceURL);
        return $arFeedback = QueryTradeInfo::CheckOut(array_merge($this->Query,array("MerchantID" => $this->MerchantID, 'EncryptType' => $this->EncryptType)) ,$this->HashKey ,$this->HashIV ,$this->ServiceURL) ;
    }
    
    //信用卡定期定額訂單查詢的方法
    function QueryPeriodCreditCardTradeInfo() {
        return $arFeedback = QueryPeriodCreditCardTradeInfo::CheckOut(array_merge($this->Query,array("MerchantID" => $this->MerchantID, 'EncryptType' => $this->EncryptType)) ,$this->HashKey ,$this->HashIV ,$this->ServiceURL);
    }

    //信用卡關帳/退刷/取消/放棄的方法
    function DoAction() {
        return $arFeedback = DoAction::CheckOut(array_merge($this->Action,array("MerchantID" => $this->MerchantID, 'EncryptType' => $this->EncryptType)) ,$this->HashKey ,$this->HashIV ,$this->ServiceURL);
    }
    
    //廠商通知退款
    function AioChargeback() {
        return $arFeedback = AioChargeback::CheckOut(array_merge($this->ChargeBack,array("MerchantID" => $this->MerchantID, 'EncryptType' => $this->EncryptType)) ,$this->HashKey ,$this->HashIV ,$this->ServiceURL);
    }
    
    //會員申請撥款／退款
    function AioCapture(){
        return $arFeedback = AioCapture::Capture(array_merge($this->Capture,array("MerchantID" => $this->MerchantID, 'EncryptType' => $this->EncryptType)) ,$this->HashKey ,$this->HashIV ,$this->ServiceURL);
    }

    //下載會員對帳媒體檔
    function TradeNoAio($target = "_self"){
        $arParameters = array_merge( array('MerchantID' => $this->MerchantID, 'EncryptType' => $this->EncryptType) ,$this->TradeNo);
        TradeNoAio::CheckOut($target,$arParameters,$this->HashKey,$this->HashIV,$this->ServiceURL);
    }

    //查詢信用卡單筆明細紀錄
    function QueryTrade(){
        return $arFeedback = QueryTrade::CheckOut(array_merge($this->Trade,array("MerchantID" => $this->MerchantID, 'EncryptType' => $this->EncryptType)) ,$this->HashKey ,$this->HashIV ,$this->ServiceURL);
    }

    //下載信用卡撥款對帳資料檔
    function FundingReconDetail($target = "_self"){
        $arParameters = array_merge( array('MerchantID' => $this->MerchantID, 'EncryptType' => $this->EncryptType) ,$this->Funding);
        FundingReconDetail::CheckOut($target,$arParameters,$this->HashKey,$this->HashIV,$this->ServiceURL);
    }
}



abstract class EncryptType {
    // MD5(預設)
    const ENC_MD5 = 0;
    
    // SHA256
    const ENC_SHA256 = 1;
}


abstract class PaymentMethod {

    /**
     * 不指定付款方式。
     */
    const ALL = 'ALL';

    /**
     * 信用卡付費。
     */
    const Credit = 'Credit';

    /**
     * 網路 ATM。
     */
    const WebATM = 'WebATM';

    /**
     * 自動櫃員機。
     */
    const ATM = 'ATM';

    /**
     * 超商代碼。
     */
    const CVS = 'CVS';

    /**
     * 財付通。
     */
    const Tenpay = 'Tenpay';

    /**
     * 儲值消費。
     */
    const TopUpUsed = 'TopUpUsed';

}


abstract class PaymentMethodItem {

    /**
     * 不指定。
     */
    const None = '';
    // WebATM 類(001~100)
    /**
     * 台新銀行。
     */
    const WebATM_TAISHIN = 'TAISHIN';

    /**
     * 玉山銀行。
     */
    const WebATM_ESUN = 'ESUN';

    /**
     * 華南銀行。
     */
    const WebATM_HUANAN = 'HUANAN';

    /**
     * 台灣銀行。
     */
    const WebATM_BOT = 'BOT';

    /**
     * 台北富邦。
     */
    const WebATM_FUBON = 'FUBON';

    /**
     * 中國信託。
     */
    const WebATM_CHINATRUST = 'CHINATRUST';

    /**
     * 第一銀行。
     */
    const WebATM_FIRST = 'FIRST';

    /**
     * 國泰世華。
     */
    const WebATM_CATHAY = 'CATHAY';

    /**
     * 兆豐銀行。
     */
    const WebATM_MEGA = 'MEGA';

    /**
     * 元大銀行。
     */
    const WebATM_YUANTA = 'YUANTA';

    /**
     * 土地銀行。
     */
    const WebATM_LAND = 'LAND';
    // ATM 類(101~200)
    /**
     * 台新銀行。
     */
    const ATM_TAISHIN = 'TAISHIN';

    /**
     * 玉山銀行。
     */
    const ATM_ESUN = 'ESUN';

    /**
     * 華南銀行。
     */
    const ATM_HUANAN = 'HUANAN';

    /**
     * 台灣銀行。
     */
    const ATM_BOT = 'BOT';

    /**
     * 台北富邦。
     */
    const ATM_FUBON = 'FUBON';

    /**
     * 中國信託。
     */
    const ATM_CHINATRUST = 'CHINATRUST';
    
    /**
     * 土地銀行。
     */
    const ATM_LAND = 'LAND';
    
    /**
     * 國泰世華銀行。
     */
    const ATM_CATHAY = 'CATHAY';

    /**
     * 大眾銀行。
     */
    const ATM_Tachong = 'Tachong';

    /**
     * 永豐銀行。
     */
    const ATM_Sinopac = 'Sinopac';

    /**
     * 彰化銀行。
     */
    const ATM_CHB = 'CHB';

    /**
     * 第一銀行。
     */
    const ATM_FIRST = 'FIRST';
    
    // 超商類(201~300)
    /**
     * 超商代碼繳款。
     */
    const CVS = 'CVS';

    /**
     * OK超商代碼繳款。
     */
    const CVS_OK = 'OK';

    /**
     * 全家超商代碼繳款。
     */
    const CVS_FAMILY = 'FAMILY';

    /**
     * 萊爾富超商代碼繳款。
     */
    const CVS_HILIFE = 'HILIFE';

    /**
     * 7-11 ibon代碼繳款。
     */
    const CVS_IBON = 'IBON';
    // 其他第三方支付類(301~400)

    /**
     * 財付通。
     */
    const Tenpay = 'Tenpay';
    // 儲值/餘額消費類(401~500)
    /**
     * 儲值/餘額消費(歐付寶)
     */
    const TopUpUsed_AllPay = 'AllPay';

    /**
     * 儲值/餘額消費(玉山)
     */
    const TopUpUsed_ESUN = 'ESUN';
    // 其他類(901~999)

    /**
     * 信用卡(MasterCard/JCB/VISA)。
     */
    const Credit = 'Credit';

    /**
     * 貨到付款。
     */
    const COD = 'COD';

}


abstract class ExtraPaymentInfo {

    /**
     * 需要額外付款資訊。
     */
    const Yes = 'Y';

    /**
     * 不需要額外付款資訊。
     */
    const No = 'N';

}

abstract class InvoiceState {
    /**
     * 需要開立電子發票。
     */
    const Yes = 'Y';

    /**
     * 不需要開立電子發票。
     */
    const No = '';
}

abstract class UseRedeem{
    //使用紅利/購物金
    const Yes = 'Y' ;

    //不使用紅利/購物金
    const No = 'N' ;
}

abstract class ActionType {

    /**
     * 關帳
     */
    const C = 'C';

    /**
     * 退刷
     */
    const R = 'R';

    /**
     * 取消
     */
    const E = 'E';

    /**
     * 放棄
     */
    const N = 'N';

}

class Send extends Aio
{   
    //付款方式物件
    public static $PaymentObj ;

    protected static function process($arParameters = array(),$arExtend = array())
    {
        //宣告付款方式物件

        $test = new allPay_Credit();

        $PaymentMethod    = 'allPay_'.$arParameters['ChoosePayment'];
        self::$PaymentObj = new $test;
        
        //檢查參數
        $arParameters = self::$PaymentObj->check_string($arParameters);
        
        //檢查商品
        $arParameters = self::$PaymentObj->check_goods($arParameters);

        //檢查各付款方式的額外參數&電子發票參數
        $arExtend = self::$PaymentObj->check_extend_string($arExtend,$arParameters['InvoiceMark']);
        
        //過濾
        $arExtend = self::$PaymentObj->filter_string($arExtend,$arParameters['InvoiceMark']);

        //合併共同參數及延伸參數
        return array_merge($arParameters,$arExtend) ;
    }
    

    static function CheckOut($target = "_self",$arParameters = array(),$arExtend = array(),$HashKey='',$HashIV='',$ServiceURL=''){
        $arParameters = self::process($arParameters,$arExtend);

        //產生檢查碼
        $szCheckMacValue = CheckMacValue::generate($arParameters,$HashKey,$HashIV,$arParameters["EncryptType"]);
       
        //生成表單，自動送出
        $szHtml =  '<!DOCTYPE html>';
        $szHtml .= '<html>';
        $szHtml .=     '<head>';
        $szHtml .=         '<meta charset="utf-8">';
        $szHtml .=     '</head>';
        $szHtml .=     '<body>';
        $szHtml .=         "<form id=\"__allpayForm\" method=\"post\" target=\"{$target}\" action=\"{$ServiceURL}\">";
        
        foreach ($arParameters as $keys => $value) {
            $szHtml .=         "<input type=\"hidden\" name=\"{$keys}\" value=\"{$value}\" />";
        }

        $szHtml .=             "<input type=\"hidden\" name=\"CheckMacValue\" value=\"{$szCheckMacValue}\" />";
        $szHtml .=         '</form>';
        $szHtml .=         '<script type="text/javascript">document.getElementById("__allpayForm").submit();</script>';
        $szHtml .=     '</body>';
        $szHtml .= '</html>';

        echo $szHtml ;
        exit;
    }

    static function CheckOutString($paymentButton,$target = "_self",$arParameters = array(),$arExtend = array(),$HashKey='',$HashIV='',$ServiceURL=''){

        $arParameters = self::process($arParameters,$arExtend);
        
        //產生檢查碼
        $szCheckMacValue = CheckMacValue::generate($arParameters,$HashKey,$HashIV,$arParameters["EncryptType"]);
        
        $szHtml =  '<!DOCTYPE html>';
        $szHtml .= '<html>';
        $szHtml .=     '<head>';
        $szHtml .=         '<meta charset="utf-8">';
        $szHtml .=     '</head>';
        $szHtml .=     '<body>';
        $szHtml .=         "<form id=\"__allpayForm\" method=\"post\" target=\"{$target}\" action=\"{$ServiceURL}\">";

        foreach ($arParameters as $keys => $value) {
            $szHtml .=         "<input type=\"hidden\" name=\"{$keys}\" value=\"{$value}\" />";
        }

        $szHtml .=             "<input type=\"hidden\" name=\"CheckMacValue\" value=\"{$szCheckMacValue}\" />";
        $szHtml .=             "<input type=\"submit\" id=\"__paymentButton\" value=\"{$paymentButton}\" />";
        $szHtml .=         '</form>';
        $szHtml .=     '</body>';
        $szHtml .= '</html>';
        return  $szHtml ;
    }

}

class QueryTradeInfo extends Aio
{
    static function CheckOut($arParameters = array(),$HashKey ='',$HashIV ='',$ServiceURL = ''){
        $arErrors = array();
        $arParameters['TimeStamp'] = time();
        $arFeedback = array();
        $arConfirmArgs = array();

        $EncryptType = $arParameters["EncryptType"];
        unset($arParameters["EncryptType"]);

        // 呼叫查詢。
        if (sizeof($arErrors) == 0) {
            $arParameters["CheckMacValue"] = CheckMacValue::generate($arParameters,$HashKey,$HashIV,$EncryptType);

            // 送出查詢並取回結果。
            $szResult = parent::ServerPost($arParameters,$ServiceURL);
            $szResult = str_replace(' ', '%20', $szResult);
            $szResult = str_replace('+', '%2B', $szResult);

            // 轉結果為陣列。
            parse_str($szResult, $arResult);

            // 重新整理回傳參數。
            foreach ($arResult as $keys => $value) {
                if ($keys == 'CheckMacValue') {
                    $szCheckMacValue = $value;
                } else {
                    $arFeedback[$keys] = $value;
                    $arConfirmArgs[$keys] = $value;
                }
            }

            // 驗證檢查碼。
            if (sizeof($arFeedback) > 0) {
                $szConfirmMacValue = CheckMacValue::generate($arConfirmArgs,$HashKey,$HashIV,$EncryptType);
                if ($szCheckMacValue != $szConfirmMacValue) {
                    array_push($arErrors, 'CheckMacValue verify fail.');
                }
            }
        }

        if (sizeof($arErrors) > 0) {
            throw new Exception(join('- ', $arErrors));
        }

        return $arFeedback ;

    }    
}

abstract class Aio
{
    protected static function ServerPost($parameters ,$ServiceURL) {
        $ch = curl_init();
        
        if (FALSE === $ch) {
            throw new Exception('curl failed to initialize');
        }
        
        curl_setopt($ch, CURLOPT_URL, $ServiceURL);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parameters));
        $rs = curl_exec($ch);
        
        if (FALSE === $rs) {
            throw new Exception(curl_error($ch), curl_errno($ch));  
        }
        
        curl_close($ch);

        return $rs;
    }

}

class allPay_Credit extends Verification
{
    public $arPayMentExtend = array(
                                    "CreditInstallment" => '',
                                    "InstallmentAmount" => 0, 
                                    "Redeem"            => FALSE, 
                                    "PeriodAmount"      => '',
                                    "PeriodType"        => '',  
                                    "Frequency"         => '',  
                                    "ExecTimes"         => '',
                                    "PeriodReturnURL"   => ''
                                );

    //過濾多餘參數
    function filter_string($arExtend = array(),$InvoiceMark = ''){
        $arExtend = parent::filter_string($arExtend, $InvoiceMark);
        return $arExtend ;
    }

}


Abstract class Verification
{
    // 付款方式延伸參數
    public $arPayMentExtend = array();

    // 電子發票延伸參數。
    public $arInvoice = array(
            "RelateNumber",
            "CustomerIdentifier",
            "CarruerType" ,
            "CustomerID" ,
            "Donation" ,
            "Print" ,
            "TaxType",
            "CustomerName" ,
            "CustomerAddr" ,
            "CustomerPhone" ,
            "CustomerEmail" ,
            "ClearanceMark" ,
            "CarruerNum" ,
            "LoveCode" ,
            "InvoiceRemark" ,
            "DelayDay",
            "InvoiceItemName",
            "InvoiceItemCount",
            "InvoiceItemWord",
            "InvoiceItemPrice",
            "InvoiceItemTaxType",
            "InvType"
        );

    //檢查共同參數
    public function check_string($arParameters = array())
    {
        $arErrors = array();

        if (strlen($arParameters['MerchantID']) == 0) {
            array_push($arErrors, 'MerchantID is required.');
        }
        if (strlen($arParameters['MerchantID']) > 10) {
            array_push($arErrors, 'MerchantID max langth as 10.');
        }
        if (strlen($arParameters['ReturnURL']) == 0) {
            array_push($arErrors, 'ReturnURL is required.');
        }
        if (strlen($arParameters['ClientBackURL']) > 200) {
            array_push($arErrors, 'ClientBackURL max langth as 10.');
        }
        if (strlen($arParameters['OrderResultURL']) > 200) {
            array_push($arErrors, 'OrderResultURL max langth as 10.');
        }
        if (strlen($arParameters['MerchantTradeNo']) == 0) {
            array_push($arErrors, 'MerchantTradeNo is required.');
        }
        if (strlen($arParameters['MerchantTradeNo']) > 20) {
            array_push($arErrors, 'MerchantTradeNo max langth as 20.');
        }
        if (strlen($arParameters['MerchantTradeDate']) == 0) {
            array_push($arErrors, 'MerchantTradeDate is required.');
        }
        if (strlen($arParameters['TotalAmount']) == 0) {
            array_push($arErrors, 'TotalAmount is required.');
        }
        if (strlen($arParameters['TradeDesc']) == 0) {
            array_push($arErrors, 'TradeDesc is required.');
        }
        if (strlen($arParameters['TradeDesc']) > 200) {
            array_push($arErrors, 'TradeDesc max langth as 200.');
        }
        if (strlen($arParameters['ChoosePayment']) == 0) {
            array_push($arErrors, 'ChoosePayment is required.');
        }
        if (strlen($arParameters['NeedExtraPaidInfo']) == 0) {
            array_push($arErrors, 'NeedExtraPaidInfo is required.');
        }
        if (sizeof($arParameters['Items']) == 0) {
            array_push($arErrors, 'Items is required.');
        }

        // 檢查CheckMacValue加密方式
        if (strlen($arParameters['EncryptType']) > 1) {
            array_push($arErrors, 'EncryptType max langth as 1.');
        }

        if (sizeof($arErrors)>0) throw new Exception(join('<br>', $arErrors));

        if (!$arParameters['PlatformID']) {
            unset($arParameters['PlatformID']);
        }

        if ($arParameters['ChoosePayment']!=='ALL') {
            unset($arParameters['IgnorePayment']);
        }

        return $arParameters ;
    }

    //檢查商品
    public function check_goods($arParameters = array())
    {
        // 檢查產品名稱。
        $arErrors = array();
        $szItemName = '';

        if (sizeof($arParameters['Items']) > 0) {
            foreach ($arParameters['Items'] as $keys => $value) {
                $szItemName .= vsprintf('#%s %d %s x %u', $arParameters['Items'][$keys]);
                if (!array_key_exists('ItemURL', $arParameters)) {
                    $arParameters['ItemURL'] = $arParameters['Items'][$keys]['URL'];
                }
            }

            if (strlen($szItemName) > 0) {
                $szItemName = mb_substr($szItemName, 1, 200);
                $arParameters['ItemName'] = $szItemName ;
            }
        } else {
            array_push($arErrors, "Goods information not found.");
        }

        if (sizeof($arErrors)>0) throw new Exception(join('<br>', $arErrors));

        unset($arParameters['Items']);

        return $arParameters ;
    }

    //過濾多餘參數
    public function filter_string($arExtend = array(),$InvoiceMark = '')
    {
        $arPayMentExtend = array_merge(array_keys($this->arPayMentExtend), ($InvoiceMark == '') ? array() : $this->arInvoice);

        foreach ($arExtend as $key => $value) {
            if (!in_array($key,$arPayMentExtend )) {
                unset($arExtend[$key]);
            }
        }

        return $arExtend ;
    }

    //檢查預設參數
    public function check_extend_string($arExtend = array(),$InvoiceMark = '')
    {
        //沒設定參數的話，就給預設參數
        foreach ($this->arPayMentExtend as $key => $value) {
            if (!isset($arExtend[$key])) $arExtend[$key] = $value;
        }

        //若有開發票，檢查一下發票參數
        if ($InvoiceMark == 'Y') $arExtend = $this->check_invoiceString($arExtend);

        return $arExtend ;
    }

    //檢查電子發票參數
    public function check_invoiceString($arExtend = array()){
        $arErrors = array();

        // 廠商自訂編號RelateNumber(不可為空)
        if(!array_key_exists('RelateNumber', $arExtend)){
            array_push($arErrors, 'RelateNumber is required.');
        }else{
            if (strlen($arExtend['RelateNumber']) > 30) {
                array_push($arErrors, "RelateNumber max length as 30.");
            }
        }

        // 統一編號CustomerIdentifier(預設為空字串)
        if(!array_key_exists('CustomerIdentifier', $arExtend)){
            $arExtend['CustomerIdentifier'] = '';
        }else{
            //統編長度只能為8
            if(strlen($arExtend['CustomerIdentifier']) != 8){
                array_push($arErrors, "CustomerIdentifier length should be 8.");
            }
        }

        // 載具類別CarruerType(預設為None)
        if(!array_key_exists('CarruerType', $arExtend)){
            $arExtend['CarruerType'] = CarruerType::None ;
        }else{
            //有設定統一編號的話，載具類別不可為合作特店載具或自然人憑證載具。
            $notPrint = array(CarruerType::Member, CarruerType::Citizen);
            if(strlen($arExtend['CustomerIdentifier']) > 0 && in_array($arExtend['CarruerType'], $notPrint)){
                array_push($arErrors, "CarruerType should NOT be Member or Citizen.");
            }
        }

        // 客戶代號CustomerID(預設為空字串)
        if(!array_key_exists('CustomerID', $arExtend)) {
            $arExtend['CustomerID'] = '';
        }else{
            if($arExtend['CarruerType'] == CarruerType::Member && strlen($arExtend['CustomerID']) == 0){
                array_push($arErrors, "CustomerID is required.");
            }
        }
        // 捐贈註記 Donation(預設為No)
        if(!array_key_exists('Donation', $arExtend)){
            $arExtend['Donation'] = Donation::No ;
        }else{
            //若有帶統一編號，不可捐贈
            if(strlen($arExtend['CustomerIdentifier']) > 0 && $arExtend['Donation'] != Donation::No  ){
                array_push($arErrors, "Donation should be No.");
            }
        }

        // 列印註記Print(預設為No)
        if(!array_key_exists('Print', $arExtend)){
            $arExtend['Print'] = PrintMark::No ;
        }else{
            //捐贈註記為捐贈(Yes)時，請設定不列印(No)
            if($arExtend['Donation'] == Donation::Yes && $arExtend['Print'] != PrintMark::No){
                array_push($arErrors, "Print should be No.");
            }
            // 統一編號不為空字串時，請設定列印(Yes)
            if(strlen($arExtend['CustomerIdentifier']) > 0 && $arExtend['Print'] != PrintMark::Yes){
                array_push($arErrors, "Print should be Yes.");
            }
        }
        // 客戶名稱CustomerName(UrlEncode, 預設為空字串)
        if(!array_key_exists('CustomerName', $arExtend)){
            $arExtend['CustomerName'] = '';
        }else{
            if (mb_strlen($arExtend['CustomerName'], 'UTF-8') > 20) {
                  array_push($arErrors, "CustomerName max length as 20.");
            }
            // 列印註記為列印(Yes)時，此參數不可為空字串
            if($arExtend['Print'] == PrintMark::Yes && strlen($arExtend['CustomerName']) == 0){
                array_push($arErrors, "CustomerName is required.");
            }
        }

        // 客戶地址CustomerAddr(UrlEncode, 預設為空字串)
        if(!array_key_exists('CustomerAddr', $arExtend)){
            $arExtend['CustomerAddr'] = '';
        }else{
            if (mb_strlen($arExtend['CustomerAddr'], 'UTF-8') > 200) {
                  array_push($arErrors, "CustomerAddr max length as 200.");
            }
            // 列印註記為列印(Yes)時，此參數不可為空字串
            if($arExtend['Print'] == PrintMark::Yes && strlen($arExtend['CustomerAddr']) == 0){
                array_push($arErrors, "CustomerAddr is required.");
            }
        }
        // 客戶電話CustomerPhone
        if(!array_key_exists('CustomerPhone', $arExtend)){
            $arExtend['CustomerPhone'] = '';
        }else{
            if (strlen($arExtend['CustomerPhone']) > 20) array_push($arErrors, "CustomerPhone max length as 20.");
        }

        // 客戶信箱CustomerEmail 
        if(!array_key_exists('CustomerEmail', $arExtend)){
            $arExtend['CustomerEmail'] = '';
        }else{
            if (strlen($arExtend['CustomerEmail']) > 200) array_push($arErrors, "CustomerEmail max length as 200.");
        }

        //(CustomerEmail與CustomerPhone擇一不可為空)
        if (strlen($arExtend['CustomerPhone']) == 0 and strlen($arExtend['CustomerEmail']) == 0) array_push($arErrors, "CustomerPhone or CustomerEmail is required.");

        //課稅類別 TaxType(不可為空)
        if (strlen($arExtend['TaxType']) == 0) array_push($arErrors, "TaxType is required.");
        
        //通關方式 ClearanceMark(預設為空字串)
        if(!array_key_exists('ClearanceMark', $arExtend)) {
            $arExtend['ClearanceMark'] = '';
        }else{
            //課稅類別為零稅率(Zero)時，ClearanceMark不可為空字串
            if($arExtend['TaxType'] == TaxType::Zero && ($arExtend['ClearanceMark'] != ClearanceMark::Yes || $arExtend['ClearanceMark'] != ClearanceMark::No)) {
                array_push($arErrors, "ClearanceMark is required.");
            }
            if (strlen($arExtend['ClearanceMark']) > 0 && $arExtend['TaxType'] != TaxType::Zero) {
                array_push($arErrors, "Please remove ClearanceMark.");
            }
        }
        
        // CarruerNum(預設為空字串)
        if (!array_key_exists('CarruerNum', $arExtend)) {
            $arExtend['CarruerNum'] = '';
        } else {
            switch ($arExtend['CarruerType']) {
                // 載具類別為無載具(None)或會員載具(Member)時，系統自動忽略載具編號
                case CarruerType::None:
                case CarruerType::Member:
                break;
                // 載具類別為買受人自然人憑證(Citizen)時，請設定自然人憑證號碼，前2碼為大小寫英文，後14碼為數字
                case CarruerType::Citizen:
                    if (!preg_match('/^[a-zA-Z]{2}\d{14}$/', $arExtend['CarruerNum'])){
                        array_push($arErrors, "Invalid CarruerNum.");
                    }
                break;
                // 載具類別為買受人手機條碼(Cellphone)時，請設定手機條碼，第1碼為「/」，後7碼為大小寫英文、數字、「+」、「-」或「.」
                case CarruerType::Cellphone:
                    if (!preg_match('/^\/{1}[0-9a-zA-Z+-.]{7}$/', $arExtend['CarruerNum'])) {
                        array_push($arErrors, "Invalid CarruerNum.");
                    }
                break;

                default:
                    array_push($arErrors, "Please remove CarruerNum.");
            }
        }

        // 愛心碼 LoveCode(預設為空字串)
        if(!array_key_exists('LoveCode', $arExtend)) $arExtend['LoveCode'] = '';
        // 捐贈註記為捐贈(Yes)時，參數長度固定3~7碼，請設定全數字或第1碼大小寫「X」，後2~6碼全數字
        if ($arExtend['Donation'] == Donation::Yes) {
            if (!preg_match('/^([xX]{1}[0-9]{2,6}|[0-9]{3,7})$/', $arExtend['LoveCode'])) {
                array_push($arErrors, "Invalid LoveCode.");
            }
        }

        //備註 InvoiceRemark(UrlEncode, 預設為空字串)
        if(!array_key_exists('InvoiceRemark', $arExtend)) $arExtend['InvoiceRemark'] = '';      
        
        // 延遲天數 DelayDay(不可為空, 預設為0) 延遲天數，範圍0~15，設定為0時，付款完成後立即開立發票
        if(!array_key_exists('DelayDay', $arExtend)) $arExtend['DelayDay'] = 0 ;
        if ($arExtend['DelayDay'] < 0 or $arExtend['DelayDay'] > 15) array_push($arErrors, "DelayDay should be 0 ~ 15.");
        
              
        // 字軌類別 InvType(不可為空)
        if (!array_key_exists('InvType', $arExtend)) array_push($arErrors, "InvType is required.");

        //商品相關整理
        if(!array_key_exists('InvoiceItems', $arExtend)){
            array_push($arErrors, "Invoice Goods information not found.");
        }else{
            $InvSptr = '|';
            $tmpItemName = array();
            $tmpItemCount = array();
            $tmpItemWord = array();
            $tmpItemPrice = array();
            $tmpItemTaxType = array();
            foreach ($arExtend['InvoiceItems'] as $tmpItemInfo) {
                if (mb_strlen($tmpItemInfo['Name'], 'UTF-8') > 0) {
                    array_push($tmpItemName, $tmpItemInfo['Name']);
                }
                if (strlen($tmpItemInfo['Count']) > 0) {
                    array_push($tmpItemCount, $tmpItemInfo['Count']);
                }
                if (mb_strlen($tmpItemInfo['Word'], 'UTF-8') > 0) {
                    array_push($tmpItemWord, $tmpItemInfo['Word']);
                }
                if (strlen($tmpItemInfo['Price']) > 0) {
                    array_push($tmpItemPrice, $tmpItemInfo['Price']);
                }
                if (strlen($tmpItemInfo['TaxType']) > 0) {
                    array_push($tmpItemTaxType, $tmpItemInfo['TaxType']);
                }
            }
                  
            if ($arExtend['TaxType'] == TaxType::Mix) {
                if (in_array(TaxType::Dutiable, $tmpItemTaxType) and in_array(TaxType::Free, $tmpItemTaxType)) {
                    // Do nothing
                }  else {
                    $tmpItemTaxType = array();
                }
            }
            if ((count($tmpItemName) + count($tmpItemCount) + count($tmpItemWord) + count($tmpItemPrice) + count($tmpItemTaxType)) == (count($tmpItemName) * 5)) {
                $arExtend['InvoiceItemName']    = implode($InvSptr, $tmpItemName);
                $arExtend['InvoiceItemCount']   = implode($InvSptr, $tmpItemCount);
                $arExtend['InvoiceItemWord']    = implode($InvSptr, $tmpItemWord);
                $arExtend['InvoiceItemPrice']   = implode($InvSptr, $tmpItemPrice);
                $arExtend['InvoiceItemTaxType'] = implode($InvSptr, $tmpItemTaxType);
            }

            unset($arExtend['InvoiceItems']); 
        }

        
        $encode_fields = array(
                'CustomerName',
                'CustomerAddr',
                'CustomerEmail',
                'InvoiceItemName',
                'InvoiceItemWord',
                'InvoiceRemark'
            );
        foreach ($encode_fields as $tmp_field) {
            $arExtend[$tmp_field] = urlencode($arExtend[$tmp_field]);
        }

        if (sizeof($arErrors) > 0) {
            throw new Exception(join('<br>', $arErrors));
        }

        return $arExtend ;
    }

}


class CheckMacValue{

    static function generate($arParameters = array(),$HashKey = '' ,$HashIV = '',$encType = 0){
        $sMacValue = '' ;
        
        if(isset($arParameters))
        {   

            unset($arParameters['CheckMacValue']);

            // 77777
            $testCheckMacValue=new CheckMacValue();
            uksort($arParameters, array($testCheckMacValue,'merchantSort'));
            
            // 組合字串
            $sMacValue = 'HashKey=' . $HashKey ;
            foreach($arParameters as $key => $value)
            {
                $sMacValue .= '&' . $key . '=' . $value ;
            }
            
            $sMacValue .= '&HashIV=' . $HashIV ;    
            
            // URL Encode編碼     
            $sMacValue = urlencode($sMacValue); 
            
            // 轉成小寫
            $sMacValue = strtolower($sMacValue);        
            
            // 取代為與 dotNet 相符的字元
            $sMacValue = str_replace('%2d', '-', $sMacValue);
            $sMacValue = str_replace('%5f', '_', $sMacValue);
            $sMacValue = str_replace('%2e', '.', $sMacValue);
            $sMacValue = str_replace('%21', '!', $sMacValue);
            $sMacValue = str_replace('%2a', '*', $sMacValue);
            $sMacValue = str_replace('%28', '(', $sMacValue);
            $sMacValue = str_replace('%29', ')', $sMacValue);
                                
            // 編碼
            switch ($encType) {
                case EncryptType::ENC_SHA256:
                    // SHA256 編碼
                    $sMacValue = hash('sha256', $sMacValue);
                break;
                
                case EncryptType::ENC_MD5:
                default:
                // MD5 編碼
                    $sMacValue = md5($sMacValue);
            }

                $sMacValue = strtoupper($sMacValue);
        }  
        
        return $sMacValue ;
    }
    /**
    * 自訂排序使用
    */
    private static function merchantSort($a,$b)
    {
        return strcasecmp($a, $b);
    }

}
/*7777777777777--*/





class PayController extends BackendController {

	
	/*首頁========================================================*/
	public function index($locale,$type="",$id="")
	{
       
        /**
        *    Credit信用卡付款產生訂單範例，參數說明請參考SDK技術文件(https://www.allpay.com.tw/Content/files/allpay_047.pdf)
        */
        //載入SDK(路徑可依系統規劃自行調整)

        try {
            
            $obj = new AllInOne();

            //服務參數
            $obj->ServiceURL  = "https://payment-stage.allpay.com.tw/Cashier/AioCheckOut/V2";   //服務位置
            $obj->HashKey     = '5294y06JbISpM5x9' ;                                            //測試用Hashkey，請自行帶入AllPay提供的HashKey
            $obj->HashIV      = 'v77hoKGq4kWxNNIS' ;                                            //測試用HashIV，請自行帶入AllPay提供的HashIV
            $obj->MerchantID  = '2000132';                                                      //測試用MerchantID，請自行帶入AllPay提供的MerchantID


            //基本參數(請依系統規劃自行調整)
                                       //交易描述
            $obj->Send['ChoosePayment']     = PaymentMethod::Credit ;                     //付款方式:Credit

            //訂單的商品資料


            $Product=Product::get();
            $ProductSeparate=ProductSeparate::get();
            $ProductSeparateArr=[];
            $ProductArr=[];
            $car_data=[];
            $sessiondata = [];
            $total = 0;
            foreach ($ProductSeparate as $key => $value) {
                $ProductSeparateArr[$value->id] = $value;
            }

            foreach ($Product as $key => $value) {
                $ProductArr[$value->id]=$value;
            }
            
            if(!empty($id))//訂單已經在了 沒付錢 再付一次前
            {
                $OrderListData=OrderList::where('id',$id)->first();
                $obj->Send['TradeDesc']         = "good to drink" ;     
                $obj->Send['MerchantTradeNo']   = "Papillon".time();

                //多帶一個ID回傳用
                $obj->Send['ReturnURL']         =  ItemMaker::url("feedback/".$OrderListData->id);
                
                $obj->Send['ClientBackURL']   = ItemMaker::url("session/ff");
                $obj->Send['MerchantTradeDate'] = Date("Y/m/d h:m:s");
                $obj->Send['TotalAmount'] =$OrderListData->TotalAmount;


                
            
                //dd(json_decode($OrderListData->buy_stuff_json),Session::get('product_data'));

                $forproductdata=[];

                foreach(json_decode($OrderListData->buy_stuff_json) as $value)
                {
                    
                    $temparr=[];
                    if(!empty($value->sep_id))
                    {
                        $temparr['separate_id']= $value->sep_id;
                    }
                    $temparr['productid']= $value->pro_id;            
                    $temparr['count']= $value->count;
                    $temparr['size']= $value->size;
                    array_push($sessiondata, $temparr);
                }

               


                foreach($sessiondata as $key => $value)
                    {

                        $car_data[$key]['price']=0;
                        $productid=$value['productid'];
                        if(!empty($value['separate_id']))
                        {
                            $separate_id=$value['separate_id'];
                        }
                        else
                        {
                            $separate_id="-";
                        }
                        $count=$value['count'];
                        $size=$value['size'];
                        // if separate_id

                        if($separate_id !="-")
                        {
                            $car_data[$key]['separate']=$ProductSeparateArr[$separate_id];
                            if(!empty($ProductSeparateArr[$separate_id]['price_discount']))
                            {
                                $car_data[$key]['price']=$ProductSeparateArr[$separate_id]->price_discount;
                            }
                            else {
                                $car_data[$key]['price']=$ProductSeparateArr[$separate_id]->price_original;
                            }
                            //串完整產品ID
                            $car_data[$key]['complete_num']=$ProductArr[$productid]['all_num'].$ProductSeparateArr[$separate_id]->code.$count;

                             $car_data[$key]['sep_title']=$ProductSeparateArr[$separate_id]->title;
                            
                        }
                        // if no separate_id
                        else
                        {
                            if(!empty($ProductArr[$productid]->price_discount))
                            {
                                $car_data[$key]['price']=$ProductArr[$productid]->price_discount;
                            }
                            else {
                                $car_data[$key]['price']=$ProductArr[$productid]->price_original;
                            }
                            //串完整產品ID
                            $car_data[$key]['complete_num']=$ProductArr[$productid]['all_num'].$count;
                            $car_data[$key]['sep_title']="";
                            $car_data[$key]['separate']['id']="";
                        }

                        //串產品圖片
                        $car_data[$key]['out_img']= !empty(ProductPhoto::where('product_id',$productid)->where('is_out_img',1)->first()->image) ? ProductPhoto::where('product_id',$productid)->where('is_out_img',1)->first()->image : !empty(ProductPhoto::where('product_id',$productid)->first()->image) ? ProductPhoto::where('product_id',$productid)->first()->image : "";
                            
                            
                        $car_data[$key]['product']=$ProductArr[$productid];
                        $car_data[$key]['count']=$count;
                        $car_data[$key]['size']=$size;

                        
                        $total+=($car_data[$key]['price']/1)*($count/1);
                        $_thistotal=($car_data[$key]['price']/1)*($count/1);
                        //組給歐富寶看的產品arr
                        array_push($obj->Send['Items'],
                        array('Name' => $car_data[$key]['product']["title"],
                          'Price' => (int)$car_data[$key]['price'],
                          'Currency' => "元", 'Quantity' => (int) $car_data[$key]['count'], 
                           'URL' => ItemMaker::url('product/'.$car_data[$key]['product']['id'])));

                        // array('Name' => 'tt',
                        //   'Price' => (int)100,
                        //   'Currency' => "元", 'Quantity' => (int) 100, 
                        //    'URL' => 'www.test.ccom'));

                    }
                
                $obj->CheckOut();

                
                
                
            }
            else//訂單產生就送歐富寶
            {

               
            $sessiondata=Session::get('product_data');             

            if(!empty($sessiondata) )
                {
                    foreach($sessiondata as $key => $value)
                    {

                        $car_data[$key]['price']=0;
                        $productid=$value['productid'];
                        $separate_id=$value['separate_id'];
                        $count=$value['count'];
                        $size=$value['size'];
                        // if separate_id

                        if($separate_id !="-")
                        {
                            $car_data[$key]['separate']=$ProductSeparateArr[$separate_id];
                            if(!empty($ProductSeparateArr[$separate_id]['price_discount']))
                            {
                                $car_data[$key]['price']=$ProductSeparateArr[$separate_id]->price_discount;
                            }
                            else {
                                $car_data[$key]['price']=$ProductSeparateArr[$separate_id]->price_original;
                            }
                            //串完整產品ID
                            $car_data[$key]['complete_num']=$ProductArr[$productid]['all_num'].$ProductSeparateArr[$separate_id]->code.$count;

                             $car_data[$key]['sep_title']=$ProductSeparateArr[$separate_id]->title;
                            
                        }
                        // if no separate_id
                        else
                        {
                            if(!empty($ProductArr[$productid]->price_discount))
                            {
                                $car_data[$key]['price']=$ProductArr[$productid]->price_discount;
                            }
                            else {
                                $car_data[$key]['price']=$ProductArr[$productid]->price_original;
                            }
                            //串完整產品ID
                            $car_data[$key]['complete_num']=$ProductArr[$productid]['all_num'].$count;
                            $car_data[$key]['sep_title']="";
                            $car_data[$key]['separate']['id']="";
                        }

                        //串產品圖片
                        $car_data[$key]['out_img']= !empty(ProductPhoto::where('product_id',$productid)->where('is_out_img',1)->first()->image) ? ProductPhoto::where('product_id',$productid)->where('is_out_img',1)->first()->image : !empty(ProductPhoto::where('product_id',$productid)->first()->image) ? ProductPhoto::where('product_id',$productid)->first()->image : "";
                            
                            
                        $car_data[$key]['product']=$ProductArr[$productid];
                        $car_data[$key]['count']=$count;
                        $car_data[$key]['size']=$size;

                        
                        $total+=($car_data[$key]['price']/1)*($count/1);
                        $_thistotal=($car_data[$key]['price']/1)*($count/1);
                        //組給歐富寶看的產品arr
                        array_push($obj->Send['Items'],
                        array('Name' => $car_data[$key]['product']["title"],
                          'Price' => (int)$car_data[$key]['price'],
                          'Currency' => "元", 'Quantity' => (int) $car_data[$key]['count'], 
                           'URL' => ItemMaker::url('product/'.$car_data[$key]['product']['id'])));

                        // array('Name' => 'tt',
                        //   'Price' => (int)100,
                        //   'Currency' => "元", 'Quantity' => (int) 100, 
                        //    'URL' => 'www.test.ccom'));

                    }
                    
                }

            $jsonarr=[];
            //串產品資料 會存到訂單資料裡面
            foreach ($car_data as $key => $value) 
                {     

                    $jsonarr[$key]['title']=$value['product']['title'];
                    $jsonarr[$key]['pro_id']=$value['product']['id'];
                    $jsonarr[$key]['sep_id']=$value['separate']['id'];
                    $jsonarr[$key]['en_title']=$value['product']['en_title'];
                    $jsonarr[$key]['complete_num']=$value['complete_num'];
                    $jsonarr[$key]['sep_title']=$value['sep_title'];
                    $jsonarr[$key]['size']=$value['size'];
                    $jsonarr[$key]['count']=$value['count'];
                    $jsonarr[$key]['price']=$value['price'];
                    $jsonarr[$key]['img']=$value['out_img'];
                }
            
            $member_id=Session::get("mb_id").Session::get("mbid_no");
            

            //串訂單標號 每天要從 01開始算
            do{
                $OrderListCount=count(OrderList::where("num_for_user",'like',"%".Date("Ymd")."%")->get());

                $num_for_user=Date("Ymd")."0000";
                
                $num_for_user=$num_for_user+$OrderListCount/1+1;            
            
            }while(count(OrderList::where("num_for_user",$num_for_user)->get())!=0);
            
            $jsonarr=json_encode($jsonarr);

            $orderlist=New OrderList;

            $orderlist->num_for_user=$num_for_user."";
            $orderlist->price=$total;
            $orderlist->buy_stuff_json=$jsonarr;
            $orderlist->member_id=$member_id;
            
            $TotalAmount=DealSession::to_discount(DealSession::gettotal())['total'];
           
            $orderlist->TotalAmount=$TotalAmount;
            
            $obj->Send['MerchantTradeDate'] = Date("Y/m/d h:m:s");                        //交易時間
            $obj->Send['TotalAmount']       = $TotalAmount;

            $obj->Send['ClientBackURL']   = ItemMaker::url("session/ff");


            //dd(DealSession::to_discount(DealSession::gettotal()));
            
            //Credit信用卡分期付款延伸參數(可依系統需求選擇是否代入)
            //以下參數不可以跟信用卡定期定額參數一起設定
            $obj->SendExtend['CreditInstallment'] = '' ;    //分期期數，預設0(不分期)，信用卡分期可用參數為:3,6,12,18,24
            $obj->SendExtend['InstallmentAmount'] = 0 ;    //使用刷卡分期的付款金額，預設0(不分期)
            $obj->SendExtend['Redeem'] = false ;           //是否使用紅利折抵，預設false
            $obj->SendExtend['UnionPay'] = false;          //是否為聯營卡，預設false;


            $orderlist->MerchantTradeNo="Papillon".time();            
            $obj->Send['MerchantTradeNo']   = $orderlist->MerchantTradeNo;

            

            $orderlist->order_list_type=1;

            $lastid=OrderList::OrderBy("id","desc")->select('id')->first();

            //付款完成通知回傳的網址
            $obj->Send['ReturnURL']         = "http://great-papillon.xwadex.com/zh-tw/feedback/" ;    
            //交易金額
            $obj->Send['TradeDesc']         = "good to drink" ;
            //發票地址
            $orderlist->ticket=json_encode(Session::get("ticket"));
            //收件地址
            $orderlist->ticket_receiver=json_encode(Session::get("ticket_receiver"));
            $orderlist->shop_receiver=json_encode(Session::get("shop_receiver"));
            $orderlist->name=str_replace('"',"", json_encode(Session::get("name")));

            
            $orderlist->is_count="no";

            $DisCountSet=DisCountSet::first();

            $Setting=Setting::first();


            $DisCountPercentRecord=DisCountPercent::where('is_visible',1)
                                  ->where('st_date',"<=",Date("Y-m-d"))
                                  ->where('ed_date',">=",Date("Y-m-d"))
                                  ->OrderBy('id','desc')
                                  ->first();

            //全館折扣紀錄 會存到訂單資料裡面
            if(count($DisCountPercentRecord) >0)
            {
                $DisCountPercentRecord=json_encode($DisCountPercentRecord);
                $orderlist->DisCountPercentRecord=$DisCountPercentRecord;
            }

            $DisCountFullRecord=DisCountFull::where('is_visible',1)
                                  ->where('st_date',"<=",Date("Y-m-d"))
                                  ->where('ed_date',">=",Date("Y-m-d"))
                                  ->OrderBy('id','desc')
                                  ->first();
            //滿額折扣紀錄 會存到訂單資料裡面
            if(count($DisCountFullRecord) >0)
            {
                $DisCountFullRecord=json_encode($DisCountFullRecord);
                $orderlist->DisCountFullRecord=$DisCountFullRecord;
            }


            $DisCountCodeRecord=DisCountCode::where('is_visible',1)
                                  ->where('code',Session::get('code'))
                                  ->OrderBy('id','desc')
                                  ->first();

            //使用紅利紀錄 會存到訂單資料裡面
            if(count($DisCountCodeRecord) >0)
            {
                $DisCountCodeRecord->title=$DisCountSet->code_title;
                $DisCountCodeRecord->content=$DisCountSet->code_content;
                $DisCountCodeRecord=json_encode($DisCountCodeRecord);
                $orderlist->DisCountCodeRecord=$DisCountCodeRecord;
            }

           


            //最後再算一次用的紅利有沒有超過
            DealSession::updateDisCountBonus($locale,Session::get("bonus"));
          

            //使用紅利紀錄 會存到訂單資料裡面
            if(Session::get("bonus") > 0)
            {
                $DisCountBonusRecord=[];
                $DisCountBonusRecord['title']=$DisCountSet->bonus_title;
                $DisCountBonusRecord['content']=$DisCountSet->bonus_content;
                $DisCountBonusRecord=json_encode($DisCountBonusRecord);
                $orderlist->DisCountBonusRecord=$DisCountBonusRecord;


                $bonus=New Bonus;
                $bonus->mb_id=Session::get("mb_id");
                $bonus->bonus=Session::get("bonus")*-1;
                $bonus->is_add=0;
                $bonus->sel_type=3;
                $bonus->order_list_id=$lastid->id+1;
                $bonus->detail=$orderlist->MerchantTradeNo;
                $bonus->save();
                

            }

            //好友回饋點數紀錄 會存到訂單資料裡面
            if(!empty(Session::get("friendfeedback") ))
            {
                
                $friendfeedback=[];
                $friendfeedback['friend_id']=Session::get('friendfeedback');
                $friendfeedback['title']=$DisCountSet->friend_title;
                $friendfeedback['content']=$DisCountSet->friend_content;
                $friendfeedback['percent']=$Setting->friend_feedback;
                $friendfeedback['friendBP']=ceil($Setting->friend_feedback/100 * $TotalAmount);
                $friendfeedback=json_encode($friendfeedback);
                $orderlist->friendfeedback=$friendfeedback;
            }

            //串紅利回饋紀錄 會存到訂單資料裡面
            $feedback=[];
            $feedback['title']=$DisCountSet->bonus_title;
            $feedback['content']=$DisCountSet->bonus_content;
            $feedback['percent']=$Setting->feedback;
            $feedback['feedbackBP']=ceil($Setting->feedback/100 * $TotalAmount);
            $feedback=json_encode($feedback);
            $orderlist->feedback=$feedback;

            //串運費的資料 會存到訂單資料裡面

            $ShippingCost=[];
            $ShippingCost['title']=$Setting->shipping_cost_title;
            $ShippingCost['content']=$Setting->shipping_cost_content;
            if(!empty(Session::get("DisCountData")['ShippingCost']))
            {
                    $ShippingCost['price']=Session::get("DisCountData")['ShippingCost'];
            }
            
            $ShippingCost=json_encode($ShippingCost);
            $orderlist->ShippingRecord=$ShippingCost;




            
            //訂單連結會員
            $orderlist->member_sql_id=Session::get("mb_id");
            
            //把折扣的金額存到訂單裡面
            $orderlist->discount=json_encode(Session::get("DisCountData"));
            $sms = new SMSHttpSend();
            $subject = $Setting->phone_text_send_order_title;  //簡訊主旨
            $subject = str_replace("@money", $total, $subject);
            $subject = str_replace("@time", Date("Y-m-d h:i:s"), $subject);
            $subject = str_replace("@name", Session::get('name'), $subject);


            ///串發簡訊
            $content = $Setting->phone_text_send_order;  
            $content = str_replace("@money", $total, $content);
            $content = str_replace("@time", Date("Y-m-d h:i:s"), $content);
            $content = str_replace("@name", Session::get('name'), $content);
            $content = str_replace("@list_num", $orderlist->num_for_user, $content);
            

            
            $mobile=Session::get('mobile');
            $sendTime="20180520000000";
          
            


            if($type == 1)
            {
                $orderlist->pay_way="信用卡";
                $orderlist->save();
                $sms->sendSMS("papillon","weddingring1985",$subject,$content,$mobile,$sendTime,1);

                $obj->CheckOut();
            }

            if($type == 2)
            {
                $orderlist->pay_way="貨到付款";
                $orderlist->save();
                $sms->sendSMS("papillon","weddingring1985",$subject,$content,$mobile,$sendTime);
                //$obj->CheckOut();
            }
            
            
               

            
        }
            

        } catch (Exception $e) {
            echo $e->getMessage();
        } 

        return redirect($locale.'/member/guide');

		
	}


    public function looklook($locale)
    {
        try
        {
         $oPayment = new AllInOne();
         /* 服務參數 */
         $oPayment->ServiceURL = "https://payment-stage.allpay.com.tw/Cashier/QueryTradeInfo/V2";
         $oPayment->HashKey = "5294y06JbISpM5x9";
         $oPayment->HashIV = "v77hoKGq4kWxNNIS";
         $oPayment->MerchantID = "2000132";
         /* 基本參數 */
         $oPayment->Query['MerchantTradeNo'] = "Test1511417862";
         $oPayment->Query['TimeStamp'] = "1511417962";
         $oPayment->Query['EncryptType'] =1;

         
         /* 查詢訂單 */
         $arQueryFeedback = $oPayment ->QueryTradeInfo();
         // 取回所有資料
         dd($arQueryFeedback);
        }
        catch (Exception $e)
        {
            dd($e);
        }
    }


    public function feedback($locale,$id="")
    {  
       

        $User=User::where('id',3)->first();
        $str="start\r\n id=".$id;
        $arr=$_POST;
        foreach($arr as $key => $value)
        {
                $str.=' key: '.$key."val:".$arr[$key];
        }
  
        //$arr['MerchantTradeNo']='Papillon1516179407';
        //把歐富寶回傳的資料寫資料庫
        if(!empty($id))
        {
            $OrderList=OrderList::where('id',$id)->first();
            
            
        }
        elseif(!empty($arr['MerchantTradeNo']))
        {
            //$str.=">".$arr['MerchantTradeNo']."<";
            
            $OrderList=OrderList::where('MerchantTradeNo',$arr['MerchantTradeNo'])->first();
           
        }
        else
        {
            $str.='empty';
            
        }
        foreach($arr as $key => $value)
        {
            $OrderList->$key=$value;                
        }
            $OrderList->order_list_type=2;
            $OrderList->save();


        $Setting=Setting::first();
        
        ProductController::NewBonusData($locale);
        $sms = new SMSHttpSend();
        $subject = $Setting->phone_text_send_after_pay_title;  //簡訊主旨
        $subject = str_replace("@money", $OrderList->TotalAmount, $subject);
        $subject = str_replace("@time", Date("Y-m-d h:i:s"), $subject);
        $subject = str_replace("@name", $OrderList->name, $subject);


        $content = $Setting->phone_text_send_after_pay;  //簡訊主旨
        $content = str_replace("@money", $OrderList->TotalAmount, $content);
        $content = str_replace("@time", Date("Y-m-d h:i:s"), $content);
        $content = str_replace("@name", $OrderList->name, $content);
        $content = str_replace("@list_num", $OrderList->num_for_user, $content);
        $content = str_replace("@transaction_num", $OrderList->MerchantTradeNo, $content);
        $Member=Member::where("id",$OrderList->member_sql_id)->first();
        $mobile=$Member->mobile;
        $sendTime="20180520000000";
        //dd($subject,$content,$mobile,$sendTime);

        
        $sms->sendSMS("papillon","weddingring1985",$subject,$content,$mobile,$sendTime,1);


        $str.="   ".$subject;
        $str.="   ".$content;
        $str.="   ".$mobile;
        $str.="   ".$sendTime;
        $User->name=$str;        
        $User->save();

    }

  
	



}
	