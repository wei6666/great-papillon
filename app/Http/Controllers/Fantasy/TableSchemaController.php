<?php 
namespace App\Http\Controllers\Fantasy;

/*laravle schema use*/
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Exception;
class TableSchemaController {
	protected $CreatedTable ;
	protected $tableName ;


	public function __construct(array $setTable,$tableName) {
		$this->CreatedTable = $setTable;
		$this->tableName = $tableName;
	}


	public function buildeTable() { 
		if($this->CreatedTable) {
			/*檢查table是否已經建立*/
			if(!$this->checktable($this->tableName)) {
				Schema::create($this->tableName, function (Blueprint $table) {
					$table->increments('id');
					$table->integer('is_visible')->default(1);
					$table->integer('rank')->default(0);
					$table->dateTime('created_at');
					$table->dateTime('updated_at');
					foreach ($this->CreatedTable as $field => $type) {
						/*is_visible 預設為1*/
							$table->$type($field);
					}
		    	});
		    	return true;
			}
			throw new Exception("The Table already build");
		}

		throw new Exception("The input data is empty somewhere");
	
	}

	/*刪除table*/
	public function removeTable(array $dropTable) {
		if($dropTable) 
			throw new Exception("The input data is empty");

		foreach ($dropTable as $tablearray) {
			/*檢查table是否存在*/
			try {
				if($this->checktable($this->tableName)) {
					Schema::drop($this->tableName);
				}
			} catch(Exception $e) {
				echo $this->tableName;
				throw new Exception("<-Table名稱不存在");
			}
		}
			
	}

	/*檢查table*/
	public function checktable($tableName){
		return Schema::hasTable($tableName);
	}

	/*抓欄位名稱*/
	public function getColumn($tableName){
		return Schema::getColumnListing($tableName);
	}
}

?>