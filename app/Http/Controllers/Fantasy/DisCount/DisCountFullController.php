<?php
namespace App\Http\Controllers\Fantasy\DisCount;

use App\Http\Controllers\CRUDBaseController;
use Redis;
use Cache;
/**相關Models**/

use App\Http\Models\Banner;


class DisCountFullController extends CRUDBaseController
{

    public $ajaxEditLink = 'Fantasy/DisCount/滿額折扣設定/ajax-list/';


    protected $modelName = "DisCountFull";

    public $index_select_field = ['id','rank','is_visible','discount','title','st_date','ed_date','full'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = 'DisCount/滿額折扣設定';

    public $viewPreFix = 'DisCountFull';
   
    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-success',
                "showText" => 'S'
            )
        ),
    );


    public $cacheData = [
        'active' => true,
        'select' => [],
        'order' => 'rank',
        'sort' => 'asc'
    ];

  
       
}

