<?php
namespace App\Http\Controllers\Fantasy\DisCount;

use App\Http\Controllers\CRUDBaseController;
use Redis;
use Cache;
/**相關Models**/

use App\Http\Models\Banner;


class DisCountCodeController extends CRUDBaseController
{

    public $ajaxEditLink = 'Fantasy/DisCount/折扣碼設定/ajax-list/';


    protected $modelName = "DisCountCode";

    public $index_select_field = ['id','rank','is_visible','code','order_list_data','order_list_limit','is_repeat','title','st_date','ed_date','money_discount'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = 'DisCount/折扣碼設定';

    public $viewPreFix = 'DisCountCode';
   
    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-success',
                "showText" => 'S'
            )
        ),
    );


    public $cacheData = [
        'active' => true,
        'select' => [],
        'order' => 'rank',
        'sort' => 'asc'
    ];

  
       
}

