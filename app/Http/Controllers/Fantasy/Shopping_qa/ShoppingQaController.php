<?php

namespace App\Http\Controllers\Fantasy\Shopping_qa;

use App\Http\Controllers\CRUDBaseController;
use Redis;
use Cache;
/**相關Models**/

use App\Http\Models\Banner;


class ShoppingQaController extends CRUDBaseController
{

    public $ajaxEditLink = 'Fantasy/ShoppingQa/問與答內容管理/ajax-list/';

    protected $modelName = "Shopping_qa";

    public $index_select_field = ['id','rank','is_visible','title','category_id'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = 'ShoppingQa/問與答內容管理';

    public $viewPreFix = 'ShoppingQa';

   

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-success',
                "showText" => 'S'
            )
        ),
    );


    public $cacheData = [
        'active' => true,
        'select' => [],
        'order' => 'rank',
        'sort' => 'asc'
    ];

     public $modelBelongs = [

       
        "Shopping_qa_category" => [
            "parent" => "category_id",
            "filed" => 'id',
            "select" => ['title','id',"category_id"]
        ],

     
      
       
    ];

    public $modelHas = [

       "Shopping_qa_content" => [
            "modelName" => "Shopping_qa_content",
            "storedName" => "Shopping_qa_content", 
            "parent" => "qa_id",
            "filed" => 'id',
            "select" => ['id','rank','is_visible','content'],
            
            ],
       
    ];

       public $saveSubData = [
                [   // 產品介紹的多圖
                    "modelName" => "Shopping_qa_content",
                    "requestModelName" => "Shopping_qa_content", //多圖的inputName
                    "to_parent" => "qa_id",
                ],

            ];

      public $photoTable = [
            'photo' => [
                "排序" => "rank",
                "問題內容(可以用&lt;br&gt;換行)" => "content",
                "狀態" => "is_visible",                
                
                
            ],
            
        ];   




       
}
