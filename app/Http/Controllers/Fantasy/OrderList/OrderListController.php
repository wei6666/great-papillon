<?php

namespace App\Http\Controllers\Fantasy\OrderList;
use App\Http\Controllers\front\ProductController as ProductController;
use App\Http\Controllers\CRUDBaseController;
use Illuminate\Http\Request;
use Redis;
use Cache;
/**相關Models**/

use App\Http\Models\Banner;
use App\Http\Controllers\Fantasy\MakeItemV2;


class OrderListController extends CRUDBaseController
{

    public $ajaxEditLink = 'Fantasy/OrderList/訂單管理/ajax-list/';

    protected $modelName = "OrderList";

    public $index_select_field = ['id','rank','is_visible','name','created_at','used_bouns','price'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = 'OrderList/訂單管理';

    public $viewPreFix = 'OrderList';

   

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-success',
                "showText" => 'S'
            )
        ),
    );


    public $cacheData = [
        'active' => true,
        'select' => [],
        'order' => 'rank',
        'sort' => 'asc'
    ];

    public function postUpdate(Request $request)
    {

        if(!empty($request->input('method')) AND $request->input('method')=='ajaxEdit')
        {
            parent::updateOne( $request->input($this->modelName), $this->modelName, 'ajaxEdit');
            //將資料做暫存
            $this->setCacheData();
        }
        else
        {

            $Datas = $request->input($this->modelName);

            if($Datas['order_list_type']==3);
            {
                    $Datas['order_list_3']=Date("Y-m-d h:i:s");
            }

            

            if( parent::updateOne( $Datas, $this->modelName, '') )
            {

                $this->checkSavedSubData($request->all(), $Datas['id']);

                //通知信
                $this->sendNoticeMail($Datas['id'], 'reply');

                //將資料做暫存
                $this->setCacheData();

                if($Datas['order_list_type']==12);
                {
                    //ProductController::DealReturnOrderList("zh-tw");
                }

                return redirect( MakeItemV2::url('Fantasy/'.$this->routePreFix.'/edit/'.$Datas['id']) )->with('Message','修改成功');
            }

        }
    }

     
       
}
