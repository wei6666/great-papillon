<?php
namespace App\Http\Controllers\Fantasy\Product;

use App\Http\Controllers\CRUDBaseController;
use Redis;
use Cache;
/**相關Models**/

use App\Http\Models\Banner;


class ProductController extends CRUDBaseController
{

    public $ajaxEditLink = 'Fantasy/Product/產品管理/ajax-list/';

    protected $modelName = "Product";

    public $index_select_field = ['id','rank','is_visible','all_num','title','category_id','is_sell_out','is_home'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = 'Product/產品管理';

    public $viewPreFix = 'Product';

   

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-success',
                "showText" => 'S'
            )
        ),
    );


    public $cacheData = [
        'active' => true,
        'select' => [],
        'order' => 'rank',
        'sort' => 'asc'
    ];

    public $modelBelongs = [

       
        "ProductSubCategory" => [
            "parent" => "category_id",
            "filed" => 'id',
            "select" => ['title','id',"category_id"]
        ],

        "Marketing" => [
            "parent" => "category_id",
            "filed" => 'id',
            "select" => ['title','id']
        ],

        "Searching" => [
            "parent" => "category_id",
            "filed" => 'id',
            "select" => ['title','id']
        ],
      
       
    ];

    public $modelHas = [

       "ProductPhoto" => [
            "modelName" => "ProductPhoto",
            "storedName" => "ProductPhoto", 
            "parent" => "product_id",
            "filed" => 'id',
            "select" => ['id','rank','is_visible','image',"is_may_like","is_happinese_img","is_out_img","is_home2_img","is_home1_img",'product_id'],
            
            ],

        "ProductContent" => [
            "modelName" => "ProductContent",
            "storedName" => "ProductContent", 
            "parent" => "product_id",
            "filed" => 'id',
            "select" => ['id','rank','is_visible','image1','image2','product_id','data_title_content','data_value_content',"img_con1","img_con2","title","content","title1"],
            
            ],
         "ProductSeparate" => [
            "modelName" => "ProductSeparate",
            "storedName" => "ProductSeparate", 
            "parent" => "product_id",
            "filed" => 'id',
            "select" => ['id','rank','is_visible','title','code','price_original','price_discount'],
            
            ],

       
    ];

       public $saveSubData = [
                [  
                    "modelName" => "ProductContent",
                    "requestModelName" => "ProductContent",
                    "to_parent" => "product_id",

                    
                ],
                [  
                    "modelName" => "ProductPhoto",
                    "requestModelName" => "ProductPhoto",
                    "to_parent" => "product_id",

                    
                ],
                [  
                    "modelName" => "ProductSeparate",
                    "requestModelName" => "ProductSeparate",
                    "to_parent" => "product_id",

                    
                ],

              
                
            ];

      public $photoTable = [
            'photo' => [
                "排序" => "rank",
                "圖片(800 x 730)" => "image",
                "狀態" => "is_visible",
                "設為首頁圖片1" => "is_home1_img",
                "設為首頁圖片2" => "is_home2_img",
                "設為類別頁封面" => "is_out_img",
                "設為幸福見證封面" => "is_happinese_img",
                "設為更多產品封面" => "is_may_like",
            ],
            'ProductSeparate' => [
                "排序" => "rank",                
                "狀態" => "is_visible",
                "代號標題" => "title",
                "商品代號" => "code",
                "原價" => "price_original",
                "折扣價格" => "price_discount",
                "編輯尺寸" => "go_news_content",
              
            ],
            'ProductContent' => [
                "排序" => "rank",
                "標題" => "title",
                "顯示狀態" => "is_visible",
                "編輯" => "go_product_content",
                
                // "圖片1<br>樣式1:<br>(1320 x 580)<br>樣式2:(450 x 450)" => "image1", 
                // "標題" => "title",
                // "內容" => "content",                
                // "圖片2<br>若樣式2為<br>(450 x 450)" => "image2",
                // "產品內容上方標題<br>(格式1)" => "title1",
                // "產品表格標題<br>用換行區隔資料<br>若不填則表格不會出現<br>(格式1)" => "data_title_content",
                // "產品表格內容<br>用換行區隔資料<br>(格式1)" => "data_value_content",
                // "圖片1下方說明(格式2)" => "img_con1",
                // "圖片2下方說明(格式2)" => "img_con2",

                
            ],
            
        ];    

       
}

