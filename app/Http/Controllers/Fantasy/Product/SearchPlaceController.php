<?php
namespace App\Http\Controllers\Fantasy\Product;

use App\Http\Controllers\CRUDBaseController;
use Redis;
use Cache;
/**相關Models**/

use App\Http\Models\Banner;


class SearchPlaceController extends CRUDBaseController
{

    public $ajaxEditLink = 'Fantasy/SearchPlace/ajax-list/';

    protected $modelName = "Home";

    public $index_select_field = ['id','rank','is_visible'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = 'SearchPlace';

    public $viewPreFix = 'SearchPlace';

   

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-success',
                "showText" => 'S'
            )
        ),
    );


    public $cacheData = [
        'active' => true,
        'select' => [],
        'order' => 'rank',
        'sort' => 'asc'
    ];

 

    public $modelHas = [

       "SearchPlace" => [
            "modelName" => "SearchPlace",
            "storedName" => "SearchPlace", 
            "parent" => "home_id",
            "filed" => 'id',
            "select" => ['id','rank','is_visible','home_id','place_title'],
            
            ],
       
    ];

       public $saveSubData = [
                [   // 產品介紹的多圖
                    "modelName" => "SearchPlace",
                    "requestModelName" => "SearchPlace", //多圖的inputName
                    "to_parent" => "home_id",

                    
                ],

              
                
            ];

      public $photoTable = [
            'place' => [
                "排序" => "rank",
                "適用地點選項" => "place_title",
                "狀態" => "is_visible"
            ],
            
        ];    

       
}
