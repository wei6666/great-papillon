<?php

namespace App\Http\Controllers\Fantasy\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\CRUDBaseController;
use Redis;
use Cache;
/**相關Models**/

use App\Http\Models\Size\AllSize;
use App\Http\Models\Size\SizeCategory;
use App\Http\Models\Product\ProductSize;
use App\Http\Models\Banner;


class SizeDetailsController extends CRUDBaseController
{

    public $ajaxEditLink = 'Fantasy/Product/SizeDetails/ajax-list/';

    protected $modelName = "ProductSeparate";

    public $index_select_field = ['id','rank','is_visible','title'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = 'Product/SizeDetails';

    public $viewPreFix = 'SizeDetail';

   

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-success',
                "showText" => 'S'
            )
        ),
    );





    public $cacheData = [
        'active' => true,
        'select' => [],
        'order' => 'rank',
        'sort' => 'asc'
    ];


    public $modelBelongs = [

       
        "SizeCategory" => [
            "parent" => "category_id",
            "filed" => 'id',
            "select" => ['title','id']
        ]
    ];


    public $modelHas = [
        "ProductSize" => [
            "modelName" => "ProductSize",
            "storedName" => "ProductSize", 
            "parent" => "category_id",
            "filed" => 'id',
            "select" => ['id','rank','is_visible','title'],
        ],       
    ];

       public $saveSubData = [
                [   // 產品介紹的多圖
                    "modelName" => "ProductSize",
                    "requestModelName" => "ProductSize", //多圖的inputName
                    "to_parent" => "category_id",

                    
                ],
            ];

      public $photoTable = [
            'photo' => [
                "排序" => "rank",               
                "狀態" => "is_visible",
                "尺寸" => "title",
                
            ],
            
        ];  



    // public function postUpdate(Request $request)
    // {
            

    //     $SizeCategory=AllSize::where("category_id",$request->input("ProductSeparate")['category_id'])->get();

    //     $ProductSize=ProductSize::where('category_id',$request->input("ProductSeparate")['id']);
    //     $is_delarr=[];
    //     $is_visarr=[];
    //     foreach($ProductSize->get() as $value)
    //     {
    //         array_push($is_delarr, $value->id);
    //         array_push($is_visarr, $value->is_visible);
    //     }

    //     dd($is_delarr,$is_visarr);
    //     $ProductSize->delete();

    //    // dd(count($ProductSize),count($SizeCategory));

    //     $md="App\Http\Models\Product\ProductSize";
    //     //$cc=1;
    //     foreach($SizeCategory as $key => $value)
    //     {    
    //            $savedata= new $md;
    //            $savedata->category_id=$request->input("ProductSeparate")['id'];
    //            $savedata->title=$value->title;
               
    //            if(!empty($is_delarr[$key]))
    //            {
    //             $savedata->id=$is_delarr[$key];
    //            }
    //            if(!empty($is_visarr[$key]))
    //            {
    //             $savedata->is_visible=$is_visarr[$key];
    //            }
    //            else
    //            {
    //             $savedata->is_visible=1;
    //            }
    //            $savedata->save();
    //            //$cc++;
    //     }
        
    //     echo "<script>location.reload();</script>";
    //     die;
    //     if(!empty($request->input('method')) AND $request->input('method')=='ajaxEdit')
    //     {
    //         parent::updateOne( $request->input($this->modelName), $this->modelName, 'ajaxEdit');
    //         //將資料做暫存
    //         $this->setCacheData();
    //     }
    //     else
    //     {

    //         $Datas = $request->input($this->modelName);

    //         if( parent::updateOne( $Datas, $this->modelName, '') )
    //         {

    //             $this->checkSavedSubData($request->all(), $Datas['id']);

    //             //通知信
    //             $this->sendNoticeMail($Datas['id'], 'reply');

    //             //將資料做暫存
    //             $this->setCacheData();

    //             // return redirect( MakeItemV2::url('Fantasy/'.$this->routePreFix.'/edit/'.$Datas['id']) )->with('Message','修改成功');
    //         }

    //     }
    // }
 



    // public function postStore(Request $request)
    // {
        
    //     $resoult = json_decode(parent::addNew([
    //         "Datas" => $request->input( $this->modelName ) ,
    //         "modelName" => $this->modelName,
    //         "routeFix" => ''.$this->routePreFix.''
    //     ]), true );

    //     if( !empty( $resoult['redirect'] ) )
    //     {
    //         $this->checkSavedSubData($request->all(), $resoult['parent_id'] );

    //         //通知信
    //         $this->sendNoticeMail($resoult['parent_id'], 'new');

    //         //將資料做暫存
    //         $this->setCacheData();
    //         return redirect( $resoult['redirect'] )->with('Message','新增成功, 轉到編輯頁面');
    //     }
    // }
}


