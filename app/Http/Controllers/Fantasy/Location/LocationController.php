<?php
namespace App\Http\Controllers\Fantasy\Location;

use App\Http\Controllers\CRUDBaseController;
use Redis;
use Cache;
/**相關Models**/

use App\Http\Models\Banner;


class LocationController extends CRUDBaseController
{

    public $ajaxEditLink = 'Fantasy/Location/門市據點管理/ajax-list/';


    protected $modelName = "Location";

    public $index_select_field = ['id','rank','is_visible','address','title','place',"category_id"];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = 'Location/門市據點管理';

    public $viewPreFix = 'Location';
   
    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-success',
                "showText" => 'S'
            )
        ),
    );


    public $cacheData = [
        'active' => true,
        'select' => [],
        'order' => 'rank',
        'sort' => 'asc'
    ];

    public $modelBelongs = [

       
        
         "LocationCategory" => [
            "parent" => "category_id",
            "filed" => 'id',
            "select" => ['title','id']
        ],
        //  "location" => [
        //     "parent" => "category_id",
        //     "filed" => 'id',
        //     "select" => ['title','id']
        // ]
    ];

  

       
}

