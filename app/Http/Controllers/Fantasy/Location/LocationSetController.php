<?php

namespace App\Http\Controllers\Fantasy\Location;

use App\Http\Controllers\CRUDBaseController;
use Redis;
use Cache;
/**相關Models**/

use App\Http\Models\Banner;


class LocationSetController extends CRUDBaseController
{

    public $ajaxEditLink = 'Fantasy/Location/門市據點設定/ajax-list/';

    protected $modelName = "LocationSet";

    public $index_select_field = ['id','rank','is_visible','title'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = 'Location/門市據點設定';

    public $viewPreFix = 'LocationSet';

   

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-success',
                "showText" => 'S'
            )
        ),
    );


    public $cacheData = [
        'active' => true,
        'select' => [],
        'order' => 'rank',
        'sort' => 'asc'
    ];

     public $modelHas = [

       "LocationCategory" => [
            "modelName" => "LocationCategory",
            "storedName" => "LocationCategory", 
            "parent" => "category_id",
            "filed" => 'id',
            "select" => ['id','rank','is_visible','title','en_title'],
            
            ],
       
    ];

       public $saveSubData = [
                [   // 產品介紹的多圖
                    "modelName" => "LocationCategory",
                    "requestModelName" => "LocationCategory", //多圖的inputName
                    "to_parent" => "category_id",
                ],
                
            ];

      public $photoTable = [
            'photo' => [
                "排序" => "rank",               
                "狀態" => "is_visible",
                "位置" => "title",
                "位置(英文)" => "en_title",
                
            ],
            
        ];   




       
}
