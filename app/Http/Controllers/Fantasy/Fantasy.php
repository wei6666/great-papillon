<?php 
namespace App\Http\Controllers\Fantasy;

use File;

/*schema class*/
use App\Http\Controllers\Fantasy\TableSchemaController;

class Fantasy {

	/*建立table 帶入參數 (陣列)*/
	public static function setTabel(array $formDate) {
	
		// formDate陣列
		// forderName =>資料夾名稱 
		// fileName   => controller model view 的檔案名稱 
		// tableName  => table名稱
		// fieldName  => table欄位
		// typeName   => 欄位的型態
		// check      => 建立的檔案種類
		// sqlTableName => 串上語系的tableName 用在建立table name

		// 將table 欄位及型態 轉為 陣列
		foreach($formDate['fieldName'] as $key => $field){
			$formDate['setdb'][$field] = $formDate['typeName'][$key];		
		}
		/*construct 預設給入 setdb & tableName*/
		$createtable = new TableSchemaController($formDate['setdb'],$formDate['sqlTableName']);

		/*建立table*/
		if($createtable->buildeTable()) {
			try {
				/*檢查table是否存在*/
				if($createtable->checktable($formDate['sqlTableName'])) {
					$field = $createtable->getColumn($formDate['sqlTableName']);
					/*取的dbfield字串*/
					$setting['SQLname'] = $formDate['tableName'];
					$setting['dir'] = $formDate['forderName'];
					$setting['name'] = $formDate['fileName'];
					$setting['masterTitle'] = $formDate['masterTitle'];
					$setting['slaverTitle'] = $formDate['slaverTitle'];
					$setting['dbfield'] = $field;
				}

				return self::createFile($setting, $formDate['check']);

			} catch (Exception $e) {
				echo "輸入欄位有空";
			}
			
			
		}
	}

	/*產生Controller & view & model*/
	public static function createFile(array $setting,array $check) {
		/*
		規則資料庫名稱
		en_csr_financal

		資料庫名稱 為 en_csr_financal
		檔案路徑為 
		controller,     mysql,     view  
		controller/csr  model/csr  Fantasy/csr/financal

		/* 
		//controller
		namcespace =  App\Http\Controllers\csr
		class Name = financalController.php
		$modelName = financal
		$index_select_field = 依照建表設定
		$ajaxEditLink = csr/financal
		$routePreFix = csr/financal
		$viewPreFix = csr/financal

		//model
		資料庫名稱 = en_csr_financal
		model class name = financal.php

		變數
		$value[dir] = csr 目錄路徑
		$value[name] = financal table名稱 mysql檔案名稱 controller名稱 view名稱
		$value[SQLname] = csr_financal
		$value['dbfield'] 為資料庫欄位 $index_select_field
		*/
		// foreach ($setting as  $value) {

			if(in_array('c',$check)) {
			/*****Controller*****************************************************/
				/*抓controller 樣本*/
				$Conrollerfileget =  file_get_contents(base_path().'/app/Http/Controllers/controllerSample.txt');
				/*檢查料夾 or 產生資料夾*/
				if (!file_exists(base_path().'/app/Http/Controllers/'.$setting['dir'])) {
			    mkdir(base_path().'/app/Http/Controllers/'.$setting['dir'], 0700);
				}
				/*產生controller.php */
				$filecreate = fopen(base_path().'/app/Http/Controllers/'.$setting['dir'].'/'.$setting['name'].'Controller.php','w');

				/*設定 - namespace名稱 {namespace}*/
				$Conrollerfileget = str_replace('{namespace}',$setting['dir'],$Conrollerfileget);

				/*設定 - class名稱 {ControllerName}*/
				$Conrollerfileget = str_replace('{ControllerName}',$setting['name'],$Conrollerfileget);

				/*設定 modelName {modelName}*/
		   		$Conrollerfileget = str_replace('{modelName}',$setting['name'],$Conrollerfileget);

		    	/*設定 ajaxEditLink {EditLink}*/
		    	$Conrollerfileget = str_replace('{EditLink}',$setting['dir'].'/'.$setting['name'],$Conrollerfileget);/*處理字串 a/b */

		    	/*設定 index_select_field {dbfield}*/
		    	$Conrollerfileget = str_replace('{dbfield}',self::getfieldString($setting['dbfield']),$Conrollerfileget);

		    	/*設定 routePreFix {routePreFix}*/
		    	$Conrollerfileget = str_replace('{routePreFix}',$setting['dir'].'/'.$setting['name'],$Conrollerfileget);

		    	/*設定 viewPreFix {viewPreFix}*/
		    	$Conrollerfileget = str_replace('{viewPreFix}',$setting['dir'].'/'.$setting['name'],$Conrollerfileget);

				fwrite($filecreate, $Conrollerfileget);
				fclose($filecreate);

		}
			if(in_array('m',$check)) {
			/*****Model**********************************************************/
				/*models 樣本*/
				$Modelfileget =  file_get_contents(base_path().'/app/Http/Models/modelSample.txt');
				/*檢查料夾 or 產生資料夾*/
				if (!file_exists(base_path().'/app/Http/Models/'.$setting['dir'])) {
			    	mkdir(base_path().'/app/Http/Models/'.$setting['dir'], 0700);
				}

				/*產生Model.php */
				$filecreate = fopen(base_path().'/app/Http/Models/'.$setting['dir'].'/'.$setting['name'].'.php','w');
			
				/*設定 - Model檔案namespace名稱 {namespace}*/
				$Modelfileget = str_replace('{namespace}',$setting['dir'],$Modelfileget);
			
				/*設定 - Model檔案class名稱 {className}*/
				$Modelfileget = str_replace('{className}',$setting['name'],$Modelfileget);
			
				/*設定 - Model檔案資料庫名稱 {SQLname} 第一個字轉小寫*/
				$Modelfileget = str_replace('{SQLname}', strtolower($setting['SQLname']),$Modelfileget);

				fwrite($filecreate, $Modelfileget);
				fclose($filecreate);
			}

			if(in_array('v',$check)) {

				/*****view**********************************************************/
				/*檢查料夾 or 產生資料夾*/
				/*檢查目錄資料夾*/
				if (!file_exists(base_path().'/public/views/Fantasy/'.$setting['dir'])) {
			    	mkdir(base_path().'/public/views/Fantasy/'.$setting['dir'], 0700);
				}
				/*檢查檔案*/
				if (!file_exists(base_path().'/public/views/Fantasy/'.$setting['dir'].'/'.$setting['name'])) {
			    	mkdir(base_path().'/public/views/Fantasy/'.$setting['dir'].'/'.$setting['name'], 0700);
				}

				/*view index樣本*/
				$Indexfile =  file_get_contents(base_path().'/public/views/Fantasy/indexSample.txt');

				$Indexfile = str_replace('{slaverTitle}',$setting['slaverTitle'],$Indexfile);
				$Indexfile = str_replace('{modelName}',$setting['name'],$Indexfile);
				$Indexfile = str_replace('{tableFeild}',self::addIndexFeild($setting['dbfield']),$Indexfile);

				
				/*產生index.blade.php */
				$filecreate = fopen(base_path().'/public/views/Fantasy/'.$setting['dir'].'/'.$setting['name'].'/index.blade.php','w');

				/*寫入內容*/
				fwrite($filecreate, $Indexfile);
				fclose($filecreate);
				
				/*view edit樣本*/
				$EditFile =  file_get_contents(base_path().'/public/views/Fantasy/editSample.txt');

				/*產生edit.blade.php */
				$filecreate = fopen(base_path().'/public/views/Fantasy/'.$setting['dir'].'/'.$setting['name'].'/edit.blade.php','w');

				$EditFile = str_replace('{slaverTitle}',$setting['slaverTitle'],$EditFile);
				/*先將項目樣本導入*/
				$EditFile = str_replace('{tableFeild}',self::addEditFeild($setting['dbfield']),$EditFile);
				/*再將modelName換置*/
				$EditFile = str_replace('{modelName}',$setting['name'],$EditFile);


				fwrite($filecreate, $EditFile);
				fclose($filecreate);
				
				/*傳入ModelArray 及 menuList 需要的變數*/
				$route = self::RouteMaster($setting);
				self::addRoute($setting,$route);
				$master = self::checkMaster($setting);
				self::addMenuList($setting,$master);
				self::addModelArray($setting);
			}

			return true;
	}

	/*帶陣列 給字串 ex   input [1,2,3] output "['1','2','3']" 字串*/
	public static function getfieldString(array $dbfield) {
		$string = "[";

		$length = count($dbfield)-1;

		foreach ($dbfield as $key => $value) {
			$string .= "'".$value."'" ;
			/*不是最後一個加入逗點*/
			if($key != $length) {
				$string .= ",";
			}
			
		}
		$string.="]";

		return $string;
		
	}

	public static function addMenuList(array $setting , $master) {
		
		if($master){
			$backend = file_get_contents(base_path().'/app/Http/Controllers/Fantasy/BackendController.php');
			/*排除抓到辨識字串 頭*/
			$string =  "\n\t\t\t\t\t\t'".$setting['slaverTitle']."'=>\"Fantasy/".$setting['dir']."/".$setting['name']."\",//{".$setting['masterTitle']."}";
			
			
			$newString = preg_replace("/\/\/\{".$setting['masterTitle']."\}/i",$string,$backend);
			$myfile = fopen(base_path().'/app/Http/Controllers/Fantasy/BackendController.php','w') or die('file not foubd');
			fwrite($myfile,$newString);
			fclose($myfile);

			return true;
		}
		else{
				$string = "],\n\t\t\"".$setting['masterTitle']."\"=> [
				\"slogan\" => \"".$setting['masterTitle']."\",
				\"icon\" => '<i class=\"icon-wrench icons font-green-haze theme-font\"></i>',
				\"link\" =>[ 
						'". $setting['slaverTitle']."'=>\"Fantasy/".$setting['dir']."/".$setting['name']."\",
					//{".$setting['masterTitle']."}
					   	]
				],
			\n\t];";


			$backend = file_get_contents(base_path().'/app/Http/Controllers/Fantasy/BackendController.php');
			/*排除抓到辨識字串 頭*/
			$start = strpos($backend,'MenuStart')+strlen('MenuStart');
			/*排除抓到辨識字串 尾*/
			$end = strpos($backend,'//MenuEnd',$start);

			$lengh = ($end-$start);

			$menuArea = substr($backend,$start,$lengh);

			/*最後,出現的地方*/
			$addPoint = strrpos($menuArea,'],');
		
			$string = substr_replace($menuArea,$string,$addPoint,-2);	
		
			$myfile = fopen(base_path().'/app/Http/Controllers/Fantasy/BackendController.php','w') or die('file not foubd');
			fwrite($myfile,substr_replace($backend,$string,$start,$lengh));
			fclose($myfile);
			return true;
			}
	}

	public static function addModelArray(array $setting) {
		

		//"$setting['name']" => App\Http\Models\$setting['dir']\$setting['name']::class,
		$path = ','."\n\t\t\"".$setting['name'].'"=> App\Http\Models\\'.$setting['dir'].'\\'.$setting['name']."::class,"."\n\t];";
		
		$backend = file_get_contents(base_path().'/app/Http/Controllers/Fantasy/BackendController.php');
		/*排除抓到辨識字串 頭*/
		$start = strpos($backend,'ModelStart')+strlen('ModelStart');
		/*排除抓到辨識字串 尾*/
		$end = strpos($backend,'//ModelEnd',$start);

		$lengh = ($end-$start);
		
		$menuArea = substr($backend,$start,$lengh);

		/*最後,出現的地方*/
		$addPoint = strrpos($menuArea,',');
		
		$string = substr_replace($menuArea,$path,$addPoint,-2);	

		//var_dump($string);die;
		$myfile = fopen(base_path().'/app/Http/Controllers/Fantasy/BackendController.php','w') or die('file not foubd');
		fwrite($myfile,substr_replace($backend,$string,$start,$lengh));
		fclose($myfile);
		return true;
	}

	public static function addRoute(array $setting,$route) {
		
		if($route){
			
			$backend = file_get_contents(base_path().'/app/Http/routes.php');
			/*排除抓到辨識字串 頭*/
			$string =  "Route::controller('".$setting['name']."','".$setting['dir']."\\".$setting['name']."Controller');
					//{prefix".$setting['dir']."}";
			
			
			$newString = preg_replace("/\/\/\{prefix".$setting['dir']."\}/i",$string,$backend);
			$myfile = fopen(base_path().'/app/Http/routes.php','w') or die('file not foubd');
			fwrite($myfile,$newString);
			fclose($myfile);
			return true;

		}
		else{
					$string = "Route::group(['prefix'=>'".$setting['dir']."'],function(){
					Route::controller('".$setting['name']."','".$setting['dir']."\\".$setting['name']."Controller');
					//{prefix".$setting['dir']."}
				});
				//{addRoute}";

				$route = file_get_contents(base_path().'/app/Http/routes.php');
				$newSting = preg_replace("/\/\/\{addRoute\}/i",$string,$route);
				//$start = strpos($route,'//{addRoute}');
				//$lengh = strlen($string);
				
				$myfile = fopen(base_path().'/app/Http/routes.php','w') or die('file not foubd');
				fwrite($myfile,$newSting);
				fclose($myfile);
				return true;
			}
		
	}

	public static function checkMaster(array $setting) {
		//$master = $setting['masterTitle'];
		$backend = file_get_contents(base_path().'/app/Http/Controllers/Fantasy/BackendController.php');
		return preg_match("/\/\/\{".$setting['masterTitle']."\}/i", $backend);
	}

	public static function RouteMaster(array $setting) {
		$backend = file_get_contents(base_path().'/app/Http/routes.php');
		
		return preg_match("/\/\/\{prefix".$setting['dir']."\}/i", $backend);

	}
	

	
	public static function addIndexFeild(array $feildArray) {
		//var_dump($feildArray);die;
		$string = '';
		$IndexFeild['id'] = '[
							"title" => "排序",
							"columns" => "rank",
							"width" => "40"
						],';

		$IndexFeild['title'] = '
						[
							"title" => "標題",
							"columns" => "title",
							"width" => "40"
						],';

		$IndexFeild['year'] = '
						[
							"title" => "年份",
							"columns" => "year",
							"width" => "40"
						],';

		$IndexFeild['is_visible'] = '
						[
							"title" => "狀態列",
							"group" => [
								[
									"title" => "是否顯示",
									"sub_title" => "S",
									"columns" => "is_visible",
									"helpText" =>"是否顯示",
									"color" => "label-success",
									"options" => $StatusOption
									]
								]
						],';
		$IndexFeild['image'] = '
						[
							"title" => "圖片",
							"columns" => "image",
							"width" => "40",
							"fliter" => "image"
						],';

		foreach($feildArray as $key => $feild) {
			foreach($IndexFeild as $key2 => $Index) {
				if($key2 === $feild ){
					$string .= $Index;
				}
			}
		}
		return $string;
	}

	public static function addEditFeild(array $feildArray) {
		$string = '';
		$IndexFeild['id'] = '{{ItemMaker::idInput([
														"inputName" => "{modelName}[id]",
														"value" => ( !empty($data["id"]) )? $data["id"] : ""
													])}}';

		$IndexFeild['title'] = '
													{{ItemMaker::textInput([
														"labelText" => "標題",
														"inputName" => "{modelName}[title]",
														"value" => ( !empty($data["title"]) )? $data["title"] : ""
													])}}';

		$IndexFeild['is_visible'] = '
													{{ItemMaker::radio_btn([
														"labelText" => "是否啟用",
														"inputName" => "{modelName}[is_visible]",
														"helpText" =>"是否啟用",
														"options" => $StatusOption,
														"value" => ( !empty($data["is_visible"]) )? $data["is_visible"] : ""
													])}}';
		$IndexFeild['rank'] = '
													{{ItemMaker::textInput([
														"labelText" => "排序",
														"inputName" => "{modelName}[rank]",
														"value" => ( !empty($data["rank"]) )? $data["rank"] : ""
													])}}';

		$IndexFeild['file'] = '
													{{ItemMaker::filePicker([
														"labelText" => "選擇檔案",
														"inputName" => "{modelName}[file]",
														"value" => ( !empty($data["file"]) )? $data["file"] : ""
													])}}';
		$IndexFeild['content'] = '
													{{ItemMaker::textarea([
														"labelText" => "內容",
														"inputName" => "{modelName}[content]",
														"value" => ( !empty($data["content"]) )? $data["content"] : ""
													])}}';

		$IndexFeild['year'] = '
													{{ItemMaker::textInput([
														"labelText" => "年份",
														"inputName" => "{modelName}[year]",
														"value" => ( !empty($data["year"]) )? $data["year"] : ""
													])}}';

		$IndexFeild['datePicker'] = '
													{{ItemMaker::datePicker([
														"labelText" => "日期選擇",
														"inputName" => "{modelName}[datePicker]",
														"value" => ( !empty($data["datePicker"]) )? $data["datePicker"] : ""
													])}}';

		$IndexFeild['date'] = '
													{{ItemMaker::textInput([
														"labelText" => "日期",
														"inputName" => "{modelName}[date]",
														"value" => ( !empty($data["date"]) )? $data["date"] : ""
													])}}';

		foreach($feildArray as $key => $feild) {
			foreach($IndexFeild as $key2 => $Index) {
				if($key2 === $feild ){
					$string .= $Index;
				}
			}
		}
		return $string;
	}

}
?>