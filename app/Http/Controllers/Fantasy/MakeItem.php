<?php namespace App\Http\Controllers\Fantasy;

use Illuminate\Routing\Controller as BaseController;

use Config;
use App;

class MakeItem extends BaseController
{
	//input 
	public static function textInput($labelText,$inputName, $inputID,$inputValue,$helpText,$disabled){

		$labelText = (empty($labelText)) ? '預設Label Text' : $labelText;

		$helpBlock = (!empty($helpText))? '<span class="help-block">'.$helpText.'</span>' : '<span class="help-block"></span>';

		$inputID = (!empty($inputID)) ? 'id="'.$inputID.'"' : 'id="'.$inputName.'"' ;

		$disabled = ( !empty($disabled) AND $disabled == 'disabled' )? 'disabled="disabled"' : '';

		print('												
			<div class="form-group">
				<label class="col-md-3 control-label">'.$labelText.'</label>
				<div class="col-md-4">
					<input type="text" class="form-control" name="'.$inputName.'" '.$inputID.' placeholder="Enter Text " value="'.$inputValue.'" '.$disabled.'>
					'.$helpBlock.'
				</div>
			</div>
		');

	}

	//input 
	public static function passwordInput($labelText,$inputName, $inputID,$inputValue,$helpText){

		$labelText = (empty($labelText)) ? '預設Label Text' : $labelText;

		$helpBlock = (!empty($helpText))? '<span class="help-block">'.$helpText.'</span>' : '<span class="help-block"></span>';

		$inputID = (!empty($inputID)) ? 'id="'.$inputID.'"' : 'id="'.$inputName.'"' ;

		print('												
			<div class="form-group">
				<label class="col-md-3 control-label">'.$labelText.'</label>
				<div class="col-md-4">
					<input type="password" class="form-control" name="'.$inputName.'" '.$inputID.' placeholder="" value="'.$inputValue.'">
					'.$helpBlock.'
				</div>
			</div>
		');

	}

	//radio
	/* $Options 必須傳入陣列，EXP: Array("否","是") */
	public static function radio($labelText, $inputName, $inputID, $Options, $inputValue, $helpText, $type){
		if (is_array($Options)) {

			$labelText = (empty($labelText)) ? '預設Label Text' : $labelText;

			$helpBlock = (!empty($helpText))? '<span class="help-block">'.$helpText.'</span>' : '<span class="help-block"></span>';

			$inputID = (!empty($inputID)) ? 'id="'.$inputID.'"' : 'id="'.$inputName.'"' ;

			$radio = '';
			foreach ($Options as $key => $value) {
				if ($inputValue == $key) {
					$checked = 'checked="checked" ';
				}else{
					$checked = '';
				}
				$radio .= '
					<label class="radio-inline">
						<input type="radio" class="manainput" name="'.$inputName.'" '.$inputID.' value="'.$key.'" '.$checked.'>'.$value.' 
					</label>
					';
			}
			if($type == 'table')
			{
				return $radio;	
			}
			else
			{
				print('												
					<div class="form-group">
						<label class="col-md-3 control-label">'.$labelText.'</label>
						<div class="col-md-4">
							'.$radio.'
							'.$helpBlock.'
						</div>
					</div>
				');				
			}


		}else{
			echo "Options is not array.";
		}
	}	

	//下拉選單
	/* $Options 必須傳入陣列，EXP: Array("否","是") */
	public static function select($labelText, $inputName, $inputID, $Options, $inputValue, $helpText){
		if (is_array($Options)) {

			$labelText = (empty($labelText)) ? '預設Label Text' : $labelText;

			$helpBlock = (!empty($helpText))? '<span class="help-block">'.$helpText.'</span>' : '<span class="help-block"></span>';

			$inputID = (!empty($inputID)) ? 'id="'.$inputID.'"' : 'id="'.$inputName.'"' ;

			$options = '';
				foreach ($Options as $row) 
				{
					//如果有下一層
					if (isset($row['children']) AND !empty($row['children'])) 
					{
						foreach ($row['children'] as $row2) {
						
							//如果有第三層
							if (isset($row2['children']) AND !empty($row2['children'])) 
							{
								foreach ($row['children'] as $row2) 
								{
									$options .='<optgroup label=" '.$row['title'].' / '.$row2['title'].'">';
										foreach ($row2['children'] as $row3)
										{
											$select = ($row3['id']==$inputValue)? 'selected': '';

											$options .='<option value="'.$row3['id'].'" '.$select.' >'.$row3['title'].'</option>';
										}
									$options .='</optgroup>';  
									
								}
							}else
							{	
								$options .='<optgroup label=" '.$row['title'].' ">';

									$select = ($row2['id']==$inputValue)? 'selected': '';
									$options .='<option value="'.$row2['id'].'" '.$select.' >'.$row2['title'].'</option>';

								$options .='</optgroup>';  
							}
						}
					}else
					{

						$select = ($row['id']==$inputValue)? 'selected': '';

						$options .='<option value="'.$row['id'].'" '.$select.' >'.$row['title'].'</option>';
					}

				}

			print('												
				<div class="form-group">
					<label class="col-md-3 control-label">'.$labelText.'</label>
					<div class="col-md-4">
						<select class="form-control select2me" name="'.$inputName.'" '.$inputID.' >
							<option value="">Select...</option>
							'.$options.'
						</select>	
						'.$helpBlock.'
					</div>
				</div>
			');		
		}else
		{
			echo "Options is not array.";
		}
	}
	//level 1
	public static function selectMulti1($labelText, $inputName, $inputID, $Options, $inputValue, $helpText){
		if (is_array($Options) ) {

			$labelText = (empty($labelText)) ? '預設Label Text' : $labelText;

			$helpBlock = (!empty($helpText))? '<span class="help-block">'.$helpText.'</span>' : '<span class="help-block"></span>';

			$inputID = (!empty($inputID)) ? 'id="'.$inputID.'"' : 'id="'.$inputName.'"' ;

			$html = '';

			foreach ($Options as $row) {

				if (is_array($row)) {
					//如果沒有傳值
					if (!empty($inputValue) AND isset($inputValue)) {
						if (in_array($row['id'], $inputValue)) {
							$select = 'selected';
						}else{
							$select = '';
						}
						$html.='<option value="'. $row['id'] .'" '.$select.'>'.$row['title'].'</option>';
					}else{
						$html.='<option value="'. $row['id'] .'" >'.$row['title'].'</option>';
					}

				}else{

					//如果沒有傳值
					if (!empty($inputValue) AND isset($inputValue)) {

						if (in_array($row['title'], $inputValue) OR in_array($row['id'], $inputValue)) {
							$select = 'selected';
						}else{
							$select = '';
						}
							$html.='<option value="'. $row['id'] .'" '.$select.'>'.$row['title'].'</option>';
					}else{
							$html.='<option value="'. $row['id'] .'" >'.$row['title'].'</option>';
					}
				}

			}

			print('												
					<div class="form-group">
						<label class="control-label col-md-3">'.$labelText.'</label>
						<div class="col-md-4">
							<select id="select2_sample2" class="form-control select2" multiple name="'.$inputName.'">
							'.$html.'
							</select>
						</div>
					</div>
			');
		}else{
			echo "Options is not array.";
		}
		
	}

	//多選下拉

	public static function selectMulti2($labelText, $inputName, $inputID, $Options, $inputValue, $helpText){
		if (is_array($Options) ) {

			$labelText = (empty($labelText)) ? '預設Label Text' : $labelText;

			$helpBlock = (!empty($helpText))? '<span class="help-block">'.$helpText.'</span>' : '<span class="help-block"></span>';

			$inputID = (!empty($inputID)) ? 'id="'.$inputID.'"' : 'id="'.$inputName.'"' ;

			$html = '';

			foreach ($Options as $key => $value) {

				if (is_array($value)) {
					$html.='<optgroup label="'.$key.'">';
						foreach ($Options[$key] as $key2=>$value2 ) {

							//如果沒有傳值
							if (!empty($inputValue) AND isset($inputValue)) {
								if (in_array($value2, $inputValue)) {
									$select = 'selected';
								}else{
									$select = '';
								}
								$html.='<option value="'.$value2 .'" '.$select.'>'.$key2.'</option>';
							}else{
								$html.='<option value="'.$value2 .'" >'.$key2.'</option>';
							}

						}
					$html.='</optgroup">';

				}else{

					//如果沒有傳值
					if (!empty($inputValue) AND isset($inputValue)) {

						if (in_array($value, $inputValue) OR in_array($key, $inputValue)) {
							$select = 'selected';
						}else{
							$select = '';
						}
							$html.='<option value="'.$key .'" '.$select.'>'.$value.'</option>';
					}else{
							$html.='<option value="'.$key .'" >'.$value.'</option>';
					}
				}

			}

			print('												
					<div class="form-group">
						<label class="control-label col-md-3">'.$labelText.'</label>
						<div class="col-md-4">
							<select id="select2_sample2" class="form-control select2" multiple name="'.$inputName.'">
							'.$html.'
							</select>
						</div>
					</div>
			');
		}else{
			echo "Options is not array.";
		}
		
	}

	//文字區塊
	public static function textArea($labelText, $inputName, $inputID,$inputValue,$helpText, $disabled, $type){

		$labelText = (empty($labelText)) ? '預設Label Text' : $labelText;

		$helpBlock = (!empty($helpText))? '<span class="help-block">'.$helpText.'</span>' : '<span class="help-block"></span>';

		$inputID = (!empty($inputID)) ? 'id="'.$inputID.'"' : 'id="'.$inputName.'"' ;

		$disabled = ( !empty($disabled) AND $disabled == 'disabled' )? 'disabled="disabled"' : '';
		if ($type == 'table') 
		{
			return '<textarea name="'.$inputName.'" '.$inputID.' class="form-control manainput" rows="5" ' .$disabled. '>'.$inputValue.'</textarea>';
		}
		else
		{
			print('												
				<div class="form-group">
					<label class="col-md-3 control-label">'.$labelText.'</label>
					<div class="col-md-6">
						<textarea name="'.$inputName.'" '.$inputID.' class="form-control manainput" rows="5" ' .$disabled. '>'.$inputValue.'</textarea>
						'.$helpBlock.'
					</div>
				</div>
			');	
		}


	}

	//photo
	public static function photo($labelText, $inputName, $inputValue, $helpText ,$type=''){

		$img = (!empty($inputValue))? '<img src="'.$inputValue.'" alt=""/>' : '<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>';

		if (empty($type) OR $type ==='form') 
		{

			$labelText = (empty($labelText)) ? '預設Label Text' : $labelText;

			$helpBlock = (!empty($helpText))? '<span class="help-block">'.$helpText.'</span>' : '<span class="help-block"></span>';

			print('		
				<div class="form-group">
				    <label class="control-label col-md-3">'.$labelText.'</label>
				    <div class="col-md-6">
						<div class="fileinput fileinput-new" data-provides="fileinput">

							<div class="fileinput-new thumbnail" id="showPic" style="width: 200px; height: 150px;">'.$img.'</div>
							<div>
								<span class="btn default btn-file">
									<span class="fileinput-new"> Select image </span>
									<input type="text" name="'.$inputName.'" id="filePicker" value="'.$inputValue.'" >
								</span>
							</div>
						</div>
						'.$helpBlock.'
				    </div>
				</div>										
			');	
		}else if($type ==='table')
		{
			return '		
				<div class="fileinput fileinput-new" data-provides="fileinput">

					<div class="fileinput-new thumbnail" id="showPic" style="width: 200px; height: 150px;">'.$img.'</div>
					<div>
						<span class="btn default btn-file">
							<span class="fileinput-new"> Select image </span>
							<input type="text" name="'.$inputName.'" id="filePicker" value="'.$inputValue.'" >
						</span>
					</div>
				</div>							
			';
		}
	
	}

	//input 
	public static function tags($labelText,$inputName,$inputValue,$helpText){

		$labelText = (empty($labelText)) ? '預設Label Text' : $labelText;

		$helpBlock = (!empty($helpText))? '<span class="help-block">'.$helpText.'</span>' : '<span class="help-block"></span>';

		print('												
			<div class="form-group">
				<label class="col-md-3 control-label">'.$labelText.'</label>
				<div class="col-md-6">
					<input id="tags_2" type="text" name="'.$inputName.'" class="form-control tags medium" value="'.$inputValue.'"/>
					'.$helpBlock.'
				</div>
			</div>
		');

	}

	public static function editor($labelText,$inputName,$inputValue,$helpText,$type)
	{
		$labelText = (empty($labelText)) ? '預設Label Text' : $labelText;

		$helpBlock = (!empty($helpText))? '<span class="help-block">'.$helpText.'</span>' : '<span class="help-block"></span>';

		if($type =='table')
		{
			return'
				<textarea class="wysihtml5 form-control" name="'.$inputName.'" rows="15" >'.$inputValue.'</textarea>
			';			
		}
		else
		{
/*			print('
				<div class="form-group">
					<label class="col-md-3 control-label">'.$labelText.'</label>
					<div class="col-md-6">
						<textarea class="wysihtml5 form-control" name="'.$inputName.'" rows="15" >'.$inputValue.'</textarea>
						'.$helpBlock.'
					</div>
				</div>
			');	*/	
			print('
				<div class="form-group">
					<label class="col-md-3 control-label">'.$labelText.'</label>
					<div class="col-md-6">
						<textarea class="ckeditor form-control" name="'.$inputName.'" rows="25" >'.$inputValue.'</textarea>
						'.$helpBlock.'
					</div>
				</div>
			');		
		}

	}

	public static function colorPicker($labelText,$inputName,$inputValue,$helpText,$type)
	{
		$labelText = (empty($labelText)) ? '預設Label Text' : $labelText;

		$helpBlock = (!empty($helpText))? '<span class="help-block">'.$helpText.'</span>' : '<span class="help-block"></span>';

		if($type == 'table')
		{

			return '<input class="mycolor" name="'.$inputName.'" value="'.$inputValue.'" style="width:100px;">';			
		}
		else
		{
			print('
				<div class="form-group">
					<label class="col-md-3 control-label">'.$labelText.'</label>
					<div class="col-md-6">
						<input class="form-control mycolor"  id="mycolor"  name="'.$inputName.'" value="'.$inputValue.'" style="width:150px;">

						'.$helpBlock.'
					</div>
				</div>
			');			
		}

	}
	//檔案欄位
	public static function filePicker($labelText,$inputName,$inputValue,$helpText,$type)
	{
		$labelText = (empty($labelText)) ? '預設Label Text' : $labelText;

		$helpBlock = (!empty($helpText))? '<span class="help-block">'.$helpText.'</span>' : '<span class="help-block"></span>';

		if($type == 'table')
		{

			return '<input name="'.$inputName.'" value="'.$inputValue.'" style="width:100px;">
					<a href="javascript:;" data-name="'.$inputName.'" id="filePicker" class="btn default btn-file">Select File</a>';			
		}
		else
		{
			print('
				<div class="form-group">
					<label class="col-md-3 control-label">'.$labelText.'</label>
					<div class="col-md-6">
						<input class="form-control"  name="'.$inputName.'" value="'.$inputValue.'" >
						<a href="javascript:;" data-name="'.$inputName.'" id="filePicker" class="btn default btn-file">Select File</a>
						'.$helpBlock.'
					</div>
				</div>
			');			
		}

	}

	//日期挑選
	public static function datePicker($labelText,$inputName,$inputValue,$helpText,$type)
	{
		$labelText = (empty($labelText)) ? '預設Label Text' : $labelText;

		$helpBlock = (!empty($helpText))? '<span class="help-block">'.$helpText.'</span>' : '<span class="help-block"></span>';

		if($type == 'table')
		{

			return '<input name="'.$inputName.'" value="'.$inputValue.'" style="width:100px;">
					<a href="javascript:;" data-name="'.$inputName.'" id="multi-add" class="btn default btn-file">Select File</a>';			
		}
		else
		{
			print('
				<div class="form-group">
					<label class="control-label col-md-3">'.$labelText.'</label>
					<div class="col-md-3">
						<input class="form-control form-control-inline input-medium date-picker" name="'.$inputName.'" size="16" type="text" value="'.$inputValue.'"/>
						'.$helpBlock.'
					</div>
				</div>

			');			
		}

	}

	//日期挑選
	public static function numberInput($labelText,$inputName,$inputValue,$helpText,$type)
	{
		$labelText = (empty($labelText)) ? '預設Label Text' : $labelText;

		$helpBlock = (!empty($helpText))? '<span class="help-block">'.$helpText.'</span>' : '<span class="help-block"></span>';

		if($type == 'table')
		{

			return '<input name="'.$inputName.'" value="'.$inputValue.'" style="width:100px;">
					<a href="javascript:;" data-name="'.$inputName.'" id="multi-add" class="btn default btn-file">Select File</a>';			
		}
		else
		{
			print('
				<div class="form-group">
					<label class="control-label col-md-3">'.$labelText.'</label>
					<div class="col-md-3">
						<input class="form-control form-control-inline input-medium mask_currency" name="'.$inputName.'" size="16" type="text" value="'.$inputValue.'"/>
						'.$helpBlock.'
					</div>
				</div>

			');			
		}

	}

	public static function url( $path )
	{
        //2015.9.23 加入語系路徑
        $path = (!empty(Config::get('app.dataBasePrefix')) )? App::getLocale().'/'.$path : $path;
        
        return url( $path );
	}

}