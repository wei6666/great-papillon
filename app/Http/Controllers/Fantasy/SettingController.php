<?php

namespace App\Http\Controllers\Fantasy;

use App\Http\Controllers\CRUDBaseController;
use Redis;
use Cache;
/**相關Models**/

use App\Http\Models\Banner;


class SettingController extends CRUDBaseController
{

    public $ajaxEditLink = 'Fantasy/Setting/ajax-list/';

    protected $modelName = "Setting";

    public $index_select_field = ['id','rank','is_visible'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = '網站基本設定';

    public $viewPreFix = 'Setting';

   

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-success',
                "showText" => 'S'
            )
        ),
    );


    public $cacheData = [
        'active' => true,
        'select' => [],
        'order' => 'rank',
        'sort' => 'asc'
    ];

   

    public $modelHas = [

       "MenuList" => [
            "modelName" => "MenuList",
            "storedName" => "MenuList", 
            "parent" => "set_id",
            "filed" => 'id',
            "select" => ['id','rank','is_visible','title','image','link','set_id','content','color','place','en_content'],
            
            ],


       
    ];

       public $saveSubData = [
                [  
                    "modelName" => "MenuList",
                    "requestModelName" => "MenuList",
                    "to_parent" => "set_id",

                    
                ],
  

              
                
            ];

      public $photoTable = [
            'photo' => [
                "排序" => "rank",
                "連結" => "link",
                "描述" => "content",
                "英文描述" => "en_content",
                "內容位置" => "place",                              
                "狀態" => "is_visible",
                "內容顏色" => "color",
                "圖片(960 x 1080)" => "image",
                
               
                
               
            ],

            
        ];    

     
       
}
