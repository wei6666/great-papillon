<?php namespace App\Http\Controllers\Fantasy;

use App\Http\Controllers\Fantasy\BackendController;
use Illuminate\Http\Request;

/**Models**/
use App\Models\WorkPhoto as WorkPhoto;

class RankController extends BackendController {

	public function __construct()
	{
		parent::__construct();

	}

	public static function changeRankUp(Request $request)
	{

		if($request->input('_token') !='' AND $request->input('_token') === csrf_token())
		{
			$data = $request->all();

			//取得這筆資料
			$model = WorkPhoto::find($data['id']);

			$checkField = $model->$data['check'];//所屬資料的檢查欄位例如 work_id -> Work::id
			$thisOrgRank = $model->rank;//目前的rank值
			$changeRank = ($thisOrgRank)-1; //Rank值往上-1，往下+1

			//$thisForgetID = $model->$data['check'];

			//如果這一個changeRank已經不是第一筆
			$find_old = WorkPhoto::where($data['check'], '=',$checkField)
									->where('rank','=', $changeRank);

			if ($find_old->count() == 0) 
			{
				//負數不執行 
				if ($changeRank > 0 || $changeRank == 0) {
					$model->rank = $changeRank;

					if($model->save())
					{
						//echo "Rank已更換為 ： ".$changeRank;
						echo 
							'{
								"message" : "ture",
								"rank" : "'.$changeRank.'"
							}';
						echo 'ture';
					}
				}else
				{
					echo "負數不執行Rank更換.";
				}

			}else
			{	
				//echo "有重複的Rank.";
				//找到Rank值相同的資料
				$same = WorkPhoto::where($data['check'],'=',$checkField)->where('rank','=',$changeRank)->get();

				$model->rank = $changeRank;
				$same[0]->rank = $thisOrgRank;

				//儲存
				if($model->save())
				{
					if($same[0]->update())
					{
						echo '
						{
							"message" : "ture",
							"rank" : "'.$model->rank.'",
							"prevRank" : "'.$same[0]->rank.'"
						}
						';
					}
				}

			}	
		}
	}

	public static function changeRankDown(Request $request)
	{

	}

}
