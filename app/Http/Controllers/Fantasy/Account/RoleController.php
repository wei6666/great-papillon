<?php namespace App\Http\Controllers\Fantasy\Account;

use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Requests;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

use App\Http\Models\Role;

class RoleController extends BackendController {

	public function __construct(){
		parent::__construct();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('/Fantasy/Role/index')->with('Role',Role::all());
	}

	public function Add()
	{
		return view('/Fantasy/Role/add')->with('Role',Role::all());
	}

	public function Edit($id ,Request $request){

		return view('/Fantasy/Role/edit')->with('Role',Role::find($id));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		if($request->input('_token') !='' AND $request->input('_token') === csrf_token()){
			//新增
			$new = new Role;
				foreach ($request->input('Role') as $key => $value) {
					if ($key ==='permission') 
					{
						$new->$key = json_encode($value);
					}else
					{
						$new->$key = $value;
					}
					
				}
				if($new->save()){
					echo "<script>
							alert('儲存成功');
							window.location = '/Fantasy/Role/Edit/$new->id';
						</script>";

				}else{
					echo "save fail !";
				}					
		}
	}

	public function update(Request $request)
	{
		if($request->input('_token') !='' AND $request->input('_token') === csrf_token()){
			//新增
			$id = $request->input('id');
			$Role = Role::find($id);

				foreach ($request->input('Role') as $key => $value) {
					if ($key ==='permission') {
						$Role->$key = json_encode($value);
					}else{
						$Role->$key = $value;
					}
					
				}

				if($Role->save()){
					echo "<script>
							alert('儲存成功');
							window.location = '/Fantasy/Role/Edit/$id';
						</script>";

				}else{
					echo "save fail !";
				}					
		}
	}

}
