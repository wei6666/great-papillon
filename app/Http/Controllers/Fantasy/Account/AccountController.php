<?php namespace App\Http\Controllers\Fantasy\Account;

use App\Http\Controllers\Fantasy\BackendController;
use App\Http\Requests;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

use App\User;
use App\Models\Role;

class AccountController extends BackendController {

	public function __construct(){
		parent::__construct();

		$Permission = Array();
		$Role = Role::where('is_visible','=','1')->get();

		foreach ($Role as $row) {
			$Permission[$row['id']] = $row['title'];
		}
		var_dump($Permission);die;
		view::share('Permissions',$Permission);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('/Fantasy/Account/index')->with('Users',User::all());
	}

	public function Add(){
		
		return view('/Fantasy/Account/add');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return view("/Fantasy/Account/edit")->with('UD',User::find($id));
	}



	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$data = $request->input();
		$data['password'] = bcrypt($data['password']);
		$data['character'] = json_encode($data['character']);


		if (!empty($data['_token']) AND $data['_token'] === csrf_token() ) {
			$user = new User;
				foreach ($data as $key=>$value) {
					if ($key != '_token' AND $key != 'password_confirmation') {
						$user->$key= $value;
					}
					
				}
				if ($user->save()) {
					echo " Create User success.";
					echo "<script>
							alert('儲存成功');
							window.location = '/Fantasy/Account/Edit/$user->id';
						</script>";
				}else{
					echo "Create User Fail.";
				}
/*			User::create([
				'name' => $data['name'],
				'email' => $data['email'],
				'password' => bcrypt($data['password']),
				'is_visible'=> $data['is_visible'],
				'character' => $data['character']
			]);	*/
		}else{
			return false;
		}
	
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @return Response
	 */
	public function update(Request $request)
	{
		$psot_token = $request->input('_token');
		
		if (!empty($psot_token) AND $psot_token === csrf_token() ) {
			$update = $request->input('Account');
			$character = $request->input('character');

			$id = $request->input("id");

			$find = User::find($id);

				foreach ($update as $key => $value) {
					$find->$key = $value;
				}
				$find->character = json_encode($character);
				if ($find->save()) {
					echo "<script>
							alert('儲存成功');
						</script>";
				}else{
					echo "Create User Fail.";
				}			
		}else{
			die;
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
