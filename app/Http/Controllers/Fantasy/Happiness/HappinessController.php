<?php
namespace App\Http\Controllers\Fantasy\Happiness;

use App\Http\Controllers\CRUDBaseController;
use Redis;
use Cache;
/**相關Models**/

use App\Http\Models\Banner;


class HappinessController extends CRUDBaseController
{

    public $ajaxEditLink = 'Fantasy/Happiness/幸福好評管理/ajax-list/';


    protected $modelName = "Happiness";

    public $index_select_field = ['id','rank','is_visible','title',"category_id","is_visible","is_home"];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = 'Happiness/幸福好評管理';

    public $viewPreFix = 'Happiness';
   
    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-success',
                "showText" => 'S'
            )
        ),
    );


    public $cacheData = [
        'active' => true,
        'select' => [],
        'order' => 'rank',
        'sort' => 'asc'
    ];

    public $modelBelongs = [

       
        "Product" => [
            "parent" => "category_id",
            "filed" => 'id',
            "select" => ['title','id']
        ],
        "ProductCategory" => [
            "parent" => "category_id",
            "filed" => 'id',
            "select" => ['title','id']
        ],
         "HappinessCategory" => [
            "parent" => "category_id",
            "filed" => 'id',
            "select" => ['title','id']
        ],
         "Location" => [
            "parent" => "category_id",
            "filed" => 'id',
            "select" => ['title','id']
        ]
    ];

  

       
}

