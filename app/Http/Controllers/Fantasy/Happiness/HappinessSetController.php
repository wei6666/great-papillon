<?php

namespace App\Http\Controllers\Fantasy\Happiness;

use App\Http\Controllers\CRUDBaseController;
use Redis;
use Cache;
/**相關Models**/

use App\Http\Models\Banner;


class HappinessSetController extends CRUDBaseController
{

    public $ajaxEditLink = 'Fantasy/Happiness/幸福好評設定/ajax-list/';

    protected $modelName = "HappinessSet";

    public $index_select_field = ['id','rank','is_visible','title'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = 'Happiness/幸福好評設定';

    public $viewPreFix = 'HappinessSet';

   

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-success',
                "showText" => 'S'
            )
        ),
    );


    public $cacheData = [
        'active' => true,
        'select' => [],
        'order' => 'rank',
        'sort' => 'asc'
    ];

     public $modelHas = [

       "HappinessCategory" => [
            "modelName" => "HappinessCategory",
            "storedName" => "HappinessCategory", 
            "parent" => "category_id",
            "filed" => 'id',
            "select" => ['id','rank','is_visible','title'],
            
            ],
       
    ];

       public $saveSubData = [
                [   // 產品介紹的多圖
                    "modelName" => "HappinessCategory",
                    "requestModelName" => "HappinessCategory", //多圖的inputName
                    "to_parent" => "category_id",
                ],
                
            ];

      public $photoTable = [
            'photo' => [
                "排序" => "rank",               
                "狀態" => "is_visible",
                "位置" => "title",
                
            ],
            
        ];   




       
}
