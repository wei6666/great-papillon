<?php

namespace App\Http\Controllers\Fantasy\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\CRUDBaseController;
use Redis;
use Cache;
use ItemMaker;
use View;

class MemberServiceController extends CRUDBaseController
{	/****
    後台開始
     ****/

    public $ajaxEditLink = 'Fantasy/Member/Service/ajax-list/';

    protected $modelName = "MemberService";

    public $index_select_field = ['id','is_visible','title','rank'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = 'Member/Service';

    public $viewPreFix = 'Member/Service';

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-success',
                "showText" => 'S'
            )
        ),
    );

    public $cacheData = [
        'active' => true,
        'select' => [],
        'order' => 'id',
        'sort' => 'desc'
    ];
    public $photoTable = [
        'MemberServicedata' => [
            "排序" => "rank",
            "啟用" => "is_visible",
            "內容" => "content",
        ],
    ];

    public $modelHas = [
        "MemberServicedata" => [
            "parent" => "service_id",
            "filed" => 'id',
            "select" => ['id','rank','content','service_id','is_visible'],
        ],
    ];

    public $saveSubData = [
        "MemberServicedata" => [
            "modelName" => "MemberServicedata",
            "to_parent" => "service_id"
        ],
    ];
}