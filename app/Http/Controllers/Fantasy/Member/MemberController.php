<?php

namespace App\Http\Controllers\Fantasy\Member;
use App\Http\Controllers\Fantasy\MakeItemV2;
use Illuminate\Http\Request;
use App\Http\Controllers\CRUDBaseController;
use App\Http\Controllers\Fantasy\BackendController;
use Redis;
use Cache;
use ItemMaker;
use View;
use Route;

class MemberController extends CRUDBaseController
{	/****
    後台開始
     ****/

    public $ajaxEditLink = 'Fantasy/Member/List/ajax-list/';

    protected $modelName = "Member";

    public $index_select_field = ['id','is_visible','name','mobile','email','city','mbid_city','mbid_no'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = 'Member/List';

    public $viewPreFix = 'Member';

    public $modelBelongs = [
        "City" => [
            "parent" => "city",
            "filed" => 'id',
            "select" => ['title','id']
        ],
    ];

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-success',
                "showText" => 'S'
            )
        ),
    );

    public $otherOptions = [                
        [
            "id" => "1",
            "title" => '男',
        ],
        
        [
            "id" => "2",
            "title" => '女',
        ]
    ];

    public $options = [                
        [
            "id" => "1",
            "title" => '會員生日禮',
        ],
        
        [
            "id" => "2",
            "title" => '購買回饋',
        ],
        
        [
            "id" => "3",
            "title" => '訂單折抵',
        ],
        
        [
            "id" => "4",
            "title" => '好友推薦',
        ],
        
        [
            "id" => "5",
            "title" => '首次消費',
        ],
        
        [
            "id" => "6",
            "title" => '會員註冊',
        ]
    ];

    public $cacheData = [
        'active' => true,
        'select' => [],
        'order' => 'id',
        'sort' => 'desc'
    ];
    public $photoTable = [
        'Bonus' => [
            "類別" => "sel_type",
            "點數" => "bonus",
            "詳細內容" => "detail",
            "獲得日期" => "created_at",
            "已加入會員資料" => "is_add"
        ],
    ];

    public $modelHas = [
       
        "Bonus" => [
            "modelName" => "Bonus",
            "storedName" => "Bonus", 
            "parent" => "mb_id",
            "filed" => 'id',
            "select" => ['id','sel_type','bonus','detail','created_at','is_add'],
            
            
        ],
    ];

    public $saveSubData = [
        [ 
                    "modelName" => "Bonus",
                    "requestModelName" => "Bonus",
                    "to_parent" => "mb_id",
        ],
    ];

    public function postUpdate(Request $request)
    {

        if(!empty($request->input('method')) AND $request->input('method')=='ajaxEdit')
        {
            parent::updateOne( $request->input($this->modelName), $this->modelName, 'ajaxEdit');
            //將資料做暫存
            $this->setCacheData();
        }
        else
        {

            $Datas = $request->input($this->modelName);

            if( !empty( $Datas['password'] ) )
            {
                $Datas['password'] = bcrypt($Datas['password']);
            }
            else
            {
                unset( $Datas['password'] );
            }

            if( parent::updateOne( $Datas, $this->modelName, '') )
            {

                $this->checkSavedSubData($request->all(), $Datas['id']);

                //通知信
                $this->sendNoticeMail($Datas['id'], 'reply');

                //將資料做暫存
                $this->setCacheData();

                
                return redirect( MakeItemV2::url('Fantasy/'.$this->routePreFix.'/edit/'.$Datas['id']) )->with('Message','修改成功');
            }

        }
    }
}