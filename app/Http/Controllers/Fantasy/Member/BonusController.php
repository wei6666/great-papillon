<?php

namespace App\Http\Controllers\Fantasy\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\CRUDBaseController;
use Redis;
use Cache;
use ItemMaker;
use View;

class BonusController extends CRUDBaseController
{	/****
    後台開始
     ****/

    public $ajaxEditLink = 'Fantasy/Member/Bonus/ajax-list/';

    protected $modelName = "BonusRule";

    public $index_select_field = ['id','is_visible','title','rank'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = 'Member/Bonus';

    public $viewPreFix = 'Member/Bonus';

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-success',
                "showText" => 'S'
            )
        ),
    );

    public $cacheData = [
        'active' => true,
        'select' => [],
        'order' => 'id',
        'sort' => 'desc'
    ];
}