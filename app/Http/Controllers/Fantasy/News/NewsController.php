<?php
namespace App\Http\Controllers\Fantasy\News;

use App\Http\Controllers\CRUDBaseController;
use Redis;
use Cache;
/**相關Models**/

use App\Http\Models\Banner;


class NewsController extends CRUDBaseController
{

    public $ajaxEditLink = 'Fantasy/News/最新消息管理/ajax-list/';


    protected $modelName = "News";

    public $index_select_field = ['id','rank','is_visible','title',"category_id",'is_home'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = 'News/最新消息管理';

    public $viewPreFix = 'News';
   
    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-success',
                "showText" => 'S'
            )
        ),
    );

    public $modelBelongs = [

       
        "NewsCategory" => [
            "parent" => "category_id",
            "filed" => 'id',
            "select" => ['title','id']
        ]
    ];


    public $cacheData = [
        'active' => true,
        'select' => [],
        'order' => 'rank',
        'sort' => 'asc'
    ];

     public $modelHas = [

       "NewsContent" => [
            "modelName" => "NewsContent",
            "storedName" => "NewsContent", 
            "parent" => "news_id",
            "filed" => 'id',
            "select" => ['id','rank','is_visible'],
            
            ],
        "NewsPhoto" => [
            "modelName" => "NewsPhoto",
            "storedName" => "NewsPhoto", 
            "parent" => "news_id",
            "filed" => 'id',
            "select" => ['id','rank','is_visible','image'],
            
            ],
       
    ];

       public $saveSubData = [
                [   // 產品介紹的多圖
                    "modelName" => "NewsContent",
                    "requestModelName" => "NewsContent", //多圖的inputName
                    "to_parent" => "news_id",
                ],
                [   // 產品介紹的多圖
                    "modelName" => "NewsPhoto",
                    "requestModelName" => "NewsPhoto", //多圖的inputName
                    "to_parent" => "news_id",
                ],
                
            ];

      public $photoTable = [
            'go_news_content' => [
                "排序" => "rank",               
                "狀態" => "is_visible",
                "編輯" => "go_news_content",
            ],
            'photo' => [
                "排序" => "rank",               
                "狀態" => "is_visible",
                "圖片(950 x 640 or 450 x 640)" => "image",
            ],
            
        ];   

  

       
}

