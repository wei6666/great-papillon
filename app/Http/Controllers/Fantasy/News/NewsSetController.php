<?php

namespace App\Http\Controllers\Fantasy\News;

use App\Http\Controllers\CRUDBaseController;
use Redis;
use Cache;
/**相關Models**/

use App\Http\Models\Banner;


class NewsSetController extends CRUDBaseController
{

    public $ajaxEditLink = 'Fantasy/News/最新消息設定/ajax-list/';

    protected $modelName = "NewsSet";

    public $index_select_field = ['id','rank','is_visible','title'];

    //public $ajaxEditField = ['id', 'rank','title', 'is_visible'];

    public $routePreFix = 'News/最新消息設定';

    public $viewPreFix = 'NewsSet';

   

    public $ajaxEditList = Array(
        /******
            設定規則
            "資料欄位"=>Array("輸入欄位類型",是否可以被編輯)
            static 則是一個狀態群組   Exp: 是否顯示首頁、是否顯示等等
        *****/
        "排序" => Array(
            "field" => "rank",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "標題" => Array(
            "field" => "title",
            "inputType" => "text",
            "is_edit"=> true
        ),
        "顯示狀態" => Array(
            "是否顯示" => Array(
                "field" => "is_visible",
                "inputType" => "radio",
                "showColor" => 'label-success',
                "showText" => 'S'
            )
        ),
    );


    public $cacheData = [
        'active' => true,
        'select' => [],
        'order' => 'rank',
        'sort' => 'asc'
    ];

     public $modelHas = [

       "NewsCategory" => [
            "modelName" => "NewsCategory",
            "storedName" => "NewsCategory", 
            "parent" => "news_id",
            "filed" => 'id',
            "select" => ['id','rank','is_visible','title'],
            
            ],
       
    ];

       public $saveSubData = [
                [   // 產品介紹的多圖
                    "modelName" => "NewsCategory",
                    "requestModelName" => "NewsCategory", //多圖的inputName
                    "to_parent" => "news_id",
                ],
                
            ];

      public $photoTable = [
            'photo' => [
                "排序" => "rank",               
                "狀態" => "is_visible",
                "標題" => "title",
            ],
            
        ];   




       
}
