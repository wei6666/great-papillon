<?php namespace App\Http\Controllers\Fantasy;

use Illuminate\Routing\Controller as BaseController;
//use App\Http\Controllers\Fantasy\MakeItem;
use ItemMaker;

class MakeForm extends BaseController {
	/***
		產生產品規格

		$tabelTags -> 表格抬頭thead的標籤

		$datas -> 資料

	***/
	public static function SpecTable($tabelTags,$datas = Array()){
		if(is_array($tabelTags)){

			$shows = Array();
			$thead ='';

			foreach ($tabelTags as $key => $value) {
				if ($value === 'Image' OR $value === 'image'){

					$thead.= '<th width="12%">'.$value.'</th>';

				}else{

					$thead.= '<th>'.$value.'</th>';

				}
				$shows[] = $value;
			}
			$tbody = '';
			if (count($datas)>0) {
				$count = 0;
				foreach ($datas as $row) {
					$tbody.='<tr>'; 

					//拆解表格內容
					$formContent = json_decode($row['form_details'],true);
					$sort = $count+1;
					$tbody.='<td width="2%" align="center"><label><h1>'.$sort.'</h1><input type="hidden" name="Spec['.$count.'][id]" value="'.$row['id'].'"></label></td>';

					//讓表格內容可以依照設定的順序顯示
					foreach ($tabelTags as $key => $value) {
						//圖片
						if ($value === 'Image' OR $value === 'image') {

							$tbody.='<td>'.ItemMaker::photo('', 'Spec['.$count.'][image]', $row['image'] ,'' , 'table').'</td>';
							
						}else{
							if (!array_key_exists($value, $formContent)) {
								$tbody.='<td><label><input type="text" name="Spec['.$count.']['.$value.']" value=""></label></td>';
							}else{
								$tbody.='<td><label><input type="text" name="Spec['.$count.']['.$value.']" value="'.$formContent[$value].'"></label></td>';
							}
							
						}
					}

					$tbody.='<td>
								<input type="hidden" name="thisID_'.$row['id'].'" id="thisID_'.$row['id'].'" value="'.$row['id'].'">

								<a name="rank_up" class="btn purple" ng-click="moveUp('.$row['id'].')"><i class="fa fa-level-up"></i></a>
								<a name="rank_down" class="btn purple" ng-click="moveDown('.$row['id'].')"><i class="fa fa-level-down"></i> </a>

								<a href="javascript:;" class="btn red" id="dataDel"  data-id="'.$row['id'].'" data-model="Spec"><i class="fa fa-times"></i></a>
							</td>
											
					';
					$tbody.='</tr>';
					$count++;	
				}
			}else{
				$span = count($tabelTags)+2;
				$tbody = '<tr><td colspan="'.$span.'" align="center"> No Datas.</td></tr>';
			}

		print('

			<table class="table table-bordered table-hover" id="specTable" name="Spec" data-tags="'.implode(",",$shows).'">
				<thead >
					<tr role="row" class="heading" id="FormTags">
					<th width="10%"></th>
						'.$thead.'
						<th width="10%"></th>
					</tr>
				</thead>
				<tbody id="FormLine">
					'.$tbody.'
				</tbody>
			</table>
			');			
		}else{
			echo "Table Tags Isn't Array. " ;
		}
	}
	/***
		相關圖片表格

		$tabelTags -> 表格抬頭thead的標籤

		$datas -> 資料

		$FilePath-> 上傳路徑
	***/

	public static function  photosTable( $set = [] )
	{
		$nameGroup = ( !empty( $set['nameGroup'] ) )? $set['nameGroup']: '';
		$datas = ( !empty( $set['datas'] ) )? $set['datas'] : [];
		$table_set= ( !empty( $set['table_set'] ) )? $set['table_set']: [];

		if (is_array($table_set))
		{
			//存放要顯示的資料庫欄位
			$shows = Array();
			$thead ='';
			$nameGroup = ( preg_match("/-/", $nameGroup) )? (explode('-', $nameGroup)) : $nameGroup;
			$prefix = ( is_array($nameGroup) )? $nameGroup[1] : $nameGroup; 
			$modelGrop = ( is_array($nameGroup) )? $nameGroup[0].'/'.$prefix : $prefix ;

			//產生標頭
			foreach ($table_set as $key => $value) {
				$thead.=($key == 'Image' OR $key == 'image') ? '<th width="15%">'.$key.'</th>' : '<th>'.$key.'</th>';
				$shows[] = $value;
			}

			$tbody = '';

			if (count($datas) > 0)
			{
				$count = 0;
				foreach ($datas as $row) 
				{
					$tbody.='<tr>';
					
						foreach ($table_set as $row2) 
						{

							//if ($row2 =='image' OR $row2 == 'images') 
							if(preg_match("/image/", $row2))
							{
								$img = (!empty($row[$row2])) ? '<img src="'.$row[$row2].'" >' : '<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>';
								
								$tbody.='<td>'.ItemMaker::photo('', $prefix.'['.$count.']['.$row2.']', $row[$row2] ,'' , 'table').'</td>';
							}else if($row2 == 'rank')
							{
								$tbody .= '<td align="center" id="sortNum"> 
												<h1>'.($count+1).'</h1>
												<input type="hidden" name="'.$prefix.'['.$count.']['.$row2.']" value="'.$row[$row2].'">
											</td>';
							}
							else if( preg_match("/color/", $row2) )
							{
								$tbody .= '<td>'.ItemMaker::colorPicker('', $prefix.'['.$count.']['.$row2.']', $row[$row2], '','table' ).'</td>';

							}
							else if( $row2 == 'skill')
							{							
								$tbody .= '<td>'.ItemMaker::textArea('', $prefix.'['.$count.']['.$row2.']', '', $row[$row2],'', '', 'table').'</td>';

							}
							else if( $row2 == 'features')
							{							
								$tbody .= '<td>'.ItemMaker::editor('',$prefix.'['.$count.']['.$row2.']', $row[$row2],'','table').'</td>';

							}
							else if( $row2 == 'time')
							{							
								$tbody .= '
								<td>
									<table class="table table-bordered table-hover" data-model ="'.$prefix.'" data-tags="time">
										<thead>
											<tr>
												<th>
													&nbsp;
												</th>
												<th>
													<a href="javascript:;" id="addItem_min" data-tags="time" class="btn green" ><i class="fa fa-plus"></i> </a>
												</th>
											</tr>
										</thead>
										<tbody id="itemTbody">
	
										</tbody>
									</table>
								</td>
								';

							}
							else if( preg_match("/is_/", $row2)  )
							{	
								if($row2 == 'is_perc')
								{
									$tbody .= '<td>'.ItemMaker::radio('', $prefix.'['.$count.']['.$row2.']', '', ['25%','50%'], $row[$row2], '','table' ).'</td>';
								}
								else
								{
									$tbody .= '<td>'.ItemMaker::radio('', $prefix.'['.$count.']['.$row2.']', '', ['否','是'], $row[$row2], '','table' ).'</td>';
								}						
								

							}
							else
							{
								$tbody .='<td><input name="'.$prefix.'['.$count.']['.$row2.']" value="'.$row[$row2].'" /></td>';
							}
							
						}
						$tbody.='<td>
									<input type="hidden" name="'.$prefix.'['.$count.'][id]" value="'.$row['id'].'" />
									<input type="hidden" name="thisID_'.$row['id'].'" id="thisID_'.$row['id'].'" value="'.$row['id'].'">

									<a href="javascript:;" name="rank_up"  class="btn purple" data-id="'.$row['id'].'" data-rank="'.$row['rank'].'" data-model="'.$modelGrop.'"  ><i class="fa fa-level-up"></i></a>
									<a href="javascript:;" name="rank_down" class="btn purple" data-id="'.$row['id'].'" data-rank="'.$row['rank'].'" data-model="'.$modelGrop.'" ><i class="fa fa-level-down"></i> </a>

									<a href="javascript:;" class="btn red" id="dataDel"  data-id="'.$row['id'].'" data-model="'.$modelGrop.'"><i class="fa fa-times"></i></a>
								</td>
												
						';
						$count++;
					$tbody.='</tr>';
				}
			}else
			{
				$span = count($table_set)+1;
				$tbody = '<tr id="is_no_data"><td colspan="'.$span.'" align="center" > No Datas.</td></tr>';
			}
			//$isMulti = ($multi == 'yes')?'<a id="multi-add" href="javascript:;"  class="btn green" data-name="'.$prefix.'_table"><i class="fa fa-plus"> 批次新增</i></a>' : '';
			print('
					<div class="row">
						<div id="tab_images_uploader_filelist" class="col-md-6 col-sm-12">
						</div>
					</div>

					<input type="hidden" name="method" value="'.$prefix.'" >
						'.$isMulti.'
					<br>
					<table class="table table-bordered table-hover" id="'.$prefix.'_table" data-model ="'.$prefix.'" name="'.$prefix.'" data-tags="'.implode(",",$shows).'" >
						<thead>
							<tr role="row" class="heading">
								'.$thead.'
								<th width="17%">
									<a id="addItem" href="javascript:;" data-tags="'.implode(",",$shows).'" class="btn green"  >
										<i class="fa fa-plus"></i> 
									</a>
								</th>
							</tr>
						</thead>
						<tbody id="itemTbody" >
							'.$tbody.'
						</tbody>
					</table>
			');
		}else
		{
			echo "Table Tags Isn't Array. " ;
		}

	}
	/*****
		
	******/
	public static function  haveSubTable($nameGroup = '', $datas = Array() ,$table_set= Array(), $multi)
	{

		if (is_array($table_set))
		{
			//存放要顯示的資料庫欄位
			$shows = Array();
			$thead ='';
			$nameGroup = (explode('-', $nameGroup))? (explode('-', $nameGroup)) : $nameGroup;

			//產生標頭
			foreach ($table_set as $key => $value) {
				$thead.=($key == 'Image' OR $key == 'image') ? '<th width="15%">'.$key.'</th>' : '<th>'.$key.'</th>';
				$shows[] = $value;
			}

			$tbody = '';

			if (count($datas) > 0)
			{
				$count = 0;
				foreach ($datas as $key => $value) 
				{
					$tbody.='<tr>';
					$tbody .='<td><input name="'.$nameGroup[1].'['.$count.'][time_area]" value="'.$key.'" /></td>';
						$timeDatas = '';
						$subCount = 0;
						foreach ($value as $key2 => $value2) 
						{
							$timeDatas.='<tr>';
							$timeDatas.='<td><input type="text" name="'.$nameGroup[1].'['.$count.']['.$subCount.'][time]" value="'.$value2.'" /></td>';

							$timeDatas.='<td>
										<input type="hidden" name="'.$nameGroup[1].'['.$count.']['.$subCount.'][id]" value="'.$key2.'" />
										<a href="javascript:;" class="btn red" id="dataDel"  data-id="'.$key2.'" data-model="'.$nameGroup[0].'/'.$nameGroup[1].'"><i class="fa fa-times"></i></a>
									</td>';
							$timeDatas.='</tr>';
							$subCount++;
						}
						$tbody .= '
						<td>
							<table class="table table-bordered table-hover" data-model ="'.$nameGroup[1].'" data-tags="time">
								<thead>
									<tr>
										<th>
											&nbsp;
										</th>
										<th>
											<a href="javascript:;" id="addItem_min" data-tags="time" class="btn green" ><i class="fa fa-plus"></i> </a>
										</th>
									</tr>
								</thead>
								<tbody id="itemTbody">
									'.$timeDatas.'
								</tbody>
							</table>
						</td>
						';

						$count++;
					$tbody.='</tr>';
				}
			}else
			{
				$span = count($table_set)+1;
				$tbody = '<tr id="is_no_data"><td colspan="'.$span.'" align="center" > No Datas.</td></tr>';
			}
			$isMulti = ($multi == 'yes')?'<a id="multi-add" href="javascript:;"  class="btn green" data-name="'.$nameGroup[1].'_table"><i class="fa fa-plus"> 批次新增</i></a>' : '';
			print('
					<div class="row">
						<div id="tab_images_uploader_filelist" class="col-md-6 col-sm-12">
						</div>
					</div>

					<input type="hidden" name="method" value="'.$nameGroup[1].'" >
						'.$isMulti.'
					<br>
					<table class="table table-bordered table-hover" id="'.$nameGroup[1].'_table" data-model ="'.$nameGroup[1].'" name="'.$nameGroup[1].'" data-tags="'.implode(",",$shows).'" >
						<thead>
							<tr role="row" class="heading">
								'.$thead.'
								<th width="17%">
									<a id="addItem" href="javascript:;" data-tags="'.implode(",",$shows).'" class="btn green"  >
										<i class="fa fa-plus"></i> 
									</a>
								</th>
							</tr>
						</thead>
						<tbody id="itemTbody" >
							'.$tbody.'
						</tbody>
					</table>
			');
		}else
		{
			echo "Table Tags Isn't Array. " ;
		}

	}

	/*批次修改*/
	public static function ajaxEditFrom ($model, $table, $data, $update_link)
	{
		if (is_array($table)) {

			//表格標頭
			$header = '';
			$tbody = '';
			foreach ($table as $key => $value) {
				$header.=($key == 'Image' OR $key == 'image') ? '<th width="15%">'.$key.'</th>' : '<th>'.$key.'</th>';
			}
			$count = 0;
			if (is_array($data)) {
				//表格內容
				foreach ($data as $row) {
					foreach ($row as $key => $value) {
						$tbody.='<tr>';

							$tbody.= '<td>'.($count+1).'<input type="hidden" name="'.$model.'['.$count.'][id]" value="'.$value['id'].'"></td>';

							foreach ($table as $row2) {
								
								//如果這一個row有多組設定，Exp:狀態
								if(isset($row2['field']) AND $row2['field']!='')
								{
									if ($row2['inputType'] =='text') {
										if ($row2['is_edit']) {
											$tbody.='<td ><input type="text" name="'.$model.'['.$count.']['.$row2['field'].']" value="'.$value[$row2['field']].'"></td>';
										}else{
											$tbody.='<td align="center">'.$value[$row2['field']].'</td>';
										}
									}

									if ($row2['inputType'] =='textarea') {
										if ($row2['is_edit']) {
											$tbody.='<td><textarea cols="30" rows="8" name="'.$model.'['.$count.']['.$row2['field'].']">'.$value[$row2['field']].'</textarea></td>';
										}else{
											$tbody.='<td align="center">'.$value[$row2['field']].'</td>';
										}
									}

								}else{
									//產出群組設定項
									$tbody.= '<td>';
									foreach ($row2 as $key2=>$value2) {

										if ($value2['inputType'] =='radio') {
											$thisValue = ($value[$value2['field']] == '1')? 'blue': 'default';

											$showText = (isset($value2['show_text']))? $value2['show_text'][$value[$value2['field']]] : $key2;

											/*暫時先不去控制項目能不能修改   不修改就先不出現
											if ($value2['is_edit']) {

											}else{
												$tbody.= ''.$key2.' : <a  class="btn btn-circle default ">'.$showText.'</a>';
											}*/
											$tbody.= '
												<input type="hidden" name="'.$model.'['.$count.']['.$value2['field'].']" value="'.$value[$value2['field']].'" >

												'.$key2.' : 
												<a id="EditStatic" href="javascript:;" data-id="'.$value['id'].'" data-model="'.$model.'" data-field="'.$model.'['.$count.']['.$value2['field'].']" data-showText="'.implode(",",$value2['show_text']).'" class="btn btn-circle '.$thisValue.' ">
													'.$showText.'
												</a>
											';
										}

									}
									$tbody.='</td>';
								}
							}
						$tbody.='</tr>';
						$count++;
					}

				}
			}else{
				echo "Data is not Array.";
			}

			print('
				<form method="POST" action="'.$update_link.'" target="hidFrame">
					<button class="btn blue" value="save">save</button>
					<input type="hidden" name="_token" value="'.csrf_token().'">
					<input type="hidden" name="method" value="ajaxEdit">
					<table class="table table-bordered table-hover" id="datatable_list" >
						<thead>
							<tr role="row" class="heading filter">
								<th></th>
								'.$header.'
							</tr>
						</thead>
						<tbody >
							'.$tbody.'
						</tbody>
					</table>
				</form>
				<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
			');
		}else{
			echo "Table Set is not Array.";
		}
	}

	//後台列表table

	public static function listTable($set = [])
	{
		$ajaxLink = ( !empty( $set['ajaxEditLink'] ) )? $set['ajaxEditLink'] : '' ;

		$trClass = ["even", "odd"];//tr 漸層類別
		$deleteButton = '
			<a class="btn red" data-id="" data-model="" id="dataDel" href="javascript:;">
				<i class="fa fa-times"></i>
			</a>';

		//thead
		$thead = '';
		$fliter = '';

		if( !empty( $set['tableSet'] ) )//tableSet 必須設定
		{
			$count = 1;
			foreach ($set['tableSet'] as $row ) {

				$thead .= '<th width=" '. $row['width'] .' " > '. $row['title'] .' </th>';

				$fliter_columns = ( !empty( $row['fliter'] ) )? $row['fliter'] : 'text';//data table 篩選欄位類型

				switch ( $fliter_columns ) {
					case 'select':
						$fliter .= '
							<td>
	                            <select name="order_status" class="form-control form-filter input-sm" id="formFilter"  data-columns="'. $count .'">
	                                <option value=""> All </option>
	                     ';         
	                     			if( !empty( $row['options'] ) )
	                     			{
		                     			foreach ($row['options'] as $row) {
		                     				$fliter .= '<option value="'. $row .'">'. $row .'</option>';
		                     			}  
	                     			}
	                     			else{
	                     				echo 'select option is empty. ';
	                     				die;
	                     			}

	                    $fliter .= '
	                    		</select>
                            </td>
						';
						break;
					
					default:
						$fliter .= '										
							<td>
								<input type="text" class="form-control input-small input-inline" id="formFilter" name="title" value="" data-columns="'. $count .'">
							</td>';
						break;
				}

				$count++;
			}
		}
		else{
			echo "tableSet is empty .";
			die;
		}
		//thead and fliter end

		$tbody = '';

		foreach ($set['Datas'] as $row) 
		{
			$tbody .='<tr>';
				$tbody .='
					<td>
						<input type="checkbox" name="ids[]"  id="ids"  value="'.$row['id'].'">
					</td>';
			foreach ($set['tableSet'] as $row2) 
			{

				$columns = ( !empty( $row2['fliter'] ) )? $row2['fliter'] : 'text';

				switch ($columns) {
					case 'select':
						$options = $row2['options'];
						$tbody .= '<td>'.$options[ $row[ $row2['columns'] ] ].'</td>';
						break;
					
					default:
						$tbody .= '<td>'.$row[ $row2['columns'] ].'</td>';
						break;
				}
			}
			$tbody .= '
				<td>
				    <a href="'. MakeItem::url( 'Fantasy/'.$set['model'].'/Edit/'.$row['id'] ) .'" class="green-sharp btn btn-sm"><i class="fa fa-pencil-square-o"></i> 編輯</a>
	                <a href="javascript:;" data-model="'. $set['model'] .'" data-id="'. $row['id'] .'" id="dataDel" class="btn btn-sm red"><i class="fa  fa-close"></i> 刪除</a>
	            </td>
	        ';
			$tbody .='</tr>';
		}

        print('
        <div class="table-container">
            <div id="datatable_ajax_wrapper" class="dataTables_wrapper dataTables_extended_wrapper no-footer">
                <div class="row">
                    <div class="col-md-8 col-sm-12">
                        <div class="dataTables_paginate paging_bootstrap_extended" id="datatable_ajax_paginate">
                            <div class="pagination-panel"> Page <a href="#" class="btn btn-sm default prev disabled"><i class="fa fa-angle-left"></i></a>
                                <input type="text" class="pagination-panel-input form-control input-sm input-inline input-mini" maxlenght="5" style="text-align:center; margin: 0 5px;"><a href="#" class="btn btn-sm default next"><i class="fa fa-angle-right"></i></a> of <span class="pagination-panel-total">18</span></div>
                        </div>
                        <div class="dataTables_length" id="datatable_ajax_length">
                            <label><span class="seperator">|</span>View
                                <select name="datatable_ajax_length" aria-controls="datatable_ajax" class="form-control input-xs input-sm input-inline">
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                    <option value="150">150</option>
                                    <option value="-1">All</option>
                                </select> records</label>
                        </div>
                        <div class="dataTables_info" id="datatable_ajax_info" role="status" aria-live="polite"><span class="seperator">|</span>Found total 178 records</div>
                    </div>
                    <!--<div class="col-md-4 col-sm-12">
                        <div class="table-group-actions pull-right">
                            <span></span>
                            <input type="search" class="form-control input-sm input-small input-inline" placeholder="" aria-controls="sample_1">
                            <button class="btn btn-sm green table-group-action-submit">
                                <i class="fa fa-search"></i> 搜尋</button>
                        </div>
                    </div>-->
                </div>
                <div class="table-responsive">
					<form id="dataContent" target="fancyBox" action="'.$ajaxLink.'" method="POST">
						<input type="hidden" name="_token" value="'.csrf_token().'">
	                    <table class="table table-striped table-bordered table-hover table-header-fixed dataTable no-footer" id="datatable_list" aria-describedby="datatable_ajax_info" role="grid">
	                        <thead>
	                            <tr role="row" class="heading">
	                                <th width="2%" class="sorting_disabled" rowspan="1" colspan="1" aria-label="
	                             ">
	                                    <input type="checkbox" id="idCheckAll" class="group-checkable">
	                                </th>
									'. $thead .'
	                                <th width="15%" class="sorting"> Actions </th>
	                            </tr>

	                            <tr role="row" class="filter">
	                                <td rowspan="1" colspan="1"> </td>
									'. $fliter .'
	                                <td rowspan="1" colspan="1">
	                                    <div class="margin-bottom-5">
	                                        <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
	                                            <i class="fa fa-search"></i> Search</button>
	                                    </div>
	                                    <button class="btn btn-sm red btn-outline filter-cancel">
	                                        <i class="fa fa-times"></i> Reset</button>
	                                </td>
	                            </tr>
	                        </thead>
	                        <tbody>
	                        	'. $tbody .'
	                        </tbody>
	                    </table>
	                </form>

                    <iframe name="fancyBox" id="fancyBox" style="display:none;" ></iframe>
                </div>
                <div class="row">
                    <div class="col-md-8 col-sm-12">
                        <div class="dataTables_paginate paging_bootstrap_extended">
                            <div class="pagination-panel"> Page <a href="#" class="btn btn-sm default prev disabled"><i class="fa fa-angle-left"></i></a>
                                <input type="text" class="pagination-panel-input form-control input-sm input-inline input-mini" maxlenght="5" style="text-align:center; margin: 0 5px;"><a href="#" class="btn btn-sm default next"><i class="fa fa-angle-right"></i></a> of <span class="pagination-panel-total">18</span></div>
                        </div>
                        <div class="dataTables_length">
                            <label><span class="seperator">|</span>View
                                <select name="datatable_ajax_length" aria-controls="datatable_ajax" class="form-control input-xs input-sm input-inline">
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                    <option value="150">150</option>
                                    <option value="-1">All</option>
                                </select> records</label>
                        </div>
                        <div class="dataTables_info"><span class="seperator">|</span>Found total 178 records</div>
                    </div>
                    <div class="col-md-4 col-sm-12"></div>
                </div>
            </div>
        </div>');
	}
	//後台列表table

}
