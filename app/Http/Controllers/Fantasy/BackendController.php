<?php
namespace App\Http\Controllers\Fantasy;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use View;
use App;
use Config;
use ItemMaker;
use Debugbar;
use Mail;
use Session;
use Image;
use Cache;

use App\Http\Controllers\Fantasy\PermissionController as Permission;

/**相關Controller**/
use App\Http\Controllers\MenuController as MenuClass;

/**Models**/
use App\Http\Models\EnProduct\item_new;
use App\Http\Models\Setting;
use App\Http\Models\Seo;
use App\Http\Models\ContactUs;
use App\Http\Models\Product\Product;
use App\Http\Models\Product\Marketing;
use App\Http\Models\Product\ProductCategory;
use App\Http\Models\Product\ProductSubCategory;
use App\Http\Models\Product\ProductPhoto;
use App\Http\Models\MenuList;
use App\Http\Models\Product\Searching;


abstract class BackendController extends BaseController {

	use DispatchesCommands, ValidatesRequests;

	public static $ProjectName = '-';

	protected static $StatusOption = ["否","是"];

	protected static $adminer_mail = "zh@wddgroup.com";
	protected static $adminer_name = "-";

	//public static $testingLink = 'Frankenstein';
	//下列標記tag 不可刪
	//MenuStart	
	public static $MenuList =
	[

		"首頁管理" => [
			"slogan" => "Contact",
			"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
			"link" => "Fantasy/Home/首頁管理/edit/1",	
			
		],

		"產品管理" => [
			"slogan" => "Contact",
			"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
			"link" => [
				// "產品設定" => "Fantasy/Product/產品設定/edit/1",				
				"產品類別管理" => "Fantasy/Product/產品類別管理",
				"產品次類別管理" => "Fantasy/Product/產品次類別管理",
				"產品管理" => "Fantasy/Product/產品管理",
				"行銷類別管理" => "Fantasy/Product/行銷類別管理",
				"搜尋類別管理" => "Fantasy/Product/搜尋類別管理",
				"珠寶搜尋項目管理" => "Fantasy/Product/珠寶搜尋項目管理",
				"商品材質搜尋項目管理" => "Fantasy/Product/商品材質搜尋項目管理",
				"商品尺寸欄位設定" => "Fantasy/Size/商品尺寸設定/edit/1",
				"商品尺寸管理" => "Fantasy/Size/商品尺寸管理",
			]
		],
		"幸福好評" => [
			"slogan" => "Contact",
			"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
			"link" => [				
				"幸福好評設定" => "Fantasy/Happiness/幸福好評設定/edit/1",
				// "幸福好評類別管理" => "Fantasy/Happiness/幸福好評類別管理",
				"幸福好評管理" => "Fantasy/Happiness/幸福好評管理",
			]
		],
		"最新消息" => [
			"slogan" => "Contact",
			"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
			"link" => [				
				"最新消息設定" => "Fantasy/News/最新消息設定/edit/1",
				"最新消息管理" => "Fantasy/News/最新消息管理",
			]
		],
		"門市據點" => [
			"slogan" => "Contact",
			"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
			"link" => [				
				"門市據點設定" => "Fantasy/Location/門市據點設定/edit/1",
				"門市據點管理" => "Fantasy/Location/門市據點管理",
			]
		],
		"預約鑑賞" => [
			"slogan" => "Contact",
			"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
			"link" => [				
				"預約鑑賞設定" => "Fantasy/Reservation/預約鑑賞設定/edit/1",
				"預約鑑賞資料顯示" => "Fantasy/Reservation/預約鑑賞資料顯示",
			]
		],
		"品牌故事" => [
			"slogan" => "Contact",
			"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
			"link" => [				
				"品牌故事設定" => "Fantasy/About/品牌故事設定/edit/1",
				"設計師照片管理" => "Fantasy/About/設計師照片管理",
				"售後服務管理" => "Fantasy/About/售後服務管理",
				"國際證書管理" => "Fantasy/About/國際證書管理",				
			]
		],
		"訂單資料" => [
			"slogan" => "Contact",
			"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
			"link" => [				
				"訂單詳細資料" => "Fantasy/OrderList/訂單管理",
				
			]
		],
		"問與答" => [
			"slogan" => "Contact",
			"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
			"link" => [				
				"問與答基本設定" => "Fantasy/ShoppingQa/問與答基本設定/edit/1",
				"問與答內容管理" => "Fantasy/ShoppingQa/問與答內容管理",
				
			]
		],
		"會員管理" => [
	       "slogan" => "會員管理",
	       "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
	       "link" => [
	        '會員列表' => 'Fantasy/Member/List',
	        '服務條款' => 'Fantasy/Member/Service',
	        '點數使用規範' => 'Fantasy/Member/Bonus',
	        ]
	    ],
	    "聯絡我們" => [
	       "slogan" => "聯絡我們",
	       "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
	       "link" => [
	        '聯絡我們設定' => 'Fantasy/Contact/聯絡我們設定/edit/1',
	        '聯絡我們' => 'Fantasy/Contact/聯絡我們',
	        
	        ]
	    ],
	    "折扣設定" => [
	       "slogan" => "折扣設定",
	       "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
	       "link" => [
	       	'折扣基本設定' => 'Fantasy/DisCount/折扣基本設定/edit/1',
	        '全館折扣' => 'Fantasy/DisCount/全館折扣',
	        '滿額折扣設定' => 'Fantasy/DisCount/滿額折扣設定',
	        '折扣碼設定' => 'Fantasy/DisCount/折扣碼設定',
	    	]
	    ],
	     "網站基本設定" => [
	       "slogan" => "網站基本設定",
	       "icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
	       "link" =>  'Fantasy/網站基本設定/edit/1',       
	        
	        
	    ],
		"SEO" => [

			"slogan" => "SEO管理",

			"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',

			"link" => 'Fantasy/SEO管理'
			
		],
		"帳號管理" => [
			"slogan" => "帳號管理簡述",
			"icon" => '<i class="icon-wrench icons font-green-haze theme-font"></i>',
			"link" => [
				'帳號列表' => 'Fantasy/帳號管理/帳號列表',
				'權限' => 'Fantasy/帳號管理/權限',
			]
		],
			
	];  //MenuEnd

	//下列註解 不可刪
	//ModelStart
	protected static $ModelsArray = [
		"Account" => App\Http\Models\User::class,
		"Role" => App\Http\Models\Role::class,
		"banner"=> App\Http\Models\Banner\banner::class,
		"category"=> App\Http\Models\Banner\category::class,
		"item"=> App\Http\Models\Product\item::class,
		"Room"=> App\Http\Models\Room\Room::class,
		"Product"=> App\Http\Models\Product\Product::class,
		"ProductPhoto"=> App\Http\Models\Product\ProductPhoto::class,
		"ProductContent"=> App\Http\Models\Product\ProductContent::class,
		"ProductCategoryAreadata"=> App\Http\Models\Product\ProductCategoryAreadata::class,
		"ProductSubCategory"=> App\Http\Models\Product\ProductSubCategory::class,
		"ProductSet"=> App\Http\Models\Product\ProductSet::class,
		"ProductCategory"=> App\Http\Models\Product\ProductCategory::class,
		"ProductSize"=> App\Http\Models\Product\ProductSize::class,
		"ProductSeparate"=> App\Http\Models\Product\ProductSeparate::class,
		"Happiness"=> App\Http\Models\Happiness\Happiness::class,
		"HappinessSet"=> App\Http\Models\Happiness\HappinessSet::class,
		"HappinessCategory"=> App\Http\Models\Happiness\HappinessCategory::class,
		"Location"=> App\Http\Models\Location\Location::class,
		"LocationSet"=> App\Http\Models\Location\LocationSet::class,
		"LocationCategory"=> App\Http\Models\Location\LocationCategory::class,
		"News"=> App\Http\Models\News\News::class,
		"NewsSet"=> App\Http\Models\News\NewsSet::class,
		"NewsCategory"=> App\Http\Models\News\NewsCategory::class,
		"NewsPhoto"=> App\Http\Models\News\NewsPhoto::class,
		"NewsContent"=> App\Http\Models\News\NewsContent::class,
		"SizeCategory"=> App\Http\Models\Size\SizeCategory::class,
		"AllSize"=> App\Http\Models\Size\AllSize::class,
		"Searching"=> App\Http\Models\Product\Searching::class,
		"Marketing"=> App\Http\Models\Product\Marketing::class,
		"Jewelry"=> App\Http\Models\Product\Jewelry::class,
		"Matirial"=> App\Http\Models\Product\Matirial::class,
		"Reservation"=> App\Http\Models\Reservation\Reservation::class,
		"DesignerPhoto"=> App\Http\Models\About\DesignerPhoto::class,
		"AfterSell"=> App\Http\Models\About\AfterSell::class,
		"OrderList"=> App\Http\Models\Pay\OrderList::class,
		"CertificatePhoto"=> App\Http\Models\About\CertificatePhoto::class,
	  	"Member"=> App\Http\Models\Member\Member::class,
	  	"MemberService"=> App\Http\Models\Member\MemberService::class,
	  	"MemberServicedata"=> App\Http\Models\Member\MemberServicedata::class,
	  	"City"=> App\Http\Models\Member\City::class,
	  	"Bonus"=> App\Http\Models\Member\Bonus::class,
	  	"BonusRule"=> App\Http\Models\Member\BonusRule::class,
	  	"Address"=> App\Http\Models\Member\Address::class,
	  	"Home"=> App\Http\Models\Home\Home::class,
	  	"Shopping_qa_category"=> App\Http\Models\Shopping_qa\Shopping_qa_category::class,
	  	"Shopping_qa"=> App\Http\Models\Shopping_qa\Shopping_qa::class,
	  	"Shopping_qa_content"=> App\Http\Models\Shopping_qa\Shopping_qa_content::class,
	  	"Shopping_qa_set"=> App\Http\Models\Shopping_qa\Shopping_qa_set::class,
	  	"Contact"=> App\Http\Models\Contact\Contact::class,
	  	"ContactSet"=> App\Http\Models\Contact\ContactSet::class,
	  	"ContactOption"=> App\Http\Models\Contact\ContactOption::class,
	  	"MenuList"=> App\Http\Models\MenuList::class,
	  	"Setting"=> App\Http\Models\Setting::class,
	  	"DisCountCode"=> App\Http\Models\DisCount\DisCountCode::class,
	  	"DisCountFull"=> App\Http\Models\DisCount\DisCountFull::class,
	  	"DisCountPercent"=> App\Http\Models\DisCount\DisCountPercent::class,
	  	"DisCountSet"=> App\Http\Models\DisCount\DisCountSet::class,
	  	"Seo"=> App\Http\Models\Seo::class,
		//"Seo"=> App\Http\Models\Seo::class,
		
	];
	//ModelEnd

	public static $repositoryType = 'Eloquent';

	public static $repositoriesArray = [
		'MakeTable'
	];



	function __construct(){

		//$this->middleware('lang');

		self::checkRouteLang();

		//進後台才處理比對路徑是否為Fantasy
		if( preg_match("/Fantasy/", urldecode($_SERVER['REQUEST_URI']) ) )
		{

			$per_path = [];
			
			foreach (self::$MenuList as $key => $value) {
				$per_path[ $key ] = $value['link'];

			}
			//後台選單設定
			View::share('SileMenu',	Permission::MenuList($per_path));

			//後台首頁 首頁區圖片
			View::share('MenuList',	self::$MenuList);

			//檢查是否有權限到該路徑
			Permission::PathPermission();

			//後台狀態選項
			View::share("StatusOption",self::$StatusOption);

		}
		//前台才處理
		else
		{
			// /*前台共用資料*/
			// $item = item_new::select('id','title')->get();
			// View::share('item',$item);

			//$Setting = Setting::first();
			$globalSeo=Seo::where('id',99)->first();
			View::share('globalSeo',$globalSeo);

			

			$ProductCategory=ProductCategory::where("is_visible",1)->OrderBy('rank','asc')->get();

			$header_data=$ProductCategory;
			foreach($ProductCategory as $key => $value)
			{
				$ProductSubCategory=ProductSubCategory::where("is_visible",1)->where("category_id",$value->id)->OrderBy('rank','asc')->get();

				
		    		
			        //取得用id 取得Marketing底下的產品
			        $Marketing=Marketing::where('is_visible',1)
			                          ->where('category_id',$value->id)
			                          ->OrderBy('rank','asc')
			                          ->get();
			      
			        // foreach ($Marketing as $key => $value) {

			        // 		$Marketing[$key]['data']=Product::where('is_visible',1)
			        //                   ->where('market_id',$value->id)
			        //                   ->OrderBy('rank','asc')
			        //                   ->get();
			        // }
			        









				$header_data[$key]['ProductSubCategory']=$ProductSubCategory;
				$header_data[$key]['Marketing']=$Marketing;
				
			}

			$ProductSubCategory=ProductCategory::where("is_visible",1)->OrderBy('rank','asc')->get();
			$sessioncount=count(Session::get('product_data'));
			$Menu_List=MenuList::where('is_visible',1)->OrderBy('rank','asc')->get();

			$Searching=Searching::where('is_visible',1)->OrderBy('rank','asc')->get();


			$_thispageurl="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

			$_thispageurlarr=explode("/", $_thispageurl);

			
			$_thispageurlarrlength=count($_thispageurlarr);
			$seo=SEO::where('url',"like","%".$_thispageurl."%")->first();

			while($_thispageurlarrlength >0  )
			{
				
				$seo=SEO::where('url',$_thispageurl)->first();
				if( count($seo) >0)
				{
					$_thispageurlarrlength=1;
					
				}				
				$_thispageurl=str_replace("/".$_thispageurlarr[$_thispageurlarrlength-1], "", $_thispageurl);	

				$_thispageurlarrlength-=1;	


			}

		
			$GlobalSeo=SEO::where('id',"999")->first();

			if( count($seo) < 0 || empty($seo->web_title))
			{
				$seo=$GlobalSeo;	
			}


			
			
			

			$Setting=Setting::first();
			View::share('Setting',$Setting);
			View::share('Searching',$Searching);
			View::share('Menu_List',$Menu_List);
			View::share('header_data',$header_data);
			View::share('product_in_car',$sessioncount);
			View::share('seo',$seo);
			
			/*$Setting = json_decode($Setting->content, true);
			View::share('Setting',$Setting);

			$Menutitle = Menutitle::where('is_visible',1)->OrderBy("rank","asc")->get();
			View::share('Menutitle',$Menutitle);*/
			
		}
		//語系
		View::share("locateLang",App::getLocale());


		View::share('locale', parent::getRouter()->current()->parameters()['locale']);

		//客戶名稱設定
		if( !empty($globalsSet['company_title']) )
		{
			View::share('ProjectName', $globalsSet['company_title']);
		}
		else
		{
			View::share('ProjectName', self::$ProjectName);
		}

	}

	public static function checkRouteLang()
	{
		$parameters = parent::getRouter()->current()->parameters();

		if(isset($parameters['locale']) AND !empty($parameters['locale']))
		{
			App::setLocale($parameters['locale']);
			//資料庫前綴字
			switch ($parameters['locale']) {
				case 'zh-tw':
					Config::set('app.dataBasePrefix','tw_');
					break;

				default:
					Config::set('app.dataBasePrefix',''.$parameters['locale'].'_');
					break;
			}

		}

	}

	protected function findDataAndAssociate( $set = [] )
	{
		$modelName = ( !empty( $set['modelName'] ) )? $set['modelName'] : '';
		$pageShow = ( isset( $_GET['show_per_page'] ) AND !empty( $_GET['show_per_page'] ) )? $_GET['show_per_page'] : '50';//每頁顯示數量
		//$toParent = ( !empty( $set['parent'] ) )? $set['parent'] : '';

		$Datas = [];


		if( !empty( $modelName ) )
		{
			$model = self::$ModelsArray[ $modelName ];

			if( isset( $_GET['search'] ) AND !empty( $_GET['search'] ) ) //如果沒有擷取到search的資料
			{
				$count = 0;
				$findWhere = '';
				foreach ($_GET['search'] as $key => $value) {


					if( $count == 0 )
					{
						if( preg_match("/_id/", $key) OR preg_match("/is_/", $key) ){

							if( preg_match("/_ids/", $key) )
							{
								$findWhere .=  " `".$key."` LIKE '%\"".$value."\"%' " ;
							}
							else
							{
								$findWhere .=  " `".$key."` = '".$value."' " ;
							}
						}
						else{
							$findWhere .=  " `".$key."` LIKE '%".$value."%' " ;
							// $findWhere .=  " `".$key."` = '".$value."' " ;
						}

					}
					else
					{
						if( preg_match("/_id/", $key) OR preg_match("/is_/", $key) ){

							if( preg_match("/_ids/", $key) )
							{
								$findWhere .=  " AND `".$key."` LIKE '%\"".$value."\"%' " ;
							}
							else
							{
								$findWhere .=  " AND `".$key."` = '".$value."' " ;
							}

						}else{
							$findWhere .=  " OR `".$key."` LIKE '%".$value."%' ";
							// $findWhere .=  " AND `".$key."` = '".$value."' " ;
						}
					}
					$count++;
				}
				$Datas = $model::select( $set['select'] )->whereRaw($findWhere);

				if( !empty($set['where']) )
				{
					foreach( $set['where'] as $where )
					{
						$field = $where[0];
						$condition = !empty($where[1]) ? $where[1] : '=' ;
						$val = !empty($where[2]) ? $where[2] : $where[1] ;

						$Datas = $Datas->where($field, $condition, $val);
					}
				}
				$Datas = $Datas->orderBy('id', 'desc')->paginate( $pageShow )->toArray();
			}
			else
			{
				$Datas = $model::select( $set['select'] );
				
				if( !empty($set['where']) )
				{
					foreach( $set['where'] as $where )
					{
						$field = $where[0];
						$condition = !empty($where[1]) ? $where[1] : '=' ;
						$val = !empty($where[2]) ? $where[2] : $where[1] ;

						$Datas = $Datas->where($field, $condition, $val);
					}
				}

				$Datas = $Datas->orderBy('id', 'desc')->paginate( $pageShow )->toArray();
			}



			if( !empty( $set['belong'] ) )//抓父關連資料
			{
				if( is_array( $set['belong'] ) AND count( $set['belong'] ) > 0 ) //關聯限定為陣列
				{

					foreach ($set['belong'] as $name => $content)
					{
						$associate = self::$ModelsArray[ $name ];

						//是否還有父類別(ex. 分館,或品牌)
						if( isset($content['withParentModel']) )
						{
							$find = $associate::select( $content['select'] )
											->with( $content['parentModel'] )
											->get()
											->toArray();// 抓全部

							/*
							原本名稱： 某某分類
							改為： 某某分館或品牌 > 某某分類
							 */
							foreach( $find as $key3 => $option )
							{
								$find[$key3]['title'] = $option[ strtolower($content['relateName']) ]['title'].' > '.$option['title'];
							}
						}
						else
						{
							$find = $associate::select( $content['select'] )->get()->toArray();// 抓全部
						}


						$Datas[ $name ] = $find;
						foreach ($Datas['data'] as $key => $data)
						{

							foreach ($find as $key2 => $value2)
							{
								if( $value2[ $content['filed'] ] ==  $data[ $content['parent'] ] )
								{
									$Datas['data'][ $key ][ strtolower( $name ) ] = $find[ $key2 ];
								}
							}

						}
					}

				}
			}


			if( !empty( $set['has'] ) )//抓子關連資料
			{
				if( is_array( $set['has'] ) AND count( $set['has'] ) > 0 ) //關聯限定為陣列
				{

					foreach ($set['has'] as $name => $content)
					{
						$associate = self::$ModelsArray[ $name ];

						foreach ($Datas['data'] as $key => $value)
						{
							$find = $associate::where($content['filed'],  $value[ 'id' ])
													->select( $content['select'] )
													->get()
													->toArray();

							$Datas['data'][ $key ][ strtolower( $name ) ] = $find;
						}
					}

				}
			}
			//將已經搜尋的條件帶到下一頁
			if( !empty( $_GET ) )
			{
				if( !empty( $Datas['next_page_url'] ) )
				{
					$org_page_link = '';
					foreach ($_GET as $key => $value) {
						if( $key != 'page' AND $key != 'search'  )
						{
							$Datas['next_page_url'] .= '&'.$key.'='.$value;
							
						}elseif ( $key == 'search' ) {
							foreach ($value as $key2 => $search) {

								$Datas['next_page_url'] .= '&search['.$key2.']='.$search;
							}
						}
					}
				}

				if( !empty( $Datas['prev_page_url'] ) )
				{
					$org_page_link = '';
					foreach ($_GET as $key => $value) {
						if( $key != 'page' AND $key != 'search' )
						{
							$Datas['prev_page_url'] .= '&'.$key.'='.$value;	
						}elseif ( $key == 'search' ) {
							foreach ($value as $key2 => $search) {

								$Datas['prev_page_url'] .= '&search['.$key2.']='.$search;
							}
						}
					}
				}

			}
			//Debugbar::info($Datas);

			return $Datas;
		}else
		{
			Debugbar::error('set model is empty.');
		}

	}

	public function sendMail( $set = [] )
	{
		$content = self::$ModelsArray[ $set['Model'] ];//抓資料的model
		$type = ( !empty($set['type']) )? $set['type'] : '';


		$data = $content::findOrFail( $set['id'] );
		$data->contact_type = ( !empty($type) )? $type : '';

		// if( !empty( $data->password ) AND $type == 'forgot' AND 0)
		// {
		// 	$cryptKey  = 'qJB0rGtIn5UB1xG03efyCp';
		// 	$data->password = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), $data->password, MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ) );
		// }

        Mail::send( $set['view'], ['data' => $data], function ($m) use ( $set ) {
            $m->to($set['to'], $set['to_name'])->subject( $set['subject'] );
        });

	}

	protected function addNew( $set = [] )
	{
		//$Datas , $modelName, $routeFix
        $new = new self::$ModelsArray[ $set['modelName'] ];
        foreach ($set['Datas'] as $key => $value)
        {
        	if( $key != 'Photo' AND $key != 'App')
        	{
        		$new->$key = ( !is_array( $value ) )? $value : json_encode($value);
        	}
        }

        if( $new->save() )
        {

            return '{ "redirect" : "'.ItemMaker::url( 'Fantasy/'.$set['routeFix'].'/edit/'.$new->id ).'", "parent_id" : "'.$new->id.'" }';
        }
        else
        {
        	return '{ "redirect" : "" }';
        }
	}

	protected function updateOne($Datas, $modelName, $type='')
	{
		
		$model = self::$ModelsArray[$modelName];

        if( !empty( $type ) AND $type == 'ajaxEdit' )
        {

            foreach ($Datas as $row)
            {
                $work = $model::find($row['id']);

                foreach ($row as $key => $value) {
                    if($key !== 'id')
                    {
                        $work->$key = ( !is_array( $value ) )? $value : json_encode($value);
                    }
                }
                //update
                if(!$work->save())
                {
                    echo "<script>alert('work id ".$row['id']." update fail.');</script>";
                }else{
                    //echo "".$row['id']." update success.";
                }
            }
            echo "<script>alert('儲存成功');</script>";
            //echo "<script>toastrAlert('success', '儲存成功');</script>";
        }
        else
        {
            $id = $Datas['id'];

            $find = $model::find($id);

            foreach ($Datas as $key => $value)
            {
            	if( $key != 'id' )
            	{
            		$find->$key = ( !is_array( $value ) )? $value : json_encode($value);
            	}

            }

            if($find->save())
            {
                //echo "<script>alert('儲存成功');</script>";
                //echo "<script>toastrAlert('success', '儲存成功');</script>";
                return true;
            }

        }
	}

	protected function updateMulti($Datas, $modelName, $parent_field, $parent_id, $miniImage='')
	{
		if(count($Datas) > 0)
		{
			$model = self::$ModelsArray[$modelName];


			//儲存Introduction
			foreach($Datas as $row)
			{
				if(!empty($row['id']))//沒有id代表是新的一筆
				{
					foreach ($row as $key => $value)
					{
						$newInt = $model::where($parent_field ,'=', $parent_id)->where('id','=',$row['id'])->get();

						if($key != 'id')
						{
							$newInt[0]->$key = $value;

							//壓縮圖片
							if( isset($miniImage) AND !empty($miniImage) )
							{
								$miniImage['image'] = $newInt[0]['image'];
								$saveField = $miniImage['saveField'];
								$newInt[0]->$saveField = '/'.$this->minImage($miniImage);
							}
						}

						if(!$newInt[0]->save())
						{
							return '{ "message" : false }';
						}
						else
						{
							// return '{ "message" : true }';
						}
					}
				}
				else
				{
					$newInt = new $model;

					$newInt->$parent_field = $parent_id;

					foreach ($row as $key => $value)
					{
						if( $key !='id' )
						{
							$newInt->$key = $value;
						}
					}

						//壓縮圖片
						if( isset($miniImage) AND !empty($miniImage) )
						{
							$miniImage['image'] = $newInt->image;
							$saveField = $miniImage['saveField'];
							$newInt->$saveField = '/'.$this->minImage($miniImage);
						}

					if(!$newInt->save())
					{
						return '{ "message" : false }';
					}
					else
					{
						//return '{ "message" : true }';
					}
				}



			}
		}
	}

	protected function updataOneColumns( $set = [] )
	{
		$model = self::$ModelsArray[ $set['modelName'] ];

        $find = $model::find($set['id'] );
        $columms = $set['columns'];

        $find->$columms = $set['value'];

        if($find->save())
        {
        	return '{ "message" : "true" }';
        }
        else
        {
        	return '{ "message" : "false" }';
        }


	}

	protected function deleteOne($modelName, $id)
	{
		$model = self::$ModelsArray[$modelName];

        $del = $model::find($id);

        //產品的話先刪除規格
        if( $modelName == 'Item' )
        {
        	$spec = self::$ModelsArray['Spec'];
        	$specDel = $spec::where('machine_id', $del->machine_no )->where('machine_name', $del->title)->get();

        	if( count( $specDel ) > 0 )
        	{
        		foreach ($specDel as $row) {
	        		if( !$row->delete() )
	        		{
	        			echo "spec delete fail.";
	        			break;
	        		}
        		}

        	}
        }

        if($del->delete())
        {
            echo "<script>alert('刪除成功');</script>";
        }
	}


	protected function minImage( $set = [] )
	{
		$resizeFolder = ( !empty( $set['resizeFolder'] ) )? $set['resizeFolder'] : '';//resize後的資料夾
		$image = ( !empty( $set['image'] ) )? $set['image'] : '';//原圖
		$width = ( !empty( $set['width'] ) )? $set['width'] : null;//原圖
		$height = ( !empty( $set['height'] ) )? $set['height'] : null;//原圖
		$small_image = '';

		if( !empty( $image ) )
		{
			$img = Image::make(public_path().$image);

			//先檢查有沒有相同檔名的縮圖存在
			$small = explode('.', $img->basename);
			$resize = 'resize_image/'.$resizeFolder.'/'.$small[0].'_rs.'.$small[1];
			//$checkRSImg = $resize;

			if( !file_exists( public_path().'/'.$resize ) )
			{
				$img->resize($width,$height, function ($constraint)
				{
						$constraint->aspectRatio();
				});

				if($small[1] == 'png')
				{
					$img->save(public_path().'/'.$resize, 100);
				}
				else
				{
					$img->save(public_path().'/'.$resize);
				}
				$small_image = $resize;
			}
			else
			{
				$small_image = $resize;
			}

			return $small_image;
		}

	}
	protected function set_optgroup( $set = [] )
	{
		if( !empty( $set['Datas'] ) )
		{
			$Datas = [];
			$count = 0;
			foreach ($set['Datas'] as $row)
			{
				//產生optgroup label
				$label = '';
				if( !empty( $set['parent_level'] ) )
				{
					for ($i=1; $i <= $set['parent_level'] ; $i++)
					{
						$label .= ( $i == 1 )? $row[ $set['parent'.$i.'_key'] ]['title'] : " / ".$row[ $set['parent'.$i.'_key'] ]['title'];
					}

				}
				// array_push($Datas[ $label ], [
				// 	"id" => $row['id'],
				// 	"title" => $row['title']
				// ]);
				$Datas[ $label ][$count]['id'] = $row['id'];
				$Datas[ $label ][$count]['title'] = $row['title'];
				$count++;
			}
			return $Datas;
		}
		else
		{
			echo "Datas is empty.";
		}

	}
	protected function var_dump_pre( $data )
	{
		echo '<pre>';
	    	var_dump($data);
	    echo '</pre>';
	    return null;
	}


	protected static function processTitleToUrl( $title )
	{
		$replace1 = str_replace(' ', '+', $title);
		$replace2 = str_replace('/', '^', $replace1);
		$replace3 = str_replace('.', '`', $replace2);

		return $replace3;
	}


	protected static function revertUrlToTitle( $url )
	{
		$replace1 = str_replace('+', ' ', $url);
		$replace2 = str_replace('^', '/', $replace1);
		$replace3 = str_replace('`', '.', $replace2);

		return $replace3;
	}


	protected function getSeoData($key)
	{
		$Seo = self::$ModelsArray['Seo'];
		$seo = Seo::select('id','title','key','web_title','meta_keyword','meta_description','ga_code','gtm_code')
                    ->where('key', $key)
                    ->first()
                    ->toArray();

        return $seo;
	}




}
