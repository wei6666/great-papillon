<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use View;
use Config;
use Session;
use Route;
use App;
use Validator;
use Debugbar;
use Cache;
use ItemMaker;

class MenuController extends BaseController
{
    public function __construct() {

		$locale = !empty( parent::getRouter()->current()->parameters()['locale'] ) ? parent::getRouter()->current()->parameters()['locale'] : '';

		$this->locale = $locale;

	}



	public static function getProductThemes() {

		$MenuClass = new MenuController();
		if( !empty( Cache::get( $MenuClass->locale.'_'.'ProductTheme' ) ) )
		{
			$themes = Cache::get( $MenuClass->locale.'_'.'ProductTheme' );
		}
		else
		{
			$ProductTheme = self::$ModelsArray['ProductTheme'];

			$themes = $ProductTheme::where('is_visible', 1)
									->orderBy('rank', 'asc')
									->get();
		}

		foreach( $themes as $key => $value )
		{
			$themeUrl = self::processTitleToUrl( $value->title );
			$themes[$key]->link = ItemMaker::url('Product/'.$themeUrl);
		}


		return $themes;

	}



	public static function getLaboratories() {

		$MenuClass = new MenuController();
		if( !empty( Cache::get( $MenuClass->locale.'_'.'Laboratory' ) ) )
		{
			$laboratories = Cache::get( $MenuClass->locale.'_'.'Laboratory' );
		}
		else
		{
			$Laboratory = self::$ModelsArray['Laboratory'];

			$laboratories = $Laboratory::where('is_visible', 1)
									->orderBy('rank', 'asc')
									->get();
		}

		foreach( $laboratories as $key => $value )
		{
			$laborUrl = self::processTitleToUrl( $value->title );
			$laboratories[$key]->link = ItemMaker::url('Laboratory/'.$laborUrl);
		}


		return $laboratories;

	}







	public static function getNewsCategories() {


		$MenuClass = new MenuController();
		if( !empty( Cache::get( $MenuClass->locale.'_'.'NewsCategory' ) ) )
		{
			$newsCategories = Cache::get( $MenuClass->locale.'_'.'NewsCategory' );
		}
		else
		{
			$NewsCategory = self::$ModelsArray['NewsCategory'];

			$newsCategories = $NewsCategory::where('is_visible', 1)
									->orderBy('rank', 'asc')
									->get();
		}

		foreach( $newsCategories as $key => $value )
		{
			$cateUrl = self::processTitleToUrl( $value->title );
			$newsCategories[$key]->link = ItemMaker::url('News/'.$cateUrl);
		}


		return $newsCategories;

	}









	protected static $ModelsArray = [
		"ProductTheme" => App\Http\Models\Product\Theme::class,
		"ProductCategory" => App\Http\Models\Product\Category::class,
		"ProductItem" => App\Http\Models\Product\Item::class,
		"ProductIntroPhoto" => App\Http\Models\Product\IntroPhoto::class,
		"ProductArticle" => App\Http\Models\Product\Article::class,
		"ProductFaqCategory" => App\Http\Models\Product\FaqCategory::class,
		"ProductFaq" => App\Http\Models\Product\Faq::class,
		"ProductProcess" => App\Http\Models\Product\Process::class,
		"ProductAd" => App\Http\Models\Product\Ad::class,
		"Laboratory" => App\Http\Models\Laboratory\Laboratory::class,
		"NewsCategory" => App\Http\Models\News\NewsCategory::class,
	];



	protected static function processTitleToUrl( $title )
	{
		$replace1 = str_replace(' ', '+', $title);
		$replace2 = str_replace('/', '^', $replace1);
		$replace3 = str_replace('.', '`', $replace2);

		return $replace3;
	}


	protected static function revertUrlToTitle( $url )
	{
		$replace1 = str_replace('+', ' ', $url);
		$replace2 = str_replace('^', '/', $replace1);
		$replace3 = str_replace('`', '.', $replace2);

		return $replace3;
	}





}
