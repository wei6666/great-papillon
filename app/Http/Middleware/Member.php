<?php 
namespace App\Http\Middleware;

use Closure;
use ItemMaker;
use App;
use Session;
use View;
use Redirect;
use Illuminate\Http\Request;
class Member {

    public function handle($request, Closure $next)
    {
        if(Session()->has('Member')){
            return $next($request);
        } else {
            return Redirect::to(App::getLocale().'/member');
            /*return View::make(App::getLocale().'.member.index',
            [  
                'locale'=>App::getLocale(),
                'locateLang'=>App::getLocale(),
            ]);*/

        }
    }

}