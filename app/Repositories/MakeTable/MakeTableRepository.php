<?php namespace App\Repositories\MakeTable;

interface MakeTableRepository
{

    public function makeTables( $setTables );

    public function removeTables( $setTables );

}
