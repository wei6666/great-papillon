<!doctype html>
<!--[if IE]><![endif]-->
<!--[if lt IE 7 ]> <html class="no-js ie6 oldie" lang="zh-tw"> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7 oldie" lang="zh-tw"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8 oldie" lang="zh-tw"> <![endif]-->
<!--[if IE 9 ]>    <html class="no-js ie9" lang="zh-tw"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->

<html lang="{{$locateLang}}" class="no-js" islogin="@if(Session::get('Member') != NULL) login @else-@endif">
<head>
    <base href="{{ItemMaker::url('/')}}" page='{{ "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}'@if(Session::get('Member') != NULL) login="member" @endif>
    <?php $li=str_replace($locale ,"",ItemMaker::url('')); ?>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/assets/img/ico/favicon.png">
    <link rel="shortcut icon" type="image/x-icon" href="/assets/img/ico/favicon.png">
    <link rel="icon" type="image/x-icon" href="/assets/img/ico/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
   
   <?php 

    if(!empty($thiscategory['web_title']))
    {
        $seo=$thiscategory;
    } 
    if(!empty($ProductSubCategory['web_title']))
    {
        $seo=$ProductSubCategory;
    } 
    if(!empty($ProductData['web_title']))
    {
        $seo=$ProductData;
    } 
    if(!empty($News['web_title']))
    {
        $seo=$News;
    } 

    ?>
   @if( !empty($og_data) )
    <title>{{ $seo['web_title'] }}</title>
    <meta property="og:title" content="{{ $seo['web_title'].$seo['meta_description'] }}">
    <meta name="keywords" content="{{ $seo['meta_keyword'] }}">
    <meta name="description" content="{{ $seo['meta_description'] }}">
    <meta property="og:url" content="{{ "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ $og_data['web_title'] }}" />
    <meta property="og:description" content="{{ $og_data['meta_description'] }}" />
    <meta property="og:image" content="{{ "http://$_SERVER[HTTP_HOST]".$og_data['og_image'] }}" />    

   @elseif( !empty($seo['web_title']) )
        <title>{{ $seo['web_title'] }}</title>
        <meta property="og:title" content="{{ $seo['web_title'].$seo['meta_description'] }}">
        <meta name="keywords" content="{{ $seo['meta_keyword'] }}">
        <meta name="description" content="{{ $seo['meta_description'] }}">
    
  @endif

    <!-- 主要css -->
    @yield('fontcss')

   <link rel="stylesheet" href="/assets/js/vendor/cgpagechange/cgpagechange.css">
    @yield('css')

</head>
    
<body @yield('bodySetting') @if(!empty($pageBodyClass))   class="{{ $pageBodyClass }}"   @else  @endif @if(!empty($pageBodyId))   id="{{ $pageBodyId }}"   @else  @endif>

    @if( !empty($seo['gtm_code'] ) )
        <?php $gtm_code = $seo['gtm_code'] ?>   
    @else
        <?php /*$gtm_code = $globalSeo['gtm_code']*/ ?>
    @endif


    @if( !empty ( $gtm_code ) )
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NVP6BP" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer',{{ $gtm_code }});</script>
    @endif



    @yield('content')

    <input id='the_token' type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

    <!--共用 js-->
    
    
    <!--共用-->
    @yield('frontscript')
    <script src="/assets/js/vendor/jquery-2.2.4.min.js"></script>
    <script src="/assets/js/vendor/waypoints/waypoints.min.js"></script>
    <script src="/assets/js/vendor/blazy/blazy.min.js"></script>
    <script src="/assets/js/vendor/jquery.easings.min.js"></script>
    <script src="/assets/js/vendor/masonry/masonry.pkgd.min.js"></script>
    <script src="/assets/js/vendor/jquery.nicescroll/jquery.nicescroll.js"></script>
    <script src="/assets/js/vendor/slick/slick.min.js"></script>
    <script src="/assets/js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.min.js"></script>
    <link rel="stylesheet" href="/assets/js/vendor/slick/slick.css">
       
    @yield('script')
    <script src="/assets/js/main.js"></script>
    <script src="/assets/js/extra.js"></script>

    <script type="text/javascript">

        //前台系統訊息
        @if( Session::get('Message') )
        $(function() {
            alert( "{{Session::get('Message')}}" );
        });
        @endif

    </script>


