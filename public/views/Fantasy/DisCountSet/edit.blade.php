@extends('Fantasy.template')

@section('title',"折扣基本設定修改 "."-".$ProjectName)
@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    #showPic.anchorImg{ width: auto!important; max-height: 250px;  }
    .copyBtn{display: none;}
    </style>

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			折扣基本設定 <small>Newscatagories Manager</small> </h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('Fantasy/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="{{ ItemMaker::url('Fantasy/'.$routePreFix)}}">折扣基本設定</a> <i class="fa fa-angle-right"></i>
				</li>
				<li>
					折扣基本設定修改 </li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
                    {{-- <div class="note note-danger">
                        <p> 注意事項: 婚宴會議單元，才需要填入短述。<br>
                            交通訊息、集團介紹、問與答、館內活動、歡樂設施 非輪播單元，所以只需要上傳一組圖片。
                        </p>
                    </div> --}}
                    <!--注意事項END-->

					<!--<form action="{{ ItemMaker::url('Fantasy/工程分類/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 折扣基本設定 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    @include('Fantasy.include.editActionBtns')

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> 折扣基本設定 </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

                                                    {{ItemMaker::idInput([
                                                            'inputName' => 'DisCountSet[id]',
                                                            'value' => ( !empty($data['id']) )? $data['id'] : ''
                                                    ])}}
{{-- 
                                                    {{ItemMaker::textInput([
                                                        'labelText' => '全館折扣標題',
                                                        'inputName' => 'DisCountSet[percent_title]',
                                                        'value' => ( !empty($data['percent_title']) )? $data['percent_title'] : ''
                                                    ])}}


                                                    {{ItemMaker::textArea([
                                                        'labelText' => '全館折扣說明',
                                                        'inputName' => 'DisCountSet[percent_content]',
                                                        'value' => ( !empty($data['percent_content']) )? $data['percent_content'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '全館折扣內容<br>(僅顯示前台不會<br>套用到價格計算規則)',
                                                        'inputName' => 'DisCountSet[percent_value]',
                                                        'value' => ( !empty($data['percent_value']) )? $data['percent_value'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '滿額折扣標題',
                                                        'inputName' => 'DisCountSet[full_title]',
                                                        'value' => ( !empty($data['full_title']) )? $data['full_title'] : ''
                                                    ])}}


                                                    {{ItemMaker::textArea([
                                                        'labelText' => '滿額折扣說明',
                                                        'inputName' => 'DisCountSet[full_content]',
                                                        'value' => ( !empty($data['full_content']) )? $data['full_content'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '滿額折扣內容<br>(僅顯示前台不會<br>套用到價格計算規則)',
                                                        'inputName' => 'DisCountSet[full_value]',
                                                        'value' => ( !empty($data['full_value']) )? $data['full_value'] : ''
                                                    ])}} --}}


                                                    {{ItemMaker::textInput([
                                                        'labelText' => '折扣碼標題',
                                                        'inputName' => 'DisCountSet[code_title]',
                                                        'value' => ( !empty($data['code_title']) )? $data['code_title'] : ''
                                                    ])}}

                                                    {{ItemMaker::textArea([
                                                        'labelText' => '折扣碼說明',
                                                        'inputName' => 'DisCountSet[code_content]',
                                                        'value' => ( !empty($data['code_content']) )? $data['code_content'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '會員紅利標題',
                                                        'inputName' => 'DisCountSet[bonus_title]',
                                                        'value' => ( !empty($data['bonus_title']) )? $data['bonus_title'] : ''
                                                    ])}}

                                                    {{ItemMaker::textArea([
                                                        'labelText' => '會員紅利說明',
                                                        'inputName' => 'DisCountSet[bonus_content]',
                                                        'value' => ( !empty($data['bonus_content']) )? $data['bonus_content'] : ''
                                                    ])}}


                                                    {{ItemMaker::textInput([
                                                        'labelText' => '好友推薦標題',
                                                        'inputName' => 'DisCountSet[friend_title]',
                                                        'value' => ( !empty($data['friend_title']) )? $data['friend_title'] : ''
                                                    ])}}

                                                     {{ItemMaker::textArea([
                                                        'labelText' => '好友推薦說明',
                                                        'inputName' => 'DisCountSet[friend_content]',
                                                        'value' => ( !empty($data['friend_content']) )? $data['friend_content'] : ''
                                                    ])}}


                                                    {{ItemMaker::textInput([
                                                        'labelText' => '購買回饋標題',
                                                        'inputName' => 'DisCountSet[feedback_title]',
                                                        'value' => ( !empty($data['feedback_title']) )? $data['feedback_title'] : ''
                                                    ])}}

                                                     {{ItemMaker::textArea([
                                                        'labelText' => '購買回饋說明',
                                                        'inputName' => 'DisCountSet[feedback_content]',
                                                        'value' => ( !empty($data['feedback_content']) )? $data['feedback_content'] : ''
                                                    ])}}

                                                     {{ItemMaker::textInput([
                                                        'labelText' => '購買回饋說明',
                                                        'inputName' => 'DisCountSet[feedback_value]',
                                                        'value' => ( !empty($data['feedback_value']) )? $data['feedback_value'] : ''
                                                    ])}}



                                                    {{ItemMaker::textInput([
                                                        'labelText' => '包裝配件標題',
                                                        'inputName' => 'DisCountSet[packet_title]',
                                                        'value' => ( !empty($data['packet_title']) )? $data['packet_title'] : ''
                                                    ])}}

                                                     {{ItemMaker::textArea([
                                                        'labelText' => '包裝配件說明',
                                                        'inputName' => 'DisCountSet[packet_content]',
                                                        'value' => ( !empty($data['packet_content']) )? $data['packet_content'] : ''
                                                    ])}}

                                                     {{ItemMaker::photo([
                                                        'labelText' => '包裝配件圖片<br>570 x 330',
                                                        'inputName' => 'DisCountSet[packet_image]',
                                                        'value' => ( !empty($data['packet_image']) )? $data['packet_image'] : ''
                                                    ])}}

                                                     {{ItemMaker::textInput([
                                                        'labelText' => '包裝配件內容',
                                                        'inputName' => 'DisCountSet[packet_value]',
                                                        'value' => ( !empty($data['packet_value']) )? $data['packet_value'] : ''
                                                    ])}}



                                                </div>
                                            </div>



                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif

		});

	</script>
@stop
