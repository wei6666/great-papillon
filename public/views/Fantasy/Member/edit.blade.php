@extends('Fantasy.template')

@section('title', "會員管理"."-".$ProjectName)
@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    </style>
@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
            會員管理 <small>Member Manager</small> </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="{{ ItemMaker::url('Fantasy/')}}">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{ ItemMaker::url('Fantasy/'.$routePreFix)}}">會員管理</a> <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    會員資料 </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->


            <div class="row">
                <div class="col-md-12">

                    <!--注意事項-->
                     <div class="note note-danger">
                     <p> 注意事項: 若非特殊情況,請勿在此修改會員資料!</p>
                    </div>
                    <!--注意事項END-->


                    {!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}
                    <?php
                        $options = [                
                            [
                                "id" => "1",
                                "title" => '會員生日禮',
                            ],
                            
                            [
                                "id" => "2",
                                "title" => '購買回饋',
                            ],
                            
                            [
                                "id" => "3",
                                "title" => '訂單折抵',
                            ],
                            
                            [
                                "id" => "4",
                                "title" => '好友推薦',
                            ],
                            
                            [
                                "id" => "5",
                                "title" => '首次消費',
                            ],
                            
                            [
                                "id" => "6",
                                "title" => '會員註冊',
                            ]
                        ]
                    ?>
                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
                                            {{ ( !empty( $data['name'] ) )? $data['name'] : '' }} - 會員資料 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    <a href=" {{ ItemMaker::url('Fantasy/'.$routePreFix) }}" type="button" name="back" class="btn default btn-secondary-outline btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="返回"> <i class="fa fa-mail-reply"></i> 返回
                                    </a>

                                    <button type="submit"  class="btn btn-lg green btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="儲存">
                                        <i class="fa fa-check"></i> 儲存
                                    </button>

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news"> 
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> Information </a>
                                            </li>
                                            <li>
                                                <a href="#tab_point" data-toggle="tab"> 會員點數 </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

                                                    {{ItemMaker::idInput([
                                                        'inputName' => 'Member[id]',
                                                        'value' => ( !empty($data['id']) )? $data['id'] : ''
                                                    ])}}
                                                    
                                                    {{ItemMaker::radio_btn([
                                                        'labelText' => '是否啟用',
                                                        'inputName' => 'Member[is_visible]',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
                                                    ])}}
                                                    
                                                    {{ItemMaker::textInput([
                                                        'labelText' => '會員編號',
                                                        'disabled' =>'disabled',
                                                        'inputName' => '',
                                                        'value' => ( !empty($data['mbid_city'].$data['mbid_no']) )? $data['mbid_city'].$data['mbid_no'] : ''
                                                    ])}}
                                                    
                                                    {{ItemMaker::textInput([
                                                        'labelText' => '會員姓名',
                                                        'inputName' => 'Member[name]',
                                                        'value' => ( !empty($data['name']) )? $data['name'] : ''
                                                    ])}}
                                                    
                                                    {{ItemMaker::textInput([
                                                        'labelText' => '手機號碼',
                                                        'inputName' => 'Member[mobile]',
                                                        'value' => ( !empty($data['mobile']) )? $data['mobile'] : ''
                                                    ])}}
                                                    
                                                    {{ItemMaker::textInput([
                                                        'labelText' => '電子郵件',
                                                        'inputName' => 'Member[email]',
                                                        'value' => ( !empty($data['email']) )? $data['email'] : ''
                                                    ])}}
                                                    {{ItemMaker::passwordInput([
                                                        'labelText' => '密碼',
                                                        'inputName' => 'Member[password]',
                                                        'helpText' =>'修改時如果有填，將更改目前密碼',
                                                        'value' => ( !empty($data['password']) )? $data['password'] : ''
                                                    ])}}
                                                    {{ItemMaker::select([
                                                        'labelText' => '性別',
                                                        'inputName' => 'Member[gender]',
                                                        'helpText' =>'性別',
                                                        'options'   => $otherOptions,
                                                        'value' => ( !empty($data['gender']) )? $data['gender'] : ''
                                                    ])}}

                                                    {{ItemMaker::datePicker([
                                                        'labelText' => '生日',
                                                        'inputName' => 'Member[birthday]',
                                                        'value' => ( !empty($data['birthday']) )? $data['birthday'] : ''
                                                    ])}}

                                                    {{ItemMaker::select([
                                                        'labelText' => '縣市',
                                                        'inputName' => 'Member[city]',
                                                        'options'   => $parent['belong']['City'],
                                                        'value' => ( !empty($data['city']) )? $data['city'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '郵遞區號',
                                                        'inputName' => 'Member[zip]',
                                                        'value' => ( !empty($data['zip']) )? $data['zip'] : ''
                                                    ])}}
                                                    
                                                    {{ItemMaker::textInput([
                                                        'labelText' => '地址',
                                                        'inputName' => 'Member[address]',
                                                        'value' => ( !empty($data['address']) )? $data['address'] : ''
                                                    ])}}

                                                    {{ItemMaker::photo([
                                                        'labelText' => '會員大頭貼',
                                                        'inputName' => 'Member[image]',
                                                        'value' => ( !empty($data['image']) )? $data['image'] : ''
                                                    ])}}
                                                    
                                                </div>
                                            </div>

                                             <?php  
                                                 $position=[
                                                        1=>['id'=>1 ,'value'=>1 ,'title'=>'會員生日禮'],
                                                        2=>['id'=>2 ,'value'=>2 ,'title'=>'購買回饋'],
                                                        3=>['id'=>3 ,'value'=>3 ,'title'=>'訂單折抵'],
                                                        4=>['id'=>4 ,'value'=>4 ,'title'=>'好友推薦'],
                                                        5=>['id'=>5 ,'value'=>5 ,'title'=>'首次消費'],
                                                        6=>['id'=>6 ,'value'=>6 ,'title'=>'會員註冊'],
                                                                                                                                                     
                                                    ];

                                            ?>


                                            <div class="tab-pane form-row-seperated" id="tab_point">



                                                  {{ FormMaker::photosTable(
                                                    [
                                                        'nameGroup' => $routePreFix.'-Bonus',
                                                        'datas' => ( !empty($parent['has']['Bonus']) )? $parent['has']['Bonus'] : [],
                                                        'table_set' =>  $bulidTable['Bonus'],
                                                        'pic' => false,
                                                        "select_special"=>1,
                                                        "selOption"=>$position,
                                                    ]
                                                 ) }}

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
                    <!--</form> 
                     END FORM-->
                     {!! Form::close() !!}
                </div>
            </div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

    <script>
        $(document).ready(function() {

            FilePicke();
            Rank();
            //資料刪除
            dataDelete();

            colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif

        });

        allbonus=0;
        $("#tab_point #itemTbody tr").each(function(){
            if($(this).find("input").eq(6).val() == 1)
            {
                allbonus+=$(this).find("input").eq(3).val()/1;
            }
        });


        $("#tab_images_uploader_container").append('<a id="tab_images_uploader_uploadfiles" href="javascript:;" data-model="Member/List/Bonus" class="btn dark btn-outline tooltips" data-container="body" data-placement="top" data-original-title="可使用點數:'+allbonus+'">  可使用點數:'+allbonus+'  <i class=""></i></a>');
        
    </script>
@stop