@extends('Fantasy.template')

@section('title', "會員服務條款管理"."-".$ProjectName)
@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    </style>
@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
            會員服務條款管理 <small>Teams Of Service Manager</small> </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="{{ ItemMaker::url('Fantasy/')}}">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{ ItemMaker::url('Fantasy/'.$routePreFix)}}">會員服務條款管理</a> <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    會員服務條款資料 </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->


            <div class="row">
                <div class="col-md-12">

                    <!--注意事項
                     <div class="note note-danger">
                     <p> 注意事項:</p>
                    </div>注意事項END-->


                    {!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}
                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
                                            {{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 會員服務條款 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    <a href=" {{ ItemMaker::url('Fantasy/'.$routePreFix) }}" type="button" name="back" class="btn default btn-secondary-outline btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="返回"> <i class="fa fa-mail-reply"></i> 返回
                                    </a>

                                    <button type="submit"  class="btn btn-lg green btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="儲存">
                                        <i class="fa fa-check"></i> 儲存
                                    </button>

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news"> 
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> Information </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

                                                    {{ItemMaker::idInput([
                                                        'inputName' => 'MemberService[id]',
                                                        'value' => ( !empty($data['id']) )? $data['id'] : ''
                                                    ])}}
                                                    
                                                    {{ItemMaker::radio_btn([
                                                        'labelText' => '是否啟用',
                                                        'inputName' => 'MemberService[is_visible]',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
                                                    ])}}
                                                    {{ItemMaker::textInput([
                                                        'labelText' => '排序',
                                                        'inputName' => 'MemberService[rank]',
                                                        'value' => ( !empty($data['rank']) )? $data['rank'] : ''
                                                    ])}}
                                                    {{ItemMaker::textInput([
                                                        'labelText' => '標題',
                                                        'inputName' => 'MemberService[title]',
                                                        'value' => ( !empty($data['title']) )? $data['title'] : ''
                                                    ])}}
                                                    {{ItemMaker::textArea([
                                                        'labelText' => '內容',
                                                        'inputName' => 'MemberService[content]',
                                                        'value' => ( !empty($data['content']) )? $data['content'] : ''
                                                    ])}}
                                                    
                                                    {{ItemMaker::textArea([
                                                        'labelText' => '短述',
                                                        'inputName' => 'MemberService[sub_content]',
                                                        'value' => ( !empty($data['sub_content']) )? $data['sub_content'] : ''
                                                    ])}}
                                                    {{ FormMaker::photosTable(
                                                    [
                                                        'nameGroup' => $routePreFix.'-MemberServicedata',
                                                        'datas' => ( !empty($parent['has']['MemberServicedata']) )? $parent['has']['MemberServicedata'] : [],
                                                        'table_set' => $bulidTable['MemberServicedata'],
                                                        'pic' => false,
                                                    ]
                                                 ) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
                    <!--</form> 
                     END FORM-->
                     {!! Form::close() !!}
                </div>
            </div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

    <script>
        $(document).ready(function() {

            FilePicke();
            Rank();
            //資料刪除
            dataDelete();

            colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif

        });
    </script>
@stop