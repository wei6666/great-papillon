@extends('Fantasy.template')

@section('title',"網站基本設定修改 "."-".$ProjectName)

@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/plugins/colorbox/colorbox.css" />
@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			網站設定 <small>Setting Manager</small>
		</h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('Fantasy/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="{{ ItemMaker::url('Fantasy/網站設定')}}">網站設定</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					網站設定修改
				</li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
                    {{-- <div class="note note-danger">
                        <p> 注意事項: The below datatable is not connected to a real database so the filter and sorting is just simulated for demo purposes only. </p>
                    </div> --}}
                    <!--注意事項END-->

					<!--<form action="{{ ItemMaker::url('Fantasy/網站設定/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
					{!! Form::open( ["url" => ItemMaker::url('Fantasy/網站設定/update'), 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											網站設定
										</span>
                                    </div>
                                    <div class="actions btn-set">
 										<a href=" {{ ItemMaker::url('Fantasy/網站設定/') }}" type="button" name="back" class="btn default btn-secondary-outline btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="返回">
                                            <i class="fa fa-mail-reply"></i> 返回
                                        </a>

                                        <button type="submit"  class="btn btn-lg green btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="儲存">
                                            <i class="fa fa-check"></i> 儲存
                                        </button>
                                        {{-- <a href="#" class="btn btn-lg green btn-circle tooltips" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="刪除">
                                            <i class="fa fa-external-link"></i> 刪除
                                        </a> --}}

                                        {{-- <div class="btn-group">
                                            <a class="btn dark btn-outline btn-circle tooltips" href="javascript:;" data-toggle="dropdown" aria-expanded="false" data-rel="fancybox-button" data-container="body" data-placement="top" data-original-title="進階功能">
                                                <i class="fa fa-cog"></i>
                                                <span class="hidden-xs"> 進階功能 </span>
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;"> Export to Excel </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> Export to CSV </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> Export to XML </a>
                                                </li>
                                                <li class="divider"> </li>
                                                <li>
                                                    <a href="javascript:;"> Print Invoices </a>
                                                </li>
                                            </ul>
                                        </div> --}}
                                        <!-- <button class="btn btn-success">
                                            <i class="fa fa-check-circle"></i> Save & Continue Edit</button> -->
                                        <!-- <div class="btn-group">
                                            <a class="btn btn-success dropdown-toggle" href="javascript:;" data-toggle="dropdown">
                                                <i class="fa fa-share"></i> More
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <div class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;"> Duplicate </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> Delete </a>
                                                </li>
                                                <li class="dropdown-divider"> </li>
                                                <li>
                                                    <a href="javascript:;"> Print </a>
                                                </li>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> 網站設定 </a>
                                            </li>

                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

													{{ItemMaker::textInput([
														'labelText' => '公司郵件地址(一般)',
														'inputName' => 'webset[email_general]',
														'helpText' =>'如果這一個欄位有填寫時，前台將會以這邊顯示於網頁title',
														'value' => ( !empty($data['email_general']) )? $data['email_general'] : ''
													])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '公司郵件地址(銷售)',
                                                        'inputName' => 'webset[email_sales]',
                                                        'value' => ( !empty($data['email_sales']) )? $data['email_sales'] : ''
                                                    ])}}


                                                    {{ItemMaker::textInput([
                                                        'labelText' => '公司電話',
                                                        'inputName' => 'webset[phone]',
                                                        'value' => ( !empty($data['phone']) )? $data['phone'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '公司傳真',
                                                        'inputName' => 'webset[fax]',
                                                        'value' => ( !empty($data['fax']) )? $data['fax'] : ''
                                                    ])}}


                                                    {{ItemMaker::textInput([
                                                        'labelText' => '公司地址',
                                                        'inputName' => 'webset[address]',
                                                        'value' => ( !empty($data['address']) )? $data['address'] : ''
                                                    ])}}


                                                    {{ItemMaker::textInput([
                                                        'labelText' => '郵政信箱',
                                                        'inputName' => 'webset[po_box]',
                                                        'value' => ( !empty($data['po_box']) )? $data['po_box'] : ''
                                                    ])}}


                                                    {{ItemMaker::textInput([
                                                        'labelText' => 'Google 地圖嵌入代碼',
                                                        'inputName' => 'webset[map_link]',
                                                        'helpText' => '請貼QR碼網址',
                                                        'value' => ( !empty($data['map_link']) )? $data['map_link'] : ''
                                                    ])}}


                                                    {{ItemMaker::textInput([
                                                        'labelText' => 'Google 分析 UA 代碼',
                                                        'inputName' => 'webset[google_ua_code]',
                                                        'value' => ( !empty($data['google_ua_code']) )? $data['google_ua_code'] : ''
                                                    ])}}


                                                    {{ItemMaker::textInput([
                                                        'labelText' => '網站 Meta 描述',
                                                        'inputName' => 'webset[meta_describe]',
                                                        'value' => ( !empty($data['meta_describe']) )? $data['meta_describe'] : ''
                                                    ])}}


                                                    {{ItemMaker::textInput([
                                                        'labelText' => '網站 Meta 關鍵字',
                                                        'inputName' => 'webset[meta_keyword]',
                                                        'helpText' => '前台的聯絡我們表單送出之後，通知信的收件信箱設定。',
                                                        'value' => ( !empty($data['meta_keyword']) )? $data['meta_keyword'] : ''
                                                    ])}}


                                                    {{ItemMaker::textInput([
                                                        'labelText' => '郵件發送時的名稱',
                                                        'inputName' => 'webset[sender_name]',
                                                        'helpText' => '前台的聯絡我們表單送出之後，通知信的收件信箱設定。',
                                                        'value' => ( !empty($data['sender_name']) )? $data['sender_name'] : ''
                                                    ])}}


                                                    {{ItemMaker::textInput([
                                                        'labelText' => '郵件發送的電子郵件',
                                                        'inputName' => 'webset[sender_email]',
                                                        'helpText' => '前台的聯絡我們表單送出之後，通知信的收件信箱設定。',
                                                        'value' => ( !empty($data['sender_email']) )? $data['sender_email'] : ''
                                                    ])}}


                                                    {{ItemMaker::textInput([
                                                        'labelText' => 'Mailgun API Key',
                                                        'inputName' => 'webset[mailgun_apikey]',
                                                        'helpText' => '前台的聯絡我們表單送出之後，通知信的收件信箱設定。',
                                                        'value' => ( !empty($data['mailgun_apikey']) )? $data['mailgun_apikey'] : ''
                                                    ])}}


                                                    {{ItemMaker::textInput([
                                                        'labelText' => 'Mailgun API Domain',
                                                        'inputName' => 'webset[mailgun_domain]',
                                                        'helpText' => '前台的聯絡我們表單送出之後，通知信的收件信箱設定。',
                                                        'value' => ( !empty($data['mailgun_domain']) )? $data['mailgun_domain'] : ''
                                                    ])}}


                                                    {{ItemMaker::textInput([
                                                        'labelText' => '電子報測試用收信信箱',
                                                        'inputName' => 'webset[newspaper_email]',
                                                        'helpText' => '前台的聯絡我們表單送出之後，通知信的收件信箱設定。',
                                                        'value' => ( !empty($data['newspaper_email']) )? $data['newspaper_email'] : ''
                                                    ])}}


                                                    {{ItemMaker::textInput([
                                                        'labelText' => '聯絡我們收信信箱',
                                                        'inputName' => 'webset[inquiry_email]',
                                                        'helpText' => '前台的聯絡我們表單送出之後，通知信的收件信箱設定。',
                                                        'value' => ( !empty($data['inquiry_email']) )? $data['inquiry_email'] : ''
                                                    ])}}

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Fantasy/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();
            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif
		});

	</script>
@stop
