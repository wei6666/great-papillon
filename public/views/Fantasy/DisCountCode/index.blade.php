@extends('Fantasy.template')

@section('css')

<link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />

@stop

@section('content')

<!-- 內容最上方大標題 -->
<h3 class="page-title"> 產品基本設定
    <small>New List</small>
</h3>
<!-- 內容最上方大標題END -->
<!--網站導覽-->
<div class="page-bar">
<ul class="page-breadcrumb">
    <li>
        <i class="icon-home"></i>
        <a href="{{ ItemMaker::url('Fantasy') }}">Home</a>
        <i class="fa fa-angle-right"></i>
    </li>

    <li>
        <span>產品基本設定</span>
    </li>
</ul>

</div>
<!--網站導覽END-->
<!-- END PAGE HEADER-->
<div class="row">
<!--內容區開始-->
<div class="col-md-12">
    <!--注意事項-->
    {{-- <div class="note note-danger">
        <p> 注意事項: The below datatable is not connected to a real database so the filter and sorting is just simulated for demo purposes only. </p>
    </div> --}}
    <!--注意事項END-->
    <!--內容區白色區塊-->
    <div class="portlet light portlet-fit portlet-datatable ">
        <!--內容白色區塊標題-->
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase">產品基本設定</span>
            </div>
            <!--actions-->
            <div class="actions">

                <div class="btn-group btn-group-devided" >
                        <a href="{{ItemMaker::url("Fantasy/".$routePreFix."/create")}}" class="btn green-sharp btn-circle btn-sm">
                            <i class="fa fa-plus"></i> 新增
                        </a>
                </div>

                @include('Fantasy.include.actionMenu')

            </div>
            <!--actions END-->
        </div>
        <!--內容白色區塊標題END-->

        <!--內容白色區塊表格-->
        <div class="portlet-body">
           
            <div class="table-container">
                <?php
                    use App\Http\Models\Discount\DisCountCode;
                    $DisCountCode=DisCountCode::where('is_visible',1)
                                      ->where('st_date',"<=",Date("Y-m-d"))
                                      ->where('ed_date',">=",Date("Y-m-d"))
                                      ->OrderBy('id','desc')
                                      ->get();

                ?>
                @if(count($DisCountCode) >0)
                <h3>目前可使用的折扣碼:
                    @foreach($DisCountCode as $value)
                        <br>{{ $value->title }} <font color='red'> {{ $value->code }}</font> 折扣 {{ $value->money_discount }} 元
                        @if( $value->is_repeat ==1 )
                        可以重複使用
                        @else
                        不可重複使用
                        @endif
                        {{-- 需要補人次使用狀況 --}}

                    @endforeach

                </h3>
                @endif

                <!--表格↓↓↓↓↓↓↓↓-->
                {{FormMaker::listTable([
                    'ajaxEditLink' => ItemMaker::url($ajaxEditLink),
                    'Datas' => $Datas,
                    'model' => $routePreFix,
                    'modelName' => $modelName,
                    'tableSet' => [                                 
                        [
                            'title' => '折扣標題',
                            'columns' => 'title',
                            'width' => '100'
                        ],
                        [
                            'title' => '折扣碼',
                            'columns' => 'code',
                            'width' => '100'
                        ],
                        [
                            'title' => '折扣價格',
                            'columns' => 'money_discount',
                            'width' => '100'
                        ],
                        [
                            'title' => '開始日期',
                            'columns' => 'st_date',
                            'width' => '100'
                        ],
                        [
                            'title' => '結束日期',
                            'columns' => 'ed_date',
                            'width' => '100'
                        ],
                        [
                            'title' => '使用人次',
                            'columns' => 'order_list_data',
                            'width' => '100'
                        ],
                        [
                            'title' => '使用人次上限',
                            'columns' => 'order_list_limit',
                            'width' => '100'
                        ],
                        [   
                            'title' => '是否啟用',
                            'group' => [
                                [
                                    'title' => '是否啟用',
                                    'sub_title' => 'S',
                                    'columns' => 'is_visible',
                                    'helpText' =>'是否啟用',
                                    'color' => 'label-success',
                                    'options' => $StatusOption 
                                ]
                            ]

                        ],
                        [   
                            'title' => '是否可以重複',
                            'group' => [
                                [
                                    'title' => '是否可以重複',
                                    'sub_title' => 'L',
                                    'columns' => 'is_repeat',
                                    'helpText' =>'是否可以重複',
                                    'color' => 'label-success',
                                    'options' => $StatusOption 
                                ]
                            ]

                        ],
                        
                    ]
                ])}}
                <!--表格END-->
            </div>
            <!--內容白色區塊表格END-->
        </div>
        <!-- 內容白色區塊END-->
    </div>
    <!--內容區END-->
</div>

@stop
@section('script')

    <script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>

    <script src="vendor/Fantasy/global/plugins/datatables/media/js/jquery.dataTables.js" type="text/javascript"></script>
    <script type="text/javascript" src="vendor/Fantasy/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

    <script type="text/javascript" src="vendor/main/datatable.js"></script>
    <script>

        //資料刪除
        activeDataTable();
        dataDelete();
        changeStatic_ajax();
        changeStatic();

    </script>   
@stop