@extends('Fantasy.template')

@section('title',"最新消息詳細頁修改修改 "."-".$ProjectName)
@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    #showPic.anchorImg{ width: auto!important; max-height: 250px;  }
    #itemTbody div.images_icon2 { display: none; }
    </style>
      <style type="text/css">
  .page-sidebar-wrapper, .page-header, .page-title, .page-bar { display: none; }
     .page-content-wrapper .page-content { margin: 0px; padding: 0px; }
     .page-content { min-height: 700px!important; }
     .page-header-fixed .page-container { margin: 0px!important; }
     .fantasy-selectImageBtn { display: none; }
     div.actions > a { display: none; }
     div.page-bar { display: none; }
    </style>

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
                    {{-- <div class="note note-danger">
                        <p> 注意事項: 婚宴會議單元，才需要填入短述。<br>
                            交通訊息、集團介紹、問與答、館內活動、歡樂設施 非輪播單元，所以只需要上傳一組圖片。
                        </p>
                    </div> --}}
                    <!--注意事項END-->

					<!--<form action="{{ ItemMaker::url('Fantasy/工程分類/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}
                    
                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 最新消息詳細頁修改 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    @include('Fantasy.include.editActionBtns')

                                    <?php  
                                     $imgtype=[
                                                        // 1=>['id'=>1 ,'value'=>1 ,'title'=>'格式1(無圖片)'],
                                                        2=>['id'=>2 ,'value'=>2 ,'title'=>'格式1(包含內容列表)'],
                                                        3=>['id'=>3 ,'value'=>3 ,'title'=>'格式2(一大圖)'],
                                                        4=>['id'=>4 ,'value'=>4 ,'title'=>'格式3(雙小圖並排)'],
                                                        5=>['id'=>5 ,'value'=>5 ,'title'=>'格式4(圖左文右)'],
                                                        6=>['id'=>6 ,'value'=>6 ,'title'=>'格式5(圖右文左)'],
                                                       
                                                                                                 
                                                    ];
                                                    ?>

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> 新聞內容設定 </a>
                                            </li>

                                        
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

                                                    {{ItemMaker::select([
                                                        'labelText' => '區塊格式',
                                                        'inputName' => 'NewsContent[type]',
                                                        //'required'  => true,
                                                        'options'   => $imgtype,
                                                        'value' => ( !empty($data['type']) )? $data['type'] : ''
                                                    ])}}

													{{ItemMaker::idInput([
														'inputName' => 'NewsContent[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
													])}}

                                                     {{ItemMaker::textInput([
                                                        'labelText' => '標題',
                                                        'inputName' => 'NewsContent[title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['title']) )? $data['title'] : ''
                                                    ])}}


                                                     {{ItemMaker::editor([
                                                        'labelText' => '內容',
                                                        'inputName' => 'NewsContent[content]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['content']) )? $data['content'] : ''
                                                    ])}}

                                                      {{ItemMaker::textArea([
                                                        'labelText' => '內容列表<br>(每筆資料用換行區隔)<br>超連結語法範例:<br>&lt;a href="完整連結"&gt;標題&lt;/a&gt;',
                                                        'inputName' => 'NewsContent[content_list]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['content_list']) )? $data['content_list'] : ''
                                                    ])}}


                                                      {{ItemMaker::photo([
                                                        'labelText' => '左圖',
                                                        'inputName' => 'NewsContent[image1]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['image1']) )? $data['image1'] : ''
                                                    ])}}


                                                    {{ItemMaker::textInput([
                                                        'labelText' => '左圖下方短述',
                                                        'inputName' => 'NewsContent[image1_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['image1_title']) )? $data['image1_title'] : ''
                                                    ])}}


                                                    {{ItemMaker::photo([
                                                        'labelText' => '右圖',
                                                        'inputName' => 'NewsContent[image2]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['image2']) )? $data['image2'] : ''
                                                    ])}}


                                                    {{ItemMaker::textInput([
                                                        'labelText' => '右圖下方短述',
                                                        'inputName' => 'NewsContent[image2_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['image2_title']) )? $data['image2_title'] : ''
                                                    ])}}

                                                </div>
                                            </div>



                                 




                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif

        $(".form-group").hide();
        $(".form-group").eq(0).show();
        $(".form-group").eq(1).show();
        $(".form-group").eq(2).show();
                if($(".select2-selection__rendered").attr('title')=="格式1(包含內容列表)")
                {
                    $(".form-group").eq(3).show()
                   
                }
                if($(".select2-selection__rendered").attr('title')=="格式2(一大圖)")
                {
                    $(".form-group").eq(3).show()
                    $(".form-group").eq(4).show();
                    $(".form-group").eq(4).find('label').eq(0).text("大圖 950 x 350");
                    $(".form-group").eq(5).show();
                    $(".form-group").eq(5).find('label').eq(0).text("大圖下方短述");
                }
                if($(".select2-selection__rendered").attr('title')=="格式3(雙小圖並排)")
                {
                     $(".form-group").eq(3).show()
                     $(".form-group").eq(4).show();
                     $(".form-group").eq(4).find('label').eq(0).text("(左圖 470 x 300)");
                     $(".form-group").eq(5).show();
                     $(".form-group").eq(6).show();
                     $(".form-group").eq(6).find('label').eq(0).text("(左圖 470 x 300)");
                     $(".form-group").eq(7).show();

                   
                }
                if($(".select2-selection__rendered").attr('title')=="格式4(圖左文右)")
                {
                     $(".form-group").eq(3).show()
                     $(".form-group").eq(4).show();
                     $(".form-group").eq(4).find('label').eq(0).text("圖片 950 x 580");
                     $(".form-group").eq(5).show();
                     $(".form-group").eq(5).find('label').eq(0).text("圖片敘述");
                    

                   
                }
                if($(".select2-selection__rendered").attr('title')=="格式5(圖右文左)")
                {
                     $(".form-group").eq(3).show()
                     $(".form-group").eq(4).show();
                     $(".form-group").eq(4).find('label').eq(0).text("圖片 950 x 580");
                     $(".form-group").eq(5).show();
                     $(".form-group").eq(5).find('label').eq(0).text("圖片敘述");

                }
              

            $("#tab_general div:nth-child(2)").change(function(){
                $(".form-group").hide();
                $(".form-group").eq(0).show();
                $(".form-group").eq(1).show();
                $(".form-group").eq(2).show();

                if($(".select2-selection__rendered").attr('title')=="格式1(包含內容列表)")
                {
                    $(".form-group").eq(3).show()
                   
                }
                if($(".select2-selection__rendered").attr('title')=="格式2(一大圖)")
                {
                    $(".form-group").eq(3).show()
                    $(".form-group").eq(4).show();
                    $(".form-group").eq(4).find('label').eq(0).text("大圖 950 x 350");
                    $(".form-group").eq(5).show();
                    $(".form-group").eq(5).find('label').eq(0).text("大圖下方短述");
                }
                if($(".select2-selection__rendered").attr('title')=="格式3(雙小圖並排)")
                {
                     $(".form-group").eq(3).show()
                     $(".form-group").eq(4).show();
                     $(".form-group").eq(4).find('label').eq(0).text("(左圖 470 x 300)");
                     $(".form-group").eq(5).show();
                     $(".form-group").eq(6).show();
                     $(".form-group").eq(6).find('label').eq(0).text("(左圖 470 x 300)");
                     $(".form-group").eq(7).show();

                   
                }
                if($(".select2-selection__rendered").attr('title')=="格式4(圖左文右)")
                {
                     $(".form-group").eq(3).show()
                     $(".form-group").eq(4).show();
                     $(".form-group").eq(4).find('label').eq(0).text("圖片 950 x 580");
                     $(".form-group").eq(5).show();
                     $(".form-group").eq(5).find('label').eq(0).text("圖片敘述");
                    

                   
                }
                if($(".select2-selection__rendered").attr('title')=="格式5(圖右文左)")
                {
                     $(".form-group").eq(3).show()
                     $(".form-group").eq(4).show();
                     $(".form-group").eq(4).find('label').eq(0).text("圖片 950 x 580");
                     $(".form-group").eq(5).show();
                     $(".form-group").eq(5).find('label').eq(0).text("圖片敘述");

                }
            });

          
		});

        




	</script>
@stop
