@extends('Fantasy.template')

@section('title',"訂單管理修改 "."-".$ProjectName)
@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    #showPic.anchorImg{ width: auto!important; max-height: 250px;  }
    </style>

@stop

@section('content')
<?php 
$is_go=['否','是'];

?>
<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			訂單管理 <small>Newscatagories Manager</small> </h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('Fantasy/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="{{ ItemMaker::url('Fantasy/'.$routePreFix)}}">訂單管理</a> <i class="fa fa-angle-right"></i>
				</li>
				<li>
					訂單管理修改 </li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">


					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 訂單管理 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    @include('Fantasy.include.editActionBtns')

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> 訂單管理 </a>
                                            </li>

                                             <li >
                                                <a href="#tab_general1" data-toggle="tab"> 訂單詳細資訊 </a>
                                            </li>

                                            <li >
                                                <a href="#tab_general2" data-toggle="tab"> 購買商品 </a>
                                            </li>
                                            {{-- @if(!empty(json_decode($data['ticket_receiver']))) --}}
                                            <li >
                                                <a href="#tab_general3" data-toggle="tab"> 寄送地址 </a>
                                            </li>
                                          {{--   @endif
                                            @if(!empty(json_decode($data['shop_receiver']))) --}}
                                            <li >
                                                <a href="#tab_general4" data-toggle="tab"> 發票資訊 </a>
                                            </li>
                                          {{--   @endif --}}
                                            <li >
                                                <a href="#tab_general5" data-toggle="tab"> 折扣內容 </a>
                                            </li>
                                             <li >
                                                <a href="#tab_general7" data-toggle="tab"> 紅利紀錄 </a>
                                            </li>

                                             <li >
                                                <a href="#tab_general6" data-toggle="tab"> 訂單問與答 </a>
                                            </li>
                                         
                                        
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

													{{ItemMaker::idInput([
														'inputName' => 'OrderList[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
													])}}

											
                                                  <?php

                                                  $order_type_arr=["未完成付款","未完成付款","訂單成立","已出貨","已到貨","取消訂單","退換貨審核中","完成訂單","退貨進行中","換貨進行中","已換貨","退換貨審核中","已退貨"];

                                                       $imgtype=[
                                                        1=>['id'=>1 ,'value'=>1 ,'title'=>'未完成付款'],
                                                        2=>['id'=>2 ,'value'=>2 ,'title'=>'訂單成立'],
                                                        3=>['id'=>3 ,'value'=>3 ,'title'=>'已出貨'],
                                                        4=>['id'=>4 ,'value'=>4 ,'title'=>'已到貨'],
                                                        5=>['id'=>5 ,'value'=>5 ,'title'=>'取消訂單'],
                                                        6=>['id'=>6 ,'value'=>6 ,'title'=>'退換貨審核中'],
                                                        7=>['id'=>7 ,'value'=>7 ,'title'=>'完成訂單'],
                                                        8=>['id'=>8 ,'value'=>8 ,'title'=>'退貨進行中'],
                                                        9=>['id'=>9 ,'value'=>9 ,'title'=>'換貨進行中'],
                                                        10=>['id'=>10 ,'value'=>10 ,'title'=>'已換貨'],
                                                        11=>['id'=>11 ,'value'=>11 ,'title'=>'退換貨審核中'],
                                                        12=>['id'=>12 ,'value'=>12 ,'title'=>'已退貨'],
                                                      
                                                                                                   
                                                    ];

                                                  ?>

                                                   {{ItemMaker::select([
                                                        'labelText' => '訂單狀態',
                                                        'inputName' => 'OrderList[order_list_type]',
                                                        'required'  => true,
                                                        'options'   => $imgtype,
                                                        'value' => ( !empty($data['order_list_type']) )? $data['order_list_type'] : ''
                                                    ])}}

                                                    

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '配送商家',
                                                        'inputName' => 'OrderList[send_company]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['send_company']) )? $data['send_company'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '配送編號',
                                                        'inputName' => 'OrderList[arrive_num]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['arrive_num']) )? $data['arrive_num'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '預定到貨時間',
                                                        'inputName' => 'OrderList[arrive_date]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['arrive_date']) )? $data['arrive_date'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '預定送達時間<br>上午下午或是其他區間',
                                                        'inputName' => 'OrderList[arrive_time_zone]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['arrive_time_zone']) )? $data['arrive_time_zone'] : ''
                                                    ])}}


                                               


                                                 

                                                </div>
                                            </div>

                                            <div class="tab-pane  form-row-seperated" id="tab_general1">
                                                <div class="form-body form_news_pad">

                                            {{ItemMaker::textInput([
                                                        'labelText' => '訂單編號',
                                                        'inputName' => 'OrderList[num_for_user]',
                                                        'disabled' =>'disabled',
                                                        'value' => ( !empty($data['num_for_user']) )? $data['num_for_user'] : ''
                                             ])}}

                                              {{ItemMaker::textInput([
                                                        'labelText' => '交易編號',
                                                        'inputName' => 'OrderList[TradeNo]',
                                                        'disabled' =>'disabled',
                                                        'value' => ( !empty($data['TradeNo']) )? $data['TradeNo'] : ''
                                             ])}}

                                              {{ItemMaker::textInput([
                                                        'labelText' => '購買人姓名',
                                                        'inputName' => 'OrderList[name]',
                                                        'disabled' =>'disabled',
                                                        'value' => ( !empty(str_replace('"', "", $data['name'])) )? str_replace('"', "", $data['name']) : ''
                                             ])}}

                                             {{ItemMaker::textInput([
                                                        'labelText' => '交易總額',
                                                        'inputName' => 'OrderList[price]',
                                                        'disabled' =>'disabled',
                                                        'value' => ( !empty($data['price']) )? $data['price'] : ''
                                             ])}}

                                              {{ItemMaker::textInput([
                                                        'labelText' => '使用紅利',
                                                        'inputName' => 'OrderList[used_bouns]',
                                                        'disabled' =>'disabled',
                                                        'value' => ( !empty($data['used_bouns']) )? $data['used_bouns'] : ''
                                             ])}}


                                             {{ItemMaker::textInput([
                                                        'labelText' => '交易手續費',
                                                        'inputName' => 'OrderList[PaymentTypeChargeFee]',
                                                        'disabled' =>'disabled',
                                                        'value' => ( !empty($data['PaymentTypeChargeFee']) )? $data['PaymentTypeChargeFee'] : ''
                                             ])}}

                                             
                                             {{ItemMaker::textInput([
                                                        'labelText' => '交易狀態',
                                                        'inputName' => 'OrderList[RtnCode]',
                                                        'disabled' =>'disabled',
                                                        'value' => ( !empty($data['RtnCode']) )? $is_go[$data['RtnCode']] : '否'
                                             ])}}

                

                                                </div>
                                            </div>

                                            <div class="tab-pane  form-row-seperated" id="tab_general2">
                                                <div class="form-body form_news_pad">
                                                    <div class="portlet-body portlet-body-news">
                                                        <div class="table-container">
                                                            <div class="table-container">
                                                                <div id="datatable_ajax_wrapper" class="dataTables_wrapper dataTables_extended_wrapper_news dataTables_extended_wrapper no-footer ">
                                                                    <div class="table-responsive">
                                                    <br>
                                                    <br>
                                                    <table class="table table-striped table-bordered table-hover table-header-fixed dataTable no-footer  table-header-news  marign-top-pad0 " id="ProductPhoto_table" data-model="ProductPhoto" name="ProductPhoto" data-tags="rank,image,is_visible,is_home1_img,is_home2_img,is_out_img,is_happinese_img,is_may_like">
                                                        <thead>
                                                            <tr role="row" class="heading"> 
                                                                <th class="header_th_0">商品名稱</th><th class="header_th_1">商品英文標題</th><th class="header_th_2">產品編號</th><th class="header_th_3">尺寸</th><th class="header_th_4">購買數量</th><th class="header_th_5">圖片</th>
                                                            </tr>
                                                        </thead>
                                                    <tbody id="itemTbody">
                                               
                                                    <?php 

                                                        foreach(json_decode($data['buy_stuff_json']) as $key => $value)
                                                        {
                                                            echo "<tr>";

                                                            echo "<td style='width:200px;'>".$value->title."</td>";
                                                            echo "<td style='width:200px;'>".$value->en_title."</td>";
                                                            echo "<td style='width:200px;'>".$value->complete_num."</td>";
                                                            echo "<td style='width:200px;'>".$value->size."</td>";
                                                            echo "<td style='width:200px;'>".$value->count."</td>";
                                                            echo "<td style='width:200px;'><img style='width:200px;' src='".$value->img."' ></td>";

                                                            echo "</tr>";
                                                        }
                                                            
                                                    ?>
                                                 
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                                            <div class="tab-pane  form-row-seperated" id="tab_general3">
                                                <div class="form-body form_news_pad">
                                                    <div class="portlet-body portlet-body-news">
                                                        <div class="table-container">
                                                            <div class="table-container">
                                                                <div id="datatable_ajax_wrapper" class="dataTables_wrapper dataTables_extended_wrapper_news dataTables_extended_wrapper no-footer ">
                                                                    <div class="table-responsive">
                                                    <br>
                                                    <br>
                                                    <table class="table table-striped table-bordered table-hover table-header-fixed dataTable no-footer  table-header-news  marign-top-pad0 " id="ProductPhoto_table" data-model="ProductPhoto" name="ProductPhoto" data-tags="rank,image,is_visible,is_home1_img,is_home2_img,is_out_img,is_happinese_img,is_may_like">
                                                        <thead>
                                                            <tr role="row" class="heading"> 
                                                                <th class="header_th_0">收件人姓名</th><th class="header_th_1">聯絡電話</th><th class="header_th_2">地址</th>
                                                            </tr>
                                                        </thead>
                                                    <tbody id="itemTbody">
                                                @if(!empty(json_decode($data['shop_receiver'])))
                                                    <?php 

                                                    $shop_data=json_decode($data['shop_receiver']);

                                                        
                                                            echo "<tr>"; 
                                                            
                                                            echo "<td style='width:200px;'>".$shop_data->name."</td>";
                                                            echo "<td style='width:200px;'>".$shop_data->mobile."</td>";
                                                            echo "<td style='width:200px;'>".$shop_data->zip." ".$shop_data->city." ".$shop_data->address."</td>";
                                                            echo "</tr>";
                                                        
                                                         
                                                    ?>
                                                 @endif
                                                    </tbody>
                                                 </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            


                                             <div class="tab-pane  form-row-seperated" id="tab_general4">
                                                <div class="form-body form_news_pad">
                                                    <div class="form-body form_news_pad">
                                                        <div class="portlet-body portlet-body-news">
                                                            <div class="table-container">
                                                                <div class="table-container">
                                                                    <div id="datatable_ajax_wrapper" class="dataTables_wrapper dataTables_extended_wrapper_news dataTables_extended_wrapper no-footer ">
                                                                    <div class="table-responsive">
                                                    <br>
                                                    <br>
                                                    <table class="table table-striped table-bordered table-hover table-header-fixed dataTable no-footer  table-header-news  marign-top-pad0 " id="ProductPhoto_table" data-model="ProductPhoto" name="ProductPhoto" data-tags="rank,image,is_visible,is_home1_img,is_home2_img,is_out_img,is_happinese_img,is_may_like">
                                                        <thead>
                                                            <tr role="row" class="heading"> 
                                                                <th class="header_th_0">收件人姓名</th>
                                                                <th class="header_th_1">聯絡電話</th><th class="header_th_2">地址</th>
                                                                <th class="header_th_3">發票格式</th>
                                                                <th class="header_th_4">統編號碼</th>
                                                                <th class="header_th_5">發票抬頭</th>
                                                            </tr>
                                                        </thead>
                                                    <tbody id="itemTbody">
                                                    @if(!empty(json_decode($data['ticket_receiver'])))
                                                    <?php

                                                    $shop_data=json_decode($data['ticket_receiver']);
                                                    $ticket_data=json_decode($data['ticket']);
                                                    
                                                        
                                                            echo "<tr>"; 
                                                            
                                                            echo "<td style='width:200px;'>".$shop_data->name."</td>";
                                                            echo "<td style='width:200px;'>".$shop_data->mobile."</td>";
                                                            echo "<td style='width:200px;'>".$shop_data->zip." ".$shop_data->city." ".$shop_data->address."</td>";
                                                            echo "<td style='width:200px;'>".$ticket_data[2]."</td>";
                                                            echo "<td style='width:200px;'>".$ticket_data[0]."</td>";
                                                            echo "<td style='width:200px;'>".$ticket_data[1]."</td>";

                                                            echo "</tr>";

                                                    ?>
                                                    @endif
                                                 
                                                    </tbody>
                                                 </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                                            <div class="tab-pane  form-row-seperated" id="tab_general5">
                                                <div class="form-body form_news_pad">
                                                    <div class="form-body form_news_pad">
                                                        <div class="form-body form_news_pad">
                                                            <div class="portlet-body portlet-body-news">
                                                                <div class="table-container">
                                                                    <div class="table-container">
                                                                        <div id="datatable_ajax_wrapper" class="dataTables_wrapper dataTables_extended_wrapper_news dataTables_extended_wrapper no-footer ">
                                                                            <div class="table-responsive">
                                                    <br>
                                                    <br>
                                                    <table class="table table-striped table-bordered table-hover table-header-fixed dataTable no-footer  table-header-news  marign-top-pad0 " id="ProductPhoto_table" data-model="ProductPhoto" name="ProductPhoto" data-tags="rank,image,is_visible,is_home1_img,is_home2_img,is_out_img,is_happinese_img,is_may_like">

                                                    @if(!empty(json_decode($data['DisCountPercentRecord'])))
                                                        <thead>
                                                            <tr role="row" class="heading"> 
                                                                <th class="header_th_0">全館折扣標題</th>
                                                                <th class="header_th_1">全館折扣內容</th><th class="header_th_2">全館折扣百分比</th>
                                                                <th class="header_th_3">全館折扣隨機碼</th>
                                                                <th class="header_th_4">全館折扣開始日期</th>
                                                                <th class="header_th_5">全館折扣結束日期</th>
                                                                <th class="header_th_5" colspan='2'>折扣金額</th>
                                                            </tr>
                                                        </thead>
                                                    <tbody id="itemTbody">
                                                    
                                                    <?php

                                                    $DisCountPercentRecord=json_decode($data['DisCountPercentRecord']);

                                                    $DisCountData=json_decode($data['discount']);

                                                            echo "<tr>"; 
                                                            
                                                            echo "<td style='width:200px;'>".$DisCountPercentRecord->title."</td>";
                                                            if(empty($DisCountPercentRecord->content))
                                                            {
                                                                $DisCountPercentRecord->content="-";
                                                            }
                                                            echo "<td style='width:200px;'>".$DisCountPercentRecord->content."</td>";

                                                            

                                                          
                                                            echo "<td style='width:200px;'>".$DisCountPercentRecord->percent."</td>";

                                                            if(empty($DisCountPercentRecord->code))
                                                            { 
                                                                $DisCountPercentRecord->code="-";
                                                            }
                                                            echo "<td style='width:200px;'>".$DisCountPercentRecord->code."</td>";
                                                           

                                                            echo "<td style='width:200px;'>".$DisCountPercentRecord->st_date."</td>";

                                                            echo "<td style='width:200px;'>".$DisCountPercentRecord->ed_date."</td>";

                                                             echo "<td  colspan='2' style='width:200px;'>".$DisCountData->PercentDisCount."</td>";

                                                            echo "</tr>";

                                                    ?>
                                                    @endif

                                                    
                                                    @if(!empty(json_decode($data['DisCountFullRecord'])))
                                                        <thead>
                                                            <tr role="row" class="heading"> 
                                                                <th class="header_th_0">滿額折扣標題</th>
                                                                <th class="header_th_1">滿額折扣內容</th><th class="header_th_2">滿額折扣</th>
                                                                <th class="header_th_3">滿額折扣隨機碼</th>
                                                                <th class="header_th_4">滿額折扣開始日期</th>
                                                                <th class="header_th_5">滿額折扣結束日期</th>
                                                                <th class="header_th_5" colspan='2'>折扣金額</th>
                                                            </tr>
                                                        </thead>
                                                    <tbody id="itemTbody">
                                                    
                                                    <?php

                                                    $DisCountFullRecord=json_decode($data['DisCountFullRecord']);

                                                  
                                                    
                                                    
                                                        
                                                            echo "<tr>"; 
                                                            
                                                            echo "<td style='width:200px;'>".$DisCountFullRecord->title."</td>";
                                                            if(empty($DisCountFullRecord->content))
                                                            {
                                                                $DisCountFullRecord->content="-";
                                                            }
                                                            echo "<td style='width:200px;'>".$DisCountFullRecord->content."</td>";
                                                            

                                                          
                                                            echo "<td style='width:200px;'>滿".$DisCountFullRecord->full." 送 ".$DisCountFullRecord->discount."</td>";
                                                            if(empty($DisCountFullRecord->code))
                                                            {
                                                                $DisCountFullRecord->code="-";
                                                            }
                                                            echo "<td style='width:200px;'>".$DisCountFullRecord->code."</td>";


                                                            echo "<td style='width:200px;'>".$DisCountFullRecord->st_date."</td>";

                                                            echo "<td style='width:200px;'>".$DisCountFullRecord->ed_date."</td>";

                                                             echo "<td colspan='2' style='width:200px;'>".$DisCountData->FullDisCount."</td>";

                                                            echo "</tr>";

                                                    ?>
                                                    @endif

                                                    @if(!empty(json_decode($data['DisCountCodeRecord'])))
                                                        <thead>
                                                            <tr role="row" class="heading"> 
                                                                <th class="header_th_0">折扣碼標題</th>
                                                                <th class="header_th_0">折扣碼折扣金額</th>
                                                                <th class="header_th_1">是否可以重複</th><th class="header_th_2">是否有上限</th>
                                                                <th class="header_th_3">折扣碼隨機碼</th>
                                                                <th class="header_th_4">折扣碼開始日期</th>
                                                                <th class="header_th_5">折扣碼結束日期</th>
                                                                <th class="header_th_5">折扣金額</th>
                                                            </tr>
                                                        </thead>

                                                    <tbody id="itemTbody">
                                                    
                                                    <?php

                                                    $DisCountCodeRecord=json_decode($data['DisCountCodeRecord']);

                                                  
                                                    
                                                    
                                                        
                                                            echo "<tr>"; 
                                                            
                                                            echo "<td style='width:200px;'>".$DisCountCodeRecord->title."</td>";
                                                           
                                                          
                                                           echo "<td style='width:200px;'>".$DisCountCodeRecord->money_discount."</td>";

                                                           $yesarr=['否','是'];

                                                           echo "<td style='width:200px;'>".$yesarr[$DisCountCodeRecord->is_repeat]."</td>";

                                                           echo "<td style='width:200px;'>".$DisCountCodeRecord->order_list_limit."</td>";
                                                           echo "<td style='width:200px;'>".$DisCountCodeRecord->code."</td>";

                                                            echo "<td style='width:200px;'>".$DisCountCodeRecord->st_date."</td>";

                                                            echo "<td style='width:200px;'>".$DisCountCodeRecord->ed_date."</td>";

                                                             echo "<td style='width:200px;'>".$DisCountData->CodeDisCount."</td>";

                                                            echo "</tr>";

                                                    ?>
                                                    @endif
                                                 
                                                    </tbody>
                                                 </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                                            <div class="tab-pane  form-row-seperated" id="tab_general6">
                                                <div class="form-body form_news_pad">
                                                    <div style="height: 50vh; overflow-y:  scroll;">
                                                    <table   class="table table-striped table-bordered table-hover table-header-fixed dataTable no-footer  table-header-news  marign-top-pad0 " id="ProductPhoto_table" data-model="ProductPhoto" name="ProductPhoto" data-tags="rank,image,is_visible,is_home1_img,is_home2_img,is_out_img,is_happinese_img,is_may_like">
                                                        <thead>
                                                            <tr role="row" class="heading"> 
                                                                <th  class="header_th_0">姓名</th><th class="header_th_1">時間</th><th class="header_th_2">內容</th>
                                                            </tr>
                                                           
                                                        </thead>
                                                    <tbody id="talkbar" class='.talk_box'>
                                               
                                                    </tbody>
                                                 </table>
                                                </div>
                                                <br>
                                               
                                                 <div style='height: 15vh; width: 90%;display: flex;'>
                                                    <textarea id='tosay' style="height: 100%; width: 90% ; "></textarea>
                                                    <input style="width: 14%; height: 100%; " type="button" name="" order_list_id='{{ $data['id'] }}' id="sent" value='傳送'>
                                                 </div> 
                                             
                                                </div>
                                            </div>


                                            <div class="tab-pane  form-row-seperated" id="tab_general7">
                                                <div class="form-body form_news_pad">
                                                    <div class="form-body form_news_pad">
                                                        <div class="form-body form_news_pad">
                                                            <div class="portlet-body portlet-body-news">
                                                                <div class="table-container">
                                                                    <div class="table-container">
                                                                        <div id="datatable_ajax_wrapper" class="dataTables_wrapper dataTables_extended_wrapper_news dataTables_extended_wrapper no-footer ">
                                                                            <div class="table-responsive">
                                                    <br>
                                                    <br>
                                                    <table class="table table-striped table-bordered table-hover table-header-fixed dataTable no-footer  table-header-news  marign-top-pad0 " id="ProductPhoto_table" data-model="ProductPhoto" name="ProductPhoto" data-tags="rank,image,is_visible,is_home1_img,is_home2_img,is_out_img,is_happinese_img,is_may_like">

                                                    @if(!empty(json_decode($data['feedback'])))
                                                   
                                                        <thead>
                                                            <tr role="row" class="heading"> 
                                                                <th class="header_th_0">購買回饋</th>
                                                                
                                                            </tr>
                                                        </thead>
                                                    <tbody id="itemTbody">
                                                    
                                                    <?php

                                                    $feedback=json_decode($data['feedback']);

                                                    $DisCountData=json_decode($data['discount']);

                                                            echo "<tr>"; 
                                                            
                                                            echo "<td style='width:200px;'>".$feedback->feedbackBP."</td>";
                                                            

                                                            echo "</tr>";

                                                    ?>
                                                    @endif

                                                    
                                                    @if(!empty(json_decode($data['friendfeedback'])))
                                                        <thead>
                                                            <tr role="row" class="heading"> 
                                                                <th class="header_th_0">好友回饋</th>
                                                                
                                                            </tr>
                                                        </thead>
                                                    <tbody id="itemTbody">
                                                    
                                                    <?php

                                                    $friendfeedback=json_decode($data['friendfeedback']);

                                                            echo "<tr>"; 
                                                            
                                                            echo "<td style='width:200px;'>".$friendfeedback->friendBP."</td>";
                                                           

                                                            echo "</tr>";

                                                    ?>
                                                    @endif

                                                    @if(!empty(json_decode($data['discount'])))
                                                        <thead>
                                                            <tr role="row" class="heading"> 
                                                                <th class="header_th_0">使用紅利</th>
                                                               
                                                            </tr>
                                                        </thead>

                                                    <tbody id="itemTbody">
                                                    
                                                    <?php

                                                    $discount=json_decode($data['discount']);

                                                  
                                                    
                                                    
                                                        
                                                            echo "<tr>"; 
                                                            
                                                            echo "<td style='width:200px;'>".$discount->BonusDisCount."</td>";
                                                           
                                                          
                                                           

                                                            echo "</tr>";

                                                    ?>
                                                    @endif
                                                 
                                                    </tbody>
                                                 </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif

            $("#sent").click(function(){
            url=$("meta").attr("lang")+"/backmember_bar/"+$(this).attr('order_list_id')+"/helper"+"/"+$("#tosay").val();
                console.log(url);
                $.ajax({
                     url: url,
                     type: 'GET',
                 })
                .done(function(data) 
                {
                    //console.log(data);
                    $("#talkbar").html(data);
                    $("#tosay").val("");
                });
            });

    renewtalkbox();   

    function renewtalkbox()
    {
        console.log("renewtalkbox");
        url=$("meta").attr("lang")+"/backmember_bar/"+$("#sent").attr('order_list_id');
        
        $.ajax({
             url: url,
             type: 'GET',
         })
        .done(function(data) 
        {
            //console.log(data);
            $("#talkbar").html(data);
            
        });

        setTimeout(function(){                
                    renewtalkbox();                
            },2000);

    }

		});

	</script>
@stop
