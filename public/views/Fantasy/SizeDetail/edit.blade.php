@extends('Fantasy.template')

@section('title',"產品基本設定修改 "."-".$ProjectName)
@section('css')

    
    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    #showPic.anchorImg{ width: auto!important; max-height: 250px;  }
    #itemTbody div.images_icon2 { display: none; }
    </style>
      <style type="text/css">
  .page-sidebar-wrapper, .page-header, .page-title, .page-bar { display: none; }
     .page-content-wrapper .page-content { margin: 0px; padding: 0px; }
     .page-content { min-height: 700px!important; }
     .page-header-fixed .page-container { margin: 0px!important; }
     .fantasy-selectImageBtn { display: none; }
     div.actions > a { display: none; }
     div.page-bar { display: none; }
    </style>

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			產品基本設定 <small>Newscatagories Manager</small> </h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('Fantasy/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="{{ ItemMaker::url('Fantasy/'.$routePreFix)}}">產品基本設定</a> <i class="fa fa-angle-right"></i>
				</li>
				<li>
					產品基本設定修改 </li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
                    {{-- <div class="note note-danger">
                        <p> 注意事項: 婚宴會議單元，才需要填入短述。<br>
                            交通訊息、集團介紹、問與答、館內活動、歡樂設施 非輪播單元，所以只需要上傳一組圖片。
                        </p>
                    </div> --}}
                    <!--注意事項END-->

					<!--<form action="{{ ItemMaker::url('Fantasy/工程分類/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 產品基本設定 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    @include('Fantasy.include.editActionBtns')

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                           {{--  <li >
                                                <a href="#tab_general" data-toggle="tab"> 尺寸表設定 </a>
                                            </li> --}}

                                            <li  class="active">
                                                <a href="#tab_genera2" data-toggle="tab">尺寸管理 </a>
                                            </li>

                                        
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane  form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

                                                  
                                                

												{{ItemMaker::idInput([
														'inputName' => 'ProductSeparate[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
												])}}


                                                {{ItemMaker::selectwithid([
                                                        'labelText' => '套用尺寸表<br>選擇完畢後請先儲存<br>再編輯各尺寸是否可以購買',
                                                        'inputName' => 'ProductSeparate[category_id]',
                                                        'required'  => true,
                                                        'options'   => $parent['belong']['SizeCategory'],
                                                        'value' => ( !empty($data['category_id']) )? $data['category_id'] : ''
                                                ])}}

                                                </div>
                                            </div>

                                               <div class="tab-pane active form-row-seperated" id="tab_genera2">
                                                <div class="form-body form_news_pad">

                                           {{--    {{ FormMaker::photosTable(
                                                    [
                                                        'nameGroup' => $routePreFix.'-ProductSize',
                                                        'datas' => ( !empty($parent['has']['ProductSize']) )? $parent['has']['ProductSize'] : [],
                                                        'table_set' =>  $bulidTable['photo'],
                                                        'pic' => false
                                                    ]
                                                 ) }} --}}


                                                <?php 
                                                use App\Http\Models\Size\AllSize;
                                                use App\Http\Models\Product\Product;
                                                use App\Http\Models\Product\ProductSubCategory;

                                                $Product=Product::where('id',$data->product_id)->first();

                                                $ProductSubcategory=ProductSubcategory::where('id',$Product->category_id)->first();

                                                $titledata=Allsize::where("is_visible",1)
                                                ->where('category_id',$ProductSubcategory->size_category)
                                                ->OrderBy('rank','asc')
                                                ->get();



                                                ?>
            <div class="form-group">
                <label style="margin-left: 7vw;">尺寸列表
                </label>
                 <label style="margin-left: 6vw;">是否可以購買
                </label>
            </div>

                                                @foreach($titledata as $key=> $value) 

                                                 {{ItemMaker::radio_btn([
                                                        'labelText' => $value->title."　　　　　" ,
                                                        'inputName' => 'ProductSeparate[a'.($key+1).']',
                                                        'value' => ( !empty($data["a".($key+1)]) )? $data["a".($key+1)] : ''
                                                    ])}}
                                                   
                                                @endforeach
                                 

                                                   </div>
                                            </div>



                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif 

            $("#tab_general div:nth-child(2)").change(function(){
                console.log($(".select2-selection__rendered").attr('id'));
                console.log($(".select2-selection__rendered").attr('title'));
                console.log($(".select2-selection__rendered").attr('value'));
             });



		});

      

         
	</script>
@stop
