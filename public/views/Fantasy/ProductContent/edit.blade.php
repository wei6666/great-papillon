@extends('Fantasy.template')

@section('title',"產品詳細類別修改 "."-".$ProjectName)
@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    #showPic.anchorImg{ width: auto!important; max-height: 250px;  }
    #itemTbody div.images_icon2 { display: none; }
    </style>
      <style type="text/css">
  .page-sidebar-wrapper, .page-header, .page-title, .page-bar { display: none; }
     .page-content-wrapper .page-content { margin: 0px; padding: 0px; }
     .page-content { min-height: 700px!important; }
     .page-header-fixed .page-container { margin: 0px!important; }
     .fantasy-selectImageBtn { display: none; }
     div.actions > a { display: none; }
     div.page-bar { display: none; }
    </style>

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
                    {{-- <div class="note note-danger">
                        <p> 注意事項: 婚宴會議單元，才需要填入短述。<br>
                            交通訊息、集團介紹、問與答、館內活動、歡樂設施 非輪播單元，所以只需要上傳一組圖片。
                        </p>
                    </div> --}}
                    <!--注意事項END-->

					<!--<form action="{{ ItemMaker::url('Fantasy/工程分類/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 產品詳細類別 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    @include('Fantasy.include.editActionBtns')

                                    <?php  

                                                   
                                                    $imgtype=[
                                                        1=>['id'=>1 ,'value'=>1 ,'title'=>'格式1一大圖(800 x 900)'],
                                                        2=>['id'=>2 ,'value'=>2 ,'title'=>'格式2一大圖 (800 x 900 ) , 一小圖左(280 x280)'],
                                                        3=>['id'=>3 ,'value'=>3 ,'title'=>'格式3(一大圖800 x 900) , 一小圖右(280 x 280))'],
                                                        4=>['id'=>4 ,'value'=>4 ,'title'=>'格式4兩中圖(800 x 410)'],
                                                                                                 
                                                    ];

                                                    $position=[
                                                        1=>['id'=>1 ,'value'=>1 ,'title'=>'左上'],
                                                        2=>['id'=>2 ,'value'=>2 ,'title'=>'左下'],
                                                        3=>['id'=>3 ,'value'=>3 ,'title'=>'右上'],
                                                        4=>['id'=>4 ,'value'=>4 ,'title'=>'右下'],
                                                                                                                                                     
                                                    ];



                                                    ?>

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> 問題與答案設定 </a>
                                            </li>

                                        
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

													{{ItemMaker::idInput([
														'inputName' => 'ProductCategoryAreadata[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
													])}}
                                                  
                                                     {{ItemMaker::select([
                                                        'labelText' => '區塊格式',
                                                        'inputName' => 'ProductCategoryAreadata[img_type]',
                                                        //'required'  => true,
                                                        'options'   => $imgtype,
                                                        'value' => ( !empty($data['img_type']) )? $data['img_type'] : ''
                                                    ])}}
                                               


                                                    {{ItemMaker::photo([
                                                        'labelText' => '區塊1圖片<br>(圖片尺寸參考區塊格式內容)',
                                                        'inputName' => 'ProductCategoryAreadata[img1]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['img1']) )? $data['img1'] : ''
                                                    ])}}

                                                     {{ItemMaker::textInput([
                                                        'labelText' => '圖片連結',
                                                        'inputName' => 'ProductCategoryAreadata[link1]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['link1']) )? $data['link1'] : ''
                                                    ])}}

                                                    {{ItemMaker::radio_btn([
                                                        'labelText' => '是否顯示第二張圖',
                                                        'inputName' => 'ProductCategoryAreadata[is_visible2]',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_visible2']) )? $data['is_visible2'] : ''
                                                    ])}}
                                                  

                                                    

                                                     {{ItemMaker::select([
                                                        'labelText' => '圖1標題位置',
                                                        'inputName' => 'ProductCategoryAreadata[title_position]',
                                                        'options'   => $position,
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['title_position']) )? $data['title_position'] : ''
                                                    ])}}

                                                    

                                                       {{ItemMaker::textInput([
                                                        'labelText' => '英文標題',
                                                        'inputName' => 'ProductCategoryAreadata[en_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['en_title']) )? $data['en_title'] : ''
                                                    ])}}

                                                       {{ItemMaker::textInput([
                                                        'labelText' => '中文標題',
                                                        'inputName' => 'ProductCategoryAreadata[title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['title']) )? $data['title'] : ''
                                                    ])}}

                                                     {{ItemMaker::photo([
                                                        'labelText' => '區塊2圖片<br>(圖片尺寸參考區塊格式內容<br>)',
                                                        'inputName' => 'ProductCategoryAreadata[img2]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['img2']) )? $data['img2'] : ''
                                                    ])}}


                                                     {{ItemMaker::textInput([
                                                        'labelText' => '圖片2連結<br>',
                                                        'inputName' => 'ProductCategoryAreadata[link2]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['link2']) )? $data['link2'] : ''
                                                    ])}}

                                                     {{ItemMaker::select([
                                                        'labelText' => '圖2標題位置2<br>',
                                                        'inputName' => 'ProductCategoryAreadata[title_position_extra]',
                                                        'options'   => $position,
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['title_position_extra']) )? $data['title_position_extra'] : ''
                                                    ])}}

                                                     {{ItemMaker::textInput([
                                                        'labelText' => '英文標題2<br>',
                                                        'inputName' => 'ProductCategoryAreadata[en_title_extra]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['en_title_extra']) )? $data['en_title_extra'] : ''
                                                    ])}}

                                                       {{ItemMaker::textInput([
                                                        'labelText' => '中文標題2<br>',
                                                        'inputName' => 'ProductCategoryAreadata[tw_title_extra]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['tw_title_extra']) )? $data['tw_title_extra'] : ''
                                                    ])}}

                                                      {{ItemMaker::colorPicker([
                                                        'labelText' => '標題顏色',
                                                        'inputName' => 'ProductCategoryAreadata[color]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['color']) )? $data['color'] : ''
                                                    ])}}

                                                

                                                 

                                                </div>
                                            </div>



                                 




                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif


        // $("ul li.select2-results__option").on('click',"li",function(){

        //     console.log($(this).attr('value')+$(this).text());
        // });
       // $(".form-group").css("opacity",'0');
       // $(".form-group").eq(0).css("opacity",'1');
        
        $(".form-group").hide();
        $(".form-group").eq(0).show();
        $(".form-group").eq(12).show();

        // var c=0;
        // $(".form-group").each(function(){
        //     $(this).find('label').text(c+"-"+$(this).find('label').text());
        //     c++;
        //     console.log(c);
        // });

        if($(".select2-selection__rendered").attr('title')=="格式1一大圖(800 x 900)")
                {
                    $(".form-group").eq(1).show();
                    $(".form-group").eq(1).find('label').text("區塊1圖片(圖片尺寸參考區塊格式內容 800 x 900)")
                    $(".form-group").eq(2).show();                    
                    $(".form-group").eq(4).show();
                    $(".form-group").eq(5).show();
                    $(".form-group").eq(6).show();
                   
                }
                if($(".select2-selection__rendered").attr('title')=="格式2一大圖 (800 x 900 ) , 一小圖左(280 x280)")
                {
                    $(".form-group").eq(1).show()
                    $(".form-group").eq(1).find('label').text("區塊1圖片(圖片尺寸參考區塊格式內容 800 x 900)")
                    $(".form-group").eq(2).show()
                    $(".form-group").eq(4).show()
                    $(".form-group").eq(7).find('label').text("區塊2圖片(圖片尺寸參考區塊格式內容 280 x 280)")                    
                    $(".form-group").eq(5).show()
                    $(".form-group").eq(6).show()
                    $(".form-group").eq(7).show()
                    
                }
                if($(".select2-selection__rendered").attr('title')=="格式3(一大圖800 x 900) , 一小圖右(280 x 280))")
                {
                    $(".form-group").eq(1).show()
                    $(".form-group").eq(1).find('label').text("區塊1圖片(圖片尺寸參考區塊格式內容 800 x 900)")
                    $(".form-group").eq(2).show()       
                    $(".form-group").eq(4).show()
                    $(".form-group").eq(7).find('label').text("區塊2圖片(圖片尺寸參考區塊格式內容 280 x 280)")                    
                    $(".form-group").eq(5).show()
                    $(".form-group").eq(6).show()
                    $(".form-group").eq(7).show()
                   
                }
                if($(".select2-selection__rendered").attr('title')=="格式4兩中圖(800 x 410)")
                {
                      $(".form-group").show()
                      var i=$(this).find('i');
                      $(".form-group").eq(1).find('label').text("圖片1(圖片尺寸參考區塊格式內容 800 x 410)");
                      $(".form-group").eq(7).find('label').text("圖片2(圖片尺寸參考區塊格式內容 800 x 410)")                    
                
                    if(i.hasClass("fa-check"))
                    {
                        $(".form-group").eq(7).show();
                        $(".form-group").eq(5).show();
                        $(".form-group").eq(9).show();
                        $(".form-group").eq(10).show();
                        $(".form-group").eq(11).show();

                    }
                    else
                    {
                        $(".form-group").eq(7).hide();
                        $(".form-group").eq(5).hide();
                        $(".form-group").eq(9).hide();
                        $(".form-group").eq(10).hide();
                        $(".form-group").eq(11).hide();
                    }
                }

      

            $("#tab_general div:nth-child(2)").change(function(){

                $(".form-group").hide();
                $(".form-group").eq(0).show()
                $(".form-group").eq(12).show();
                       if($(".select2-selection__rendered").attr('title')=="格式1一大圖(800 x 900)")
                {
                    $(".form-group").eq(1).show();
                    $(".form-group").eq(1).find('label').text("區塊1圖片(圖片尺寸參考區塊格式內容 800 x 900)")
                    $(".form-group").eq(2).show();                    
                    $(".form-group").eq(4).show();
                    $(".form-group").eq(5).show();
                    $(".form-group").eq(6).show();
                   
                }
                if($(".select2-selection__rendered").attr('title')=="格式2一大圖 (800 x 900 ) , 一小圖左(280 x280)")
                {
                    $(".form-group").eq(1).show()
                    $(".form-group").eq(1).find('label').text("區塊1圖片(圖片尺寸參考區塊格式內容 800 x 900)")
                    $(".form-group").eq(2).show()
                    $(".form-group").eq(4).show()
                    $(".form-group").eq(7).find('label').text("區塊2圖片(圖片尺寸參考區塊格式內容 280 x 280)")                    
                    $(".form-group").eq(5).show()
                    $(".form-group").eq(6).show()
                    $(".form-group").eq(7).show()
                    
                }
                if($(".select2-selection__rendered").attr('title')=="格式3(一大圖800 x 900) , 一小圖右(280 x 280))")
                {
                    $(".form-group").eq(1).show()
                    $(".form-group").eq(1).find('label').text("區塊1圖片(圖片尺寸參考區塊格式內容 800 x 900)")
                    $(".form-group").eq(2).show()       
                    $(".form-group").eq(4).show()
                    $(".form-group").eq(7).find('label').text("區塊2圖片(圖片尺寸參考區塊格式內容 280 x 280)")                    
                    $(".form-group").eq(5).show()
                    $(".form-group").eq(6).show()
                    $(".form-group").eq(7).show()
                   
                }
                if($(".select2-selection__rendered").attr('title')=="格式4兩中圖(800 x 410)")
                {
                      $(".form-group").show()
                      var i=$(this).find('i');
                      $(".form-group").eq(1).find('label').text("圖片1(圖片尺寸參考區塊格式內容 800 x 410)");
                      $(".form-group").eq(7).find('label').text("圖片2(圖片尺寸參考區塊格式內容 800 x 410)")                    
                
                    if(i.hasClass("fa-check"))
                    {
                        $(".form-group").eq(7).show();
                        $(".form-group").eq(5).show();
                        $(".form-group").eq(9).show();
                        $(".form-group").eq(10).show();
                        $(".form-group").eq(11).show();

                    }
                    else
                    {
                        $(".form-group").eq(7).hide();
                        $(".form-group").eq(5).hide();
                        $(".form-group").eq(9).hide();
                        $(".form-group").eq(10).hide();
                        $(".form-group").eq(11).hide();
                    }
                }

            });

            $("#EditStatic").click(function(){
                var i=$(this).find('i');
                
                if(i.hasClass("fa-check"))
                {
                    $(".form-group").eq(7).show();
                    $(".form-group").eq(5).show();
                    $(".form-group").eq(9).show();
                    $(".form-group").eq(10).show();
                    $(".form-group").eq(11).show();

                }
                else
                {
                    $(".form-group").eq(7).hide();
                    $(".form-group").eq(5).hide();
                    $(".form-group").eq(9).hide();
                    $(".form-group").eq(10).hide();
                    $(".form-group").eq(11).hide();
                }
            
            })

		});

      

	</script>
@stop
