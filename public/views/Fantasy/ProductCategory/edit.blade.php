@extends('Fantasy.template')

@section('title',"類別項目修改 "."-".$ProjectName)
@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    #showPic.anchorImg{ width: auto!important; max-height: 250px;  }
    </style>

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			類別項目 <small>Newscatagories Manager</small> </h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('Fantasy/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="{{ ItemMaker::url('Fantasy/'.$routePreFix)}}">類別項目</a> <i class="fa fa-angle-right"></i>
				</li>
				<li>
					類別項目修改 </li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
                    {{-- <div class="note note-danger">
                        <p> 注意事項: 婚宴會議單元，才需要填入短述。<br>
                            交通訊息、集團介紹、問與答、館內活動、歡樂設施 非輪播單元，所以只需要上傳一組圖片。
                        </p>
                    </div> --}}
                    <!--注意事項END-->

					<!--<form action="{{ ItemMaker::url('Fantasy/工程分類/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 類別項目 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    @include('Fantasy.include.editActionBtns')

                                    </div>
                                </div>

                                 <?php  

                                                   
                                                    $imgtype=[
                                                        1=>['id'=>1 ,'value'=>1 ,'title'=>'格式1一大圖(800 x 900)'],
                                                        2=>['id'=>2 ,'value'=>2 ,'title'=>'格式2一大圖 (800 x 810 ) , 一小圖左(280 x280)'],
                                                        3=>['id'=>3 ,'value'=>3 ,'title'=>'格式3(一大圖800 x 810) , 一小圖右(280 x 280))'],
                                                        4=>['id'=>4 ,'value'=>4 ,'title'=>'格式4兩中圖(800 x 410)'],
                                                                                                 
                                                    ];



                                                    ?>


                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">

                                             <li class="active">
                                                <a href="#tab_general9" data-toggle="tab"> 基本設定 </a>
                                            </li>
                                            <li >
                                                <a href="#tab_general1" data-toggle="tab"> 進內頁區塊設定 </a>
                                            </li>
                                             <li >
                                                <a href="#tab_general8" data-toggle="tab"> 內容區塊設定 </a>
                                            </li>
                                            <li >
                                                <a href="#tab_genera6" data-toggle="tab"> SEO管理 </a>
                                            </li>

                                           

                                            {{-- <li >
                                                <a href="#tab_general2" data-toggle="tab"> 區塊1 </a>
                                            </li>

                                            <li >
                                                <a href="#tab_general3" data-toggle="tab"> 區塊2 </a>
                                            </li>

                                            <li >
                                                <a href="#tab_general4" data-toggle="tab"> 區塊3 </a>
                                            </li>

                                            <li >
                                                <a href="#tab_general5" data-toggle="tab"> 區塊4 </a>
                                            </li> --}}

                                           
                                        </ul>
                                        <div class="tab-content">
                                         <div class="tab-pane active form-row-seperated" id="tab_general9">
                                                <div class="form-body form_news_pad">

                                                {{ItemMaker::idInput([
                                                        'inputName' => 'ProductCategory[id]',
                                                        'value' => ( !empty($data['id']) )? $data['id'] : ''
                                                    ])}}

                                                
                                                     {{ItemMaker::radio_btn([
                                                        'labelText' => '是否啟用',
                                                        'inputName' => 'ProductCategory[is_visible]',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
                                                    ])}}



                                                    {{ItemMaker::textInput([
                                                        'labelText' => '類別名稱(英文)',
                                                        'inputName' => 'ProductCategory[en_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['en_title']) )? $data['en_title'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '類別名稱(中文)',
                                                        'inputName' => 'ProductCategory[title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['title']) )? $data['title'] : ''
                                                    ])}}

                                                  <?php 

                                                use App\Http\Models\Product\Marketing;
                                              
                                                $allcate=[];

                                               
                                                    $datacate=Marketing::where("is_visible",1)   
                                                    ->OrderBy('rank','asc')
                                                    ->get()
                                                    ->toArray();


                                                    foreach($datacate as $value1)
                                                    {
                                                       
                                                    $allcate[$value1['title']]=$value1['id'];
                                                    }
                                                

                                                
                                                $productdata=( !empty( $data['market_id'] ) )?json_decode($data['market_id'], true) : [];
                                                
                                              
                                               
                                                ?>

                                               {{--  {{ItemMaker::selectMulti([
                                                        'labelText' => '行銷產品',
                                                        'inputName' => 'ProductCategory[market_id][]',
                                                        'helpText' =>'使用者帳號與聯絡用信箱',
                                                        "options" => $allcate,
                                                        "type" => "Role",
                                                        'value' => ( !empty($productdata) )? $productdata : ''
                                                    ])}} --}}

{{-- 
                                                {{ItemMaker::select([
                                                        'labelText' => '行銷類別',
                                                        'inputName' => 'ProductCategory[market_id]',
                                                        'required'  => true,
                                                        'options'   => $parent['belong']['Marketing'],
                                                        'value' => ( !empty($data['market_id']) )? $data['market_id'] : ''
                                                ])}} --}}


                                                {{ItemMaker::photo([
                                                        'labelText' => '首頁輪播區塊圖片(180 x 180)',
                                                        'inputName' => 'ProductCategory[home_image]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['home_image']) )? $data['home_image'] : ''
                                                    ])}}

                                                 {{ItemMaker::photo([
                                                        'labelText' => '行銷商品圖片(280 x 145)',
                                                        'inputName' => 'ProductCategory[product_header_img]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['product_header_img']) )? $data['product_header_img'] : ''
                                                    ])}}

                                                 {{ItemMaker::textInput([
                                                        'labelText' => '行銷商品連結',
                                                        'inputName' => 'ProductCategory[product_header_link]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['product_header_link']) )? $data['product_header_link'] : ''
                                                    ])}}

                                               



                                              {{--    {{ItemMaker::textInput([
                                                        'labelText' => '標題',
                                                        'inputName' => 'ProductCategory[home_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['home_title']) )? $data['home_title'] : ''
                                                    ])}} --}}


                                            </div>
                                        </div>
                                        
                                            <div class="tab-pane  form-row-seperated" id="tab_general1">
                                                <div class="form-body form_news_pad">
                                                 
                                                 <div class="form-group">

                                                  <div border='1' style="margin-top:  10px; margin: 0 20%; width: 10vw; height: 25vh; /*background-color: blue;*/ display: flex; flex-wrap: wrap; /*justify-content: space-between;*/ border: 2px solid;">

                                                         <div border='1' style="margin-top: 2px; margin-left:5%;  width: 90%; border: 2px solid; height: 35%; background-color: red;"></div>

                                                         <div border='1' style="margin-left:5%;  width: 42%; border: 2px solid; height: 25%; /*background-color: red;*/"></div>

                                                         <div border='1' style="margin-left:5%;  width: 42%; border: 2px solid; height: 25%; /*background-color: red;*/"></div>

                                                        <div border='1' style="margin-left:5%;  width: 42%; border: 2px solid; height:  25%; /*background-color: red;*/"></div>

                                                        <div border='1' style="margin-left:5%;  width: 42%; border: 2px solid; height:  25%; /*background-color: red;*/"></div>

                                                    </div>

                                                    </div>

                                                       {{ItemMaker::photo([
                                                        'labelText' => '區塊一圖片<br>(1500 x 670)',
                                                        'inputName' => 'ProductCategory[s1_img]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_img']) )? $data['s1_img'] : ''
                                                    ])}}
{{-- <br>(如果填此欄位就會取代選取圖片請填影片代號 EX:<br>www.youtube.com/watch?v=xxxx<br> xxxx為影片代號) --}}
                                                     {{ItemMaker::textInput([
                                                        'labelText' => '區塊1影片連結<br>(若此欄位及區塊1連結皆有填寫則以此欄位為主)',
                                                        'inputName' => 'ProductCategory[s1_vidio]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_vidio']) )? $data['s1_vidio'] : ''
                                                    ])}}


                                                    {{ItemMaker::textInput([
                                                        'labelText' => '區塊1連結',
                                                        'inputName' => 'ProductCategory[s1_link]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_link']) )? $data['s1_link'] : ''
                                                    ])}}


                                                       {{ItemMaker::textInput([
                                                        'labelText' => '英文標題',
                                                        'inputName' => 'ProductCategory[s1_en_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_en_title']) )? $data['s1_en_title'] : ''
                                                    ])}}

                                                       {{ItemMaker::textInput([
                                                        'labelText' => '中文標題',
                                                        'inputName' => 'ProductCategory[s1_tw_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['s1_tw_title']) )? $data['s1_tw_title'] : ''
                                                    ])}}
                                                    <?php 
                                                         $position=[
                                                        1=>['id'=>1 ,'value'=>1 ,'title'=>'左上'],
                                                        2=>['id'=>2 ,'value'=>2 ,'title'=>'左下'],
                                                        3=>['id'=>3 ,'value'=>3 ,'title'=>'右上'],
                                                        4=>['id'=>4 ,'value'=>4 ,'title'=>'右下'],
                                                                                                                                                     
                                                    ];

                                                    ?>

                                                     {{ItemMaker::select([
                                                        'labelText' => '標題位置',
                                                        'inputName' => 'ProductCategory[title_position]',
                                                        'options'   => $position,
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['title_position']) )? $data['title_position'] : ''
                                                    ])}}

                                                    {{ItemMaker::colorPicker([
                                                        'labelText' => '標題顏色',
                                                        'inputName' => 'ProductCategory[color]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['color']) )? $data['color'] : ''
                                                    ])}}

													

                                                </div>
                                            </div>


                                             <div class="tab-pane  form-row-seperated" id="tab_general8">
                                                <div class="form-body form_news_pad">
                                                 {{ FormMaker::photosTable(
                                                    [
                                                        'nameGroup' => $routePreFix.'-ProductCategoryAreadata',
                                                        'datas' => ( !empty($parent['has']['ProductCategoryAreadata']) )? $parent['has']['ProductCategoryAreadata'] : [],
                                                        'table_set' =>  $bulidTable['photo'],
                                                        'pic' => false
                                                    ]
                                                 ) }}

                                            </div>
                                        </div>


                                        <div class="tab-pane  form-row-seperated" id="tab_genera6">
                                                <div class="form-body form_news_pad">
                                                    <div class="form-group" style='background-color:lightblue'>
                                                        <div class="col-md-2 control-label" ><h4><b>如果seo標題有填資料<br>則以此區塊設定為主</b></h4></div>
                                                        <div class="col-md-10">
                                                        </div>
                                                    </div>

                                                    {{ItemMaker::textInput([
                                                        'labelText' => 'seo標題',
                                                        'inputName' => 'ProductCategory[web_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['web_title']) )? $data['web_title'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => 'seo描述',
                                                        'inputName' => 'ProductCategory[meta_description]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['meta_description']) )? $data['meta_description'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => 'seo關鍵字',
                                                        'inputName' => 'ProductCategory[meta_keyword]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['meta_keyword']) )? $data['meta_keyword'] : ''
                                                    ])}}

                                                </div>
                                        </div>


                                           
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif

        var co=0;
        $('#itemTbody > tr').each(function(){
            co++;
            console.log(co);
            
            table_str=" <div class='tt' border='1' style='margin-top: 10px; width: 10vw; height: auto; /*background-color: blue;*/ display: flex; flex-wrap: wrap; /*justify-content: space-between;*/ border: 2px solid;'><div border='1' style='margin-top: 5px; margin-left:5%;  width: 90%; border: 2px solid; height: 6vh; '></div>";
            var toappend="<div border='1' style='margin-left:5%; margin-top: 5px; width: 42%; border: 2px solid; height: 5vh;''></div>";
            // alert(table_str);
            // alert(toappend);
            $(this).find('td').eq(3).css('width','300px');
            
            for(var a=0;a<co;a++)
            {
                //$(this).find('td').find('tt').append(toappend);
                table_str+=toappend;
            }
            table_str+="</div>";
            
            $(this).find('td').eq(3).html(table_str);

            $(this).find('.tt div').last().css('background-color','red');


        });
            
          $('input#go_news_content').off('click').on('click',function(){

            var id=$(this).parents('tr').children('td:first').find('input').eq(0).val();
         
            url='{{ ItemMaker::url('Fantasy/Product/ProductContent/edit') }}/'+id;
        
            console.log(url);

            $(this).colorbox({
                href: url,
                iframe:true,
                width:"80%",
                height:"80%",
                overlayClose: false,
                onClosed: function(){  
                    //location.reload();
                    
                },
                onComplete: function() {
                    $('#cboxClose, #cboxOverlay').on('click', function() {
                        $('html').css('overflow-y','auto');
                    });

                }
            });

            $('html').css('overflow-y','hidden');
        });

    });


	</script>
@stop
