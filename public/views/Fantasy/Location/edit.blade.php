@extends('Fantasy.template')

@section('title',"門市據點管理修改 "."-".$ProjectName)
@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    #showPic.anchorImg{ width: auto!important; max-height: 250px;  }
    </style>

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			門市據點管理 <small>Newscatagories Manager</small> </h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('Fantasy/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="{{ ItemMaker::url('Fantasy/'.$routePreFix)}}">門市據點管理</a> <i class="fa fa-angle-right"></i>
				</li>
				<li>
					門市據點管理修改 </li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
                    {{-- <div class="note note-danger">
                        <p> 注意事項: 婚宴會議單元，才需要填入短述。<br>
                            交通訊息、集團介紹、問與答、館內活動、歡樂設施 非輪播單元，所以只需要上傳一組圖片。
                        </p>
                    </div> --}}
                    <!--注意事項END-->

					<!--<form action="{{ ItemMaker::url('Fantasy/工程分類/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 門市據點管理 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    @include('Fantasy.include.editActionBtns')

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> 問題與答案設定 </a>
                                            </li>

                                        
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">
                                                {{ItemMaker::radio_btn([
                                                        'labelText' => '啟用狀態',
                                                        'inputName' => 'Location[is_visible]',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
                                                ])}}

                                                {{ItemMaker::textInput([
                                                        'labelText' => '分店名稱',
                                                        'inputName' => 'Location[title]',  
                                                        'value' => ( !empty($data['title']) )? $data['title'] : ''
                                                ])}}


                                                {{ItemMaker::select([
                                                        'labelText' => '所在城市',
                                                        'inputName' => 'Location[category_id]',
                                                        'required'  => true,
                                                        'options'   => $parent['belong']['LocationCategory'],
                                                        'value' => ( !empty($data['category_id']) )? $data['category_id'] : ''
                                                ])}}


													{{ItemMaker::idInput([
														'inputName' => 'Location[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
													])}}

                                                     {{ItemMaker::textInput([
                                                        'labelText' => '門市地址',
                                                        'inputName' => 'Location[address]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['address']) )? $data['address'] : ''
                                                    ])}}


                                                     {{ItemMaker::textInput([
                                                        'labelText' => '電話',
                                                        'inputName' => 'Location[phone]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['phone']) )? $data['phone'] : ''
                                                    ])}}

                                                      {{ItemMaker::textInput([
                                                        'labelText' => '傳真',
                                                        'inputName' => 'Location[fax]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['fax']) )? $data['fax'] : ''
                                                    ])}}

                                                   

                                                    {{--  {{ItemMaker::textInput([
                                                        'labelText' => '內頁標題',
                                                        'inputName' => 'Location[in_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['in_title']) )? $data['in_title'] : ''
                                                    ])}} --}}

                                                   

                                                    {{ItemMaker::textArea([
                                                        'labelText' => '內頁簡述(用換行區隔每筆簡述)',
                                                        'inputName' => 'Location[statement]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['statement']) )? $data['statement'] : ''
                                                    ])}}

                                                 

                                                    {{ItemMaker::textInput([
                                                        'labelText' => 'Line區塊標題<br>(沒上標題將會將line區塊隱藏)',
                                                        'inputName' => 'Location[line_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['line_title']) )? $data['line_title'] : ''
                                                    ])}}


                                                     {{ItemMaker::textArea([
                                                        'labelText' => 'Line區塊描述<br>',
                                                        'inputName' => 'Location[line_content]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['line_content']) )? $data['line_content'] : ''
                                                    ])}}

                                                    {{ItemMaker::photo([
                                                        'labelText' => 'Line區塊圖片(QR Code)<br>(150 x 150)',
                                                        'inputName' => 'Location[line_photo]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['line_photo']) )? $data['line_photo'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => 'Line圖片連結',
                                                        'inputName' => 'Location[line_link]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['line_link']) )? $data['line_link'] : ''
                                                    ])}}



                                                    {{ItemMaker::photo([
                                                        'labelText' => '內頁圖片br(550 x 320)',
                                                        'inputName' => 'Location[in_photo]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['in_photo']) )? $data['in_photo'] : ''
                                                    ])}}

                                                     {{ItemMaker::textInput([
                                                        'labelText' => '活動標題',
                                                        'inputName' => 'Location[in_content_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['in_content_title']) )? $data['in_content_title'] : ''
                                                    ])}}

                                                     {{ItemMaker::textArea([
                                                        'labelText' => '活動內容',
                                                        'inputName' => 'Location[in_content]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['in_content']) )? $data['in_content'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => 'google地圖-X',
                                                        'inputName' => 'Location[map_x]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['map_x']) )? $data['map_x'] : ''
                                                    ])}}


                                                     {{ItemMaker::textInput([
                                                        'labelText' => 'google地圖-Y',
                                                        'inputName' => 'Location[map_y]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['map_y']) )? $data['map_y'] : ''
                                                    ])}}

                                               



                                               

                                                </div>
                                            </div>



                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif

		});

	</script>
@stop
