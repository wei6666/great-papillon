@extends('Fantasy.template')

@section('title',"首頁基本設定修改 "."-".$ProjectName)
@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    #showPic.anchorImg{ width: auto!important; max-height: 250px;  }
    </style>

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			首頁基本設定 <small>Newscatagories Manager</small> </h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('Fantasy/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="{{ ItemMaker::url('Fantasy/'.$routePreFix)}}">首頁基本設定</a> <i class="fa fa-angle-right"></i>
				</li>
				<li>
					首頁基本設定修改 </li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
                    {{-- <div class="note note-danger">
                        <p> 注意事項: 婚宴會議單元，才需要填入短述。<br>
                            交通訊息、集團介紹、問與答、館內活動、歡樂設施 非輪播單元，所以只需要上傳一組圖片。
                        </p>
                    </div> --}}
                    <!--注意事項END-->

					<!--<form action="{{ ItemMaker::url('Fantasy/工程分類/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 首頁基本設定 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    @include('Fantasy.include.editActionBtns')

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> 首頁基本設定 </a>
                                            </li>

                                           
                                        
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

													{{ItemMaker::idInput([
														'inputName' => 'Home[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
													])}}

												 <div class="form-group" style='background-color:lightblue'>
                                                        <div class="col-md-2 control-label" ><h4><b>最上方影片區塊</b></h4></div>
                                                        <div class="col-md-10">
                                                        </div>
                                                    </div>
                                                  

                                                   

                                                    {{ItemMaker::textInput([
                                                        'labelText' => 'youtube代碼',
                                                        'inputName' => 'Home[home_video]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['home_video']) )? $data['home_video'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '標題',
                                                        'inputName' => 'Home[video_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['video_title']) )? $data['video_title'] : ''
                                                    ])}}


                                                    {{ItemMaker::textInput([
                                                        'labelText' => '英文標題',
                                                        'inputName' => 'Home[video_en_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['video_en_title']) )? $data['video_en_title'] : ''
                                                    ])}}

                                                     {{ItemMaker::photo([
                                                        'labelText' => '手機板圖片<br>(手機板時所要上的圖片375 x 812)',
                                                        'inputName' => 'Home[video_phone_img]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['video_phone_img']) )? $data['video_phone_img'] : ''
                                                    ])}}

                                                    <div class="form-group" style='background-color:lightblue'>
                                                        <div class="col-md-2 control-label" ><h4><b>影片下方banner區塊</b></h4></div>
                                                        <div class="col-md-10">
                                                        </div>
                                                    </div>

                                                    {{ItemMaker::photo([
                                                        'labelText' => 'banner圖片',
                                                        'inputName' => 'Home[banner_img]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['banner_img']) )? $data['banner_img'] : ''
                                                    ])}}

                                                     {{ItemMaker::textInput([
                                                        'labelText' => 'banner標題',
                                                        'inputName' => 'Home[banner_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['banner_title']) )? $data['banner_title'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => 'banner英文標題',
                                                        'inputName' => 'Home[banner_en_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['banner_en_title']) )? $data['banner_en_title'] : ''
                                                    ])}}

                                                    <div class="form-group" style='background-color:lightblue'>
                                                        <div class="col-md-2 control-label" ><h4><b>幸福系列</b></h4></div>
                                                        <div class="col-md-10">
                                                        </div>
                                                    </div>


                                                     {{ItemMaker::textInput([
                                                        'labelText' => '幸福系列標題',
                                                        'inputName' => 'Home[happiness_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['happiness_title']) )? $data['happiness_title'] : ''
                                                    ])}}

                                                     {{ItemMaker::textInput([
                                                        'labelText' => '幸福系列英文標題',
                                                        'inputName' => 'Home[happiness_en_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['happiness_en_title']) )? $data['happiness_en_title'] : ''
                                                    ])}}

                                                    <div class="form-group" style='background-color:lightblue'>
                                                        <div class="col-md-2 control-label" ><h4><b>幸福精選</b></h4></div>
                                                        <div class="col-md-10">
                                                        </div>
                                                    </div>


                                                     {{ItemMaker::textInput([
                                                        'labelText' => '幸福精選標題',
                                                        'inputName' => 'Home[product_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['product_title']) )? $data['product_title'] : ''
                                                    ])}}

                                                     {{ItemMaker::textInput([
                                                        'labelText' => '幸福精選英文標題',
                                                        'inputName' => 'Home[product_en_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['product_en_title']) )? $data['product_en_title'] : ''
                                                    ])}}


                                                     <div class="form-group" style='background-color:lightblue'>
                                                        <div class="col-md-2 control-label" ><h4><b>幸福精選下方行銷區塊</b></h4></div>
                                                        <div class="col-md-10">
                                                        </div>
                                                    </div>

                                                     {{ItemMaker::photo([
                                                        'labelText' => '區塊圖片',
                                                        'inputName' => 'Home[ad_img]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['ad_img']) )? $data['ad_img'] : ''
                                                    ])}}


                                                     {{ItemMaker::textInput([
                                                        'labelText' => '區塊標題',
                                                        'inputName' => 'Home[ad_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['ad_title']) )? $data['ad_title'] : ''
                                                    ])}}

                                                     {{ItemMaker::textInput([
                                                        'labelText' => '區塊英文標題',
                                                        'inputName' => 'Home[ad_en_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['ad_en_title']) )? $data['ad_en_title'] : ''
                                                    ])}}

                                                    {{ItemMaker::textArea([
                                                        'labelText' => '區塊內容',
                                                        'inputName' => 'Home[ad_content]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['ad_content']) )? $data['ad_content'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '區塊按鈕標題',
                                                        'inputName' => 'Home[ad_btn_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['ad_btn_title']) )? $data['ad_btn_title'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '區塊按鈕連結',
                                                        'inputName' => 'Home[ad_btn_link]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['ad_btn_link']) )? $data['ad_btn_link'] : ''
                                                    ])}}

                                                <div class="form-group" style='background-color:lightblue'>
                                                    <div class="col-md-2 control-label" ><h4><b>新聞區塊</b></h4></div>
                                                    <div class="col-md-10">
                                                    </div>
                                                </div>

                                                 {{ItemMaker::textInput([
                                                        'labelText' => '區塊標題',
                                                        'inputName' => 'Home[news_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['news_title']) )? $data['news_title'] : ''
                                                    ])}}

                                                     {{ItemMaker::textInput([
                                                        'labelText' => '區塊英文標題',
                                                        'inputName' => 'Home[news_en_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['news_en_title']) )? $data['news_en_title'] : ''
                                                    ])}}

                                                <div class="form-group" style='background-color:lightblue'>
                                                    <div class="col-md-2 control-label" ><h4><b>幸福見證詳細區塊</b></h4></div>
                                                    <div class="col-md-10">
                                                    </div>
                                                </div>

                                                 {{ItemMaker::textInput([
                                                        'labelText' => '區塊標題',
                                                        'inputName' => 'Home[happiness_detail_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['happiness_detail_title']) )? $data['happiness_detail_title'] : ''
                                                    ])}}

                                                {{ItemMaker::textInput([
                                                        'labelText' => '區塊英文標題',
                                                        'inputName' => 'Home[happiness_detail_en_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['happiness_detail_en_title']) )? $data['happiness_detail_en_title'] : ''
                                                    ])}}

                                                 {{ItemMaker::textArea([
                                                        'labelText' => '區塊內容說明',
                                                        'inputName' => 'Home[happiness_detail_content]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['happiness_detail_content']) )? $data['happiness_detail_content'] : ''
                                                    ])}}

                                                {{ItemMaker::textInput([
                                                        'labelText' => '區塊按鈕標題',
                                                        'inputName' => 'Home[happiness_detail_btn_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['happiness_detail_btn_title']) )? $data['happiness_detail_btn_title'] : ''
                                                    ])}}

                                                {{ItemMaker::textInput([
                                                        'labelText' => '區塊標籤標題',
                                                        'inputName' => 'Home[happiness_detail_tag]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['happiness_detail_tag']) )? $data['happiness_detail_tag'] : ''
                                                    ])}}

                                                {{ItemMaker::textInput([
                                                        'labelText' => '區塊標籤連結',
                                                        'inputName' => 'Home[happiness_detail_link]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['happiness_detail_link']) )? $data['happiness_detail_link'] : ''
                                                    ])}}


                                             <div class="form-group" style='background-color:lightblue'>
                                                    <div class="col-md-2 control-label" ><h4><b>關於我們區塊</b></h4></div>
                                                    <div class="col-md-10">
                                                    </div>
                                            </div>

                                                {{ItemMaker::photo([
                                                        'labelText' => '區塊圖片',
                                                        'inputName' => 'Home[about_img]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['about_img']) )? $data['about_img'] : ''
                                                    ])}}

                                                 {{ItemMaker::textInput([
                                                        'labelText' => '區塊標題',
                                                        'inputName' => 'Home[about_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['about_title']) )? $data['about_title'] : ''
                                                    ])}}

                                                 {{ItemMaker::textInput([
                                                        'labelText' => '區塊英文標題',
                                                        'inputName' => 'Home[about_en_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['about_en_title']) )? $data['about_en_title'] : ''
                                                    ])}}

                                                    {{ItemMaker::textArea([
                                                        'labelText' => '區塊內容標題',
                                                        'inputName' => 'Home[about_content]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['about_content']) )? $data['about_content'] : ''
                                                    ])}}


                                                 

                                                </div>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif

		});

	</script>
@stop
