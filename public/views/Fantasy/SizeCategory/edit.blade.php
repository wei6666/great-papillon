@extends('Fantasy.template')

@section('title',"產品尺寸基本設定修改 "."-".$ProjectName)
@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    #showPic.anchorImg{ width: auto!important; max-height: 250px;  }
    .sizeForm {
        margin-top: 80px; }
.sizeForm p {
          width: 70px;
          font-size: 0.875rem;
          letter-spacing: 1px;
          text-align: center;
          font-family: "微軟正黑體"; }
.sizeForm .tableName {
          display: -webkit-box;
          display: -webkit-flex;
          display: -ms-flexbox;
          display: flex;
          -webkit-justify-content: space-around;
          -ms-flex-pack: distribute;
          justify-content: space-around;
          -webkit-box-align: center;
          -webkit-align-items: center;
          -ms-flex-align: center;
          align-items: center;
          -webkit-flex-wrap: wrap;
          -ms-flex-wrap: wrap;
          flex-wrap: wrap; }


         .sizeForm .tableName p {
            font-weight: bold; }
        .sizeForm .tableName:after {
            content: "";
            display: block;
            width: 100%;
            height: 4px;
            background-color: #000;
            margin-top: 15px; }
      .sizeForm ul li {
          display: -webkit-box;
          display: -webkit-flex;
          display: -ms-flexbox;
          display: flex;
          -webkit-justify-content: space-around;
          -ms-flex-pack: distribute;
          justify-content: space-around;
          -webkit-box-align: center;
          -webkit-align-items: center;
          -ms-flex-align: center;
          align-items: center;
          -webkit-flex-wrap: wrap;
          -ms-flex-wrap: wrap;
          flex-wrap: wrap;
          padding: 20px 0;
          border-bottom: 1px solid #7f7f7f; }
    </style>

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			產品尺寸基本設定 <small>Newscatagories Manager</small> </h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('Fantasy/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="{{ ItemMaker::url('Fantasy/'.$routePreFix)}}">產品尺寸基本設定</a> <i class="fa fa-angle-right"></i>
				</li>
				<li>
					產品尺寸基本設定修改 </li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
                    {{-- <div class="note note-danger">
                        <p> 注意事項: 婚宴會議單元，才需要填入短述。<br>
                            交通訊息、集團介紹、問與答、館內活動、歡樂設施 非輪播單元，所以只需要上傳一組圖片。
                        </p>
                    </div> --}}
                    <!--注意事項END-->

					<!--<form action="{{ ItemMaker::url('Fantasy/工程分類/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 產品尺寸基本設定 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    @include('Fantasy.include.editActionBtns')

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> 產品尺寸基本設定 </a>
                                            </li>
                                            <li >
                                                <a href="#tab_general1" data-toggle="tab"> 產品尺寸設定 </a>
                                            </li>

                                        
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

													{{ItemMaker::idInput([
														'inputName' => 'SizeCategory[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
													])}}

												 <div class="form-group" style='background-color:lightblue'>
                                                        <div class="col-md-2 control-label" ><h4><b>尺寸設定</b></h4></div>
                                                        <div class="col-md-10">
                                                        </div>
                                                    </div>
                                                  

                                                   

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '標題<br>(方便判斷不會出現在前台)',
                                                        'inputName' => 'SizeCategory[title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['title']) )? $data['title'] : ''
                                                    ])}}
                                                    <div class="form-group" style='background-color:lightblue'>
                                                        <div class="col-md-2 control-label" ><h4><b>選擇尺寸設定</b></h4></div>
                                                        <div class="col-md-10">
                                                        </div>
                                                    </div>

                                                    {{ItemMaker::textArea([
                                                        'labelText' => '選擇尺寸上方內容',
                                                        'inputName' => 'SizeCategory[content]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['content']) )? $data['content'] : ''
                                                    ])}}

                                                    <div class="form-group" style='background-color:lightblue'>
                                                        <div class="col-md-2 control-label" ><h4><b>尺寸說明設定</b></h4></div>
                                                        <div class="col-md-10">
                                                        </div>
                                                    </div>

                                                     {{ItemMaker::textArea([
                                                        'labelText' => '尺寸說明內容',
                                                        'inputName' => 'SizeCategory[content_inf]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['content_inf']) )? $data['content_inf'] : ''
                                                    ])}}

                                                     {{ItemMaker::photo([
                                                        'labelText' => '尺寸說明圖片<br>(550 x 250)',
                                                        'inputName' => 'SizeCategory[image_inf]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['image_inf']) )? $data['image_inf'] : ''
                                                    ])}}


                                                     {{ItemMaker::textArea([
                                                        'labelText' => '尺寸說明表格<br>(首行為標題用@分隔每個欄位,<br>換行分隔每筆資料)<br>ex:<br>標題1@標題2<br>內容1@內容2',
                                                        'inputName' => 'SizeCategory[table_inf]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['table_inf']) )? $data['table_inf'] : ''
                                                    ])}}
                                                    <input  style="margin-left:30vw" type="button" id='view'  value='預覽表格'>
                                                    

                                                </div>
                                            </div>

                                              <div class="tab-pane  form-row-seperated" id="tab_general1">
                                                <div class="form-body form_news_pad">
                                                 {{ FormMaker::photosTable(
                                                    [
                                                        'nameGroup' => $routePreFix.'-AllSize',
                                                        'datas' => ( !empty($parent['has']['AllSize']) )? $parent['has']['AllSize'] : [],
                                                        'table_set' =>  $bulidTable['photo'],
                                                        'pic' => false
                                                    ]
                                                 ) }}

                                            </div>
                                        </div>

<section class="sizeDescription" style="width:20%; margin:0 auto;">
<div class="sizeForm">
                        {{-- <div class="tableName">
                            <p class="diameter">內圍直徑</p>
                            <p class="perimeter">圖尺繞圈</p>
                            <p class="sizeNumb">國際戒圍</p>
                        </div>
                        <ul>
                            <li>
                                <p class="diameter">13.8 mm</p>
                                <p class="perimeter">43.3 mm</p>
                                <p class="sizeNumb">5</p>
                            </li>
                            <li>
                                <p class="diameter">14.1 mm</p>
                                <p class="perimeter">44.3 mm</p>
                                <p class="sizeNumb">6</p>
                            </li>
                            <li>
                                <p class="diameter">14.5 mm</p>
                                <p class="perimeter">45.5 mm</p>
                                <p class="sizeNumb">7</p>
                            </li>
                            <li>
                                <p class="diameter">15.0 mm</p>
                                <p class="perimeter">47.1 mm</p>
                                <p class="sizeNumb">8</p>
                            </li>
                            <li>
                                <p class="diameter">15.5 mm</p>
                                <p class="perimeter">48.7 mm</p>
                                <p class="sizeNumb">9</p>
                            </li>
                        </ul> --}}
                    </div>
                </section>
                                 




                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif

            $("#view").click(function(){
                var append='';
                var text=$("#tab_general > div > div:nth-child(9) > div > textarea").val();
                
                text=text.split("\n");
                //console.log(text);
                title=text[0].split("@");
                append+='<div class="tableName">';
                //console.log(title);
                
                title.forEach(function(ele){
                    
                        append+='<p class="diameter">'+ele+'</p>';
                        
                   
                });
                append+='</div>';
                //console.log(text.length);
                if(text.length>1)
                {
                    append+='<ul>';
                    var a=0;
                    text.forEach(function(ele){
                        var inc=ele.split("@");
                        if(a !=0 && ele.length!=0) 
                        {
                        
                        
                          append+='<li>';
                            inc.forEach(function(ele1){
                            append+='<p class="diameter">'+ele1+'</p>';
                            });
                            append+='</li>';
                        }
                        a++;
                    });

                    append+='</ul>';
                }
                
                $(".sizeForm").html(append);
            });

		});

	</script>
@stop
