@extends('Fantasy.template')

@section('title',"產品基本設定修改 "."-".$ProjectName)
@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    #showPic.anchorImg{ width: auto!important; max-height: 250px;  }
    .copyBtn{display: none;}
    </style>

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			產品基本設定 <small>Newscatagories Manager</small> </h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('Fantasy/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="{{ ItemMaker::url('Fantasy/'.$routePreFix)}}">產品基本設定</a> <i class="fa fa-angle-right"></i>
				</li>
				<li>
					產品基本設定修改 </li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
                    {{-- <div class="note note-danger">
                        <p> 注意事項: 婚宴會議單元，才需要填入短述。<br>
                            交通訊息、集團介紹、問與答、館內活動、歡樂設施 非輪播單元，所以只需要上傳一組圖片。
                        </p>
                    </div> --}}
                    <!--注意事項END-->

					<!--<form action="{{ ItemMaker::url('Fantasy/工程分類/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 產品基本設定 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    @include('Fantasy.include.editActionBtns')

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> 問題與答案設定 </a>
                                            </li>

                                        
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

                                                      {{ItemMaker::radio_btn([
                                                        'labelText' => '啟用狀態',
                                                        'inputName' => 'Happiness[is_visible]',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
                                                        ])}}

                                                      {{ItemMaker::radio_btn([
                                                        'labelText' => '顯示首頁',
                                                        'inputName' => 'News[is_home]',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_home']) )? $data['is_home'] : ''
                                                       ])}}

                                                    {{ItemMaker::idInput([
                                                            'inputName' => 'Happiness[id]',
                                                            'value' => ( !empty($data['id']) )? $data['id'] : ''
                                                    ])}}

                                               {{--      <div class="form-group" style='background-color:lightblue'>
                                                        <div class="col-md-2 control-label" ><h4><b>外頁設定</b></h4></div>
                                                        <div class="col-md-10">
                                                        </div>
                                                    </div>

													


                                                    {{ItemMaker::textInput([
                                                        'labelText' => '外頁標題',
                                                        'inputName' => 'Happiness[out_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['out_title']) )? $data['out_title'] : ''
                                                    ])}} --}}

                                                     <div class="form-group" style='background-color:lightblue'>
                                                        <div class="col-md-2 control-label" ><h4><b>內頁設定</b></h4></div>
                                                        <div class="col-md-10">
                                                        </div>
                                                    </div>

                                                    {{ItemMaker::select([
                                                        'labelText' => '選擇分類',
                                                        'inputName' => 'Happiness[category_id]',
                                                        'options'   => $parent['belong']['HappinessCategory'],
                                                        'value' => ( !empty($data['category_id']) )? $data['category_id'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '標題',
                                                        'inputName' => 'Happiness[title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['title']) )? $data['title'] : ''
                                                    ])}}

                                                    {{-- {{ItemMaker::textInput([
                                                        'labelText' => '英文標題',
                                                        'inputName' => 'Happiness[en_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['en_title']) )? $data['en_title'] : ''
                                                    ])}} --}}

                                                    {{ItemMaker::editor([
                                                        'labelText' => '內容',
                                                        'inputName' => 'Happiness[content]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['content']) )? $data['content'] : ''
                                                    ])}}

                                                    {{ItemMaker::photo([
                                                        'labelText' => '封面圖片<br>(280 x 280 )',
                                                        'inputName' => 'Happiness[out_image]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['out_image']) )? $data['out_image'] : ''
                                                    ])}}

                                                    {{ItemMaker::photo([
                                                        'labelText' => '內頁圖片<br>(960 x 1080 or 960 x 655)',
                                                        'inputName' => 'Happiness[image]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['image']) )? $data['image'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '服務人員',
                                                        'inputName' => 'Happiness[angel]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['angel']) )? $data['angel'] : ''
                                                    ])}}

                                                {{ItemMaker::select([
                                                        'labelText' => '購買產品',
                                                        'inputName' => 'Happiness[product_id]',
                                                        'options'   => $parent['belong']['Product'],
                                                        'value' => ( !empty($data['product_id']) )? $data['product_id'] : ''
                                                ])}}

                                                {{ItemMaker::select([
                                                        'labelText' => '產品類別',
                                                        'inputName' => 'Happiness[product_category_id]',
                                                        'options'   => $parent['belong']['ProductCategory'],
                                                        'value' => ( !empty($data['product_category_id']) )? $data['product_category_id'] : ''
                                                ])}}


                                               

                                                {{ItemMaker::select([
                                                        'labelText' => '購買據點',
                                                        'inputName' => 'Happiness[location_id]',
                                                        'options'   => $parent['belong']['Location'],
                                                        'value' => ( !empty($data['location_id']) )? $data['location_id'] : ''
                                                ])}}


                                                </div>
                                            </div>



                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif

		});

	</script>
@stop
