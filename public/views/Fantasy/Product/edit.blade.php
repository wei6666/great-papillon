@extends('Fantasy.template')

@section('title',"類別項目修改 "."-".$ProjectName)
@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    #showPic.anchorImg{ width: auto!important; max-height: 250px;  }
    </style>

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			類別項目 <small>Newscatagories Manager</small> </h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('Fantasy/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="{{ ItemMaker::url('Fantasy/'.$routePreFix)}}">類別項目</a> <i class="fa fa-angle-right"></i>
				</li>
				<li>
					類別項目修改 </li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
                    {{-- <div class="note note-danger">
                        <p> 注意事項: 婚宴會議單元，才需要填入短述。<br>
                            交通訊息、集團介紹、問與答、館內活動、歡樂設施 非輪播單元，所以只需要上傳一組圖片。
                        </p>
                    </div> --}}
                    <!--注意事項END-->

					<!--<form action="{{ ItemMaker::url('Fantasy/工程分類/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 類別項目 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    @include('Fantasy.include.editActionBtns')

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> Information </a>
                                            </li>
                                             <li >
                                                <a href="#tab_genera2" data-toggle="tab"> 商品照片輪播 </a>
                                            </li>

                                             <li >
                                                <a href="#tab_genera3" data-toggle="tab"> 商品內容管理 </a>
                                            </li>
                                            
                                            @if(empty($parent['has']['ProductSeparate']) || 1)
                                            <li >
                                                <a href="#tab_genera5" data-toggle="tab"> 商品尺寸表管理 </a>
                                            </li>
                                            @endif
                                            <li >
                                                <a href="#tab_genera4" data-toggle="tab"> 子商品管理 </a>
                                            </li>
                                            <li >
                                                <a href="#tab_genera6" data-toggle="tab"> 商品SEO管理 </a>
                                            </li>



                                           
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

													{{ItemMaker::idInput([
														'inputName' => 'Product[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
													])}}

												
                                                  

                                                {{ItemMaker::radio_btn([
                                                        'labelText' => '啟用狀態',
                                                        'inputName' => 'Product[is_visible]',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
                                                ])}}


                                                {{ItemMaker::radio_btn([
                                                        'labelText' => '顯示首頁<br>(最多勾4個若超過<br>從最新上的商品優先顯示)',
                                                        'inputName' => 'Product[is_home]',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_home']) )? $data['is_home'] : ''
                                                ])}}


                                                {{ItemMaker::radio_btn([
                                                        'labelText' => '可否購買<br>若沒勾選擇不能購買',
                                                        'inputName' => 'Product[is_sell_out]',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_sell_out']) )? $data['is_sell_out'] : ''
                                                    ])}}

                                                {{ItemMaker::textInput([
                                                        'labelText' => '排序',
                                                        'inputName' => 'Product[rank]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['rank']) )? $data['rank'] : ''
                                                    ])}}
                                                   
                                                    <?php  
                                                        use App\Http\Models\Product\Product;
                                                    ?>
                                              
                                                

                                         <?php  
                                         use App\Http\Models\Product\ProductCategory;

                                         use App\Http\Models\Product\ProductSubCategory;

                                         $ProductCategory=ProductCategory::get();

                                         $ProductCategoryArr=[];
                                         foreach($ProductCategory as $value)
                                         {
                                            $ProductCategoryArr[$value->id]=$value->title;
                                         }

                                         
                                         foreach ($parent['belong']['ProductSubCategory'] as $key => $value) 
                                         {
                                            $parent['belong']['ProductSubCategory'][$key]['title']=$ProductCategoryArr[$value["category_id"]]."--".$value['title'];
                                             
                                         }

                                         
                                         


                                         ?>

                                                {{ItemMaker::select([
                                                        'labelText' => '所屬類別',
                                                        'inputName' => 'Product[category_id]',
                                                        'required'  => true,
                                                        'options'   => $parent['belong']['ProductSubCategory'],
                                                        'value' => ( !empty($data['category_id']) )? $data['category_id'] : ''
                                                ])}}

                                                 <?php 

                                                use App\Http\Models\Product\Marketing;
                                              
                                                $allcate=[];

                                               $ProductCategory=ProductCategory::where('is_visible',1)->get();
                                               $Productv=[];
                                               foreach ($ProductCategory as $key => $value) {
                                                   $Productv[$value->id]=$value->title;
                                               }

                                                    $datacate=Marketing::where("is_visible",1)   
                                                    ->OrderBy('rank','asc')
                                                    ->get()
                                                    ->toArray();


                                                    foreach($datacate as $value1)
                                                    {
                                                       
                                                       if(!empty( $value1['category_id']))
                                                       {
                                                            $allcate[$Productv[$value1['category_id']]."--".$value1['title']]=$value1['id'];
                                                        }
                                                    }
                                                

                                                
                                                $productdata=( !empty( $data['market_id'] ) )?json_decode($data['market_id'], true) : [];
                                                
                                              
                                               
                                                ?>

                                                {{ItemMaker::selectMulti([
                                                        'labelText' => '所屬行銷類別',
                                                        'inputName' => 'Product[market_id][]',
                                                        'required'  => true,
                                                         "options" => $allcate,
                                                        "type" => "Role",
                                                        'value' => ( !empty($productdata) )? $productdata : ''
                                                    
                                                ])}}


                                                <?php 

                                                use App\Http\Models\Product\Searching;
                                              
                                                $allcate=[];

                                               $ProductCategory=ProductCategory::where('is_visible',1)->get();
                                               $Productv=[];
                                               foreach ($ProductCategory as $key => $value) {
                                                   $Productv[$value->id]=$value->title;
                                               }

                                                    $datacate=Searching::where("is_visible",1)   
                                                    ->OrderBy('rank','asc')
                                                    ->get()
                                                    ->toArray();


                                                    foreach($datacate as $value1)
                                                    {
                                                        if(!empty( $value1['category_id']))
                                                       {
                                                            $allcate[$Productv[$value1['category_id']]."--".$value1['title']]=$value1['id'];
                                                        }
                                                    }

                                                


                                                $productdata=( !empty( $data['search_id'] ) )?json_decode($data['search_id'], true) : [];
                                               
                                                ?>


                                                  {{ItemMaker::selectMulti([
                                                        'labelText' => '所屬搜尋類別',
                                                        'inputName' => 'Product[search_id][]',
                                                        'required'  => true,
                                                         "options" => $allcate,
                                                        "type" => "Role",
                                                        'value' => ( !empty($productdata) )? $productdata : ''
                                                    
                                                ])}}





                                                <?php 

                                                use App\Http\Models\Product\Matirial;
                                              
                                                $allcate=[];

                                               
                                                    $datacate=Matirial::where("is_visible",1)   
                                                    ->OrderBy('rank','asc')
                                                    ->get()
                                                    ->toArray();


                                                    foreach($datacate as $value1)
                                                    {
                                                       
                                                    $allcate[$value1['title']]=$value1['id'];
                                                    }
                                                

                                                
                                                $productdata=( !empty( $data['search_matirial'] ) )?json_decode($data['search_matirial'], true) : [];
                                                
                                              
                                               
                                                ?>

                                                {{ItemMaker::selectMulti([
                                                        'labelText' => '商品材質(搜尋用)',
                                                        'inputName' => 'Product[search_matirial][]',
                                                        'helpText' =>'使用者帳號與聯絡用信箱',
                                                        "options" => $allcate,
                                                        "type" => "Role",
                                                        'value' => ( !empty($productdata) )? $productdata : ''
                                                    ])}}


                                                    <?php 

                                                use App\Http\Models\Product\Jewelry;
                                              
                                                $allcate=[];

                                               
                                                    $datacate=Jewelry::where("is_visible",1)   
                                                    ->OrderBy('rank','asc')
                                                    ->get()
                                                    ->toArray();


                                                    foreach($datacate as $value1)
                                                    {
                                                       
                                                    $allcate[$value1['title']]=$value1['id'];
                                                    }
                                                

                                                
                                                $productdata=( !empty( $data['search_jewelry'] ) )?json_decode($data['search_jewelry'], true) : [];
                                                
                                              
                                               
                                                ?>

                                                {{ItemMaker::selectMulti([
                                                        'labelText' => '使用珠寶(搜尋用)',
                                                        'inputName' => 'Product[search_jewelry][]',
                                                        'helpText' =>'使用者帳號與聯絡用信箱',
                                                        "options" => $allcate,
                                                        "type" => "Role",
                                                        'value' => ( !empty($productdata) )? $productdata : ''
                                                    ])}}



                                                {{ItemMaker::textInput([
                                                        'labelText' => '商品編號(英文)',
                                                        'inputName' => 'Product[en_num]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['en_num']) )? $data['en_num'] : ''
                                                    ])}}

                                                 {{ItemMaker::textInput([
                                                        'labelText' => '商品編號(數字)',
                                                        'inputName' => 'Product[num]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['num']) )? $data['num'] : ''
                                                    ])}}


                                                {{ItemMaker::textInput([
                                                        'labelText' => '中文標題',
                                                        'inputName' => 'Product[title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['title']) )? $data['title'] : ''
                                                    ])}}

                                                {{ItemMaker::textInput([
                                                        'labelText' => '英文標題',
                                                        'inputName' => 'Product[en_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['en_title']) )? $data['en_title'] : ''
                                                    ])}}

                                                 {{ItemMaker::textInput([
                                                        'labelText' => '商品材質',
                                                        'inputName' => 'Product[matirial]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['matirial']) )? $data['matirial'] : ''
                                                    ])}}

                                                 {{ItemMaker::textInput([
                                                        'labelText' => '價錢(打折前)',
                                                        'inputName' => 'Product[price_original]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['price_original']) )? $data['price_original'] : ''
                                                    ])}}

                                                 {{ItemMaker::textInput([
                                                        'labelText' => '價錢(打折後 <br> 如果有填寫此欄位 以此欄位為主)',
                                                        'inputName' => 'Product[price_discount]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['price_discount']) )? $data['price_discount'] : ''
                                                    ])}}

                                                 {{ItemMaker::textInput([
                                                        'labelText' => '商品標籤<br>快速搜尋使用不會顯示於前台',
                                                       'inputName' => 'Product[tag]',
                                                        'value' => ( !empty($data['tag']) )? $data['tag'] : ''
                                                    ])}}

                                                    <?php 
                                                    if(!empty($data['num']))
                                                    {   
                                                        $num=$data['num'];
                                                    }
                                                    else
                                                    {
                                                        $num="";
                                                    }

                                                    if(!empty($data['en_num']))
                                                    {   
                                                        $en_num=$data['en_num'];
                                                    }
                                                    else
                                                    {
                                                        $en_num="";
                                                    }
                                                    
                                                    ?>
                                                    <input type='hidden' id='hd' name="Product[all_num]" value="{{ $en_num.$num }}">


                                                </div>
                                            </div>



                                            <div class="tab-pane  form-row-seperated" id="tab_genera2">
                                                <div class="form-body form_news_pad">

                                              {{ FormMaker::photosTable(
                                                    [
                                                        'nameGroup' => $routePreFix.'-ProductPhoto',
                                                        'datas' => ( !empty($parent['has']['ProductPhoto']) )? $parent['has']['ProductPhoto'] : [],
                                                        'table_set' =>  $bulidTable['photo'],
                                                        'pic' => false
                                                    ]
                                                 ) }}
                                 

                                                   </div>
                                            </div>


                                            <div class="tab-pane  form-row-seperated" id="tab_genera3">
                                                <div class="form-body form_news_pad">

                                              {{ FormMaker::photosTable_title_no_input(
                                                    [
                                                        'nameGroup' => $routePreFix.'-ProductContent',
                                                        'datas' => ( !empty($parent['has']['ProductContent']) )? $parent['has']['ProductContent'] : [],
                                                        'table_set' =>  $bulidTable['ProductContent'],
                                                        'pic' => false
                                                    ]
                                                 ) }}


                                                  <?php  
                                                    

                                                    $p=Product::get();
                                                    ?>

                                                   
                                                    

                                                <input type="hidden" id='allpro' data-num='@foreach($p as $value){{$value->all_num}},@endforeach'> 

                                                   

                                 

                                                   </div>
                                            </div>

                                             <div class="tab-pane  form-row-seperated" id="tab_genera4">
                                                <div class="form-body form_news_pad">

                                              {{ FormMaker::photosTable(
                                                    [
                                                        'nameGroup' => $routePreFix.'-ProductSeparate',
                                                        'datas' => ( !empty($parent['has']['ProductSeparate']) )? $parent['has']['ProductSeparate'] : [],
                                                        'table_set' =>  $bulidTable['ProductSeparate'],
                                                        'pic' => false
                                                    ]
                                                 ) }}
                                 

                                                   </div>
                                            </div>

                                            <div class="tab-pane  form-row-seperated" id="tab_genera5">
                                                <div class="form-body form_news_pad">
                                                       <?php 
                                                use App\Http\Models\Size\AllSize;
                                                $titledata;
                                                if(!empty($data['category_id']))
                                                {
                                                    $subcate=ProductSubCategory::where("id",$data['category_id'])->first();
                                                    $titledata=Allsize::where("is_visible",1)
                                                    ->where('category_id',$subcate->size_category)
                                                    ->OrderBy('rank','asc')
                                                    ->get();
                                                }
                                                ?>
                                                @if(!empty($titledata ) > 0)
                                                    @foreach($titledata as $key=> $value) 

                                                     {{ItemMaker::radio_btn([
                                                            'labelText' => "是否顯示   ".$value->title ,
                                                            'inputName' => 'Product[a'.($key+1).']',
                                                            'value' => ( !empty($data["a".($key+1)]) )? $data["a".($key+1)] : ''
                                                        ])}}
                                                       
                                                    @endforeach
                                                @endif

                                                </div>
                                            </div>
                                            <div class="tab-pane  form-row-seperated" id="tab_genera6">
                                                <div class="form-body form_news_pad">
                                                    <div class="form-group" style='background-color:lightblue'>
                                                        <div class="col-md-2 control-label" ><h4><b>如果商品seo標題有填資料<br>則以此區塊設定為主</b></h4></div>
                                                        <div class="col-md-10">
                                                        </div>
                                                    </div>

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '商品seo標題',
                                                        'inputName' => 'Product[web_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['web_title']) )? $data['web_title'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '商品seo描述',
                                                        'inputName' => 'Product[meta_description]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['meta_description']) )? $data['meta_description'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '商品seo關鍵字',
                                                        'inputName' => 'Product[meta_keyword]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['meta_keyword']) )? $data['meta_keyword'] : ''
                                                    ])}}

                                                </div>
                                            </div>




                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif

		});


        $('input#go_product_content').off('click').on('click',function(){

            var id=$(this).parents('tr').children('td:first').find('input').eq(0).val();
         
            url='{{ ItemMaker::url('Fantasy/Product/ProductDetails/edit') }}/'+id;
        
            //console.log(url);

            $(this).colorbox({
                href: url,
                iframe:true,
                width:"80%",
                height:"80%",
                overlayClose: false,
                onClosed: function(){  
                    //location.reload();
                    
                },
                onComplete: function() {
                    $('#cboxClose, #cboxOverlay').on('click', function() {
                        $('html').css('overflow-y','auto');
                    });

                }
            });

            $('html').css('overflow-y','hidden');
        });


                $('input#go_news_content').off('click').on('click',function(){

            var id=$(this).parents('tr').children('td:first').find('input').eq(0).val();
         
            url='{{ ItemMaker::url('Fantasy/Product/SizeDetails/edit') }}/'+id;
        
            console.log(url);

            $(this).colorbox({
                href: url,
                iframe:true,
                width:"80%",
                height:"80%",
                overlayClose: false,
                onClosed: function(){  
                    //location.reload();
                    
                },
                onComplete: function() {
                    $('#cboxClose, #cboxOverlay').on('click', function() {
                        $('html').css('overflow-y','auto');
                    });

                }
            });

            $('html').css('overflow-y','hidden');
        });




        inputen=$("#tab_general > div > div:nth-child(5) > div > input");
        inputunm=$("#tab_general > div > div:nth-child(6) > div > input");
          



            var func=function(){
                //console.log('in');
                $("#hd").val($(inputen).val()+$(inputunm).val());
              allproarr=$('#allpro').attr('data-num').split(",");
              //alert(jQuery.inArray( $("#hd").val(), allproarr ));
              //console.log(jQuery.inArray( $("#hd").val(), allproarr ) );
              if( jQuery.inArray( $("#hd").val(), allproarr ) !=-1)
              {
               // console.log('in');
                $(".portlet-title").find('button').attr('disabled',true);
                alert('商品的編號重複了 請修改商品編號英文及數字');
              }
              else
              {
               //console.log('no');
                $(".portlet-title").find('button').attr('disabled',false);

              }
                
            }

            $(inputen).change(func);
            $(inputunm).change(func);
          



 

	</script>
@stop
