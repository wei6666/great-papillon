@extends('Fantasy.template')

@section('title',"產品詳細類別修改 "."-".$ProjectName)
@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    #showPic.anchorImg{ width: auto!important; max-height: 250px;  }
    #itemTbody div.images_icon2 { display: none; }
    </style>
      <style type="text/css">
  .page-sidebar-wrapper, .page-header, .page-title, .page-bar { display: none; }
     .page-content-wrapper .page-content { margin: 0px; padding: 0px; }
     .page-content { min-height: 700px!important; }
     .page-header-fixed .page-container { margin: 0px!important; }
     .fantasy-selectImageBtn { display: none; }
     div.actions > a { display: none; }
     div.page-bar { display: none; }
    </style>

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
                    {{-- <div class="note note-danger">
                        <p> 注意事項: 婚宴會議單元，才需要填入短述。<br>
                            交通訊息、集團介紹、問與答、館內活動、歡樂設施 非輪播單元，所以只需要上傳一組圖片。
                        </p>
                    </div> --}}
                    <!--注意事項END-->

					<!--<form action="{{ ItemMaker::url('Fantasy/工程分類/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}
                    
                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 產品詳細類別 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    @include('Fantasy.include.editActionBtns')

                                    <?php  
                                     $imgtype=[
                                                        1=>['id'=>1 ,'value'=>1 ,'title'=>'說明與規格'],
                                                        2=>['id'=>2 ,'value'=>2 ,'title'=>'圖片展示'],
                                                       
                                                                                                 
                                                    ];
                                                    ?>

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> 問題與答案設定 </a>
                                            </li>

                                        
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

													{{ItemMaker::idInput([
														'inputName' => 'ProductContent[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
													])}}

                                                     {{ItemMaker::textInput([
                                                        'labelText' => '標題',
                                                        'inputName' => 'ProductContent[title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['title']) )? $data['title'] : ''
                                                    ])}}


                                                     {{ItemMaker::textArea([
                                                        'labelText' => '內容',
                                                        'inputName' => 'ProductContent[content]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['content']) )? $data['content'] : ''
                                                    ])}}

                                                     {{ItemMaker::select([
                                                        'labelText' => '區塊格式',
                                                        'inputName' => 'ProductContent[type]',
                                                        //'required'  => true,
                                                        'options'   => $imgtype,
                                                        'value' => ( !empty($data['type']) )? $data['type'] : ''
                                                    ])}}


                                                    {{ItemMaker::photo([
                                                        'labelText' => '圖片1<br>說明與規格:(1320 x 580)<br>圖片展示:(450 x 450)',
                                                        'inputName' => 'ProductContent[image1]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['image1']) )? $data['image1'] : ''
                                                    ])}}

                                                     {{ItemMaker::photo([
                                                        'labelText' => '圖片2<br>(450 x 450)<br>(圖片展示)',
                                                        'inputName' => 'ProductContent[image2]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['image2']) )? $data['image2'] : ''
                                                    ])}}

                                                     {{ItemMaker::textInput([
                                                        'labelText' => '產品表格上方標題<br>(說明與規格)',
                                                        'inputName' => 'ProductContent[title1]',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['title1']) )? $data['title1'] : ''
                                                    ])}}


                                                    {{ItemMaker::textArea([
                                                        'labelText' => '產品表格標題<br>用換行區隔資料<br>若不填則表格不會出現<br>(說明與規格)',
                                                        'inputName' => 'ProductContent[data_title_content]',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['data_title_content']) )? $data['data_title_content'] : ''
                                                    ])}}


                                                    {{ItemMaker::textArea([
                                                        'labelText' => '產品表格內容<br>用換行區隔資料<br>資料比數要與表格標題一樣<br>(說明與規格)',
                                                        'inputName' => 'ProductContent[data_value_content]',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['data_value_content']) )? $data['data_value_content'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '圖片1下方說明(圖片展示)',
                                                        'inputName' => 'ProductContent[img_con1]',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['img_con1']) )? $data['img_con1'] : ''
                                                    ])}}

                                                  {{ItemMaker::textInput([
                                                        'labelText' => '圖片2下方說明(圖片展示)',
                                                        'inputName' => 'ProductContent[img_con2]',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['img_con2']) )? $data['img_con2'] : ''
                                                    ])}}

                                                    
                                                </div>
                                            </div>



                                 




                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif

          
		});

        




	</script>
@stop
