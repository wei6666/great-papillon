@extends('Fantasy.template')

@section('title',"全館碼設定修改 "."-".$ProjectName)
@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    #showPic.anchorImg{ width: auto!important; max-height: 250px;  }
    .copyBtn{display: none;}
    </style>

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			全館碼設定 <small>Newscatagories Manager</small> </h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('Fantasy/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="{{ ItemMaker::url('Fantasy/'.$routePreFix)}}">全館碼設定</a> <i class="fa fa-angle-right"></i>
				</li>
				<li>
					全館碼設定修改 </li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
                    {{-- <div class="note note-danger">
                        <p> 注意事項: 婚宴會議單元，才需要填入短述。<br>
                            交通訊息、集團介紹、問與答、館內活動、歡樂設施 非輪播單元，所以只需要上傳一組圖片。
                        </p>
                    </div> --}}
                    <!--注意事項END-->

					<!--<form action="{{ ItemMaker::url('Fantasy/工程分類/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 全館碼設定 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    @include('Fantasy.include.editActionBtns')

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> 全館碼設定 </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

                                                    {{ItemMaker::idInput([
                                                            'inputName' => 'DisCountFull[id]',
                                                            'value' => ( !empty($data['id']) )? $data['id'] : ''
                                                    ])}}

                                                      {{ItemMaker::radio_btn([
                                                        'labelText' => '啟用狀態',
                                                        'inputName' => 'DisCountFull[is_visible]',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
                                                        ])}}

                                                  

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '標題',
                                                        'inputName' => 'DisCountFull[title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['title']) )? $data['title'] : ''
                                                    ])}}

                                                    {{ItemMaker::textArea([
                                                        'labelText' => '折扣內容',
                                                        'inputName' => 'DisCountFull[content]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['content']) )? $data['content'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '滿額金額',
                                                        'inputName' => 'DisCountFull[full]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['full']) )? $data['full'] : ''
                                                    ])}}

                                                     {{ItemMaker::textInput([
                                                        'labelText' => '滿額折扣金額',
                                                        'inputName' => 'DisCountFull[discount]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['discount']) )? $data['discount'] : ''
                                                    ])}}

                                                    {{ItemMaker::DatePicker([
                                                        'labelText' => '折扣開始日期',
                                                        'inputName' => 'DisCountFull[st_date]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['st_date']) )? $data['st_date'] : ''
                                                    ])}}

                                                     {{ItemMaker::DatePicker([
                                                        'labelText' => '折扣結束日期',
                                                        'inputName' => 'DisCountFull[ed_date]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['ed_date']) )? $data['ed_date'] : ''
                                                    ])}}


                                                    {{ItemMaker::textInput([
                                                        'labelText' => '隨機碼',
                                                        'inputName' => 'DisCountFull[code]',
                                                        'readonly'=>'readonly',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['code']) )? $data['code'] : ''
                                                    ])}}

                                                @if(empty($data['code']))
                                                 <div class="form-group">
                                                    <label class="col-md-2 control-label" style='margin: 0 auto;'>折扣碼
                                                         
                                                    </label>
                                                    <div class="col-md-10">
                                                        <input type='button' id="getrendom" value='產生隨機折扣馬'>
                                                    </div>
                                                    
                                                </div>
                                                @endif

                                                  
                                                </div>
                                            </div>



                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif



            function getrendom()
            {
                arr=[];
                //組出大小寫Ａ－Ｚ　跟０－９

                for( i=65; i<91;i++){
                
                    arr.push(String.fromCharCode(i));
                }

                for( i=97; i<123;i++){
                    arr.push(String.fromCharCode(i));
                }


                for( i=48; i<58;i++){
                    arr.push(String.fromCharCode(i));
                }

                //組7個字的url

                str="";
                for(p=0; p<7; p++)
                {
                    str+=arr[Math.floor((Math.random() * 60) + 1)];
                }
                //console.log(str);
                $("[name='DisCountFull[code]").val(str);
            }
            
            $("#getrendom").on("click",function(){
                getrendom();

            });

		});

	</script>
@stop
