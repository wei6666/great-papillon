@extends('Fantasy.template')

@section('title',"產品次類別設定修改 "."-".$ProjectName)
@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    #showPic.anchorImg{ width: auto!important; max-height: 250px;  }
    </style>

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			產品次類別設定 <small>ProductSubCategorycatagories Manager</small> </h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('Fantasy/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="{{ ItemMaker::url('Fantasy/'.$routePreFix)}}">產品次類別設定</a> <i class="fa fa-angle-right"></i>
				</li>
				<li>
					產品次類別設定修改 </li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
                    {{-- <div class="note note-danger">
                        <p> 注意事項: 婚宴會議單元，才需要填入短述。<br>
                            交通訊息、集團介紹、問與答、館內活動、歡樂設施 非輪播單元，所以只需要上傳一組圖片。
                        </p>
                    </div> --}}
                    <!--注意事項END-->

					<!--<form action="{{ ItemMaker::url('Fantasy/工程分類/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '' }} - 產品次類別設定 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    @include('Fantasy.include.editActionBtns')

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_ProductSubCategory">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> 產品次類別管理 </a>
                                            </li>
                                             <li >
                                                <a href="#tab_genera6" data-toggle="tab"> SEO管理 </a>
                                            </li>

                                        
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_ProductSubCategory_pad">

													{{ItemMaker::idInput([
														'inputName' => 'ProductSubCategory[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
													])}}

                                                     {{ItemMaker::radio_btn([
                                                        'labelText' => '是否啟用',
                                                        'inputName' => 'ProductSubCategory[is_visible]',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '排序',
                                                        'inputName' => 'ProductSubCategory[rank]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['rank']) )? $data['rank'] : ''
                                                    ])}}

                                                   

                                                    {{ItemMaker::textInput([
                                                        'labelText' => '中文標題',
                                                        'inputName' => 'ProductSubCategory[title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['title']) )? $data['title'] : ''
                                                    ])}}


                                                     {{ItemMaker::textInput([
                                                        'labelText' => '英文標題',
                                                        'inputName' => 'ProductSubCategory[en_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['en_title']) )? $data['en_title'] : ''
                                                    ])}}


                                                    {{ItemMaker::select([
                                                        'labelText' => '所屬類別',
                                                        'inputName' => 'ProductSubCategory[category_id]',
                                                        'required'  => true,
                                                        'options'   => $parent['belong']['ProductCategory'],
                                                        'value' => ( !empty($data['category_id']) )? $data['category_id'] : ''
                                                    ])}}
                                                   
                                                    {{ItemMaker::select([
                                                        'labelText' => '套用尺寸表<br>選擇完畢後請先儲存<br>再編輯各尺寸是否可以購買',
                                                        'inputName' => 'ProductSubCategory[size_category]',
                                                        'required'  => true,
                                                        'options'   => $parent['belong']['SizeCategory'],
                                                        'value' => ( !empty($data['size_category']) )? $data['size_category'] : ''
                                                ])}}


                                                </div>
                                            </div>

                                            <div class="tab-pane  form-row-seperated" id="tab_genera6">
                                                <div class="form-body form_ProductSubCategory_pad">
                                                    <div class="form-group" style='background-color:lightblue'>
                                                        <div class="col-md-2 control-label" ><h4><b>如果seo標題有填資料<br>則以此區塊設定為主</b></h4></div>
                                                        <div class="col-md-10">
                                                        </div>
                                                    </div>

                                                    {{ItemMaker::textInput([
                                                        'labelText' => 'seo標題',
                                                        'inputName' => 'ProductSubCategory[web_title]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['web_title']) )? $data['web_title'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => 'seo描述',
                                                        'inputName' => 'ProductSubCategory[meta_description]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['meta_description']) )? $data['meta_description'] : ''
                                                    ])}}

                                                    {{ItemMaker::textInput([
                                                        'labelText' => 'seo關鍵字',
                                                        'inputName' => 'ProductSubCategory[meta_keyword]',
                                                        //'helpText' =>'前台列表顯示之排序',
                                                        'value' => ( !empty($data['meta_keyword']) )? $data['meta_keyword'] : ''
                                                    ])}}

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif

		});

	</script>
@stop
