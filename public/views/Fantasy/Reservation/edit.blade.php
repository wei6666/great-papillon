@extends('Fantasy.template')

@section('title',"預約鑑賞修改 "."-".$ProjectName)
@section('css')

    <link rel="stylesheet" href="vendor/Fantasy/global/plugins/colorbox/colorbox.css" />
    <style type="text/css">
    #showPic{ width: auto!important; max-height: 140px;  }
    #showPic.anchorImg{ width: auto!important; max-height: 250px;  }
    </style>

@stop

@section('content')

<!--fancybox for File Manager-->
<a href="" id="toFileManager"></a>

		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">
			預約鑑賞 <small>Newscatagories Manager</small> </h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ ItemMaker::url('Fantasy/')}}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="{{ ItemMaker::url('Fantasy/'.$routePreFix)}}">預約鑑賞</a> <i class="fa fa-angle-right"></i>
				</li>
				<li>
					預約鑑賞 </li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->


			<div class="row">
				<div class="col-md-12">

                    <!--注意事項-->
                    {{-- <div class="note note-danger">
                        <p> 注意事項: 婚宴會議單元，才需要填入短述。<br>
                            交通訊息、集團介紹、問與答、館內活動、歡樂設施 非輪播單元，所以只需要上傳一組圖片。
                        </p>
                    </div> --}}
                    <!--注意事項END-->

					<!--<form action="{{ ItemMaker::url('Fantasy/工程分類/Update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data" target="hidFrame">-->
					{!! Form::open( ["url" => ''.$actionUrl.'', 'method'=> 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'target' => '' ] ) !!}

                            <div class="portlet light portlet-fit portlet-datatable">
                                <div class="portlet-title portlet-title2">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">
											{{ ( !empty( $data['title'] ) )? $data['title'] : '-' }} - 預約鑑賞 </span>
                                    </div>
                                    <div class="actions btn-set">

                                    @include('Fantasy.include.editActionBtns')

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs nav_news">
                                            <li class="active">
                                                <a href="#tab_general" data-toggle="tab"> 預約鑑賞基本 </a>
                                            </li>

                                        
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active form-row-seperated" id="tab_general">
                                                <div class="form-body form_news_pad">

													{{ItemMaker::idInput([
														'inputName' => 'Reservation[id]',
														'value' => ( !empty($data['id']) )? $data['id'] : ''
													])}}

											
                                      {{ItemMaker::radio_btn([
                                                        'labelText' => '已經回復客戶',
                                                        'inputName' => 'Reservation[is_visible]',
                                                        'options' => $StatusOption,
                                                        'value' => ( !empty($data['is_visible']) )? $data['is_visible'] : ''
                                                    ])}}             

                                                   


                <div class="form-group">
                    <label class="col-md-2 control-label" style='font-weight:bold;'>
                        姓名                        　　:
                    </label>
                    <div class="col-md-10" style='margin-top: 1vh;'>
                       {{  (!empty($data['name']) )? $data['name'] : '-' }}
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-2 control-label" style='font-weight:bold;'>
                        電話                        　　:
                    </label>
                    <div class="col-md-10" style='margin-top: 1vh;'>
                       {{  (!empty($data['phone']) )? $data['phone'] : '-' }}
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-2 control-label" style='font-weight:bold;'>
                        Email                        　　:
                    </label>
                    <div class="col-md-10" style='margin-top: 1vh;'>
                       {{  (!empty($data['email']) )? $data['email'] : '-' }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label" style='font-weight:bold;'>
                        前往分店                        　　:
                    </label>
                    <div class="col-md-10" style='margin-top: 1vh;'>  
                        @if(!empty($data['location_to_visit']))
                       {!!  explode(",",$data['location_to_visit'])[0] !!}
                       @else
                       -
                       @endif
                        @if(0)
                       {!!  (!empty($data['location_to_visit']) )?
                       "<a href='".ItemMaker::url('location?lo=').explode(',',$data['location_to_visit'])[1]."''>".explode(',',$data['location_to_visit'])[0]."</a>" : '' !!}
                       @endif
                    </div>
                </div>

                 <div class="form-group">
                    <label class="col-md-2 control-label" style='font-weight:bold;'>
                        前往日期                        　　:
                    </label>
                    <div class="col-md-10" style='margin-top: 1vh;'>
                       {{  (!empty($data['date']) )? $data['date'] : '-' }}
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-2 control-label" style='font-weight:bold;'>
                        尋覓之商品                        　　:
                    </label>
                    <div class="col-md-10" style='margin-top: 1vh;'>
                       @if(!empty($data['productcategory']))
                       {!!  explode(",",$data['productcategory'])[0] !!}
                       @else
                       -
                       @endif
                      
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label" style='font-weight:bold;'>
                        來店人數                        　　:
                    </label>
                    <div class="col-md-10" style='margin-top: 1vh;'>
                       {{  (!empty($data['number_to_visit']) )? $data['number_to_visit'] : '-' }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label" style='font-weight:bold;'>
                        是否初次來店                        　　:
                    </label>
                    <div class="col-md-10" style='margin-top: 1vh;'>
                       {{  (!empty($data['is_first_time']) )? $data['is_first_time'] : '-' }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label" style='font-weight:bold;'>
                        詢問內容                        　　:
                    </label>
                    <div class="col-md-10" style='margin-top: 1vh;'>
                       {{  (!empty($data['content']) )? $data['content'] : '-' }}
                    </div>
                </div>

                <?php $mailarr=['否','是'];?>
                 <div class="form-group">
                    <label class="col-md-2 control-label" style='font-weight:bold;'>
                        發信狀態                        　　:
                    </label>
                    <div class="col-md-10" style='margin-top: 1vh;'>
                       {{  (!empty($data['is_mail']) )? $mailarr[$data['is_mail']] : $mailarr[0] }}
                    </div>
                </div>

                                                  

                                        </div>
                                    </div>
                                </div>
                            </div>
						<iframe name="hidFrame" id="hidFrame" style="display:none;"></iframe>
					<!--</form>
					 END FORM-->
					 {!! Form::close() !!}
				</div>
			</div>

@stop

@section("script")

<script src="vendor/Fantasy/global/plugins/colorbox/jquery.colorbox.js" type="text/javascript"></script>


<script src="vendor/main/colorpick.js" type="text/javascript"></script>
<script src="vendor/main/datatable.js" type="text/javascript" ></script>
<script src="vendor/main/rank.js" type="text/javascript" ></script>

	<script>
		$(document).ready(function() {

			FilePicke();
			Rank();
			//資料刪除
			dataDelete();

			colorPicker();

            changeStatic();

            @if($Message)
                toastrAlert('success', '{{$Message}}');
            @endif

            $("#tab_general > div > div:nth-child(2) > label").eq(0).css('font-weight','bold');

		});

        $("#itemTbody > tr ").width('300px');
        $("#itemTbody > tr > td > input").width('300px');

	</script>
@stop
