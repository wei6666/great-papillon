
@extends('template')

    <!--RWD Setting-->
@section('css')
  
  
 
   
    <link rel="stylesheet" href="/assets/js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="/assets/js/vendor/slick/slick.css">
    <link rel="stylesheet" href="/assets/js/vendor/slick/slick-theme.css">
    <link rel="stylesheet" href="/assets/js/vendor/easydropdown/easydropdown.css">
    <link rel="stylesheet" href="/assets/js/vendor/datedropper/datedropper.css">
    <link rel="stylesheet" href="/assets/js/vendor/datedropper/my-style.css">
    <link rel="stylesheet" href="/assets/css/reservation.css">

@stop
@section('content')


    <main>
        <article class="_banner">
            <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="{!! $ReservationSet->res_photo !!}" alt="">
        </article>
        <article class="_content">
            <div class="_close" onclick="history.back()">
                <p>CLOSE</p>
            </div>
            <div class="_title">
                <h2>{!! $ReservationSet->res_en_title !!}</h2>
                <p>{!! $ReservationSet->res_title !!}</p>
            </div>
            <form class="_table" mathod='post' action='{{ ItemMaker::url('reservationpost') }}'>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <ul class="_input">
                    <!-- 姓名 -->
                    <li>
                        <p>姓名</p>
                        <input type="text" name='data[name]' placeholder="請輸入您的姓名">
                    </li>
                    <!-- 電話 -->
                    <li>
                        <p>電話</p>
                        <input type="text" name='data[phone]' placeholder="請輸入您的電話">
                    </li>
                    <!-- e-mail -->
                    <li>
                        <p>E-MAIL</p>
                        <input type="text" name='data[email]' placeholder="請輸入您的E-MAIL">
                    </li>
                    <!-- 前往分店 -->
                    <li class="searchStore">
                        <p>希望前往分店</p>
                        <div class="menulist location_to_visit">
                            <span class="selectMenu"><p>請選擇分店</p></span>
                            <ul>
                                <!-- 此處內容須讓客戶可新增 -->
                                @foreach($Location as $value)

                                <li  name-id="{{ $value->title.",".$value->id }}">
                                    <p>{!! $value->title !!}</p>
                                </li>
                                @endforeach
                              
                            </ul>
                        </div>
                        
                    </li>
                    <input  type='hidden' id='location_to_visit' name="data[location_to_visit]" >
                    <!-- 前往日期 -->
                    <li>
                        <p>希望前往日期</p>
                        <div class="_datebox date">
                            <input type="text" class="_date" data-large-mode="true" data-auto-lang="true" data-theme="my-style" data-init-set="false" data-translate-mode="true" data-modal="true" data-lock="from" data-min-year='2017' data-max-year='2030' placeholder="請選擇日期"><span></span></div>
                    </li>
                    <input type='hidden' id='date' name="data[date]" >
                    <!-- 尋覓商品 -->
                    <li class="searchProduct">
                        <p>尋覓之商品</p>
                        <div class="menulist productcategory">
                            <span class="selectMenu"><p>請選擇</p></span>
                            <ul>
                                <!-- 此處內容須讓客戶可新增 -->
                                @foreach($ProductCategory as $value)
                                <li name-id="{{ $value->title.",".$value->id }}">
                                    <p>{{ $value->title }}</p>
                                </li>
                                @endforeach

                               
                            </ul>
                        </div>
                    </li>
                    <input type='hidden' id='productcategory' class='productcategory' name="data[productcategory]" >
                    <!-- 人數 -->
                    <li>
                        <p>來店人數</p>
                        <input type="text"  name="data[number_to_visit]"  placeholder="請輸入來店人數">
                    </li>
                    <!-- 是否第一次嗎? -->
                    <li class="firstTime">
                        <p>是否初次來店</p>
                        <div class="menulist is_first_time" >
                            <span class="selectMenu"><p>請選擇</p></span>
                            <ul>
                                <li name-id="是">
                                    <p >是</p>
                                </li>
                                <li name-id="否">
                                    <p>否</p>
                                </li>
                            </ul>
                        </div>
                        <input type='hidden' id="is_first_time" name="data[is_first_time]" >
                    </li>
                    <!-- 內容 -->
                    <li class="_textarea">
                        <p>詢問內容</p>
                        <textarea name="data[content]" placeholder="請輸入您的內容"></textarea>
                    </li>
                </ul>
                <ul class="_button">
                    <li>
                        <button type="submit">立即預約</button>
                    </li>
                    <li>
                        <button type="reset">清除重填</button>
                    </li>
                </ul>
            </form>
        </article>
    </main>

 

 
@section('script')  
  
    <script src="/assets/js/vendor/datedropper/datedropper.js"></script>
    <script src="/assets/js/vendor/easydropdown/jquery.easydropdown.js"></script>
    <script src="/assets/js/vendor/jquery.nicescroll/jquery.nicescroll.min.js"></script>
    <script>
        $(".location_to_visit li").on("click",function(){
            $("#location_to_visit").val($(this).attr('name-id'));
            console.log($("#location_to_visit").val());

        });

        $(".productcategory li").on("click",function(){
            $("#productcategory").val($(this).attr('name-id'));
            console.log($("#productcategory").val());

        });


         $(".is_first_time li").on("click",function(){
            $("#is_first_time").val($(this).attr('name-id'));
            console.log($("#is_first_time").val());

        });

         // $(".pick-submit").on("click",function(){
         //        console.log($(".pick-sl").eq(0));
         //        console.log($(".pick-sl").eq(1));
         //        console.log($(".pick-sl").eq(2));
         //        console.log($(".pick-sl").eq(3));
         //        console.log($(".pick-sl").eq(4));
         // });
        
    </script>
@stop


@stop
