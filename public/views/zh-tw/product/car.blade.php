{{-- 

        <!--關閉按鈕-->
        <div class="_close close_btn">
            <a href="javascript:void(0)">
                <p>CLOSE</p>
            </a>
        </div>

        標題
        <section class="_top">
            
          <div class="_title">
            <h2>GIFT SHOP</h2>
            <p>購物車</p>
          </div>
        
        </section> --}}

        <!--已選擇數量-->
        <div class="chosen">
            @if(empty($car_data))
            <p>您目前選購了<span>0</span>件精品</p>
            @else
            <p>您目前選購了<span>{{ count($car_data) }}</span>件精品</p>
            @endif
        </div>
        
        <!--框-->
            <section class="cart_frame">
                <ul class="frame">
                    @if(empty($car_data))
                    <div class="" style='padding: 5vh 0 5vh 0; text-align: center;'><h1  style='font-size:4vmin;'>購物車內尚無商品</h1></div>
                    @else

                    
                        @foreach($car_data as $key => $value)
                        <li class="list">
                            <!--item-->
                            <section class="item">

                                <!--點擊連到被點擊的產品項頁面-->
                                <a class="item_box" href="{{ ItemMaker::url('product/'.$value['product']->id) }}">
                                    <!--100*100-->
                                    <div class="img">
                                        <picture>
                                            <img class="b-lazy b-loaded" src="{{ $value["out_img"] }}" alt="">
                                        </picture>
                                    </div>
                
                                    <div class="info">
                                        <p class="name">{{ $value['product']->en_title }}</p>
                                        <p class="vice">{{ $value['product']->title }}</p>
                                        <p class="price" p='{{ $value['price'] * $value['count']}}'>NT$ {{ $value['price'] }}</p>
                                        <p class="size">
                                            @if(!empty($value['separate']))
                                            {{ $value['separate']->title }}
                                            @endif
                                            @if(!empty($value['separate']))
                                            <span>尺寸 : {{ $value['size'] }} </span>
                                            @endif
                                            <span>數量 : {{  $value['count']}}</span>
                                        </p>
                                    </div>
                                </a>

                            </section>
                            <!--刪除按鈕-->
                            <div class="delete delete_btn" link='{{ ItemMaker::url('sessiondel/'.$key) }}'></div>
                        </li>
                        @endforeach
                    @endif
                  
                </ul>
            </section>

        <!--消費金額總計-->
        @if(!empty($car_data))
        <section class="total">
            <div class="bin">
                <p class="title">TOTAL</p>
                <p class="number">NT$ {{ $total }}</p>
            </div>
            <!--繼續 付款 按鈕-->
            <div class="form_btn">
                <a href="javascript:void(0)" class="gray two close_btn">
                <div>
                    <p>繼續選購</p>
                </div>
                </a>
                <a href="{{ ItemMaker::url("order_list")}}" class="black two">
                <div>
                    <p>前往結帳</p>
                </div>
                </a>
            </div>
        </section>
        @endif

