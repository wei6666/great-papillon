
@extends('template')

    <!--RWD Setting-->
@section('css')
     
     <link rel="stylesheet" href="/assets/css/member.css">

@stop
@section('content')

@include($locale.'.include.header') 
   

  <main>

    <article class="m_box">

      <!--標題-->
      <section class="_top">
          
        <div class="_title">
          <h2>SHOPPING CART</h2>
          <p>購物車</p>
        </div>
      
      </section>
  
      <!--已選擇數量-->
      <div class="chosen">
        <p>您目前選購了<span>{{ count($car_data) }}</span>件精品</p>
      </div>
  
      <!--購物車清單-->
      <article class="order_nav_frame detail_content">

        <!--商品明細-->
        <section class="order_nav_detail order_navBox">

          <!--title-->
          <div class="order_li_title">
            <p class="title">Product Details</p>
            <p class="vice">商品明細</p>
          </div>

          <!--列表-->
          <div class="order_list_box product_area" >
            <ul class="order_ul">
              
                @foreach($car_data as $key => $value)
                <li class="order_li real_list" >
       
                  <!--圖 140*140-->
                  <div class="img box_1">
                    <picture>
                      <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="{{ $value["out_img"] }}" alt="">
                    </picture>
                  </div>

                  <!--資訊-->
                  <div class="info box_2">
                    <p class="s_number">{{ $value["product"]['complete_num'] }}</p>
                    <p class="name">{{ $value["product"]['en_title'] }}</p>
                    <p class="vice">{{ $value["product"]['title'] }}</p>
                    @if($value['size'] !="-")
                      <p class="size">尺寸 : {{ $value['size'] }}</p>
                    @endif
                  </div>

                  <!--數量增減-->
                  <div class="operation box_3">
                    <!--數量-->
                    <div class="count btnparent" link='{{ ItemMaker::url("updatenum/".$key."/") }}'>
                      <button class="reduce"></button>
                      <input type="number" class='pro_num' name="" value="{{ $value['count'] }}" placeholder="1">
                      <button class="add"></button>
                    </div>
                    <!--刪除按鈕-->
                    <div class="delete delete_btn dellist" link='{{ ItemMaker::url('sessiondel/'.$key) }}'></div>
                  </div>

                  <!--金額-->
                  <div class="num_word box_4">
                    <p class='countprice' price="{{ $value['price'] }}">NT$ {{ $value['price'] * $value['count'] }}</p>
                  </div>                

                </li>
                @endforeach
              

              </ul>
          </div>

        </section>

        <!--優惠折抵-->
        <section class="order_nav_discount order_navBox">

          <!--title-->
          <div class="order_li_title">
            <p class="title">Discount</p>
            <p class="vice">優惠折抵</p>
          </div>

          <!--列表-->
          <div class="order_list_box">
            <ul class="order_ul">
               @if(!empty($DisCountPercent)) 
              <!--全館折扣-->
              <li class="order_li">
    
                <!--title-->
                <div class="title box_1">
                  <p>
                    {!! $DisCountPercent->title !!}
                    
                  </p>
                </div>

                <!--notice-->
                <div class="notice box_2">
                  <p>
                    {!! $DisCountPercent->content !!}
                    
                  </p>
                </div>

                <!--數量-->
                <div class="num_word box_3">
                  <p>
                    {!! -((1-($DisCountPercent->percent)) * 100)."%" !!}
                    
                  </p>
                </div>

                <!--金額-->
                <div id="PercentDisCount" class="num_word minus box_4">
                  
                  <p>NT$ {{ $DisCountData['PercentDisCount'] }}</p>
                  
                 
                </div>                

              </li>
              @endif
              <!--滿千送百-->
              @if(!empty($DisCountFull))
              <li class="order_li">
                  
                <!--title-->
                
                <div class="title box_1">
                  <p>{!! $DisCountFull->title !!}</p>
                </div>

                <!--notice-->
                <div class="notice box_2">
                  <p>{!! $DisCountFull->content !!}</p>
                </div>

                <!--數量-->
                <div class="num_word box_3">
                  <p>滿 {!! $DisCountFull->full !!} 送 {!! $DisCountFull->discount !!}</p>
                </div>
               
                <!--金額-->                
                <div id="FullDisCount" class="num_word minus box_4">
                  <p>NT$ {{ $DisCountData["FullDisCount"] }}</p>
                </div>                

              </li> 
              @endif

              <!--活動折扣碼-->
              <li class="order_li">
                  
                <!--title-->
                <div class="title box_1">
                  <p>{!! $DisCountSet->code_title !!}</p>
                </div>

                <!--notice-->
                <div class="notice box_2">
                  <p>{!! $DisCountSet->code_content !!}</p>
                </div>

                <!--折扣碼-->
                <div class="operation box_3">
                  <div class="count">
                    <input id="DisCountCode" type="text" ajax_type='go' name="" value="{{ $DisCountData['Code'] }}" placeholder="請輸入折扣碼">
                    <input id="DisCountCodetemp" type="hidden" ajax_type='go' name="" value="{{ $DisCountData['Code'] }}" placeholder="請輸入折扣碼">
                  </div>
                  <!--開始時兩個icon都不顯示 等到使用者輸入折扣碼正確就顯示icon-check 錯誤就顯示icon-x-->
                  <!--顯示方法 在span加 show-->
                  <div class="code check">
                    <span class="icon-check "></span>                    
                   {{--  <span id="CodeMsg"></span> --}}
                  </div>
                </div>

                <!--金額-->
                <div id="CodeDisCount" class="num_word minus box_4">
                  <p>{{-- NT$ {{ $DisCountData['CodeDisCount'] }} --}}</p>
                </div>                

              </li>

              <!--會員紅利折抵-->
              <li class="order_li">
                  
                <!--title-->
                <div class="title box_1">
                  <p>{!! $DisCountSet->bonus_title !!}</p>
                </div>

                <!--notice-->
                <div class="notice box_2">
                  <p>{!! $DisCountSet->bonus_content !!}</p>
                </div>

                <!--數量-->
                <div class="operation box_3 bonuscode_area">
                  <div class="count">
                    <button class="reduce"></button>
                    <input  id="DisCountBonusval" type="text" ajax_type='go' name="" value="{{ $DisCountData['BonusDisCount'] }}" placeholder="請輸入折扣碼">
                    <input  id="DisCountBonusvaltemp" type="hidden" ajax_type='go' name="" value="{{ $DisCountData['BonusDisCount'] }}" placeholder="請輸入折扣碼">
                    <button class="add"></button>
                  </div>
                </div>

                <!--金額-->
                <div id="BonusDisCount"  class="num_word minus box_4">
                  <p>{{-- NT$ {{ $DisCountData['BonusDisCount'] }} --}}</p>
                  
                </div>                

              </li>

              </ul>
          </div>

        </section>

        <!--紅利回饋-->
        <section class="order_nav_bonus order_navBox">

          <!--title-->
          <div class="order_li_title">
            <p class="title">Bonus</p>
            <p class="vice">紅利回饋</p>
          </div>

          <!--列表-->
          <div class="order_list_box">
            <ul class="order_ul">
                
              <li class="order_li">
    
                <!--title-->
                <div class="title box_1">
                  <p>{!! $DisCountSet->friend_title !!}</p>
                </div>

                <!--notice-->
                <div class="notice box_2">
                  <p>{!! $DisCountSet->friend_content !!}</p>
                </div>

                <!--會員編號-->
                <div class="operation box_3">
                  <div class="count">
                    <input type="text" name="" ajax_type='go' id="Friend" value="" placeholder="請輸入好友會員編號">
                    <input type="hidden" name="" id="Friendtemp"  value="" placeholder="請輸入好友會員編號">
                  </div>
                  <!--開始時兩個icon都不顯示 等到使用者輸入會員編號正確就顯示icon-check 錯誤就顯示icon-x-->
                  <!--顯示方法 在span加 show-->
                  <div class="friend check">
                    <span class="icon-check"></span>
                    
                  </div>
                </div>

                <!--金額-->
                <div class="num_word box_4" id="friendfeedback">
                  <p>{{-- {{ $friend_feedback }} BP --}} -</p>
                </div>                

              </li>

              <li class="order_li">
                  
                <!--title-->
                <div class="title box_1">
                  <p>{!! $DisCountSet->feedback_title !!}</p>
                </div>

                <!--notice-->
                <div class="notice box_2">
                  <p>{!! $DisCountSet->feedback_content !!}</p>
                </div>

                <!--數量-->
                <div class="num_word box_3">
                  <p>{!! $DisCountSet->feedback_value !!}</p>
                </div>

                <!--金額-->
                <div class="num_word box_4" id="feedback">
                  <p>{{ $feedback }} BP</p>
                </div>                

              </li>

            </ul>
          </div>

        </section>

        <!--商品運送-->
        <section class="order_nav_shipping order_navBox">
          
          <!--title-->
          <div class="order_li_title">
            <p class="title">Shipping</p>
            <p class="vice">商品運送</p>
          </div>

          <!--列表-->
          <div class="order_list_box">
            <ul class="order_ul">
                
              <li class="order_li">
    
                <!--title-->
                <div class="title box_1">
                  <p>收件人資訊</p>
                </div>

                <!--notice-->
                <div class="notice box_6" id='shop_receiver'>
                  <p>
                    <span>收件人姓名 : </span>
                    <span>-</span>
                  </p>
                  <p>
                    <span>收件人住址 : </span>
                    <span>-</span>
                  </p>
                  <p>
                    <span>收件人電話 : </span>
                    <span>-</span>
                  </p>
                  <p>
                    <span>收件人手機 : </span>
                    <span>-</span>
                  </p>
                </div>

                <!--更改收件人-->
                <div class="form_btn box_4" type="shop_receiver">
                  <a href="javascript:void(0)" link="{{ ItemMaker::url('shop_receiver') }}" class="black one shipping_btn">
                    <div>
                      <p>更改收件人</p>
                    </div>
                  </a>
                </div>                

              </li>

              <li class="order_li">
                  
                <!--title-->
                <div class="title box_1">
                  <p>{{ $Setting->shipping_cost_title }}</p>
                </div>

                <!--notice-->
                <div class="notice box_6">
                  <p class="tip">{!! $Setting->shipping_cost_content !!}</p>
                </div>

                <!---->
                <div class="text box_4" id="ShippingCost">
                  <p>-</p>
                </div>                

              </li>

            </ul>
          </div>

        </section>

        <!--發票資訊-->
        <section class="order_nav_shipping order_navBox">
          
          <!--title-->
          <div class="order_li_title">
            <p class="title">Invoice</p>
            <p class="vice">發票資訊</p>
          </div>

          <!--列表-->
          <div class="order_list_box">
            <ul class="order_ul">
                
              <li class="order_li">
    
                <!--title-->
                <div class="title box_1">
                  <p>發票內容</p>
                </div>

                <!--發票-->
                <div class="invoce box_5">

                  <section class="check_list">
                    <div class="check_box show">
                      <input type="radio" id="radio01" name="radio">
                      <label for="radio01" class=""><span></span>捐贈慈善基金會</label>
                    </div>
                    <div class="check_box">
                      <input type="radio" id="radio02" name="radio">
                      <label for="radio02" class=""><span></span>二連式發票</label>
                    </div>
                    <div class="check_box">
                      <input type="radio" id="radio03" name="radio">
                      <label for="radio03" class=""><span></span>三聯式發票</label>
                    </div>
                  </section>

                  <section class="input_list">
                    <input type="text" name="" value="" placeholder="請輸入統一編號">
                    <input type="text" name="" value="" placeholder="請輸入發票抬頭">
                  </section>

                </div>                

              </li>

              <li class="order_li">
                  
                <!--title-->
                <div class="title box_1">
                  <p>發票收件人</p>
                </div>

                <!--notice-->
                <div class="notice box_6" id='ticket_receiver'>
                  <p>
                    <span>收件人姓名 : </span>
                    <span>-</span>
                  </p>
                  <p>
                    <span>收件人住址 : </span>
                    <span>-</span>
                  </p>
                  <p>
                    <span>收件人電話 : </span>
                    <span>-</span>
                  </p>
                  <p>
                    <span>收件人手機 : </span>
                    <span>-</span>
                  </p>
                </div>

                <!--更改收件人-->
                <div class="form_btn box_4" type="ticket_receiver">
                  <a href="javascript:void(0)" link="{{ ItemMaker::url('ticket_receiver') }}" class="black one shipping_btn">
                    <div>
                      <p>更改收件人</p>
                    </div>
                  </a>
                </div>                

              </li>

            </ul>
          </div>

        </section>

        <!--商品配件包裝-->
        <section class="order_nav_shipping order_navBox">
            
          <!--title-->
          <div class="order_li_title">
            <p class="title">Packaging</p>
            <p class="vice">商品配件包裝</p>
          </div>

          <!--列表-->
          <div class="order_list_box">
            <ul class="order_ul">
                
              <li class="order_li">
    
                <!--title-->
                <div class="title box_1">
                  <p>{!! $DisCountSet->packet_title !!}</p>
                </div>

                <!--包裝-->
                <div class="pack box_7">

                  <div class="text">
                    <p>{!! $DisCountSet->packet_content !!}</p>
                  </div>

                  <!--570*330-->
                  <div class="img">
                    <img src="{!! $DisCountSet->packet_image !!}" alt="">
                  </div>

                </div>
                
                <!---->
                <div class="text box_4">
                  <p>{!! $DisCountSet->packet_value !!}</p>
                </div>

              </li>

            </ul>
          </div>

        </section>

        <!--消費總金額-->
        <section class="total" id='total'>
          <div class="box">
            <span class="title">本次消費金額總計</span>
            <p class="number" >NT$ {{ $total }}</p>
          </div>
          <!--繼續 付款 按鈕-->
          <div class="form_btn">
            <a href="{{ ItemMaker::url('productcategory/1') }}" class="gray two">
              <div>
                <p>繼續選購</p>
              </div>
            </a>
            <a href="javascript:void(0)" id='inticketdata' class="black two next_content_btn">
              <div>
                <p>立即付款</p>
              </div>
            </a>
          </div>
        </section>

      </article>
      
    </article>

  </main>
    @include($locale.'.include.footer') 
  <!--結帳區塊-->
  <article class="payment_block">
      
    <!--結帳-->
    <article class="pay payment">
        
      <!--結帳畫面-->
      <section class="box box_1">

        <!--標題-->
        <section class="_top">
            
            <div class="_title">
              <h2>PAYMENT</h2>
              <p>結帳</p>
            </div>
        
        </section>

        <!--偷頭總額-->
        <section class="total_amount">

          <div class="title">
            <p>TOTAL AMOUNT</p>
          </div>

          <div class="number">
            <p>NT$ 58,750</p>
          </div>

        </section>

        <!--付款方式 被選擇的項目在li加上'clicked'-->
        <section class="pay_way">
          <ul>
            <li class="credit pay_way_list clicked" type="1">
              <a href="javascript:void(0)">

                <div class="tick">
                  <div>
                    <span class="icon-check"></span>
                  </div>
                </div>

                <div class="notice">
                  <div>
                    <p class="title">信用卡</p>
                    <p>親愛的會員您好，附款完成後，請記得回到法蝶網站確認您的消費是否完成。</p>
                  </div>
                </div>

                <div class="logo">
                  <div>
                    <img src="/assets/icon/Visa.svg" alt="">
                    <img src="/assets/icon/MasterCard.svg" alt="">
                    <img src="/assets/icon/JCB.svg" alt="">
                  </div>
                </div>

              </a>
            </li>
            <li class="express pay_way_list" type="2">
              <a href="javascript:void(0)">

                <div class="tick">
                  <div>
                    <span class="icon-check"></span>
                  </div>
                </div>

                <div class="notice">
                  <div>
                    <p class="title">貨到付款</p>
                    <p>選擇貨到付款，我們在商品出貨時會進行通知，也請您在收到通知後，務必到您的會員訂單管理確認宅配等相關訊息。</p>
                  </div>
                </div>

                <div class="logo">
                  <div>
                    <span class="icon-express"></span>
                  </div>
                </div>

              </a>
            </li>
            <li class="phone_pay pay_way_list" type="3">
              <a href="javascript:void(0)">

                <div class="tick">
                  <div>
                    <span class="icon-check"></span>
                  </div>
                </div>

                <div class="notice">
                  <div>
                    <p class="title">APPLE PAY</p>
                    <p>使用您的行動支付，付款完成後，請記得回到網站確認您的消費是否完成。</p>
                  </div>
                </div>

                <div class="logo">
                  <div>
                    <span class="icon-phonePay"></span>
                  </div>
                </div>

              </a>
            </li>
          </ul>
        </section>

        <!--取消 確認 按鈕-->
        <div class="form_btn">
          <a href="javascript:void(0)" class="gray two cancel_content_btn">
            <div>
              <p>取消</p>
            </div>
          </a>
          <a id='togo' href="javascript:void(0)" class="black two next_content_btn">
            <div>
              <p>確認</p>
            </div>
          </a>
        </div>

      </section>

      <!--結帳中畫面-->
      <section class="box box_2">
        
        <!--標題-->
        <section class="_top">
            
            <div class="_title">
              <h2>PAYMENT</h2>
              <p>結帳</p>
            </div>
        
        </section>

        <!---->
        <div class="warning">
          <p>附款連線中，請勿關閉視窗 ...</p>
        </div>

        <!---->
        <div class="notify">
          <p>若您的等待時間過長，您可以按下重新附款，我們將會再次進行連線付款，感謝您。</p>
        </div>

        <!--取消 重新付款 按鈕-->
        <div class="form_btn">
          <a href="javascript:void(0)" class="gray two cancel_content_btn">
            <div>
              <p>取消</p>
            </div>
          </a>
          <a href="javascript:void(0)" class="black two">
            <div>
              <p>重新付款</p>
            </div>
          </a>
        </div>

      </section>

      <!--結帳完成畫面-->
      <section class="box box_3">
        
        <!--標題-->
        <section class="_top">
            
            <div class="_title">
              <h2>PAYMENT</h2>
              <p>完成付款</p>
            </div>
        
        </section>

        <!---->
        <div class="notify">
          <p>親愛的會員您好，感謝您對法蝶珠寶的支持，您此次的訂單付款已經完成，請記得到會員資料管理確認您的訂單，以及後續出貨流程。</p>
        </div>

        <!--取消 重新附款 按鈕-->
        <div class="form_btn">
          <a href="../member/member_guide.html" class="black two">
            <div>
              <p>確認</p>
            </div>
          </a>
        </div>

      </section>

    </article>

  </article>

  <!--商品運送區塊-->
  <article class="shipping_block">

    <article class="shipping_frame" opentype="">

      <!--關閉按鈕-->
      <div class="_close close_btn">
        <a href="javascript:void(0)">
          <p>CLOSE</p>
        </a>
      </div>

      <!--標題-->
      <section class="_top">
          
        <div class="_title">
          <h2>SHIPPING</h2>
          <p>商品運送</p>
        </div>
      
      </section>

      <!--商品nav-->
      <section class="shipping_nav">
        <ul>
          <li>
            <a href="javascript:void(0)" class="black content_btn">
              <p>使用會員資訊</p>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)" class="gray content_btn">
              <p>從地址簿選擇</p>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)" class="gray content_btn">
              <p>新增地址</p>
            </a>
          </li>
        </ul>
      </section>

      <!--內容清單-->
      <section class="shipping_list">
        <ul>
          <li class="box_1 box">
            
            <ul class="box_ul detail_1">
              <li>
                  <label for="">MOBILE</label>
                  <input class="content" type="number" name="" value="{{ $MemberData->mobile  }}" placeholder="0912345678">
                </li>
                <li>
                  <label for="">NAME</label>
                  <input class="content" type="text" name="" value="{{ $MemberData->name  }}" placeholder="昆靈">
                </li>
                <li>
                  <label for="">CITY</label>
                  
                  <input class="content" type="text" name="" value="{{ $Citydata[$MemberData->city] }}" placeholder="{{-- 台中市 --}}">
                </li>
                <li>
                  <label for="">POSTAL CODE</label>
                  <input class="content" type="number" name="" value="{{ $MemberData->zip  }}" placeholder="{{-- 104 --}}">
                </li>
                <li>
                  <label for="">ADDRESS</label>
                  <input class="content" type="text" name="" value="{{ $MemberData->address  }}" placeholder="{{-- 西屯區忠孝東路四段45號 --}}">
                </li>
            </ul>

            <!--取消 選擇地址 按鈕-->
            <div class="form_btn">
              <a href="javascript:void(0)" class="gray two cancel_btn">
                <div>
                  <p>取消</p>
                </div>
              </a>
              <a href="javascript:void(0)" id='detail_1' class="black two save_btn" btn-id="0">
                <div>
                  <p>選擇地址</p>
                </div>
              </a>
            </div>

          </li>
          <li class="box_2 box">
              @if(count($addressdata) >0) 
                <ul class="box_ul">
                  <li class="dropMenu">
                    <label for="">ADDRESS BOOK</label>
                    <div class="menulist">
                      <span class="selectMenu arrow-icon"><p>{{ $addressdata[0]->title }}</p></span>
                    </div>
                    <ul tabindex="1" class="dropMenu_box">
                      @foreach($addressdata as $value)
                      <li data_id='{{ $value->id }}'>
                        <p>{{ $value->title }}</p>
                      </li>
                      @endforeach
                    
                    </ul>
                  </li>
                  <?php $coo=0; ?>
                  @foreach($addressdata as $value)
                  <div class='data @if($coo==0) toshow @endif' data_id="{{ $value->id  }}" @if($coo==0) style="display: none; " @endif>
                    <?php $coo++;?>
                    <li>
                      <label for="">MOBILE</label>
                      <input class="content" type="number" name="" value="{{ $value->mobile  }}" placeholder="0912345678">
                    </li>
                    <li>
                      <label for="">NAME</label>
                      <input class="content" type="text" name="" value="{{ $value->name  }}" placeholder="昆靈">
                    </li>
                    <li>
                      <label for="">CITY</label>

                      <input class="content" type="text" name="" value="{{ $Citydata[$value->city] }}" placeholder="台中市">
                    </li>
                    <li>
                      <label for="">POSTAL CODE</label>
                      <input class="content" type="number" name="" value="{{ $value->zip  }}" placeholder="104">
                    </li>
                    <li>
                      <label for="">ADDRESS</label>
                      <input class="content" type="text" name="" value="{{ $value->address  }}" placeholder="西屯區忠孝東路四段45號">
                    </li>
                  </div>
                  @endforeach
                </ul>
              @endif

              <!--取消 選擇地址 按鈕-->
              <div class="form_btn">
                <a href="javascript:void(0)" class="gray two cancel_btn">
                  <div>
                    <p>取消</p>
                  </div>
                </a>
                <a href="javascript:void(0)" id='detail_2' class="black two save_btn" btn-id="0">
                  <div>
                    <p>選擇住址</p>
                  </div>
                </a>
              </div>
          </li>
          <li class="box_3 box ajaxnewaddress">              
            <ul class="box_ul">
              <li>
                <label for="">ADDRESS BOOK</label>
                <input class="content" type="text" name="" value="" placeholder="請輸入地址簿名稱">
              </li>
              <li>
                <label for="">MOBILE</label>
                <input id='mobilecheck' class="content" type="number" name="" value="" placeholder="請填寫您的手機號碼">
              </li>
              <li>
                <label for="">NAME</label>
                <input class="content" type="text" name="" value="" placeholder="請填寫您的姓名">
              </li>
              <li class="dropMenu">
                <label for="">CITY</label>
                <div class="menulist">
                  <span class="selectMenu arrow-icon"><p>請選擇您居住的縣市</p></span>
                </div>
                <ul id='areaid' tabindex="1" class="dropMenu_box citydrop">
                  @foreach($City as $value)
                  <li city_id="{{$value ->title}}">
                    <p>{{ $value->title }}</p>
                  </li>
                  @endforeach
                  <input class="content" id='City' type="hidden" name="" value="" placeholder="請填寫您的姓名">
                </ul>
              </li>
              <li>
                <label for="">POSTAL CODE</label>
                <input class="content" type="number" name="" value="" placeholder="請輸入您的郵遞區號">
              </li>
              <li>
                <label for="">ADDRESS</label>
                <input class="content" type="text" name="" value="" placeholder="請輸入您的住址">
              </li>
            </ul>
            <!--取消 選擇地址 按鈕-->
            <div class="form_btn">
              <a href="javascript:void(0)" class="gray two cancel_btn">
                <div>
                  <p>取消</p>
                </div>
              </a>
              <a href="javascript:void(0)" class="black two save_btn newAddr" btn-id="0">
                <div>
                  <p>儲存選擇</p>
                </div>
              </a>
            </div>
          </li>
        </ul>
      </section>

    </article>

  </article>

  <!-- <div>
    <svg version="1.1" baseProfile="tiny" id="圖層_1" x="0px" y="0px" width="595.28px" height="841.89px" viewBox="0 0 595.28 841.89" xml:space="preserve">
      <g stroke="#000" stroke-width="5" fill="none">
        <path d="M291.854,591.537c-0.715,0-1.423-0.303-1.895-0.863L99.518,363.602c-0.693-0.863-0.76-2.072-0.177-2.935l70.284-109.193   c0.458-0.693,1.231-1.121,2.094-1.121h240.319c0.818,0,1.6,0.428,2.117,1.121l70.238,109.193c0.605,0.862,0.517,2.071-0.169,2.935   L293.778,590.674C293.299,591.234,292.584,591.537,291.854,591.537z M104.502,361.787l187.352,223.408l187.363-223.408   l-68.559-106.435H173.075L104.502,361.787z"/>
        <path d="M291.854,364.465c-0.885,0-1.704-0.428-2.161-1.291l-58.088-105.182L173.938,363.13c-0.435,0.774-1.246,1.29-2.16,1.29   c0,0,0,0-0.015,0c-0.885,0-1.681-0.472-2.16-1.246l-33.417-57.255c-0.671-1.209-0.28-2.713,0.907-3.406   c1.143-0.693,2.699-0.31,3.407,0.899l31.197,53.457l57.682-105.233c0.458-0.767,1.298-1.283,2.183-1.283l0,0   c0.929,0,1.74,0.517,2.197,1.283L294.029,360.8c0.679,1.165,0.244,2.714-0.988,3.362   C292.672,364.376,292.259,364.465,291.854,364.465z"/>
        <path d="M291.854,591.537c-0.907,0-1.748-0.516-2.205-1.291L170.207,364.465h-68.794c-1.364,0-2.456-1.121-2.456-2.5   c0-1.342,1.091-2.419,2.456-2.419h70.239c0.066,0,0.088,0,0.088,0h120.114c1.364,0,2.5,1.077,2.5,2.419v227.117   c0,1.12-0.796,2.108-1.895,2.366C292.259,591.537,292.053,591.537,291.854,591.537z M175.833,364.465l113.566,214.56v-214.56   H175.833z"/>
        <path d="M291.854,364.465c-0.406,0-0.818-0.089-1.188-0.303c-1.209-0.648-1.659-2.197-0.973-3.362l60.281-109.164   c0.436-0.767,1.254-1.283,2.16-1.283l0,0c0.922,0,1.764,0.517,2.176,1.283l57.727,105.233l31.197-53.457   c0.686-1.209,2.242-1.593,3.451-0.899c1.164,0.693,1.556,2.197,0.862,3.406l-33.483,57.255c-0.428,0.774-1.246,1.246-2.107,1.246   c-0.045,0-0.045,0-0.045,0c-0.863,0-1.726-0.516-2.16-1.29l-57.617-105.138l-58.106,105.182   C293.579,364.037,292.739,364.465,291.854,364.465z"/>
        <path d="M291.854,591.537c-0.214,0-0.406,0-0.583-0.089c-1.099-0.258-1.873-1.246-1.873-2.366V361.965   c0-1.342,1.077-2.419,2.456-2.419h120.139c0.044,0,0.044,0,0.044,0h70.284c1.378,0,2.507,1.077,2.507,2.419   c0,1.379-1.129,2.5-2.507,2.5h-68.816L294.059,590.246C293.624,591.021,292.783,591.537,291.854,591.537z M294.354,364.465v214.56   L407.9,364.465H294.354z"/>
      </g>
    </svg>
  </div> -->


@section('script')
  <script src="/assets/js/vendor/drawsvg/jquery.drawsvg.js"></script>
  <script src="/assets/js/member.js"></script>
  <script >
    
    function firstaddress(the_type)
    {
       $.ajax({
                 url: $("base").attr('href')+"/member/changesession",
                 type: 'POST',                 
                 data:{
                  
                  mobile:$(".detail_1 input").eq(0).val(),
                  name:$(".detail_1 input").eq(1).val(),
                  city:$(".detail_1 input").eq(2).val(),
                  zip:$(".detail_1 input").eq(3).val(),
                  address:$(".detail_1 input").eq(4).val(),                  
                  _token:$("#the_token").val(),
                  type:the_type,
                 }
             })
            .done(function(data) 
            {
              // console.log(data);
              var type=data.split("--")[0];
              var indata=data.split("--")[1];
              $("#"+type).html(indata);
              $(".close_btn a").click();
              $('.add').click();
              $('.reduce').click();

            });
    }

    firstaddress("shop_receiver");
    firstaddress("ticket_receiver");
  </script>
@stop


@stop
