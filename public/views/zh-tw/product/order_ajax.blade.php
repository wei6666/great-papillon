@foreach($ProductData as $value)
                <?php
                if(!empty($value['search_jewelry']))
                {
                    $search_jewelry=$value['search_jewelry'];
                    $search_jewelry=str_replace("[", "", $search_jewelry);
                    $search_jewelry=str_replace("]", "", $search_jewelry);
                    $search_jewelry=str_replace('"', "", $search_jewelry);
                }
                 else
                {
                    $search_jewelry='-';
                }

                if(!empty($value['search_jewelry']))
                {
                    $search_matirial=$value['search_matirial'];
                    $search_matirial=str_replace("[", "", $search_matirial);
                    $search_matirial=str_replace("]", "", $search_matirial);
                    $search_matirial=str_replace('"', "", $search_matirial);
                }
                else
                {
                    $search_matirial='-';
                }



                ?>
                <li class='is_show jewelry_show matirial_show'>
                    
                    <a   jewelry='{{$search_jewelry }}' matirial='{{$search_matirial }}' price="@if(!empty($value['price_discount'])){{ $value['price_discount'] }}@else{{ $value['price_original'] }}@endif" href="{{ ItemMaker::url('product/'.$value['id']."/".$value['title']) }}">
                        <div class="_img">
                            <img src="{{ $value['out_img'] }}" alt="">
                        </div>
                        <h2>{!! $value['en_title'] !!}</h2>
                        <h3>{!! $value['title'] !!}{{$search_jewelry }}</h3>
                        <p>NT$ <span>@if(!empty($value['price_discount'])) {{ $value['price_discount'] }} @else  {{ $value['price_original'] }} @endif</span></p>
                    </a>
                </li> 
@endforeach        
