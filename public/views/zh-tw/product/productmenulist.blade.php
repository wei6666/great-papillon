

                <div class="_close">
                    <p>CLOSE</p>
                </div>
                <div class="_title">
                    
                    <h2>{!! $ProductSet->size_sub_title !!}</h2>
                    <p>{!! $ProductSet->size_title !!}</p>
                </div>
                <div class="changBtn" id="changBtn">
                    <ul>
                        <li>
                            {!! $ProductSet->size_menu_title_left !!}
                        </li>
                        <li>
                            {!! $ProductSet->size_menu_title_right !!}
                        </li>
                    </ul>
                </div>
                <section class="selectSize">
                    <div class="_text">
                        <p>{!! $SizeCategory->content !!}</p>
                    </div>
                    <div class="sizechoice">
                        <ul>
                            <!-- 後端請注意:有可能會有尺寸沒貨的情形 在那個尺寸所屬的li 加上class="noproduct"-->

                            @foreach($ProductSizetitle as $key=> $value)
                                <li @if($correct_size['a'.($key+1)] != 1 ) class="noproduct" @endif>{{ $value->title }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="_btn">
                        <div class="hiddenMessage">
                            <p>您選擇了尺寸　<span></span></p>
                            <p>您尚未選擇尺寸</p>
                            </div>
                        <ul>
                            <li>
                                <button class="reduce"></button>
                                <p id='pcount'>購買數量</p>
                                <button class="add"></button>
                            </li>
                            <li id='addgo' pro_count="1" pro_size="-" pro_type="{{ $ProductTitle }}" pro_no="{{ $Productnum }}" link="{{ ItemMaker::url('addsession/'.$Productnum."/".$ProductTitle."/") }}">
                                <button>加入購物</button>
                            </li>
                        </ul>
                    </div>
                </section>
                <section class="sizeDescription" table_data="{{  $SizeCategory->table_inf }}">
                    <div class="_text">
                        {!! $SizeCategory->content_inf !!}
                    </div>
                    <!-- 說明圖片 -->
                    <div class="_img">
                        <img src="{{  $SizeCategory->image_inf }}" alt="">
                    </div>
                    <!-- 尺寸表格 -->
                    <!-- 看到時候需要讓哪個資料隱藏 就使用  例:$(".diameter").css("display","none"); 讓它消失 -->
                    <div class="sizeForm">                       
                    </div>
                </section>

