
@extends('template')

    <!--RWD Setting-->
@section('css')
    <link rel="stylesheet" href="/assets/css/product.css">
@stop
@section('content')

@include($locale.'.include.header') 

    <main>
        <?php 
        if($thiscategory->title_position==0)
        {
            $thiscategory->title_position=1;
        }

        ?>
        <!-- 產品標題 -->
        
        <article class="product_title">

                <h2>{!! $thiscategory->en_title !!}</h2>
                <p>{!! $thiscategory->title !!}</p>
        </article>
        <!-- 上方主要廣告 -->
        <article class="content">
        <div class="topAd">
            <!-- <a href="../product/product_inf.html"> -->
            <!-- 後端請注意:此處要讓客戶是否要放youtube網址 -->
            <a href="@if(!empty($thiscategory->s1_vidio)){{ $thiscategory->s1_vidio }} @else {{ $thiscategory->s1_link }} @endif" target="_blank">
                <div class="topAd_img" >
                    <img src="{{ $thiscategory->s1_img }}" alt="" >
                    <!-- 後端請注意:如果有影片連結的話請在span加上class="_video" -->
                     <span class="icon-play @if( !empty($thiscategory->s1_vidio)) _video @endif"></span>
                </div>
                <!-- 文字位置 有四種 分別是 左上left_top , 右上right_top , 左下left_down ,右下right_down -->
                <!-- 文字的顏色 需要讓客戶可以自填 style="color:;"-->

                <div class="topAd_text {{ $position[$thiscategory->title_position] }}" style="color: {{ $thiscategory->color }};">
                    <h2>{!! $thiscategory->s1_en_title !!}</h2>
                    <p>{!! $thiscategory->s1_tw_title !!}</p>
                </div>
                          
            </a>
        </div>
        <!-- 產品 -->
        <div class="products grid">
            <!-- type1:大圖配小圖 -->
            
            @foreach($thiscategoryareadata as $value)

            @if($value->img_type==2)
            <div class="type1 grid-item">
                 
                <a href="{{ $value->link1 }}">
                    <div class="type1_bigImg">
                        <!-- 大圖 --><img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="{{ $value->img1 }}" alt="" >
                        <!-- 後端請注意:如果有影片連結的話請在span加上class="_video" -->
                        <span class="icon-play"></span>
                    </div>
                    <!-- 小圖位置 有兩種 分別是 左left , 右right -->
                    <div class="type1_smallImg left">
                        <!-- 小圖 --><img src="{{ $value->img2 }}" alt="">
                    </div>
                    <!-- 文字位置 有四種 分別是 左上left_top , 右上right_top , 左下left_down ,右下right_down -->

                    <div class="_text {{ $position[$value->title_position] }}" style="color: {{ $value->color }}">

                        <h2>{!! $value->en_title !!}</h2>
                        <p>{!! $value->title !!}</p>

                    </div>
                 
                </a>
            </div>
            @elseif($value->img_type ==4)

            <!-- type2:只有小圖 -->
            <div class="type2 grid-item">
                <a href="{{ $value->link1 }}">
                    <div class="type2_img">
                        <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="{{ $value->img1  }}" alt="">
                        <!-- 後端請注意:如果有影片連結的話請在span加上class="_video" -->
                        <span class="icon-play"></span>
                    </div>
                    <!-- 文字位置 有四種 分別是 左上left_top , 右上right_top , 左下left_down ,右下right_down -->
                    <div class="_text {{ $position[$value->title_position] }}" style="color: {{ $value->color }}">
                        <h2>{!! $value->en_title !!}</h2>
                        <p>{!! $value->title !!}</p>
                    </div>
                    
                </a>
            </div>
            <!-- type2:只有小圖 -->
                @if( $value->is_visible2 == 1)
                <div class="type2 grid-item">
                    <a href="{{ $value->link2 }}">
                        <div class="type2_img">
                            <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="{{ $value->img2 }}" alt="">
                            <!-- 後端請注意:如果有影片連結的話請在span加上class="_video" -->
                            <span class="icon-play"></span>
                        </div>
                        <!-- 文字位置 有四種 分別是 左上left_top , 右上right_top , 左下left_down ,右下right_down -->
                        <div class="_text {{ $position[$value->title_position_extra] }}" style="color: #000">
                            <h2>{!! $value->en_title_extra !!}</h2>
                            <p>{!! $value->title_extra !!}</p>
                        </div>
                        
                    </a>
                </div>
                @endif
            @elseif($value->img_type ==1 )
            <!-- type3:只有大圖 -->
            <div class="type3 grid-item">
                <a href="javascript:void(0);">
                    <div class="type3_img">
                        <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="{{ $value->img1 }}" alt="">
                        <!-- 後端請注意:如果有影片連結的話請在span加上class="_video" -->
                        <span class="icon-play"></span>
                    </div>
                    <!-- 文字位置 有四種 分別是 左上left_top , 右上right_top , 左下left_down ,右下right_down -->

                    <div class="_text {{ $position[$value->title_position] }}" style="color: {{ $value->color }}">
                        <h2>{!! $value->en_title !!}</h2>
                        <p>{!! $value->title !!}</p>
                    </div>
                    
                </a>
            </div>
            <!-- type1:大圖配小圖 -->
            @elseif($value->img_type== 3)
            <div class="type1 grid-item">
                <a href="{{ $value->link1 }}">
                    <div class="type1_bigImg">
                        <!-- 大圖 --><img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="{{ $value->img1 }}" alt=""><!-- 後端請注意:如果有影片連結的話請在span加上class="_video" -->
                         <span class="icon-play"></span>
                    </div>
                    <div class="type1_smallImg right">
                        <!-- 小圖 --><img src="{{ $value->img2 }}" alt="">
                    </div>
                    <!-- 文字位置 有四種 分別是 左上left_top , 右上right_top , 左下left_down ,右下right_down -->
                    <div class="_text right_top" style="color: {{ $value->color }}">
                        <h2>{!! $value->en_title !!}</h2>
                        <p>{!! $value->title !!}</p>
                    </div>
                </a>
            </div>
            @endif
            @endforeach
        </div>
        </article>
    </main>
    @include($locale.'.include.footer') 
@section('script')  
  
  <script >
   
    </script>
@stop


@stop