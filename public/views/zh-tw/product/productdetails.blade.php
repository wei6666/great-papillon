
@extends('template')

    <!--RWD Setting-->
@section('css')
       <link rel="stylesheet" href="/assets/css/product_inf.css">       
       <link rel="stylesheet" href="/assets/js/vendor/slick/slick-theme.css">
       
@stop
@section('content')
@include($locale.'.include.header') 
   
    <main>
        <article class="top_nav">
            <!-- 商品分類 -->
            <div class="series">
                <span><a href="{{ ItemMaker::url("productcategory/".$bigcate->id) }}">{{ $bigcate->title }}</a></span>
                <span><a href="{{ ItemMaker::url("productsubcategory/".$ProductData->category_id) }}">{{ $ProductSubCategory->title }}</a></span>
                <!-- 最後一個span不用加a連結 -->
                <span>{{ $ProductData->title }}</span>
            </div>
            <!-- 回到上一頁按鈕 -->
            <div class="_back" onclick="history.back()">
                <span></span>
                <p>BACK</p>
            </div>
        </article>
        <!-- 產品簡略資訊 -->
        <article class="product_inf">
            <div class="_slick">
                <ul>

                    @foreach($ProductPhoto as $value)
                    <li>
                        <div class="_slick_img">
                            <img src="{{$value->image}}" alt="">
                        </div>
                    </li>
                    @endforeach

                    
                </ul>
                <div class="_arrow">
                    <span class="icon-slick-arrow-left"></span>
                    <span class="icon-slick-arrow-right"></span>
                </div>
                <!-- slick頁數 -->
                <div class="slick_page">
                    <p><span>1</span><span> / </span><span></span></p>
                </div>
            </div>
            <div class="_text_bg">
                <div class="_text">
                    <!-- 批號 -->
                    <p class="_number" num='{{$ProductData->en_num.$ProductData->num}}'>@if(!empty( $ProductSeparate[0]->code ))
                        {{ $ProductData->en_num.$ProductData->num.$ProductSeparate[0]->code }}@else{{$ProductData->en_num.$ProductData->num}}@endif
                    </p>
                    <div class="_title">
                        <h3>{!!$ProductData->en_title !!}</h3>
                        <h2>{!!$ProductData->title !!}</h2>
                    </div>
                    <?php $thispageurl="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>
                    <div class="socialMedia">
                        <p>SHARE</p>
                        <!-- facebook 後端請加連結-->
                        <a target="_blank" href="http://www.facebook.com/share.php?u={{ $thispageurl }}">
                        <span class="icon-facebook"></span>
                        </a>
                        <!-- twitter 後端請加連結-->
                        <a target="_blank" href="http://twitter.com/home/?status={{ $thispageurl }}">
                        <span class="icon-twitter"></span>
                        </a>
                    </div>
                    <!-- 材質 -->
                    <div class="material">
                        <p>{!!$ProductData->matirial !!}</p>
                    </div>
                    <div class="seedetail">
                        <span></span>
                        <p>探索細節</p>
                    </div>

                    @if(!empty($ProductSeparate->toArray()))                    
                     <div class="gender">
                        <!-- 後端請注意:span裡的文字內容須由後端填入作為預設值 -->

                        <span class="selectMenu" data_id='{{ $ProductSeparate[0]->id }}' data_code="{{ $ProductSeparate[0]->code }}"><p>{{ $ProductSeparate[0]->title }}</p><b></b></span>
                        <ul> 
                            @foreach($ProductSeparate as $value)
                            <li data_id='{{ $value->id }}' data_code="{{ $value->code }}" price_original="{{ $value->price_original }}" price_discount="{{ $value->price_discount }}">
                                <p>{{ $value->title }}</p>
                            </li>
                            @endforeach                           
                        </ul>
                    </div>
                    @endif
                    
                    @if(!empty($ProductSeparate->toArray()))                    
                    <?php
                        $ProductData->price_original=$ProductSeparate[0]->price_original;
                        $ProductData->price_discount=$ProductSeparate[0]->price_discount;

                    ?>
                    @endif
                    <!-- 價格   後端請注意: 如果是特價 請加class: special -->
                    <ul class="price @if(!empty($ProductData->price_discount)) special @endif">
                        <li>
                            <h2>售價</h2>
                            <p>NT$ <span>{!!$ProductData->price_original !!}</span></p>
                        </li>
                        @if(!empty($ProductData->price_discount))
                        <li>
                            <h2>特惠價</h2>
                            <p>NT$ <span>{!!$ProductData->price_discount !!}</span></p>
                        </li>
                        @endif
                    </ul>
                    <!-- 按鈕 -->
                    <ul class="_btn">

                        @if(!empty($ProductSeparate->toArray()))

                        <li id='add' add-ajax="" link="{{ ItemMaker::url('changemenu/'.$ProductData->id.'/'.$ProductSeparate[0]->id) }}"  pro_id="{{ $ProductData->id }}">
                            <button>加入購物</button>
                        </li>
                        @else
                        <li id='add' add-ajax="{{ ItemMaker::url('addsession/'.$ProductData->id."/-/1/-") }}"  link="{{ ItemMaker::url('changemenu/'.$ProductData->id) }}"  pro_id="{{ $ProductData->id }}">
                            <button>加入購物</button>
                        </li>

                        @endif

                        @if($is_in_favorite !='in')
                        <li id='addfavorite' pro_id="{{ $ProductData->id }}">
                            <button style=" color: black;  background-color: #cccccc" >加入最愛</button>
                        </li>
                        @else
                        <li id='addfavorite' pro_id="{{ $ProductData->id }}">
                            <button style=" color: white;  background-color: #d80b18">從最愛中移除</button>
                        </li>
                        @endif
                        
                    </ul>
                </div>
            </div>
        </article>

        <!-- 產品詳細介紹 -->
        @if(!empty($ProductContent->toArray()))
        <article class="product_intro">
            <!-- 樣式一 -->
            @foreach($ProductContent as $value)
            @if($value->type==1)
            <section class="type1">
                <div class="type1_box">
                    <div class="_text">
                        
                            <h2>{!! $value->title !!} </h2>
                            <p>{!! $value->content !!}</p>
                       
                    </div>
                    <div class="_table">
                        <?php
                        $table_title=explode("\r\n",$value->data_title_content);
                        $table_value=explode("\r\n",$value->data_value_content);
                        ?>
                            <h2>{!! $value->title1 !!}</h2>
                            <ul>
                                @foreach($table_title as $key => $value1)
                                <li>
                                    <h3>{!!$value1!!}</h3>
                                    <p>{!!$table_value[$key]!!}</p>
                                </li>
                                @endforeach                                
                            </ul>
                        </div>
                    <div class="_img">
                        <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="{{$value->image1}}" alt="">
                    </div>
                </div>
            </section>
            @else
            <!-- 樣式二 -->
            <section class="type2">
                <div class="type2_box">
                    <div class="_text">
                        <h2>{!! $value->title !!}</h2>
                        <p>{!! $value->content !!}</p>
                    </div>
                    <div class="imgs">
                        <div class="_img1">
                            <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="{{$value->image1 }}" alt="">
                            <i>{!! $value->img_con1 !!}</i>
                        </div>
                        <div class="_img2">
                            <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="{{$value->image2 }}" alt="">
                            <i>{!! $value->img_con2 !!}</i>
                        </div>
                    </div>
                </div>
            </section>
            @endif
            @endforeach
        </article>
        @endif
        <!-- 推薦商品 -->
        @if(!empty($ProductMayLike))
        <article class="mayLike">
            <div class="_title">
                <h2>YOU MAY LIKE IT</h2>
                <p>您或許會喜歡</p>
            </div>
            <div class="_slick">
                <ul>
                    @foreach($ProductMayLike as $value )
                    <li>

                        <!-- 連結到其他產品 -->
                        <a href="{{ItemMaker::url('product/'.$value['id'].'/'.$value['title'])}}">
                            <div class="_slick_img">
                                <img src="{{$value['out_img']}}">
                            </div>
                            <h2>{{$value['en_title']}}</h2>
                            <h3>{{$value['title']}}</h3>
                        </a>
                    </li>
                    @endforeach
                </ul>
                <div class="_arrow">
                    <span class="icon-slick-arrow-left"></span>
                    <span class="icon-slick-arrow-right"></span>
                </div>
            </div>
        </article>
        @endif
        <!-- 幸福好評 -->
        @if(!empty($ProductHappiness) )
        <article class="praise">
            <div class="_title">
                <h2>HAPPY PRAISE</h2>
                <p>幸福好評</p>
            </div>
            <div class="_slick">
                <ul>
                    @foreach($ProductHappiness as $value)
                    <li>
                        <!-- 連結到幸福好評內頁 -->
                        <a href="{{ ItemMaker::url('happiness/'.$value->id) }}">
                            <div class="_slick_img">
                                <img src="{{$value['out_img']}}">
                                <span></span>
                            </div>
                            <p>{{ $value->title }}</p>
                        </a>
                    </li>
                    @endforeach
               
                </ul>
                <div class="_arrow">
                    <span class="icon-slick-arrow-left"></span>
                    <span class="icon-slick-arrow-right"></span>
                </div>
            </div>
        </article>
        @endif
        <article class="sizeLibox">
            <div class="_content">
               
            </div>
        </article>

        <article class="_libox">
            <div class="workbox_bg">
                <div class="workbox">
                    <div class="_title">
                        <h2>INFORMATION</h2>
                        <p>商品訊息</p>
                    </div>
                    <div class="_text">
                        <p>親愛的顧客您好，目前商品並無現貨，如果您仍有商品需求，並且願意等候商品訂製時間，歡迎加入法蝶珠寶Line生活圈與我們聯繫，請掃描或點擊下方 QR code 加入我們，法蝶珠寶感謝您。</p>
                    </div>
                    <div class="QRcode">
                        <!-- line連結 -->
                        <!-- 請後端填上連結 -->
                        <a href="javascript:void(0);">
                    <img src="../../upload/f2e/product/product_inf/QRcode.jpg" alt="">
                    </a>
                    </div>
                    <button class="close">
                        已經了解商品訊息
                    </button>
                </div>
            </div>
        </article>
        
    </main>
     @include($locale.'.include.footer') 
@section('script')
  <script src="/assets/js/vendor/picturefill.min.js"></script>
  <script >
   
    </script>
@stop


@stop