
@extends('template')

    <!--RWD Setting-->
@section('css')
        <link rel="stylesheet" href="/assets/js/vendor/slick/slick.css">
    <!-- 非共用CSS end-->
    <link rel="stylesheet" href="/assets/css/product_series.css">
@stop
@section('content')
@include($locale.'.include.header') 
    <main>
        <!-- 標題 -->
        <article class="_title">
            <h2><span>{!! $ProductSubCategory->en_title !!}</span></h2>
            <p><span>{!! $ProductSubCategory->title !!}</span></p>
        </article>
        <article class="detail_nav">
            <!-- 商品分類 -->
            <div class="series">

                <span><a href="{{ ItemMaker::url("productcategory/".$bigcate->id) }}">{{ $bigcate->title }}</a></span>
                <span>{!! $ProductSubCategory->title !!}</span>
            </div>
            <!-- 篩選表格 -->
            <div class="selectCondition">
                <p>快速篩選</p>
                <div class="price menulist">
                    <span class="selectMenu"><p>價格</p></span>
                    <ul>
                        <!-- 此處需可新增內容 -->
                        <li class='pricel' v='asc'>
                            <!-- 內容建議字數4個字以內 -->
                            <p>低至高</p>
                        </li>
                        <li class='pricel' v='desc'>
                            <p>高至低</p>
                        </li>
                    </ul>
                </div>
                <div class="gem menulist">
                    <span class="selectMenu"><p>寶石</p></span>
                    <ul class='jewelryul'>
                        <!-- 此處需可新增內容 -->
                        <li j_id='*' class="jewelry">
                            <!-- 內容建議字數4個字以內 -->
                            <p>全部</p>
                        </li>

                        @foreach($Jewelry as $value)
                        <li j_id="{{ $value->id }}" class="jewelry">
                            <p>{{ $value->title }}</p>
                        </li>
                        @endforeach
                       
                    </ul>
                </div>
                <div class="material menulist">
                    <span class="selectMenu"><p>材質</p></span>
                    <ul class='matirialul'>
                        <!-- 此處需可新增內容 -->
                        <!-- 內容建議字數4個字以內 -->

                        <li ma_id='*'  class='matirial'>
                            <p>全部</p>
                        </li>
                        @foreach($Matirial as $value)
                        <li ma_id='{{ $value->id }}' class='matirial'>
                            <p>{{ $value->title }}</p>
                        </li>
                        @endforeach
                      
                    </ul>
                </div>
            </div>
        </article>
        <article class="prducts">
            <ul class='prductsul'>
                @foreach($ProductData as $value)
                <?php
                if(!empty($value['search_jewelry']))
                {
                    $search_jewelry=$value['search_jewelry'];
                    $search_jewelry=str_replace("[", "", $search_jewelry);
                    $search_jewelry=str_replace("]", "", $search_jewelry);
                    $search_jewelry=str_replace('"', "", $search_jewelry);
                }
                 else
                {
                    $search_jewelry='-';
                }

                if(!empty($value['search_jewelry']))
                {
                    $search_matirial=$value['search_matirial'];
                    $search_matirial=str_replace("[", "", $search_matirial);
                    $search_matirial=str_replace("]", "", $search_matirial);
                    $search_matirial=str_replace('"', "", $search_matirial);
                }
                else
                {
                    $search_matirial='-';
                }



                ?>
                <li class='is_show jewelry_show matirial_show'>
                    
                    <a   jewelry='{{$search_jewelry }}' matirial='{{$search_matirial }}' price="@if(!empty($value['price_discount'])){{ $value['price_discount'] }}@else{{ $value['price_original'] }}@endif" href="{{ ItemMaker::url('product/'.$value['id']."/".$value['title']) }}">
                        <div class="_img">
                            <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="{{ $value['out_img'] }}" alt="">
                        </div>
                        <h2>{!! $value['en_title'] !!}</h2>
                        <h3>{!! $value['title'] !!}{{$search_jewelry }}</h3>
                        <p>NT$ <span>@if(!empty($value['price_discount'])) {{ $value['price_discount'] }} @else  {{ $value['price_original'] }} @endif</span></p>
                    </a>
                </li> 
                @endforeach               
            </ul>

            
        </article>
        <hidden id='selectcondition' order='*' matirial='*' jewelry='*'>
    </main>   
     @include($locale.'.include.footer') 
@section('script')  
  
  <script >
   
    </script>
@stop


@stop