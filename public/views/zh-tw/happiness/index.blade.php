
@extends('template')

    <!--RWD Setting-->
@section('css')
        <link rel="stylesheet" href="/assets/js/vendor/slick/slick.css">
    <!-- 非共用CSS end-->
    <link rel="stylesheet" href="/assets/css/witness.css">
@stop
@section('content')
@include($locale.'.include.header')
    <main>
        <article class="_title">
            <h2>{!! $HappinessSet->title !!}</h2>
            <h3>{!! $HappinessSet->en_title !!}</h3>
            <div class="countup">
                <p>{!! $HappinessSet->content1 !!}<span class="counter" id="counter">@if(!empty($HappinessSet->contnet_num)) {{ $HappinessSet->contnet_num }}
                @else {{ count($Happiness) }} @endif </span>{!! $HappinessSet->content2 !!}</p>
            </div>
            <div class="select">
                <div class="All">
                    <p>全部列表</p>
                </div>
                <div class="menulist">
                    <span class="selectMenu"><p>選擇分類</p></span>
                    <ul>
                        @foreach($HappinessCategory as $value)
                        <li ca-id="{{ $value->id }}"><p>{{ $value->title }}</p></li>
                        @endforeach
                    </ul>
                </div>
            </div>            
        </article>
        <article class="couples">
            <ul>
                @foreach($Happiness as $value)
                <li cate-id='{{ $value->category_id }}'>
                    <a href="{{ ItemMaker::url('witness/'.$value['id']."/".$value['title'])  }}">
                        <div class="_img">
                            <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="{{ $value['out_image'] }}" alt="">
                            <span></span>
                        </div>
                        <!-- 商品名稱 -->
                        <h2>{{ $value['probig'] }}</h2>
                        <!-- 新人名子 -->
                        <p>{{ $value['title'] }}</p>
                    </a>
                </li>
                @endforeach
                
            </ul>
        </article>
    </main>
     @include($locale.'.include.footer') 
@section('script')  
  
 
@stop


@stop