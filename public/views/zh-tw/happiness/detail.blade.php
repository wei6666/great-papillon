
@extends('template')

    <!--RWD Setting-->
@section('css')
    <link rel="stylesheet" href="/assets/js/vendor/slick/slick.css">
    <!-- 非共用CSS end-->
    <link rel="stylesheet" href="/assets/css/witness_detail.css">
@stop
@section('content')
    <main>
        <article class="_banner">
            <img class="b-lazy" src="{{ $Happiness->image }}" alt="">
        </article>
        <article class="_inf">
            <div class="_close" onclick="history.back()">
                <p>CLOSE</p>
            </div>
            <div class="_title">
                <h2>{{ $HappinessSet->en_title }}</h2>
                <!-- 新人名子 -->
                <h3>{{ $Happiness->title }}</h3>
                <!-- 產品分類 -->
                <p>{{ $Happiness['probig'] }}</p>
                <?php $thispageurl="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>
                <div class="socialMedia">
                    <p>SHARE</p>
                    <!-- facebook連結 -->
                        <a target="_blank" href="http://www.facebook.com/share.php?u={{ $thispageurl }}">
                        <span class="icon-facebook"></span>
                            </a>
                            <!-- twitter 後端請加連結-->
                        <a target="_blank" href="http://twitter.com/home/?status={{ $thispageurl }}">
                        <span class="icon-twitter"></span>
                        </a>
               </a>
                </div>
            </div>
            <div class="_text">
                <p>{!! $Happiness->content !!}
                </p>
            </div>
            <div class="location">
                <ul>
                    @if(!empty( $Happiness->angel))
                    <li>
                        <!-- 服務人員名稱 -->
                        <p>{!! $HappinessSet->in_angel_title !!}<span>{!! $Happiness->angel !!}</span></p>
                    </li>
                    @endif
                    <!-- 服務據點名稱 -->
                    @if(!empty($Location))
                    <li>
                        <!-- 連結到服務據點內頁 -->
                        <a href="{{ ItemMaker::url('location?lo='.$Location->id) }}">
                            <p>
                                {!! $HappinessSet->in_location_title !!}<span>{{ $Location->title }}</span>
                            </p>
                        </a>
                    </li>
                    @endif
                </ul>
            </div>
            @if(!empty($Happiness->product_id))
            <div class="product">
                <div class="_img">
                    <a href="{{ ItemMaker::url("product/".$Product->id."/".$Product->title) }}">
                        <img src="{{ $ProductPhoto->image }}" alt="">
                    </a>
                </div>
                <div class="_intro">
                    <h2>{{ $Product->en_title }}</h2>
                    <h3>{{ $Product->title }}</h3>
                    <div class="more">
                        <a href="{{ ItemMaker::url("product/".$Product->id."/".$Product->title) }}">
                        <span></span>
                        <p>Read More</p>
                        </a>
                    </div>
                </div>
            </div>
            @endif
        </article>

        <article class="_reservation _hideUp">
            <ul>
                <li class="_goTop">
                    <p>GO TOP</p>
                </li>
                <!-- 連結到預約鑑賞內頁 -->
                <li>
                    <a href="../reservation/index.html">
                        <p>預約鑑賞</p>
                    </a>
                </li>
                <!-- 連結到line  -->
                <!-- 請後端填入連結 -->
                <li>
                    <a href="javascript:void(0);"></a>
                </li>
            </ul>
        </article>
    </main>
@section('script')  
  
 
    
   
@stop


@stop