<!--預約紅標籤-->
<article class="reservation">
  <ul>
    <li class="goTop">
      <p>GO TOP</p>
    </li>
    <!-- 連結到預約鑑賞內頁 -->
    <li>
      <a href="{{ ItemMaker::url('reservation') }}">
          <p>預約鑑賞</p>
      </a>
    </li>
    <!-- 連結到line  -->
    <!-- 請後端填入連結 -->
    <li>
      @if($Setting->line_link)
      <a href="{{ $Setting->line_link }}"></a>
      @else
      <a href="javascript:;"></a>
      @endif
    </li>

  </ul>
</article>