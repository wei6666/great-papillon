<footer>
<article class="footer">

  <!--頁角所有資訊-->
  <section class="footer_information">

    <div>
      <!--重點訊息-->
      <ul class="important_link">
        <li>
          <div class="board">
            <h2 class="title">store location</h2>
            <p class="vice_title">尋找法蝶</p>
          </div>
          <div>
            <a href="{{ ItemMaker::url('location') }}">
              <p>尋找據點</p>
            </a>
          </div>
        </li>
        <li>
          <div class="board">
            <h2 class="title">join members</h2>
            <p class="vice_title">加入會員</p>
          </div>
          <div>
            <a href="{{ ItemMaker::url('member/guide') }}">
              <p>立即申請</p>
            </a>
          </div>
        </li>
        <li>
          <div class="board">
            <h2 class="title">gift shop</h2>
            <p class="vice_title">購物車</p>
          </div>
          <div>
            <a href="{{ ItemMaker::url('shopping_qa') }}">
              <p>詳細流程</p>
            </a>
          </div>
        </li>
      </ul>
    </div>

    <span class="line"></span>

    <div>
      <!--產品連結-->
      <ul class="product_link">
        @foreach($header_data as $value)
        <li>
          <a href="{{ItemMaker::url('productcategory/'.$value->id)}}">
            <p>{{$value->title}}</p>
          </a>
        </li>
        @endforeach
      </ul>

      <!--單元連結-->
      <ul class="unit_link">
        <li>
          <a href="{{ ItemMaker::url('about') }}">
            <p>品牌故事</p>
          </a>
        </li>
        <li>
          <a href="{{ ItemMaker::url('news') }}">
            <p>最新訊息</p>
          </a>
        </li>
        <li>
          <a href="{{ ItemMaker::url('witness') }}">
            <p>幸福好評</p>
          </a>
        </li>
        <li>
          <a href="{{ ItemMaker::url('contact') }}">
            <p>聯絡我們</p>
          </a>
        </li>
        <li>
          <a href="javascript:void(0)">
            <p>人才招募</p>
          </a>
        </li>
      </ul>

      <!--社群按鈕-->
      <ul class="community_link">
        @if(!empty( $Setting->company_fb ))
          <li>
            <a target="_blank" href="{{ $Setting->company_fb }}">
              <span class="icon-facebook"></span>
            </a>
          </li>
        @endif
        
        @if(!empty( $Setting->company_instagram ))
          <li>
            <a target="_blank" href="{{ $Setting->company_instagram }}">
              <span class="icon-instagram"></span>
            </a>
          </li>
        @endif
        
        @if(!empty( $Setting->company_twitter ))
          <li>
            <a target="_blank" href="{{ $Setting->company_twitter }}">
                <span class="icon-twitter"></span>
            </a>
          </li>
        @endif
        
        @if(!empty( $Setting->company_youtube ))
          <li>
            <a target="_blank" href="{{ $Setting->company_youtube }}">
                <span class="icon-youtube"></span>
            </a>
          </li>
        @endif
        
      </ul>
    </div>

  </section>

  <!--隱私權 + who disign-->
  <section class="design_block">

    <a class="privacy" href="javascript:void(0)">
      <p>隱私權政策</p>
    </a>

    <p class="design">
      <span>© papillon jewelry ALL RIGHTS RESERVED.. DESIGNED BY </span>
      <a href="http://www.wddgroup.com/">WDD</a>
    </p>

  </section>
  


</article>



@include($locale.'.include.float')
</footer>