<header>
    <article class="header">
    <!--header NAV-->
    <section class="header_box">
        <!--選單，搜尋-->
        <div class="menu_search">
            <a class="menu_switch" href="javascript:void(0)">
                <p>menu</p>
                <div class="hamburger">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </div>
            </a>
            <span class="point"></span>
            <a class="search_box" href="javascript:void(0)">
                <div>
                    <span class="icon-search"></span>
                </div>
            </a>
        </div>
        <!--logo-->
        <div class="logo">
            <a href="{{ ItemMaker::url('home') }}">
                <h2>papillon</h2>
                <img src="/assets/img/Logo1.png" alt="">
            </a>
        </div>
        <!--會員，禮品-->
        <div class="member_gift">
            <!--連去會員頁面，還沒登入前顯示 "會員登入" 登入後顯示 "您好，XXX"-->
            <a class="member_dropdown" href="{{ ItemMaker::url('member/guide') }}">
                @if(Session::get('Member') != NULL)
                <p>您好，{{ Session::get('name') }}</p>
                @else
                <p>會員登入</p>
                @endif
                
                
                <span class="icon-person"></span>
            </a>
            @if(Session::get('Member') != NULL)
            <div class="gift" >
                <a class="gift_switch" href="javascript:void(0)">
                  <span class="icon-gift"></span>
                  <p class="gift_box" id="gift">
                   
                    <span class="num">{{ $product_in_car }}</span>
                    <span class="text">件禮品</span>

                  </p>
                </a>
            </div>
            @else
            <a href="{{ ItemMaker::url('member/guide') }}">
                <span class="icon-gift"></span>
            </a>

            @endif
        </div>
    </section>
    <!--產品NAV-->
    <section class="product_box">
        <!--產品大分類-->
        <ul class="big_category">
            @foreach($header_data as $value)
            <li>
                <a href="{{ $locale."/productcategory/".$value->id."?g=1" }}" class="big_category_list">
                    <p>{!! $value->title !!}</p>
                </a>
                <!--產品大分類下拉 img = 280*145-->
                <section class="product_nav">

                    <section class="frame">
                        <div class="area">
                            <p class="title">CATEGORY</p>
                            <ul>
                                @if(!empty($value['ProductSubCategory']))
                                    @foreach($value['ProductSubCategory'] as $subcategory)
                                    <li>
                                        <a href="{{$locale.'/productsubcategory/'.$subcategory->id }} ">
                                            <p>{!! $subcategory->title !!}</p>
                                        </a>
                                    </li>
                                    @endforeach       
                                @endif                        
                            </ul>
                        </div>
                        <div class="area">
                            <p class="title">STYLE</p>
                            <ul>
                                @foreach($value['Marketing'] as $subcategory)
                                <li>
                                    <a href="{{ ItemMaker::url("productsubcategory/".$subcategory->id."?Marketing=".$subcategory->id) }}">
                                        <p>{{ $subcategory->title }}</p>
                                    </a>
                                </li>
                                @endforeach
                               
                            </ul>
                        </div>

                        @if(!empty($value->product_header_img))                        
                        <!--我記得是可以讓客戶設定連結，可以再跟設計師確認-->
                        <div class="img">
                            <a href="{{ ItemMaker::url('product/'.$value->product_header_link) }}">
                                <picture>
                                
                                    <img src="{{ $value->product_header_img }}" alt="">

                                </picture>
                            </a>
                        </div>

                        @endif
                    </section>
                </section>
            </li>
            @endforeach
        </ul>
    </section>
    <!--搜尋-->
    <section class="search_frame">
        <div class="box">
            <!--關閉按鈕-->
            <div class="_close">
                <p>CLOSE</p>
            </div>
            <!--標題-->
            <section class="_top">
                <div class="_title">
                    <h2>Find more</h2>
                    <p>尋找更多</p>
                </div>
            </section>
            <!--搜尋bar-->
            <div class="search_input">
                <span class="icon-search"></span>
                <input type="text" name="" value="" placeholder="搜尋精品">
            </div>
            <!--搜尋list，連結連去產品各小單元-->
            <div class="search_list">
                <p class="title">熱門搜尋：</p>
                <ul>
                    @foreach($Searching as $value)
                    <li>
                        <a href="{{ ItemMaker::url("productsubcategory/1?Searching=".$value->id) }}">
                            <p>鑽石戒指</p>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </section>
</article>
<article class="_menu">
    <div class="menu_content">
        <div class="main_list">
            <div class="menu_close">
                <p>CLOSE</p>
            </div>
            <div class="menu_logo">
                <a href="{{ ItemMaker::url("/") }}">
                  <img src="/assets/img/Logo1.png" alt="">
                </a>
            </div>
            <!--會員，禮品-->
            <div class="member">

                @if(Session::get('Member') != NULL)
                <div class="member_box">
                    <!--連去會員頁面，還沒登入前顯示 "會員登入" 登入後顯示 "您好，XXX"-->
                    
                    <div class="member_name">
                            <a class="member_dropdown" href="{{ ItemMaker::url('member/guide') }}">
                                <p>您好，{{ Session::get('name') }}</p> 
                            </a>                        
                    </div>                    
                    <div class="gift" id='header_car'>
                        <a class="gift_switch" href="javascript:void(0)">
                            <p class="gift_box">
                                <span class="num">{{ $product_in_car }}</span>件禮品
                            </p>
                        </a>
                    </div>
                </div>
                <ul class="member_btn">
                    
                    <li>
                        <!-- 此頁面尚未完成 -->
                        <a href="{{ ItemMaker::url('member/guide') }}">會員專區</a>
                    </li>
                    <li>
                        <a href="{{ ItemMaker::url('member/logout') }}">
                            <button class="logOut" type="button">
                                登出
                            </button>
                        </a>
                    </li>                   
                </ul> 
                @else
                <ul class="member_btn">
                    
                    <li>
                        <!-- 此頁面尚未完成 -->
                        <a href="{{ ItemMaker::url('member/guide') }}">會員登入</a>
                    </li>
                              
                </ul> 

                @endif
            </div>
            <div class="_product header_product">
                <h2>HAPPY PRODUCT</h2>
                <ul>
                    @foreach($header_data as $value)
                    <li class_data='subcate{{$value->id}}'>
                        <p>{{ $value->title }}</p>
                    </li>
                    @endforeach
                    
                </ul>
            </div>
            <div class="_explore">
                <h2>EXPLORE MORE</h2>
                <ul>
                    <li>
                        <a href="{{ ItemMaker::url("about") }}">
                            <p>品牌故事</p>
                        </a>
                    </li>
                    <li>
                        <a href="{{ ItemMaker::url("news") }}">
                            <p>最新訊息</p>
                        </a>
                    </li>
                    <li>
                        <a href="{{ ItemMaker::url("witness") }}">
                            <p>幸福好評</p>
                        </a>
                    </li>
                    <li>
                        <a href="{{ ItemMaker::url("location") }}">
                            <p>幸福據點</p>
                        </a>
                    </li>
                    <li>
                        <!-- 此頁面尚未完成 -->
                        <a href="{{ ItemMaker::url("contact") }}">
                            <p>連絡我們</p>
                        </a>
                    </li>
                    <li>
                        <!-- 連結到104人力銀行 -->
                        <a href="javascript:void(0)">
                            <p>人才招募</p>
                        </a>
                    </li>
                    <li>
                        <!-- 此頁面尚未完成 -->
                        <a href="{{ ItemMaker::url("shopping_qa") }}">
                            <p>SHOPPING QA</p>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="_socialMedia">
                <ul>

                    @if($Setting->company_fb)
                        <li>
                            <a target="_blank" href="{{ $Setting->company_fb }}">
                            <span class="icon-facebook"></span>
                            </a>                           
                        </li> 
                    @endif

                    @if($Setting->company_instagram)
                        <li>
                            <!-- instagram -->
                            <a target="_blank" href="{{ $Setting->company_instagram }}">
                                <span class="icon-instagram"></span>
                            </a>
                        </li>
                    @endif

                    @if($Setting->company_twitter)
                        <li>
                            <!-- twitter -->
                            <a target="_blank" href="{{ $Setting->company_twitter }}">
                                <span class="icon-twitter"></span>
                            </a>
                        </li>
                    @endif

                    @if($Setting->company_youtube)
                        <li>
                            <!-- youtube -->
                            <a target="_blank" href="{{ $Setting->company_youtube }}">
                                <span class="icon-youtube"></span>
                            </a>
                        </li>
                    @endif
                </ul>
                <p class="seePrivacy privacy" >隱私權政策</p>
            </div>
        </div>
        <div class="child_list">
            @foreach($header_data as $value)
            <div class="child_list_box subcate{{$value->id}}">
                <div class="_back_main_list">
                    <span class="icon-slick-arrow-left"></span>
                    <p>BACK</p>
                </div>
                <div class="list_title">
                    <h2>{{ $value->title }}</h2>
                    <!--後端請注意: 此處的連結為暫時 請在此填入正確的連結 -->
                    <a href="{{ ItemMaker::url('productcategory/'.$value->id) }}">
                        <p>View More</p>
                    </a>
                </div>
                <!-- 產品分類 -->
                <div class="category">
                    <h2>CATEGORY</h2>
                    <ul>
                        <!-- 此處需可新增項目 -->
                        @if(!empty($value['ProductSubCategory']))
                            @foreach($value['ProductSubCategory'] as $value1)
                            <li>
                                <!--後端請注意: 此處的連結為暫時 請在此填入正確的連結 -->
                                <a href="{{ ItemMaker::url('productsubcategory/'.$value1->id) }}">
                                    <p>{{ $value1->title }}</p>
                                </a>
                            </li>
                            @endforeach 
                        @endif                  
                    </ul>
                </div>
                <div class="style">
                    <h2>STYLE</h2>
                    <ul>
                        <!-- 此處需可新增項目 -->
                        @foreach($value['Marketing'] as $value1)
                        <li>
                            <!--後端請注意: 此處的連結為暫時 請在此填入正確的連結 -->
                            <a href="{{ ItemMaker::url('productsubcategory/'.$value1->id."?Marketing=".$value1->id) }}">
                                <p>{{ $value1->title }}</p>
                            </a>
                        </li>
                        @endforeach                       
                    </ul>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <!-- 此處slick須由客戶決定是否存在 -->
    <div class="_slick">
        <ul>
            @foreach($Menu_List as $value)
            <li>
                <a href="{{ $value->link }}">
                    <div class="_slick_img">
                        <img src="@if(!empty( $value->image )){{ $value->image }} @else /upload/f2e/menu/slick_img1_960_1080.jpg @endif" alt="">
                    </div>
                    <!-- 文字位置 有四種 分別是 左上top_left , 右上top_right , 左下down_left ,右下down_right -->
                    <?php $positionarr=['','top_left','down_left','top_right','top_left'];?>
                    <div class="slogn  @if(!empty($positionarr[$value->place])){{ $positionarr[$value->place] }} @else top_left @endif">
                        <!-- 文字的顏色須由客戶決定 -->
                        <h2 style="@if(empty($value->color)) color: #d80b18 @else $value->color @endif ">{!! $value->en_content!!}</h2>
                        <span style="background-color:#585553 "></span>
                        <p style="@if(empty($value->color)) color: #d80b18 @else $value->color @endif ">{!! $value->content!!}</p>
                    </div>
                </a>
            </li>
            @endforeach
           
        </ul>
        <div class="_pageCount">
            <span>1</span>
            <span>/</span>
            <span></span>
        </div>
    </div>
</article>
<article class="cart">

    <section class="box">

        <!--關閉按鈕-->
        <div class="_close close_btn">
            <a href="javascript:void(0)">
                <p>CLOSE</p>
            </a>
        </div>

        <!--標題-->
        <section class="_top">
            
          <div class="_title">
            <h2>GIFT SHOP</h2>
            <p>購物車</p>
          </div>
        
        </section>
        <div class='carchange'>
            <!--已選擇數量-->
            <div class="chosen">
                <p>您目前選購了<span>5</span>件精品</p>
            </div>

            <!--框-->
            <section class="cart_frame">
                <ul class="frame">
                    <li class="list">
                        <!--item-->
                        <section class="item">

                            <!--點擊連到被點擊的產品項頁面-->
                            <a class="item_box" href="javascript:void(0)">
                                <!--100*100-->
                                <div class="img">
                                    <picture>
                                        <img class="b-lazy b-loaded" src="../../upload/f2e/member/detail-1.jpg" alt="">
                                    </picture>
                                </div>
            
                                <div class="info">
                                    <p class="name">THE HEART OF THE ROSE</p>
                                    <p class="vice">玫瑰心情</p>
                                    <p class="price">NT$ 55,000</p>
                                    <p class="size">
                                        <span>尺寸 : 7 號</span>
                                        <span>數量 : 1</span>
                                    </p>
                                </div>
                            </a>

                        </section>
                        <!--刪除按鈕-->
                        <div class="delete delete_btn"></div>
                    </li>
                    <li class="list">
                        <!--item-->
                        <section class="item">

                            <!--點擊連到被點擊的產品項頁面-->
                            <a class="item_box" href="javascript:void(0)">
                                <!--100*100-->
                                <div class="img">
                                    <picture>
                                        <img class="b-lazy b-loaded" src="../../upload/f2e/member/detail-2.jpg" alt="">
                                    </picture>
                                </div>
            
                                <div class="info">
                                    <p class="name">HEART COMMUNION</p>
                                    <p class="vice">心心相映</p>
                                    <p class="price">NT$ 12,000</p>
                                    <p class="size">
                                        <span>女仕</span>
                                        <span>尺寸 : 6 號</span>
                                        <span>數量 : 1</span>
                                    </p>
                                </div>
                            </a>

                        </section>
                        <!--刪除按鈕-->
                        <div class="delete delete_btn"></div>
                    </li>
                    <li class="list">
                        <!--item-->
                        <section class="item">

                            <!--點擊連到被點擊的產品項頁面-->
                            <a class="item_box" href="javascript:void(0)">
                                <!--100*100-->
                                <div class="img">
                                    <picture>
                                        <img class="b-lazy b-loaded" src="../../upload/f2e/member/detail-2.jpg" alt="">
                                    </picture>
                                </div>
            
                                <div class="info">
                                    <p class="name">HEART COMMUNION</p>
                                    <p class="vice">心心相映</p>
                                    <p class="price">NT$ 15,000</p>
                                    <p class="size">
                                        <span>紳士</span>
                                        <span>尺寸 : 8 號</span>
                                        <span>數量 : 1</span>
                                    </p>
                                </div>
                            </a>

                        </section>
                        <!--刪除按鈕-->
                        <div class="delete delete_btn"></div>
                    </li>
                    <li class="list">
                        <!--item-->
                        <section class="item">

                            <!--點擊連到被點擊的產品項頁面-->
                            <a class="item_box" href="javascript:void(0)">
                                <!--100*100-->
                                <div class="img">
                                    <picture>
                                        <img class="b-lazy b-loaded" src="../../upload/f2e/member/detail-3.jpg" alt="">
                                    </picture>
                                </div>
            
                                <div class="info">
                                    <p class="name">YOUR FAIRY TALE</p>
                                    <p class="vice">妳的童話</p>
                                    <p class="price">NT$ 15,000</p>
                                    <p class="size">
                                        <span>數量 : 1</span>
                                    </p>
                                </div>
                            </a>

                        </section>
                        <!--刪除按鈕-->
                        <div class="delete delete_btn"></div>
                    </li>
                    <li class="list">
                        <!--item-->
                        <section class="item">

                            <!--點擊連到被點擊的產品項頁面-->
                            <a class="item_box" href="javascript:void(0)">
                                <!--100*100-->
                                <div class="img">
                                    <picture>
                                        <img class="b-lazy b-loaded" src="../../upload/f2e/member/detail-2.jpg" alt="">
                                    </picture>
                                </div>
            
                                <div class="info">
                                    <p class="name">YOUR FAIRY TALE</p>
                                    <p class="vice">天使之羽</p>
                                    <p class="price">NT$ 13,000</p>
                                    <p class="size">
                                        <span>尺寸 : 1號</span>
                                        <span>數量 : 1</span>
                                    </p>
                                </div>
                            </a>

                        </section>
                        <!--刪除按鈕-->
                        <div class="delete delete_btn"></div>
                    </li>
                </ul>
            </section>

            <!--消費金額總計-->
            <section class="total">
                <div class="bin">
                    <p class="title">TOTAL</p>
                    <p class="number">NT$ 107,000</p>
                </div>
                <!--繼續 付款 按鈕-->
                <div class="form_btn">
                    <a href="javascript:void(0)" class="gray two close_btn">
                    <div>
                        <p>繼續選購</p>
                    </div>
                    </a>
                    <a href="../shopping_cart/index.html" class="black two">
                    <div>
                        <p>前往結帳</p>
                    </div>
                    </a>
                </div>
            </section>
        </div>

    </section>

</article>
</header>