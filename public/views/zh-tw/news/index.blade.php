
@extends('template')

    <!--RWD Setting-->
@section('css')
       <link rel="stylesheet" href="/assets/css/news.css">
@stop
@section('content')
@include($locale.'.include.header') 
<main>
        <article class="_top">
            <div class="_title">
                <h2>{!! $NewsSet->en_title !!}</h2>
                <p>{!! $NewsSet->title !!}</p>
            </div>
            <div class="select">
                <div class="All">
                    <p>全部訊息</p>
                </div>
                <div class="menulist">
                    <span class="selectMenu"><p>選擇分類</p></span>
                    <ul>
                        <!-- 後端注意:此處需可讓客戶新增內容 -->
                        @foreach($NewsCategory as $value)
                        <li ca-id="{{ $value->id }}">
                            <p>{{$value->title}}</p>
                        </li>
                        @endforeach
                       
                    </ul>
                </div>
            </div>
        </article>
        <!-- 最新消息內容 -->
        <article class="content">
            <div class="grid">
                
                @foreach($NewsNews as $value)
                <div class="box grid-item" cate_id="{{  $value->category_id }}">
                    <a href="{{ItemMaker::url('newsdetails/'.$value->id."/".str_replace("/","",$value->title))}}">
                        <!-- img的width 會隨機決定 -->
                        <div class="box_img">
                            <!-- 圖片橫版寬高450*300 -->
                            <!-- img的width 會隨機決定 -->

                            <img src="{{$value['out_image']}}" alt="" style="width: {{$value['sz']}}%">
                        </div>
                        <div class="box_text">
                            <!-- 日期 -->
                            <h4>{{$value['date']}}</h4>
                            <!-- 標題 -->
                            <h3>{{$value['title']}}</h3>
                            <!-- 此內文字數限制50字多餘的部分會變點點 -->
                            <p>{!! $value['out_content']!!}</p>
                        </div>
                    </a>
                </div>
                @endforeach

        </article>
    </main>
@include($locale.'.include.footer') 
@section('script')  
<script src="/assets/js/vendor/map/jquery.tinyMap.min.js"></script>

@stop


@stop