
@extends('template')

    <!--RWD Setting-->
@section('css')
            <link rel="stylesheet" href="/assets/css/news_detail.css">
            <link rel="stylesheet" href="/assets/js/vendor/slick/slick-theme.css">
            
@stop
@section('content')
@include($locale.'.include.header') 

        <main>
        <article class="detail_nav">
            <div class="series">
                
                <span><a href="{{ ItemMaker::url('news') }}">最新消息</a></span>
                <span>{{ $NewsCategorydata[$News->category_id] }}</span>
            </div>
            <div class="_back" onclick="history.back()">
                <span></span>
                <p>BACK</p>
            </div>
        </article>
        <article class="_title">
            <h2>{!! $News->title !!}</h2>
            <div class="_date">
                <p>{!! $News->date !!}</p>
            </div>
            <?php $thispageurl="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>
            <div class="socialMedia">
                        <p>SHARE</p>
                        <!-- facebook 後端請加連結-->
                        <a target="_blank" href="http://www.facebook.com/share.php?u={{ $thispageurl }}">
                        <span class="icon-facebook"></span>
                        </a>
                        <!-- twitter 後端請加連結-->
                        <a target="_blank" href="http://twitter.com/home/?status={{ $thispageurl }}">
                        <span class="icon-twitter"></span>
                        </a>
                    </div>
        </article>

        @if(!empty($NewsPhoto->toArray()))
        <article class="detail_slick">
            <ul>
                @foreach($NewsPhoto as $value)
                <li>
                    <div class="_slick_img">
                        <img src="{{ $value->image }}" alt="">
                    </div>
                </li>
                @endforeach               
            </ul>
            <div class="arrow">
                <span class="icon-slick-arrow-left"></span>
                <span class="icon-slick-arrow-right"></span>
            </div>
            
        </article>
        @endif
        <article class="content">
            @foreach($NewsContent as $value)
                @if($value->type ==1 || $value->type ==2)
                    <section class="type1">
                        <div class="_text">
                            <h2>{!! $value->title !!}</h2>
                            <p>{!! $value->content !!}</p>
                            @if(!empty($value->content_list))
                            <ul>
                                <?php  $content_list=explode("\r\n", $value->content_list);
                                
                                ?>
                                @foreach($content_list as $listdata)
                                <li><p>{!! $listdata !!}</p></li>     
                                @endforeach                  
                            </ul>
                            @endif
                        </div>
                    </section>
                @endif
            <!-- type2 文字配兩張圖 -->
            @if($value->type ==4)
                <section class="type2">
                    <div class="_text">
                        <h2>{!! $value->title !!}</h2>
                        <p>{!! $value->content !!}</p>
                        @if(!empty($value->content_list))
                            <ul>
                                <?php  $content_list=explode("\r\n", $value->content_list);
                                
                                ?>
                                @foreach($content_list as $listdata)
                                <li><p>{!! $listdata !!}</p></li>     
                                @endforeach                  
                            </ul>
                        @endif
                    </div>
                    <div class="imgs">
                        @if(!empty($value->image1))
                        <div class="_img">
                            <div>
                            <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="{{ $value->image1 }}" alt="">
                            </div>
                            <i>{{ $value->image1_title }}</i>
                        </div>
                        @endif
                        @if(!empty($value->image2))
                        <div class="_img">
                            <div>
                            <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="{{ $value->image2 }}" alt="">
                            </div>
                            <i>{{ $value->image2_title }}</i>
                        </div>
                        @endif
                    </div>
                </section>
            @endif
            @if($value->type ==3)
            <!-- type3 文字配一張大圖 -->
            <section class="type3">
                <div class="_text">
                    <h2>{!! $value->title !!}</h2>
                    <p>{!! $value->content !!}</p>
                    @if(!empty($value->content_list))
                            <ul>
                                <?php  $content_list=explode("\r\n", $value->content_list);
                                
                                ?>
                                @foreach($content_list as $listdata)
                                <li><p>{!! $listdata !!}</p></li>     
                                @endforeach                  
                            </ul>
                    @endif
                </div>
                <div class="_img">
                    <div>
                    <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="{{ $value->image1 }}" alt="">
                </div>
                    <i>{{ $value->image1_title }}</i>
                </div>
            </section>
            @endif
            <!-- type4 文字在左 圖片在右-->
            @if($value->type ==6)
            <section class="type4">
                <div class="_text">
                   <h2>{!! $value->title !!}</h2>
                   <p>{!! $value->content !!}</p>
                   @if(!empty($value->content_list))
                            <ul>
                                <?php  $content_list=explode("\r\n", $value->content_list);
                                
                                ?>
                                @foreach($content_list as $listdata)
                                <li><p>{!! $listdata !!}</p></li>     
                                @endforeach                  
                            </ul>
                  @endif
                </div>
                <div class="_img">
                    <div>
                    <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="{{ $value->image1 }}" alt="">
                </div>
                    <i>{{ $value->image1_title }}</i>
                </div>
            </section>
            @endif
            <!-- type5 圖片在左 文字在右-->
            @if($value->type ==5)
            <section class="type5">
                <div class="_text">
                    <h2>{!! $value->title !!}</h2>
                    <p>{!! $value->content !!}</p>
                    @if(!empty($value->content_list))
                            <ul>
                                <?php  $content_list=explode("\r\n", $value->content_list);
                                
                                ?>
                                @foreach($content_list as $listdata)
                                <li><p>{!! $listdata !!}</p></li>     
                                @endforeach                  
                            </ul>
                    @endif
                </div>
                <div class="_img">
                    <div>
                    <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="{{ $value->image1 }}" alt="">
                </div>
                    <i>{{ $value->image1_title }}</i>
                </div>
            </section>
            @endif
            @endforeach
            <!-- 回到上一頁按鈕 -->
            <div class="bt_back" onclick="history.back()">
                <span></span>
                <p>BACK</p>
            </div>
        </article>
    </main>
@include($locale.'.include.footer') 
@section('script') 
    <script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.11';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- 非共用end-->

<script src="/assets/js/vendor/slick/slick.min.js"></script>
<script src="/assets/js/vendor/map/jquery.tinyMap.min.js"></script>
@stop


@stop