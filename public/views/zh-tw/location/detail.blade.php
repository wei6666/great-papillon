
                <div class="_libox_content">
                    <div class="_close">
                        <p>CLOSE</p>
                    </div>
                    <div class="_libox_title">
                        <h2>{{ $LocationSet->en_title }}</h2>
                        <!-- 櫃位 -->
                        <h3>{{ $Location->title }}</h3>
                        <!-- 區域 -->
                        <h4>{{ $forchange[$Location->category_id] }}</h4>                       
                        <div class="socialMedia">
                            <!-- 請後端填上連結 -->
                            <p>SHARE</p>
                            <!-- facebook連結 -->
                            <a target="_blank" href="http://www.facebook.com/share.php?u={{ ItemMaker::url('location/'.$Location->id) }}">
                            <span class="icon-facebook"></span>
                        </a>
                            <!-- twitter連結 -->
                            <a target="_blank" href="http://twitter.com/home/?status={{ ItemMaker::url('location/'.$Location->id) }}">
                            <span class="icon-twitter"></span>
                        </a>
                        </div>
                    </div>
                    <div class="_inf">
                        <!-- 地址 -->
                        <p>{{ $Location->address }}</p>
                        <!-- 電話 -->
                        @if(!empty($Location->phone))
                        <p>電話 : <span>{{ $Location->phone }}</span></p>
                        <!-- 傳真 -->
                        @endif
                        @if(!empty($Location->fax))
                        <p>傳真 : <span>{{ $Location->fax }}</span></p>
                        @endif
                        <!-- 營業時間 -->
                        <ul class="opentime">
                            <?php $each=explode("\r\n", $Location->statement ); ?>
                            @foreach($each as $value)
                            <li><p>{{ $value }}</p></li>
                            @endforeach
                            
                        </ul>
                    </div>
                    @if(!empty($Location->line_title))
                    <div class="line">
                        <h2><strong>{{ $Location->line_title }}</strong> </h2>
                        <p>{{ $Location->line_content }}</p>
                        <a href="{{ $Location->line_link }}" target="_blank">
                    <img src="{{ $Location->line_photo }} " alt="">
                    </a>
                    </div>
                    @endif
                    <div class="activities">
                        @if(!empty($Location->in_photo))
                        <div class="_img">
                            <img src="{{ $Location->in_photo }}" alt="">
                        </div>
                        @endif
                        @if(!empty($Location->in_content_title))
                            <div class="_text">                            
                                <h2>{{ $Location->in_content_title }}</h2>
                               
                                <p>{{ $Location->in_content }}</p>
                            </div> 
                        @endif
                    </div>
                   
                    <div class="_googleMap" style="overflow:none; ">   
                        <div class="location_map">
                            <div class="map" style='width: 550px;    height: 380px;'></div>
                            <!--  關掉-->
                            <div class="location_close_icon">
                            <div class="close">
                                <span class="x_close"></span>
                            </div>
                          </div>
                        </div>
                    </div>

                </div>
