
@extends('template')

    <!--RWD Setting-->
@section('css')
        <link rel="stylesheet" href="/assets/js/vendor/slick/slick.css">
    <!-- 非共用CSS end-->
    <link rel="stylesheet" href="/assets/css/storehold.css">
@stop
@section('content')
@include($locale.'.include.header') 

    <main>
        <article class="_title">
            <h2>{!! $LocationSet->en_title !!}</h2>
            <h3>{!! $LocationSet->title !!}</h3>
            <h4>{!! $LocationSet->content !!}</h4>
            <div class="select">
                <div class="All">
                    <p>全台門市</p>
                </div>

                <div class="menulist">
                    <span class="selectMenu"><p>選擇區域</p></span>
                    <ul>

                        @foreach($LocationCategory as $value)
                            <li category="{{ $value->id }}">
                                <p>{{ $value->title }}</p>
                            </li>
                        @endforeach
                        
                    </ul>
                </div>
            </div>
        </article>
        <article class="_content">
            <div class="_list_name">
                <p>{{ $LocationSet->area }}</p>
                <p>{{ $LocationSet->info }}</p>
            </div>
                @foreach($CategoryWithData as $value)
                    <div class="_areas" category='{{ $value->id }}'>
                    <!-- 地區 -->                
                    @if(count($value['location']) >0 )
                            <div class="_area" category="{{ $value->category_id }}" >
                                <div class="_area_name">
                                    <h2>{{ $value->en_title }}</h2>
                                    <p>{{ $value->title }}</p>
                                </div>
                                <ul>

                                    @if(!empty($value['location']))
                                        @foreach($value['location'] as $value1)
                                            <li id="lo{{$value1->id }}" ajax_id="{{ $value1->id }}" x='{{ $value1->map_x }}' y='{{ $value1->map_y }}'>
                                                <!-- 樓層資訊 -->
                                                <p class="floor">{{ $value1->title }}</p>
                                                <!-- 地址 -->
                                                <p class="address">{{ $value1->address }}</p>
                                                <!-- 電話 -->
                                                <p class="tel">{{ $value1->phone }}</p>
                                                <!-- 活動消息 ps:可有可無 須由客戶決定 -->
                                                <p class="action">{{ $value1->in_content_title }}</p>
                                                <span class="icon-mapdot2"></span>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        @endif
                    </div>
                @endforeach

            <div class="_area" id='nodata'><h1 style='display: none; margin: 0 auto;''>no data found</h1></div>
        </article>
        <article class="_libox">
            <div class="_libox_content_bg">

                </div>

            </div> 

           <div class="_reservation _hideUp">
                <ul>
                    <li class="_goTop">
                        <p>GO TOP</p>
                    </li>
                    <!-- 連結到預約鑑賞內頁 -->
                    <li>
                        <a href="../reservation/index.html">
                            <p>預約鑑賞</p>
                        </a>
                    </li>
                    <!-- 連結到line  -->
                    <!-- 請後端填入連結 -->
                    <li>
                        <a href="javascript:void(0);"></a>
                    </li>
                </ul>
           </div>

        </article>
    </main>
     @include($locale.'.include.footer') 
@section('script')  
<script src="/assets/js/vendor/map/jquery.tinyMap.min.js"></script>

@if(!empty($_GET['lo']))
    <script >    
        $(document).ready(function(){
            
            setTimeout(function(){
                $("#lo{{ $_GET['lo'] }}").click();
            },500);

        });
    </script>
@endif
@stop


@stop