
@extends('template')

    <!--RWD Setting-->
@section('css')
        <link rel="stylesheet" href="/assets/js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.min.css">
        <link rel="stylesheet" href="/assets/css/shopping_qa.css">
@stop
@section('content')
@include($locale.'.include.header')
  <header></header>

  <main>

    <article class="m_box">

      <!--標題-->
      <section class="_top">
        
        <div class="_title">
          <h2>SHOPPING QA</h2>
          <p>購物QA</p>
        </div>
    
      </section>

      <!--下拉選單-->
      <div class="dropMenu">
        <div class="menulist">
          <span class="selectMenu arrow-icon"><p>{{ $Category[0]->title }}</p></span>
        </div>
        <ul tabindex="1" class="dropMenu_box" style="overflow: hidden; outline: none;">
          @foreach($Category as $value)
          <li>
            <p>{{ $value->title }}</p>
          </li>
          @endforeach         
        </ul>
      </div>

      <section class="work_1">
        <!--清單 購物需知--><!--預設出現-->
        @foreach($Category as $key => $value)
        <ul class="container cont_{{ $key }} @if($key==0) show @endif">
          
          @foreach($value['qa_title'] as $key1 => $value1)          
          @if(!empty($value1->title))
          <li class="list">
            <div class="click_btn">
              <div class="title">               
                <p class="num">@if($key1+1 < 10) 0 @endif{{ $key1+1 }}</p>
                <p class="text">{{ $value1->title }}</p>
              </div>
              <div class="content">
                <ul class="frame">
                  @foreach($value1['content'] as $key2 => $value2)
                  <li>
                    <p>{!! $value2->content !!}</p>
                  </li>
                  @endforeach                  
                </ul>
              </div>
            </div>
          </li>
          @endif
        
          @endforeach

        </ul>
        @endforeach
      </section>
      
    </article>

  </main>

@include($locale.'.include.footer') 
@section('script')  
  
 <script src="/assets/js/extra.js"></script>
@stop


@stop

 