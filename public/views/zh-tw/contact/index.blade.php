@extends('template')

    <!--RWD Setting-->
@section('css')

  <link rel="stylesheet" href="/assets/js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.min.css">
  <link rel="stylesheet" href="/assets/css/contact.css">
@stop
@section('content')
<main>
    <article class="m_box" style="background-image: url({{ $ContactSet->bg_image }});">

      <!--標題-->
      <section class="_top">
        
        <div class="_title">
          <h2>{{ $ContactSet->banner_sub_title }}</h2>
          <p>{{ $ContactSet->banner_title }}</p>
        </div>
    
      </section>

      <!--info-->
      <section class="info">
        <p>{!! $ContactSet->banner_content !!}</p>
      </section>

      <!--表單-->
      <article class="work_1">
          
        <!--box_1-->
        <section class="box_1 frame_box">

          <!--標題-->
          <article class="title">
            <h2 class="">聯絡表單</h2>
          </article>
        
          <!--會員登入表單-->
          <form method="POST" action="{{ ItemMaker::url('getpostdata') }}" class="member_form" id='form_go'>
            <input id='the_token' type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <ul class="login_member">
              <li>
                <label for="">NAME</label>
                <input type="text" name="name" id="name" value="" placeholder="請填寫您的姓名">
              </li>
              <li>
                <label for="">EMAIL</label>
                <input type="email" name="mail"  value="" placeholder="請填寫您的電子郵件">
              </li>
              <li>
                <label for="">MOBILE</label>
                <input type="number" name="phone" id="phone" value="" placeholder="請填寫您的手機號碼">
              </li>
              <li class="dropMenu">
                <label for="">QUESTION</label>
                <div class="menulist">
                  <span class="selectMenu arrow-icon"><p>請選擇您要詢問的項目</p></span>
                </div>
                <ul tabindex="1" class="dropMenu_box" style="overflow: hidden; outline: none;">
                  @foreach($ContactOption as $value)
                  <li>
                    <p>{{ $value->title }}</p>
                  </li>
                  @endforeach
                  
                </ul>
                <input type="hidden" name="question" id="question" {{-- value="{{ $ContactOption[0]->title }}" --}} placeholder="請填寫您的手機號碼">
              </li>
              <li class="textBox">
                <textarea rows="" name="content" cols="" placeholder="請填寫您要聯繫的內容"></textarea>
              </li>
            </ul>
          </form>
        
          <!--form_btn 登入按鈕 跳頁-->
          <div class="form_btn">
            <a href="javascript:void(0)" class="black one">
              <div>
                <p>送出</p>
              </div>
            </a>
          </div>

        </section>

        <!--box_2-->
        <section class="box_2 frame_box">

          <!--標題-->
          <article class="title">
            <h2 class="">{{ $ContactSet->title }}</h2>
          </article>

          <!--box_frame-->
          <div class="box_frame">
            <!--QR code-->
            <article class="QR_code">
              <div class="img">
                <!--110*110 可點擊的連結-->
                <a href="{{ $ContactSet->link }}">
                  <img src="{{ $ContactSet->image }}" alt="">
                </a>
              </div>
            </article>

            <!--訊息-->
            <div class="message">
              
              <p>{{ $ContactSet->content }}</p>

            </div>

            <!--服務時間-->
            <div class="time_box">
              @if(!empty($ContactSet->service_time_title))
              <div class="time">
                <p class="title">{{ $ContactSet->service_time_title }}</p>
                <p>{{ $ContactSet->service_time_content }}</p>
              </div>
              @endif
              @if(!empty($ContactSet->service_time_title))
              <div class="time">
                <p class="title">{{ $ContactSet->open_time_title }}</p>
                <p>{{ $ContactSet->open_time_content }}</p>
              </div>
              @endif
            </div>

            <!--社群按鈕-->
            <ul class="community_link">
              <li>
                <a href="javascript:void(0)">
                  <span class="icon-facebook"></span>
                </a>
              </li>
              <li>
                <a href="javascript:void(0)">
                  <span class="icon-instagram"></span>
                </a>
              </li>
              <li>
                <a href="javascript:void(0)">
                    <span class="icon-twitter"></span>
                </a>
              </li>
              <li>
                <a href="javascript:void(0)">
                    <span class="icon-youtube"></span>
                </a>
              </li>
            </ul>
          </div>

          <!--form_btn 註冊 跳頁-->
          <div class="form_btn">
            <a href="../storehold/index.html" class="gray one">
              <div>
                <p>門市據點</p>
              </div>
            </a>
          </div>

        </section>

      </article>
      
    </article>

  </main>


@section('script')  
  
      
      <script >

        $(".form_btn").on("click",function(){
          var length= $('html').attr('lang');
          if($("#name").val()=="")
          {
            if(length =="zh-tw")
            {
              alert('請輸入姓名');
            }  
            else
            {
              alert('please fill the name field');
            }

            
          }
          else if($("#phone").val()=="")
          {
            if(length =="zh-tw")
            {
              alert('請輸入電話');
            }  
            else
            {
              alert('please fill the phone field');
            }

          }
           else if($("#question").val()=="")
          {
            if(length =="zh-tw")
            {
              alert('請選擇問題類型');
            }  
            else
            {
              alert('please select the type of question');
            }

          }
          else
          {
            $("#form_go").submit();
          }

          
        });


        $(".dropMenu_box li").on("click",function(){
          $("#question").val($(this).find("p").text());
        });

      </script>
 
@stop


@stop
 

