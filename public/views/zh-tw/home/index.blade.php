
@extends('template')

    <!--RWD Setting-->
@section('css')
    
    <link rel="stylesheet" href="/assets/js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.min.css"> 
    <link rel="stylesheet" href="/assets/js/vendor/slick/slick-theme.css">
    <link rel="stylesheet" href="/assets/js/vendor/slick/slick.css">
    <link rel="stylesheet" href="/assets/css/home.css">
@stop
@section('content')



  <!--客戶可以選擇 顯示papillon_video(body 加上 has_video(class name)) 或是 不顯示papillon_video(隱藏整個結構)(body 移除 has_video(class name))-->
  @if(!empty($Home->home_video))
  <article class="papillon_video">

    <!--影片header-->
    <div class="video_header">
      <article class="header">

        <!--header NAV-->
        <section class="header_box">

          <!--選單，搜尋-->
          <div class="menu_search">
            <a class="menu_switch" href="javascript:void(0)">
              <p>menu</p>
            </a>
            <span class="point"></span>
            <a class="search_box" href="javascript:void(0)">
              <div>
                <span class="icon-search"></span>
              </div>
            </a>
          </div>

          <!--logo-->
          <div class="logo">
            <a href="{{ItemMaker::url('home')}}">
              <h2>papillon</h2>
              <img src="/assets/img/Logo1.png" alt="">
            </a>
          </div>

          <!--會員，禮品-->
           <div class="member_gift">
            <!--連去會員頁面，還沒登入前顯示 "會員登入" 登入後顯示 "您好，XXX"-->
            <a class="member_dropdown" href="{{ ItemMaker::url('member/guide') }}">
                @if(Session::get('Member') != NULL)
                <p>您好，{{ Session::get('name') }}</p>
                @else
                <p>會員登入</p>
                @endif
                
                
                <span class="icon-person"></span>
            </a>
            @if(Session::get('Member') != NULL)
            <div class="gift" >
                <a class="gift_switch" href="javascript:void(0)">
                  <span class="icon-gift"></span>
                  <p class="gift_box" id="gift">
                   
                    <span class="num">{{ $product_in_car }}</span>
                    <span class="text">件禮品</span>

                  </p>
                </a>
            </div>
            @else
            <a href="{{ ItemMaker::url('member/guide') }}">
                <span class="icon-gift"></span>
            </a>

            @endif
        </div>

        </section>

      </article>
    </div>

    <!--影片-->
    <section class="video_container">

      <div id="video_box" video-id="{!! $Home->home_video !!}"></div>

    </section>

    <!--影片播放按鈕-->
    <div class="hidden_btn">
      <span class="icon-arrow_drop_up"></span>
      <p>2017 形象影片</p>
    </div>

    <!--logo & 標題-->
    <section class="_top">

      <!--logo-->
      <div class="logo">
        <span class="icon-big-logo"></span>
      </div>

      <div class="_title">
          <h2>{!! $Home->video_en_title !!}</h2>
          <p>{!! $Home->video_title !!}</p>
      </div>

    </section>

    <!--音量開關-->
    <div class="volume_switch switch_on">
      <span class="icon-volume-high"></span>
      <span class="icon-volume-mute"></span>
    </div>

    <!--scroll down-->
    <a class="scroll_down" href="#node_1">
      <p>scroll down</p>
    </a>
    
  </article>
  @endif

  @include($locale.'.include.header') 

  <main id="node_1">

    <!--banner 1920*964-->
    <article class="slogan_section">

      <picture>
        <img src="{!! $Home->banner_img !!}" alt="">
      </picture>

      <!--標題-->
      <section class="_top">
          
        <div class="_title">
            <h2>{!! $Home->banner_en_title !!}</h2>
            <span></span>
            <p>{!! $Home->banner_title !!}</p>
        </div>
  
      </section>

    </article>

    <!--幸福系列-->
    <article class="collection">

      <!--標題-->
      <section class="_top">
  
        <div class="_title">
            <h2>{!! $Home->happiness_title !!}</h2>
            <p>{!! $Home->happiness_en_title !!}</p>
        </div>
  
      </section>

      <!--輪播 180*180 商品數量需大於5個 才會啟動輪播-->
      <section class="_slick_frame">
        <div class="home_ar">
            <div class="home_arL"><i class="icon-slick-arrow-left" aria-hidden="true"></i></div>
            <div class="home_arR"><i class="icon-slick-arrow-right" aria-hidden="true"></i></div>
        </div>
        <ul class="_slick_framebox slick">
          @foreach($ProductCategory as $value)
          <li class="_slick_list">
            <a href="{{ItemMaker::url('productcategory/'.$value->id)}}">
              <div class="img">
                <picture>
                  @if(!empty($value->home_image))
                  <img src="{{ $value->home_image }}" alt="">
                  @else
                  <img src="/upload/f2e/home/collection-01.jpg"  alt="">
                  @endif
                </picture>
              </div>
              <div class="text">
                <p>{{$value->title }}</p>
                <p class="vice">{{ $value->en_title }}</p>
              </div>
            </a>
          </li>          
          @endforeach

        </ul>
      </section>

    </article>

    <!--幸福精選-->
    @if(!empty(count($Product)) >0 )
    <article class="featured">

      <!--標題-->
      <section class="_top">
    
        <div class="_title">
          <h2>{{ $Home->product_en_title }}</h2>
          <p>{{ $Home->product_title }}</p>
        </div>
    
      </section>

      <!--660*540  s_img = 275*280-->
      <section class="featured_frame">
        @foreach($Product as $key => $value)
        
          @if(($key+1)%2 == 1)
            <div class="featured_bg">
              <div class="featured_box">
                  
                <!---->
                <div class="text">

                  <div class="_inner">
                    <p>{{$value['cate_en_title']}}</p>
                    <p class="vice">{{$value['cate_title']}}</p>
                    <span class="line"></span>
                    @if(!empty($value['content']))
                    <p class="adv_word">{!! $value['content']->content !!}</p>
                    @endif
                  </div>

                  <div class="_intro">

                    <div class="info">
                      <p>{{ $value->en_title }}</p>
                      <p class="vice">{{ $value->title }}</p class="vice">
                      <div class="more">
                          <a href="{{ItemMaker::url('priduct/'.$value->id)}}">
                          <span></span>
                          <p>READ MORE</p>
                          </a>
                      </div>
                    </div>

                    <div class="s_img">
                      <a href="{{ItemMaker::url('priduct/'.$value->id)}}">
                        <picture>
                          @if(!empty( $value['img1']->image))
                          <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="{{ $value['img1']->image }}" alt="">
                          @else
                          <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="/upload/f2e/home/featured-3.jpg" alt="">
                          @endif
                        </picture>
                      </a>
                    </div>

                  </div>

                </div>
      
                <!--大圖-->
                <div class="img">
                  <a href="{{ItemMaker::url('priduct/'.$value->id)}}">
                    <picture>
                      @if(!empty( $value['img2']->image))
                      <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="{{ $value['img2']->image }}" alt="">
                      @else
                       <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="/upload/f2e/home/featured-1.jpg" alt="">
                      @endif
                    </picture>
                  </a>
                </div>
      
              </div>
            </div>
          @else
          <div class="featured_bg">
              <div class="featured_box">

                <!--大圖-->
                <div class="img">
                  <a href="{{ItemMaker::url('priduct/'.$value->id)}}">
                    <picture>
                      @if(!empty( $value['img2']->image))
                      <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="{{ $value['img2']->image }}" alt="">
                      @else
                       <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="/upload/f2e/home/featured-1.jpg" alt="">
                      @endif
                    </picture>
                  </a>
                </div>
                  
                <!---->
                <div class="text">

                  <div class="_inner">
                    <p>{{$value['en_title']}}</p>
                    <p class="vice">{{$value['title']}}</p>
                    <span class="line"></span>
                      @if(!empty($value['content']))
                        <p class="adv_word">{!! $value['content']->content !!}</p>
                      @endif
                  </div>

                  <div class="_intro">                    

                    <div class="s_img">
                      <a href="{{ItemMaker::url('priduct/'.$value->id)}}">
                        <picture>
                          @if(!empty( $value['img1']->image))
                          <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="{{ $value['img1']->image }}" alt="">
                          @else
                          <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="/upload/f2e/home/featured-3.jpg" alt="">
                          @endif
                        </picture>
                      </a>
                    </div>

                    <div class="info">
                      <p>{{ $value->en_title }}</p>
                      <p class="vice">{{ $value->title }}</p class="vice">
                      <div class="more">
                          <a href="{{ItemMaker::url('priduct/'.$value->id)}}">
                          <span></span>
                          <p>READ MORE</p>
                          </a>
                      </div>
                    </div>

                  </div>

                </div>
      
                
      
              </div>
            </div>
          @endif
        @endforeach 
        
      </section>
      
    </article>
    @endif

    <!--珍藏一生 顧客可以換背景圖，標題，標語，按鈕文字，指定按鈕連結-->
    <article class="life" style="background-image: url({{ $Home->ad_img }}); color: #fff;">

      <!--標題-->
      <section class="_top">
          
        <div class="_title">
            <h2>{{$Home->ad_en_title}}</h2>
            <p>{{$Home->ad_title}}</p>
        </div>

      </section>

      <!--標語 & 按鈕-->
      <section class="box_1">

        <div class="slogan">
          <p>{{$Home->ad_content}}</p>
        </div>

        <div class="btn">
          <a href="{{$Home->ad_btn_link}}">
            <p>{{$Home->ad_btn_title}}</p>
          </a>
        </div>

      </section>
        
    </article>

    @if(count($News)>0)
    <!--最新消息-->
      <article class="news">
          
        <!--標題-->
        <section class="_top">
            
          <div class="_title">
              <h2>{{$Home->news_en_title}}</h2>
              <p>{{$Home->news_title}}</p>
          </div>
    
        </section>

        <!--輪播 340*230 商品數量需大於3個 才會啟動輪播-->
        
          <section class="_slick_frame">
            <div class="home_ar">
                <div class="home_arL"><i class="icon-slick-arrow-left" aria-hidden="true"></i></div>
                <div class="home_arR"><i class="icon-slick-arrow-right" aria-hidden="true"></i></div>
            </div>
            <ul class="_slick_framebox slick">
              @foreach($News as $value)          
              <li class="_slick_list">
                <a href="{{ItemMaker::url('news/'.$value->id)}}">
                  <div class="img">
                    <picture>
                      <img src="" data-lazy="{{$value->out_image}}" alt="">
                    </picture>
                  </div>
                  <div class="text">
                    <p>{{$value['day']}}</p>
                    <p class="vice">{{ $value->out_content}}</p>
                  </div>
                </a>
              </li>
              @endforeach
              
            </ul>
          </section>
      </article>
    @endif


    @if(count($Happiness)>0)
    <!--幸福見證-->
    <article class="witness b-lazy" style="background-image: url(/assets/img/blazy_blank.svg);" data-src="/upload/f2e/home/witness-bg.jpg">

        <!--標題-->
        <section class="_top">
        
          <div class="_title">
              <h2>{{$Home->happiness_detail_en_title}}</h2>
              <p>{{$Home->happiness_detail_title}}</p>
          </div>

        </section>

        <!--數字見證-->
        <section class="watch_number">

          <!--人數 請從count-number抓-->
          <div class="countup">
                <p>{!! $HappinessSet->content1 !!}<span class="counter" id="counter">@if(!empty($HappinessSet->contnet_num)) {{ $HappinessSet->contnet_num }}
                @else {{ count($Happinesscount) }} @endif </span>{!! $HappinessSet->content2 !!}</p>
          </div>

          <div class="watched">
            <span class="line"></span>
            <a href="{{ItemMaker::url("witness")}}">
              <p>{{$Home->happiness_detail_tag}}</p>
            </a>
          </div>

        </section>

        <!--輪播-->
        <section class="_slick_frame">
          <div class="home_ar">
              <div class="home_arL"><i class="icon-slick-arrow-left" aria-hidden="true"></i></div>
              <div class="home_arR"><i class="icon-slick-arrow-right" aria-hidden="true"></i></div>
          </div>
          <ul class="_slick_framebox slick">
            @foreach($Happiness as $value)
            <li class="_slick_list">
              <a href="{{ItemMaker::url('witness/'.$value->id) }}">
                <div class="img">
                  <picture>
                    
                    <img src="{{$value->out_image}}" data-lazy="" alt="">
                                   
                    <span></span>
                  </picture>
                </div>
                <div class="text">
                  @if(!empty($pro_cateid_to_title[$value->product_category_id]))
                    <p>{{$pro_cateid_to_title[$value->product_category_id]}}</p>
                  @endif
                  <p class="vice">{{$value->title}}</p>
                </div>
              </a>
            </li>
            @endforeach
          </ul>
        </section>

        <!--標語 & 按鈕-->
        <section class="box_1">

          <div class="slogan">
            <p>{{$Home->happiness_detail_content}}</p>
          </div>
  
          <div class="btn">
            <a href="{{ItemMaker::url('witness')}}">
              <p>{{$Home->happiness_detail_btn_title}}</p>
            </a>
          </div>

        </section>

    </article>
    @endif

    <!--法蝶故事-->
    <article class="story">
        
      <section class="story_frame">

        <div class="text">

          <div class="box">

            <!--標題-->
            <section class="_top">
              
              <div class="_title">
                @if(!empty($Home->about_en_title))
                  <h2>{{$Home->about_en_title}}</h2>
                @endif
                <p>{{$Home->about_title}}</p>
              </div>

            </section>

            <!--品牌短述-->
            @if(!empty($Home->about_content))
            <div class="brand_short">
              <p>
                {{$Home->about_content}}
              </p>
            </div>
            @endif

            <!--more btn-->
            <div class="more">
              <a href="{{ItemMaker::url('about')}}">
              <span></span>
              <p>READ MORE</p>
              </a>
            </div>

          </div>

        </div>

        <div class="img">
          <picture>           
            <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="{{$Home->about_img}}" alt="">          
          </picture>
        </div>

      </section>

    </article>
    
  </main>
@include($locale.'.include.footer') 
@section('script')  
  <script src="/assets/js/vendor/map/jquery.tinyMap.min.js"></script>
  <script src="/assets/js/extra.js"></script>
@if(!empty($_GET['lo']))
    <script >    
        $(document).ready(function(){
            
            setTimeout(function(){
                $("#lo{{ $_GET['lo'] }}").click();
            },500);

        });
    </script>
@endif
@stop


@stop

