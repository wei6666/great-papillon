@extends('template')
@section('css')
    <link rel="stylesheet" href="/assets/js/vendor/slick/slick.css">
    <link rel="stylesheet" href="/assets/js/vendor/slick/slick-theme.css">

    <link rel="stylesheet" href="/assets/css/about_detail.css">
@stop
@section('content')
    <main>
        <article class="top">
            <a href="{{ ItemMaker::url("about") }}">
                <div class="_close" >
                    <p>CLOSE</p>
                </div>
            </a>
            <div class="_title">
                <h2>GUARANTEE SERVICE</h2>
                <p>售後服務</p>
            </div>
            <div class="text">
                <p>法蝶在各大百貨皆有據點，若您有任何疑問或飾品需要清潔保養，歡迎您至法蝶專櫃，我們將提供最貼心的服務。</p>
            </div>
        </article>
        <article class="service_content">
            @foreach($Service as $value)
            <div class="content_box">
                <div class="_img">
                    <img src="{{ $value->photo }}" alt="">
                </div>
                <div class="_text">
                    <h2>{{ $value->title }}</h2>
                    <p>{!! $value->content !!}</p>
                </div>
            </div>       
            @endforeach
        </article>
        <article class="_reservation">
            <ul>
                <li class="_goTop">
                    <p>GO TOP</p>
                </li>
                <!-- 連結到預約鑑賞內頁 -->
                <li>
                    <a href="../reservation/index.html">
                        <p>預約鑑賞</p>
                    </a>
                </li>
                <!-- 連結到line  -->
                <!-- 請後端填入連結 -->
                <li>
                    <a href="javascript:void(0);"></a>
                </li>
            </ul>
        </article>
    </main>
@section('script')  
 <script src="/assets/js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.min.js"></script>
    <script src="/assets/js/vendor/slick/slick.min.js"></script>
@stop


@stop