
@extends('template')

    <!--RWD Setting-->
@section('css')
          <link rel="stylesheet" href="/assets/js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="/assets/js/vendor/slick/slick.css">
    <link rel="stylesheet" href="/assets/js/vendor/slick/slick-theme.css">
    <!-- 共用CSS end-->
    <!-- 非共用CSS -->
    <!-- 非共用CSS end-->
    <link rel="stylesheet" href="/assets/css/about.css">
@stop
@section('content')
@include($locale.'.include.header')


    <main>
        <!-- banner -->
        <article class="_banner">
            <div class="_banner_bg">
                <img src="/upload/f2e/about/banner_bg_1920_1680.jpg" alt="">
            </div>
            <div class="banner_content">
                <div class="_title">
                    <div class="_logo">
                        <img src="/upload/f2e/about/banner_logo_265_85.png" alt="">
                    </div>
                    <h2>穿越八世紀的璀璨經典</h2>
                    <p><span>“ </span> 鑽中有愛，永恆存在 <span> ”</span></p>
                </div>
                <div class="_img">
                    <img src="/upload/f2e/about/banner_img_1080_670.png" alt="">
                </div>
                <div class="_text">
                    <h2>藝術啟蒙</h2>
                    <p>一個具貴族傳奇色彩，來自法國的頂級珠寶品牌，設計靈感起源路易十四皇朝鼎盛的時期；當時皇宮充滿奢華與璀璨的氣息，無論是服飾珠寶或是生活起居的各種用品，在所有的細節上傾注了全付的精力，不容許一絲絲的差池與重複。</p>
                </div>
            </div>
            <div class="_banner_srcollDown">
                <p>SCROLL DOWN</p>
            </div>
        </article>
        <!-- content -->
        <article class="_content">
            <div class="row story">
                <div class="row_img">
                    <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="/upload/f2e/about/img1_660_500.jpg" alt="">
                </div>
                <div class="row_text">
                    <div class="text_box">
                        <h2>BRAND SRORY</h2>
                        <h3>品牌故事</h3>
                        <!-- 最多字數65字 -->
                        <h4>法蝶珠寶Papillon，法文意謂『蝴蝶』，之所以用蝴蝶為名，是在刻劃女人生命中點點滴滴的幸福歷程，也代表了法蝶創辦人 M & J 創業的心路歷程....</h4>
                       <div class="more">
                            <a href="{{ ItemMaker::url('about/story')}}">
                                <span></span>
                                <p>READ MORE</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row design">
                <div class="row_img">
                    <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="/upload/f2e/about/img2_470_640.jpg" alt="">
                </div>
                <div class="row_text">
                    <div class="text_box">
                        <h2>SPECIAL DESIGN</h2>
                        <h3>獨特設計</h3>
                        <!-- 最多字數65字 -->
                        <h4>法蝶珠寶的設計團包括國內外才華滿溢的設計師，其中還包含需多美國GIA設計畢業的資深設計師，資深設計師們將時尚與藝術的元素融合在法蝶的商....</h4>
                       <div class="more">
                            <a href="{{ ItemMaker::url('about/design')}}">
                                <span></span>
                                <p>READ MORE</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row service">
                <div class="row_img">
                    <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="/upload/f2e/about/img3_530_400.jpg" alt="">
                </div>
                <div class="row_text">
                    <div class="text_box">
                        <h2>GUARANTEE SERVICE</h2>
                        <h3>售後服務</h3>
                        <!-- 最多字數65字 -->
                        <h4>凡在法蝶購買之商品，憑保證書，初次享有免費改圍，終生另享有免費電鍍或改圍兩次；鉑金戒指享有一次改圍或電鍍(固定圍除外); K金項鍊斷修乙次....</h4>
                        <div class="more">
                            <a href="{{ ItemMaker::url('about/service')}}">
                                <span></span>
                                <p>READ MORE</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row quality">
                <div class="row_img">
                    <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="/upload/f2e/about/img4_660_500.jpg" alt="">
                </div>
                <div class="row_text">
                    <div class="text_box">
                        <h2>Quality Assurance</h2>
                        <h3>品質堅持</h3>
                        <!-- 最多字數65字 -->
                        <h4>一般所謂的「鑽石鑑定證書」或「鑽石等級證書」，法蝶稱它為「國際鑑定證書」(International Grading Certificate)。這報告是....</h4>
                        <div class="more">
                            <a href="{{ ItemMaker::url('about/quality')}}">
                                <span></span>
                                <p>READ MORE</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row charity">
                <div class="row_img">
                    <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="/upload/f2e/about/img5_390_530.jpg" alt="">
                </div>
                <div class="row_text">
                    <div class="text_box">
                        <h2>CHARITY SERVICE</h2>
                        <h3>公益服務</h3>
                        <!-- 最多字數65字 -->
                        <h4>法蝶珠寶，幸福傳承 – 帶給台灣人幸福的百貨珠寶品牌！今年已邁入31周年! 一直以來，積極為台灣盡一份心力，在不景氣時，更帶頭做公益</h4>
                        <div class="more">
                            <a href="{{ ItemMaker::url('about/charity')}}">
                                <span></span>
                                <p>READ MORE</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </main>
   

     @include($locale.'.include.footer') 
@section('script')  
    <script src="/assets/js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.min.js"></script>
    <script src="/assets/js/vendor/slick/slick.min.js"></script>   
    <script src="/assets/js/main.js"></script>
    <script src="/assets/js/about.js"></script>
 <script src="/assets/js/extra.js"></script>
@stop


@stop