@extends('template')


@section('bodySetting', 'id="member" class="member memberJoin_frame"')

@section('content')

@section('css')
    
    <link rel="stylesheet" href="/assets/css/member.css">
@stop

  <main>

    <!--登入會員與加入會員 member login and join box-->
    <article class="m_box" style="background-image: url(/upload/f2e/member/member-bg2.jpg);">

      <div class="m_box_inner">

        <!--關閉按鈕-->
        <div class="_close">
          <!--如果是從member_account過來的 這個close要返回去member_account的頁面-->
          <a href="javascript:void(0)" onclick="history.back();">
            <p>CLOSE</p>
          </a>
        </div>

        <!--輸入會員資料區塊 從member_account的 變更郵件及手機號碼 按鈕連接過來時 請後端將此區塊隱藏-->
        <!--點了"我同意"按鈕後才能點擊"確認註冊"，如果資料未填完全點了"確認註冊"後才秀出"您資料尚未完成填寫"-->
        <section class="member_login frame_box @if(Session::get('getcode')=='true' || Session::get('success')=='true' ) not_show @endif">

          <!--標題-->
          <section class="_top">
              
            <div class="_title">
              <h2>JOIN MEMBERS</h2>
              <p>成為法蝶會員</p>
            </div>
        
          </section>

          <!--提醒訊息-->
          <div class="information">
            <p>請正確填寫以下內容，以保障未來您驗證資料，及領取各項優惠。</p>
          </div>
        
          <!--會員註冊表單-->
          <form class="member_form" action="{{ ItemMaker::url('member/register_submit')}}" method='get' id='regForm'>
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <ul class="login_member">
              <li>
                <label for="">NAME</label>
                <input type="text" name="Register[name]" value="{{ Session::get('name') }}" placeholder="請填寫您的姓名" class="required">
              </li>
              <li>
                <label for="">MOBILE</label>
                <input type="number" name="Register[mobile]" id="mobilecheck" value="{{ Session::get('mobile') }}" placeholder="請填寫您的手機號碼" class="required">
              </li>
              <li>
                <label for="">EMAIL</label>
                <input type="email" name="Register[email]" id="mailcheck" value="{{ Session::get('email') }}" placeholder="請填寫您的電子郵件" class="required">
              </li>
              <li>
                <label for="">PASSWORD</label>
                <input type="password" name="Register[pass]" id="passcheck" value="" placeholder="請設定英文及數字組成之8個字元新密碼" class="required">
              </li>
              <li class="dropMenu">
                <label for="">GENDER</label>
                <div class="menulist">
                  <span class="selectMenu arrow-icon"><p>請選擇您的性別</p></span>
                </div>
                <ul tabindex="1" class="dropMenu_box sel_gender">
                    <li value="1">
                        <p>紳士</p>
                    </li>
                    <li value="2">
                        <p>淑女</p>
                    </li>
                </ul>
                <input type="hidden" name="Register[gender]" value="{{ Session::get('gender') }}">
              </li>
              <li class="dropMenu">
                <label for="">CITY</label>
                <div class="menulist">
                  <span class="selectMenu arrow-icon"><p>請選擇您居住的縣市</p></span>
                </div>
                <ul tabindex="1" class="dropMenu_box sel_city">
                  @foreach($city as $value)
                  <li value="{{ $value['id'] }}">
                      <p>{{ $value['title'] }}</p>
                  </li>
                  @endforeach
                </ul>
                <input type="hidden" name="Register[city]" value="{{ Session::get('city') }}">
              </li>
              <li class="member_verification">
                <input type="text" name="captcha" value="" placeholder="請輸入驗證碼">
                <div class="f_code_box">
                  <div id="changeCap" style="width: 70px; height: 35px">{!! Captcha::img() !!}</div>
                </div>
              </li>
            </ul>
          </form>

          <!--提醒訊息 ， 資料填寫完畢後 就把 尚未完成填寫 改成 已完成填寫-->
          <div class="information">
            <p class="warning" style="display:none; font-style: italic; color: #d80b18;">您資料尚未完成填寫</p>
            <!-- <p class="warning" style="font-style: italic;">您的資料已完成填寫</p> -->
            <p class="agree_info">我同意收到法蝶珠寶各項優惠訊息，且已經閱讀及瞭解服務條款並同意註冊成為會員。</p>
          </div>

          <!--icon_btn_group 服務條款 與 我同意-->
          <div class="icon_btn_group">
            <a href="javascript:void(0)" class="terms">
              <div>
                <span class="icon-i black"></span>
                <p>服務條款</p>
              </div>
            </a>
            <a href="javascript:void(0)" class="tick">
              <div>
                <span class="icon-check gray"></span>
                <p>我同意</p>
              </div>
            </a>
          </div>
        
          <!--form_btn 確認註冊-->
          <div class="form_btn">
            <a href="javascript:void(0)" class="next_content_btn black one disabled">
              <div>
                <p>確認註冊</p>
              </div>
            </a>
          </div>

          <!--服務條款-->
          <article class="service_term">

            <section class="box">

              <!--關閉按鈕-->
              <div class="_close">
                <a href="javascript:void(0)">
                  <p>CLOSE</p>
                </a>
              </div>

              <!--標題-->
              <section class="_top">
                  
                <div class="_title">
                  <h2>TERMS OF SERVICE</h2>
                  <p>服務條款</p>
                </div>
            
              </section>

              <!---->
              <div class="content">

                <!--外-->
                <ul class="out_side">
                  @foreach($service as $key => $value)
                  <li class="out_list animate">
                      @if(!empty($value['title']))
                      <p class="out_title">{{ $value['title'] }}</p>
                      @endif
                      <ul class="in_side">
                        <p class="paragraph">{!! str_replace("\r\n", "<br>", $value['content']) !!}</p>
                        <p class="paragraph">{!! str_replace("\r\n", "<br>", $value['sub_content']) !!}</p>
                        @if(!empty($list[$key]))
                        @foreach($list[$key] as $key1 => $value1)
                        <li>
                          <p class="paragraph">{!! str_replace("\r\n", "<br>", $value1['content']) !!}</p>
                        </li>
                        @endforeach
                        @endif
                      </ul>
                  </li>
                  @endforeach

                </ul>

              </div>

            </section>

          </article>

        </section>

        <!--身分驗證區塊-->
        <section class="member_identity frame_box  @if(Session::get('getcode')!='true') not_show @endif">

          <!--標題-->
          <section class="_top">
              
            <div class="_title">
              <h2>IDENTITY VERIFICATION</h2>
              <p>會員身分驗證</p>
            </div>
        
          </section>

          <!--提醒訊息-->
          <div class="information">
            <p>親愛的會員您好，為了避免您的身分被盜用，我們會通過簡訊發送五位數驗證碼到您的電話{{ Session::get('mobile') }}，請於五分鐘內完成驗證，若您尚未收到驗證簡訊，請重新發送驗證碼，或者回到註冊畫面確認您輸入的資料是否正確。</p>
          </div>

          <!--驗證碼區塊-->
          <form class="member_form" style="margin-bottom:20px;" action="{{ ItemMaker::url('member/code_submit')}}" method='get' id='codeForm'>
            <ul id="vcode" class="enter_verification">
              <li>
                <input type="text" maxlength="1" size="1" min="0" max="9" name="Vcode[no1]" pattern="[0-9]{1}">
              </li>
              <li>
                <input type="text" maxlength="1" size="1" min="0" max="9" name="Vcode[no2]" pattern="[0-9]{1}">
              </li>
              <li>
                <input type="text" maxlength="1" size="1" min="0" max="9" name="Vcode[no3]" pattern="[0-9]{1}">
              </li>
              <li>
                <input type="text" maxlength="1" size="1" min="0" max="9" name="Vcode[no4]" pattern="[0-9]{1}">
              </li>
              <li>
                <input type="text" maxlength="1" size="1" min="0" max="9" name="Vcode[no5]" pattern="[0-9]{1}" onblur="chkCode('{{ ItemMaker::url('member/chk_code')}}')">
              </li>
            </ul>
          </form>

          <!--驗證碼勾勾 與 倒數計時器-->
          <div class="tick_countdown">

            <!--一開始都不顯示-->
            <div class="tick">

              <!--驗證碼輸入正確 加上show(class) 顯示icon-check-->
              <span class="icon-check"></span>

              <!--驗證碼輸入錯誤 加上show(class) 顯示icon-x-->
              <span class="icon-x"></span>

            </div>

            <div class="countdown">
              <p data-m="5"></p>
              <p data-s="00"></p>
            </div>

          </div>

          <!--icon_btn_group 確認會員資料 與 重新發送-->
          <div class="icon_btn_group">
            <a href="javascript:void(0)" class="back_content_btn">
              <div>
                <span class="icon-slick-arrow-left black"></span>
                <p>確認會員資料</p>
              </div>
            </a>
            <a href="{{ ItemMaker::url('member/register_submit?resend=true')}}" class="resend">
              <div>
                <span class="icon-refresh gray"></span>
                <p>重新發送</p>
              </div>
            </a>
          </div>

          <!--form_btn 完成認證-->
          <div class="form_btn">
            <a href="javascript:void(0)" class="open_success black one disabled">
              <div>
                <p>完成認證</p>
              </div>
            </a>
          </div>

        </section>

        <!--驗證成功區塊-->
        <section class="member_success  @if(Session::get('success')!='true') not_show @endif">

          <div class="box" style="background-image: url(/upload/f2e/member/success-bg.jpg);">

            <!--標題-->
            <section class="_top">
                
              <div class="_title">
                <h2>SUCCESSFUL</h2>
                <p>會員驗證成功</p>
              </div>
          
            </section>

            <!--提醒訊息-->
            <div class="information">
              @if(Session::get('success')=='true')
              <p>親愛的會員您好，恭喜您的身分驗證成功，您現在就可以立即登入，成為法蝶珠寶會員，可享有多項優惠；法蝶珠寶感謝您的加入。</p>
              @endif
              {{-- <p>親愛的會員您好，恭喜您的身分驗證成功，我們已經將您的登入密碼重置，並將新的密碼發送簡訊到您的手機，建議您登入會員之後更改您的登入密碼，祝您購物於快。</p> --}}
            </div>

            <!--form_btn 立即登入 跳頁-->
            <div class="form_btn">
              <a href="javascript:void(0)" class="black one" onClick="reg_success('{{ ItemMaker::url('member/success') }}','{{ ItemMaker::url('member') }}')">
                <div>
                  <p>立即登入</p>
                </div>
              </a>
            </div>

          </div>

        </section>

      </div>

    </article>

  </main>

  @section('script')
  
  <script src="/assets/js/member.js"></script>
  <style type="text/css">
    a.disabled {
       pointer-events: none;
    }
  </style>
  <script type="text/javascript">
  $(document).ready(function(){
      /*驗證碼點擊更換*/
      var cap = document.querySelector('div[id="changeCap"]');

      cap.addEventListener("click", function () {
          var d = new Date();
              this.innerHTML=this.innerHTML.replace("{{ $locale }}" ,"");
              this.innerHTML=this.innerHTML.replace("/member" ,"");
              this.innerHTML=this.innerHTML.replace("/register" ,"");
              img = this.childNodes[0];

          img.src = img.src.split('?')[0] + '?t=' + d.getMilliseconds();

      });
      $('#changeCap').click();
      $('#changeCap').click();
      $('#changeCap').click();

      if($('.member_identity').hasClass('not_show')){

      }else{
        var time=<?php echo (strtotime(date('Y-m-d H:i:s'))-strtotime(Session::get('submitTime')))?>;
        console.log(time);
        var min=5-Math.floor(time/60);
        var sec=60-time%60;
        console.log(min);
        console.log(sec);
        if(min<0){
          $.ajax({
              type :  'get',
              url :  window.location.href+'/timeUp' ,
              async :false,
              success :  function(result) {
                alert('驗證碼已過期,請重新輸入資料!');
                window.location.reload();
              },
              error : function(xhr,textStatus,errorThrown){
                console.log(textStatus);
                console.log(errorThrown);
              }
            });
        }else{
          $('.countdown p').eq(0).attr('data-m', min);
          $('.countdown p').eq(1).attr('data-s', sec);
          strat_countdown();
        }
        /*function padLeft(str){
          str = '' + str;
          if(str.length >= 2){
            return str;
          }else{
            return padLeft("0" +str);
          }
        }*/
      }

  });
  /*****輸完驗證碼移開滑鼠判斷*****/
  function chkCode(s_url){
    $.ajax({
      type :  'get',
      url :  s_url ,
      async :false,
      data :{
        Vcode1:$('input[name="Vcode[no1]"]').val(),
        Vcode2:$('input[name="Vcode[no2]"]').val(),
        Vcode3:$('input[name="Vcode[no3]"]').val(),
        Vcode4:$('input[name="Vcode[no4]"]').val(),
        Vcode5:$('input[name="Vcode[no5]"]').val(),
      },
      success :  function(result) {
        if(result!='false'){
          $('.icon-x').removeClass('show');
          $('.icon-check').addClass('show');
          $('.open_success').removeClass('disabled');
          $('.member_success .information p ').text(result);

        }else{
          $('.icon-x').addClass('show');
          $('.icon-check').removeClass('show');
          $('.open_success').addClass('disabled');
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown){
          console.log(textStatus);
          console.log(errorThrown);
      }
    });
  }
  /*****點選完成驗證*****/
  $('.open_success').on('click',function(){
      $('#codeForm').submit();
  });
  /*****點了"我同意"按鈕後才能點擊"確認註冊"*****/
    $('.icon_btn_group .tick').on('click',function(){
        if ( $(this).hasClass('agree') == false ) {
            $('.next_content_btn').removeClass('disabled');
        }else {
            $('.next_content_btn').addClass('disabled');
        }
    });
    $('.next_content_btn').click('click',function(){
        var warning = 0;
        var mail = /^\w+((-\w+)|(.\w+))\@[A-Za-z0-9]+((.|-)[A-Za-z0-9]+).[A-Za-z0-9]+$/;
        var cellphone = /^09[0-9]{8}$/;
        var passCheck = /^([a-zA-Z]+\d+|\d+[a-zA-Z]+)[a-zA-Z0-9]*$/;
        //檢查input類型必填欄位
        $(".required").each(function(){
            var data = $(this);
            if(data.val().length==0){
                warning++;
            }
        });

        if (warning!=0){
          $('.warning').text('您資料尚未完成填寫');
          $('.warning').show();
        }else if(!cellphone.test($("#mobilecheck").val())){
          $('.warning').text('手機號碼格式錯誤');
          $('.warning').show();
        }else if (!mail.test($("#mailcheck").val())){
          $('.warning').text('電子郵件格式錯誤');
          $('.warning').show();
        }else if (!passCheck.test($("#passcheck").val())){
          $('.warning').text('請設定英文及數字組成之8個字元新密碼');
          $('.warning').show();
        }else{
          $('.warning').hide();
          $('.sel_gender li').each(function(){
            if($(this).hasClass('checked')){
              $('input[name="Register[gender]"]').val($(this).attr('value'));
            }
          });
          $('.sel_city li').each(function(){
            if($(this).hasClass('checked')){
              $('input[name="Register[city]"]').val($(this).attr('value'));
            }
          });
          $('#regForm').submit();
        }
    });
    /*******註冊成功刪session*******/
    function reg_success($url,$go){
        $.ajax({
            url: $url,
            success: function(result){
                window.location.href=$go;
            },
            error: function(xhr,textStatus,errorThrown){
                console.log(xhr);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    }

    $("#changeCap").click();
    $("#changeCap").click();

    
  </script>
  @stop

@stop