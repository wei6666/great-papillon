@extends('template')


@section('bodySetting', 'id="member" class="member member_guide"')

@section('content')

@section('css')
    
    <link rel="stylesheet" href="/assets/css/member.css">
@stop
@include($locale.'.include.header')
  <main>

    <article class="m_box">

      <!--標題-->
      <section class="_top">
        
        <div class="_title">
          <h2>MEMBER CENTER</h2>
          <p>會員中心</p>
        </div>
    
      </section>

      <!--自拍照 紅利點數-->
      <article class="work_1">

        <!---->
        <section class="box_1">
            
          <!--自拍照-->
          <div class="selfie">
            <a href="javascript:void(0)">
              <picture>
                <img src="/upload/f2e/member/head-1.jpg" alt="">
              </picture>
            </a>
          </div>

          <!--會員姓名、祝賀詞、編號、管理-->
          <div class="block">

            <div class="notify">
              <p class="name">{{ Session::get('name') }}<span>，歡迎您回來</span></p>
              @if(date("m-d", strtotime(Session::get('birthday')))==date("m-d"))
              <p>今天是您的生日，祝您生日快樂</p>
              @endif
            </div>

            <div class="serial">
              <p>會員編號：{{ Session::get('mbid_city').Session::get('mbid_no') }}</p>
            </div>

            <ul class="link">
              <li class="red">
                <a href="{{ ItemMaker::url('member/account') }}">
                  <p>會員資料管理</p>
                </a>
              </li>
              <li>
                <a href="{{ ItemMaker::url('member/logout') }}">
                  <p>會員登出</p>
                </a>
              </li>
            </ul>

          </div>

        </section>
          
        <!--紅利點數-->
        <section class="box_2">
  
          <p class="title">BONUS POINTS</p>
  
          <!--標題-->
          <section class="_top">
            
            <div class="_title">
              <h2>您的紅利點數</h2>
            </div>
        
          </section>
  
          <!--點數-->
          <p class="point">{{ $tempusedbonuscount }}<span>BP</span></p>

          <!---->
          <p class="change">1BP ＝ 1元</p>

          <!--日期-->
          
          <p class="date">使用期限 : {{ date('Y / m / d', strtotime('+3 year',strtotime($orderlastdate))) }}</p>
  
          <!--跳頁按鈕-->
          <div class="form_btn">
            <a href="{{ ItemMaker::url('member/bonus') }}" class="black one">
              <div>
                <p>紅利點數管理</p>
              </div>
            </a>
          </div>
  
        </section>

      </article>

      <!--方塊排列-->
      <article class="work_2">

        <!---->
        <section class="box_1">
          <ul class="frame clearfix">
            <!--regular 固定樣式 寬高不變  img = 560*370-->
            <!--commer 非固定樣式 寬可從block的style:width更改-->
            <li class="list regular">

              <div class="block">
                <div class="img">
                  <picture>
                    <img src="/upload/f2e/member/guide-01.jpg" alt="">
                  </picture>
                </div>

                <div class="text">
                  <p class="title">我的最愛收藏</p>
                  <a href="{{ ItemMaker::url('member/favorite') }}" class="link">
                    <span></span>
                    <p>瀏覽您收藏的最愛精品</p>
                  </a>
                </div>
              </div>

            </li>
            <li class="list regular">
                
              <div class="block">
                <div class="img">
                  <picture>
                    <img src="/upload/f2e/member/guide-02.jpg" alt="">
                  </picture>
                </div>

                <div class="text">
                  <p class="title">您的購買 / 退換貨紀錄</p>
                  <a href="{{ ItemMaker::url('member/order') }}" class="link">
                    <span></span>
                    <p>購買記錄</p>
                  </a>
                  <a href="{{ ItemMaker::url('member/return') }}" class="link">
                    <span></span>
                    <p>退換貨記錄</p>
                  </a>
                </div>
              </div>

            </li>
            <li class="list regular">
                
              <div class="block">
                <div class="img">
                  <picture>
                    <img src="/upload/f2e/member/guide-03.jpg" alt="">
                  </picture>
                </div>

                <div class="text">
                  <p class="title">您的地址簿</p>
                  <a href="{{ ItemMaker::url('member/address') }}" class="link">
                    <span></span>
                    <p>瀏覽您的地址簿</p>
                  </a>
                </div>
              </div>

            </li>
            <li class="list commer">

              <div class="block" style="width:75%;">
                <div class="img">
                  <picture>
                    <img src="{{ $newPro_img['image'] }}" alt="">
                  </picture>
                </div>
    
                <div class="text">
                  <p class="title">{!! str_replace("\r\n", "<br>", $newPro['title']) !!}</p>
                  <a href="{{ ItemMaker::url('product/'.$newPro['id']."/".$newPro['title']) }}" class="link">
                    <span></span>
                    <p>READ MORE{{ $newPro_img['title'] }}</p>
                  </a>
                </div>
              </div>

            </li>
            <li class="list commer">
                
              <div class="block" style="width:80%;">
                <div class="img">
                  <picture>
                    <img src="{{ $news['out_image'] }}" alt="">
                  </picture>
                </div>

                <div class="text">
                  <p class="title">{{ $news['title'] }}</p>
                  <p class="info">
                    {{ ((mb_strlen($news['out_content'], "UTF8")>50) ? mb_substr($news['out_content'],0,50, "UTF8").'...' : $news['out_content']) }}</p>
                  <a href="{{ItemMaker::url('newsdetails/'.$news['id'])}}" class="link">
                    <span></span>
                    <p>READ MORE</p>
                  </a>
                </div>
              </div>

            </li>
          </ul>
        </section>

      </article>
      
    </article>

  </main>
@include($locale.'.include.footer')
  @section('script')
  
  <script src="/assets/js/member.js"></script>
  <script type="text/javascript">

  </script>
  @stop

@stop