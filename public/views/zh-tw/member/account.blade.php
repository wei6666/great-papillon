@extends('template')


@section('bodySetting', 'id="member" class="memberAccount_frame"')

@section('content')

@section('css')
    
    <link rel="stylesheet" href="/assets/js/vendor/datedropper/datedropper.min.css">
    <link rel="stylesheet" href="/assets/css/member.css">
@stop
@include($locale.'.include.header')
  <main>

    <article class="m_box">

      <!--標題-->
      <section class="_top">
        
        <div class="_title">
          <h2>ACCOUNT</h2>
          <p>會員資料管理</p>
        </div>
    
      </section>

      <!--麵包屑-->
      <article class="detail_nav">

        <!--麵包屑-->
        <div class="series">
            <span><a href="{{ ItemMaker::url('member/guide') }}">會員中心</a></span>
            <span>會員資料管理</span>
        </div>

        <!--返回鍵-->
        <div class="_back">
          <a href="{{ ItemMaker::url('member/guide') }}">
            <span></span>
            <p>BACK</p>
          </a>
        </div>

      </article>

      <!--設定大頭貼-->
      <article class="work_1">

        <!---->
        <section class="member_section">
            
          <!--自拍照 306*226-->
          <div class="member_selfie">
            <a href="javascript:void(0)" class="set_pic">
              <picture>
                @if(!empty($memberData['image']))
                <img src="{{ $memberData['image'] }}" alt="" id="myPhoto">
                @else
                <img src="/upload/f2e/member/head-1.jpg" alt="">
                @endif
              </picture>
            </a>
          </div>

          <!--編號、管理-->
          <div class="member_block">

            <div class="notify">
              <p>設定您的大頭貼</p>
            </div>

            <div class="member_serial">
              <p>會員編號：{{ $memberData['mbid_city'].$memberData['mbid_no'] }}</p>
            </div>

            <!--編輯 移除 按鈕-->
            <div class="form_btn">
              <a href="javascript:void(0)" class="black two set_pic">
                <div>
                  <p>編輯照片</p>
                </div>
              </a>
              <a href="javascript:void(0)" class="gray two" onclick="delPhoto('{{ ItemMaker::url('member/photo_del') }}')">
                <div>
                  <p>移除照片</p>
                </div>
              </a>
            </div>

          </div>

        </section>

        <!--大頭貼設定PHOTO SETTING-->
        <section class="set_picture">

          <section class="set_box">

            <!--標題-->
            <section class="_top">
              
              <div class="_title">
                <h2>PHOTO SETTING</h2>
                <p>大頭貼設定</p>
              </div>
          
            </section>

            <!--自拍照 306*226-->
            <div class="member_selfie">
              <a href="javascript:void(0)">
                <picture>
                  @if(!empty($memberData['image']))
                  <img src="{{ $memberData['image'] }}" alt="" class="preview">
                  @else
                  <img src="/upload/f2e/member/head-1.jpg" alt="" class="preview">
                  @endif
                </picture>
              </a>
            </div>
            <!--上傳按鈕 規則-->
            <section class="upload">

              <!--form_btn-->
              <div class="form_btn">
                <a href="javascript:;" class="black one" onclick="pick()">
                  <div>
                    <p>上傳照片
                      </p>
                      
                  </div>
                </a>
              </div>
            <form action="{{ ItemMaker::url('member/photo_submit') }}" method="post" enctype="multipart/form-data" id="photoForm">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="file" name="photo"  value="{{ $memberData['image'] }}" style="display: none;" id="filePicker">
            </form>
              <div class="notify">
                <p>圖片尺寸請限制在 306px * 226px</p>
                <p>圖片的格式為 Jpg / png / gif</p>
              </div>

            </section>

            <!--form_btn 儲存 離開 按鈕-->
            <div class="form_btn">
              <a href="javascript:void(0)" class="black two close_pic" onclick="photoSubmit()">
                <div>
                  <p>儲存設定</p>
                </div>
              </a>
              <a href="javascript:void(0)" class="gray two close_pic">
                <div>
                  <p>離開設定</p>
                </div>
              </a>
            </div>

          </section>

        </section>

      </article>

      <!--會員帳號資料詳細設定-->
      <article class="work_2">

        <!---->
        <ul class="account_detail_box">
          
          <!--Account 帳號設定-->
          <li class="list">

            <div class="detail">
              <p class="title">Account</p>
              <p class="vice">帳號設定</p>
              <p class="text">若您需要重新設定電子郵件帳號、手機號碼以及密碼，我們會再重新驗證您的修改設定，以確認是否為您本人所有。</p>
            </div>

            <div class="option">

              <form class="member_form">
                <ul class="login_member">
                  <li>
                    <label for="">EMAIL</label>
                    <input type="text" value="{{ $memberData['email'] }}" disabled="disabled">
                  </li>
                  <li>
                    <label for="">MOBILE</label>
                    <input type="number" value="{{ $memberData['mobile'] }}" disabled="disabled">
                  </li>
                  <li class="form_btn">
                    <!--變更郵件及手機號碼 變更密碼 按鈕-->
                    <a href="{{ ItemMaker::url('member/change/1') }}" class="black two">
                      <div>
                        <p>變更郵件及手機號碼</p>
                      </div>
                    </a>
                    <a href="{{ ItemMaker::url('member/change/2') }}" class="black two">
                      <div>
                        <p>變更密碼</p>
                      </div>
                    </a>
                  </li>
                </ul>
              </form>

            </div>

          </li>

          <!--Basic Information 您的基本資料-->
          <li class="list">
            
            <div class="detail">
              <p class="title">Basic Information</p>
              <p class="vice">您的基本資料</p>
              <p class="text">還沒設定你的生日嗎?</p>
              <p class="text">在生日當天登入網站，將獲得法蝶珠寶送給您的生日禮，快來設定喔。</p>
            </div>

            <div class="option">

              <form class="member_form">
                <ul class="login_member">
                  <li>
                    <label for="">NAME</label>
                    <input type="text" name="name" value="{{ $memberData['name'] }}" placeholder="請填寫您的姓名">
                  </li>
                  <li class="dropMenu" id="gender">
                    <label for="">GENDER</label>
                    <div class="menulist">
                      <span class="selectMenu arrow-icon"><p>@if($memberData['gender']=='1')紳士 @elseif($memberData['gender']=='2')淑女 @else 請選擇您的性別@endif</p></span>
                    </div>
                    <ul tabindex="1" class="dropMenu_box">
                        <li @if($memberData['gender']=='1') class="checked" @endif value="1">
                            <p>紳士</p>
                        </li>
                        <li @if($memberData['gender']=='2') class="checked" @endif value="2">
                            <p>淑女</p>
                        </li>
                    </ul>
                  </li>
                  <li class="calender">
                    <span class="arrow-icon"></span>
                    <label for="">BIRTHDAY</label>
                    <input type="text" name="birthday" value="@if($memberData['birthday']!='0000-00-00'){{ $memberData['birthday'] }}@endif" placeholder="請選擇您的出生日期" data-init-set="false" data-modal="true" data-min-year="1917" data-lock="to" data-large-default="true" data-large-mode="true" data-auto-lang="true" data-lang="zh" data-format="Y-m-d">
                    <!--data-lock="to" 不能選今日之後的日期 data-lock="from" 不能選擇今日之前的日期-->
                    <!--data-large-default="true" 預設為大月曆-->
                    <!--data-min-year="1917" 可選擇的最早年份-->
                    <!--data-modal="true" 月曆至中背景黑色-->
                    <!--data-init-set="false" 預設不會先顯示日期(原本會顯示今日日期)-->
                  </li>
                </ul>
              </form>

            </div>

          </li>

          <!--Shipping Address 您的收件地址-->
          <li class="list">
            
            <div class="detail">
              <p class="title">Shipping Address</p>
              <p class="vice">您的收件地址</p>
              <p class="text">設定您的收件地址，將可以加快您購物的速度喔。</p>
            </div>

            <div class="option">

              <form class="member_form">
                <ul class="login_member">
                  <li class="dropMenu" id="city">
                    <label for="">CITY</label>
                    <div class="menulist">
                      <span class="selectMenu arrow-icon"><p>請選擇您居住的縣市</p></span>
                    </div>
                    <ul tabindex="1" class="dropMenu_box">
                      @foreach($city as $value)
                        <li @if($memberData['city']==$value['id']) class="checked" title="{{ $value['title'] }}" @endif value="{{ $value['id'] }}"><p>{{ $value['title'] }}</p></li>
                      @endforeach
                    </ul>
                  </li>
                  <li>
                    <label for="">POSTAL CODE</label>
                    <input type="number" name="zip" value="{{ $memberData['zip'] }}" placeholder="請輸入您的郵遞區號">
                  </li>
                  <li>
                    <label for="">ADDRESS</label>
                    <input type="text" name="address" value="{{ $memberData['address'] }}" placeholder="請輸入您的住址">
                  </li>
                </ul>
              </form>

            </div>

          </li>

        </ul>

        <!--form_btn 跳頁 送出更新 離開設定 按鈕-->
        <div class="form_btn">
          <a href="javascript:void(0)" class="black two" onclick="mbSave('{{ ItemMaker::url('member/save') }}')">
            <div>
              <p>送出更新</p>
            </div>
          </a>
          <a href="{{ ItemMaker::url('member/guide') }}" class="gray two">
            <div>
              <p>離開設定</p>
            </div>
          </a>
        </div>

      </article>
      
    </article>

  </main>

@include($locale.'.include.footer')
  @section('script')
  <script src="/assets/js/vendor/jquery.nicescroll/jquery.nicescroll.js"></script>
  <script src="/assets/js/vendor/datedropper/datedropper.min.js"></script>
  <script src="/assets/js/member.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('#city .selectMenu p').text($('#city .checked').attr('title'));
    })
    function mbSave($url){
      $.ajax({
          type: 'GET',
          url: $url,
          data: { 
              name: $('input[name="name"]').val(),
              gender: $('#gender .checked').attr('value'),
              birthday: $('input[name="birthday"]').val(),
              city: $('#city .checked').attr('value'),
              zip: $('input[name="zip"]').val(),
              address: $('input[name="address"]').val()
          }, 
          dataType : 'text',

          success: function(result){
              alert('修改完成!');
              window.location.reload();
          },
          error: function(XMLHttpRequest, textStatus, errorThrown){
              console.log(textStatus);
              console.log(errorThrown);
          }
      });
    }
    function pick(){
      $('#filePicker').click();
    }
    $("#filePicker").on("change", function (){
        preview(this);
    })
    function preview(input) { 
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    function photoSubmit(){
      $('#photoForm').submit();
    }
    function delPhoto($url){
      alert($('#myPhoto').attr('src'));
      $.ajax({
        url:$url,
        dataType:'text',
        data:{
          photo:$('#myPhoto').attr('src')
        },
        type:'GET',
        success: function(result){
          if(result=='success'){
            window.location.reload();
          }else{
            alert(result);
          }
        },
        error: function(xhr,textStatus,errorThrown){
          console.log(textStatus);
          console.log(errorThrown);
        }

      })
    }
  </script>
  @stop

@stop