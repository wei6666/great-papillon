@extends('template')


@section('bodySetting', 'id="member" class="member member_address"')

@section('content')

@section('css')
    
    <link rel="stylesheet" href="/assets/css/member.css">
@stop
@include($locale.'.include.header')
<main>

    <article class="m_box">

      <!--標題-->
      <section class="_top">
        
        <div class="_title">
          <h2>ADDRESS BOOK</h2>
          <p>您的地址簿</p>
        </div>
    
      </section>

      <!--麵包屑-->
      <article class="detail_nav">
        
        <!--麵包屑-->
        <div class="series">
            <span><a href="{{ ItemMaker::url('member/guide') }}">會員中心</a></span>
            <span>您的地址簿</span>
        </div>

        <!--返回鍵-->
        <div class="_back">
          <a href="{{ ItemMaker::url('member/guide') }}">
            <span></span>
            <p>BACK</p>
          </a>
        </div>

      </article>

      <!---->
      <article class="box_1">

        <!--state_1 = 新增地址-->
        <!--state_2 = 既有地址-->
        <ul class="frame">
          <li style="height:433px" class="list state_1 btn_add open_btn">
            <div class="add_tetx">
              <span class="icon-bold-plus"></span>
              <h2>Add Address</h2>
              <p>新增地址</p>
            </div>
          </li>
          @foreach($add as $key => $value)
          <li class="list state_2">
            <form class="member_form block">

              <p class="title">{{ $value['title'] }}</p>
              <ul class="login_member">
                <li>
                  <label for="">MOBILE</label>
                  <p class="text">{{ $value['mobile'] }}</p>
                </li>
                <li>
                  <label for="">NAME</label>
                  <p class="text">{{ $value['name'] }}</p>
                </li>
                <li>
                  <label for="">ADDRESS</label>
                  <p class="text">{{ $value['zip'].' '.$value['relations']['City']['title'].$value['address'] }}</p>
                </li>
              </ul>
              <!--繼續 付款 按鈕-->
              <div class="form_btn">
                <a href="javascript:void(0)" class="gray two btn_delete open_btn" onclick="editAddr('{{ ItemMaker::url('member/ajax') }}','{{ $value['id'] }}',2)">
                  <div>
                    <p>刪除</p>
                  </div>
                </a>
                <a href="javascript:void(0)" class="black two btn_edit open_btn" onclick="editAddr('{{ ItemMaker::url('member/ajax') }}','{{ $value['id'] }}',1)">
                  <div>
                    <p>編輯</p>
                  </div>
                </a>
              </div>

            </form>
          </li>
          @endforeach
        </ul>

      </article>

      <!--紅利點數使用說明 bonus rule-->
      <article class="address_box">

        <section class="box">

          <!--關閉按鈕-->
          <div class="_close close_btn">
            <a href="javascript:void(0)">
              <p>CLOSE</p>
            </a>
          </div>

          <!--新增地址-->
          <section class="adbox adbox_1">
            <!--標題-->
            <section class="_top">
                
              <div class="_title">
                <h2>ADD ADDRESS</h2>
                <p>新增地址</p>
              </div>
            
            </section>

            <!--表格-->
            <form class="member_form" action="{{ ItemMaker::url('member/new_submit')}}" method='get' id='addrForm'>
              <ul class="adbox_form login_member">
                <li>
                  <label for="">ADDRESS BOOK</label>
                  <input type="text" class="required" name="New[title]" value="" placeholder="請輸入地址簿名稱">
                </li>
                <li>
                  <label for="">MOBILE</label>
                  <input type="number" class="required" name="New[mobile]" id="mobilecheck" value="" placeholder="請填寫您的手機號碼">
                </li>
                <li>
                  <label for="">NAME</label>
                  <input type="text" class="required" name="New[name]" value="" placeholder="請填寫您的姓名">
                </li>
                <li class="dropMenu">
                  <label for="">CITY</label>
                  <div class="menulist">
                    <span class="selectMenu arrow-icon"><p>請選擇您居住的縣市</p></span>
                  </div>
                  <ul tabindex="1" class="dropMenu_box sel_city">
                    @foreach($city as $value)
                    <li value="{{ $value['id'] }}">
                      <p>{{ $value['title'] }}</p>
                    </li>
                    @endforeach
                  </ul>
                </li>
                <input type="hidden" name="New[city]" value="">
                <li>
                  <label for="">POST CODE</label>
                  <input type="number" class="required" name="New[zip]" value="" placeholder="請輸入您的郵遞區號">
                </li>
                <li>
                  <label for="">ADDRESS</label>
                  <input type="text" class="required" name="New[address]" value="" placeholder="請輸入您的住址">
                </li>
              </ul>
            </form>
          
            <!--繼續 付款 按鈕-->
            <div class="form_btn">
              <a href="javascript:void(0)" class="gray two close_btn">
                <div>
                  <p>取消</p>
                </div>
              </a>
              <a href="javascript:void(0)" class="black two" id="newAddr">
                <div>
                  <p>新增</p>
                </div>
              </a>
            </div>
          </section>

          <!--編輯地址-->
          <section class="adbox adbox_2">
            <!--標題-->
            <section class="_top">
                
              <div class="_title">
                <h2>EDIT ADDRESS</h2>
                <p>編輯地址</p>
              </div>

            </section>

            <!--表格-->
            <form class="member_form" action="{{ ItemMaker::url('member/edit_submit')}}" method='get' id='editForm'>
              <input type="hidden" name="Edit[id]" value="">
              <ul class="adbox_form login_member">
                <li>
                  <label for="">ADDRESS BOOK</label>
                  <input type="text" class="required1" name="Edit[title]" value="">
                </li>
                <li>
                  <label for="">MOBILE</label>
                  <input type="number" class="required1" name="Edit[mobile]" value="" id="mobilecheck1">
                </li>
                <li>
                  <label for="">NAME</label>
                  <input type="text" class="required1" name="Edit[name]" value="">
                </li>
                <li class="dropMenu">
                  <label for="">CITY</label>
                  <div class="menulist" id="thisCity">
                    <span class="selectMenu arrow-icon"><p>請選擇您居住的縣市</p></span>
                  </div>
                  <ul tabindex="1" class="dropMenu_box sel_city1">
                    @foreach($city as $value)
                    <li value="{{ $value['id'] }}">
                      <p>{{ $value['title'] }}</p>
                    </li>
                    @endforeach
                  </ul>
                </li>
                <input type="hidden" class="required1" name="Edit[city]" value="">
                <li>
                  <label for="">POST CODE</label>
                  <input type="number" class="required1" name="Edit[zip]" value="">
                </li>
                <li>
                  <label for="">ADDRESS</label>
                  <input type="text" class="required1" name="Edit[address]" value="">
                </li>
              </ul>
            </form>

            <!--繼續 付款 按鈕-->
            <div class="form_btn">
              <a href="javascript:void(0)" class="gray two close_btn">
                <div>
                  <p>取消</p>
                </div>
              </a>
              <a href="javascript:void(0)" class="black two" id="editAddr">
                <div>
                  <p>更新</p>
                </div>
              </a>
            </div>
          </section>

          <!--刪除地址-->
          <section class="adbox adbox_3">

            <!--標題-->
            <section class="_top">
                
              <div class="_title">
                <h2>REMOVE ADDRESS</h2>
                <p>刪除地址</p>
              </div>
            
            </section>

            <!--表格-->
            <form class="member_form" action="{{ ItemMaker::url('member/del_submit')}}" method='get' id='delForm'>
              <input type="hidden" name="Del_id" id="Del_id" value="">
              <ul class="adbox_form login_member">
                <li>
                  <label for="">ADDRESS BOOK</label>
                  <p class="text" id="Del_title"></p>
                </li>
                <li>
                  <label for="">MOBILE</label>
                  <p class="text" id="Del_mobile"></p>
                </li>
                <li>
                  <label for="">NAME</label>
                  <p class="text" id="Del_name"></p>
                </li>
                <li class="dropMenu">
                  <label for="">CITY</label>
                  <p class="text" id="Del_city"></p>
                </li>
                <li>
                  <label for="">POST CODE</label>
                  <p class="text" id="Del_zip"></p>
                </li>
                <li>
                  <label for="">ADDRESS</label>
                  <p class="text" id="Del_address"></p>
                </li>
              </ul>
            </form>
            
            <!--繼續 付款 按鈕-->
            <div class="form_btn">
              <a href="javascript:void(0)" class="gray two close_btn">
                <div>
                  <p>取消</p>
                </div>
              </a>
              <a href="javascript:void(0)" class="black two" id="delAddr">
                <div>
                  <p>確定刪除</p>
                </div>
              </a>
            </div>

          </section>

        </section>

      </article>
      
    </article>

  </main>
@include($locale.'.include.footer')
  @section('script')
  <script src="/assets/js/vendor/datedropper/datedropper.min.js"></script>
  <script src="/assets/js/member.js"></script>
  <script type="text/javascript">
    $('#newAddr').on('click',function(){
        var warning = 0;
        var cellphone = /^09[0-9]{8}$/;
        //檢查input類型必填欄位
        $(".required").each(function(){
            var data = $(this);
            if(data.val().length==0){
                warning++;
            }
        });
        if (warning!=0){
          alert('您資料尚未完成填寫');
          return;
        }else if(!cellphone.test($("#mobilecheck").val())){
          alert('手機號碼格式錯誤');
          return;
        }else{
          $('.sel_city li').each(function(){
            if($(this).hasClass('checked')){
              $('input[name="New[city]"]').val($(this).attr('value'));
            }
          });
          $('#addrForm').submit();
        }
    });
    $('#editAddr').on('click',function(){
        var warning = 0;
        var cellphone = /^09[0-9]{8}$/;
        //檢查input類型必填欄位
        $(".required1").each(function(){
            var data = $(this);
            if(data.val().length==0){
                warning++;
            }
        });
        if (warning!=0){
          alert('您資料尚未完成填寫');
          return;
        }else if(!cellphone.test($("#mobilecheck1").val())){
          alert('手機號碼格式錯誤');
          return;
        }else{
          $('.sel_city1 li').each(function(){
            if($(this).hasClass('checked')){
              $('input[name="Edit[city]"]').val($(this).attr('value'));
            }
          });
          $('#editForm').submit();
        }
    });
    $('#delAddr').on('click',function(){
        $('#delForm').submit();
    });
    
    function editAddr($url,$id,$type){
      $.ajax({
        url:$url,
        data:{
          id:$id
        },
        dataType:'json',
        type:'get',
        success: function(result){
          if($type==1){
            $('input[name="Edit[id]"]').val($id);
            $('input[name="Edit[title]"]').val(result.title);
            $('input[name="Edit[mobile]"]').val(result.mobile);
            $('input[name="Edit[name]"]').val(result.name);
            $('input[name="Edit[zip]"]').val(result.zip);
            $('input[name="Edit[address]"]').val(result.address);
            $('#thisCity p').text(result.city);
            $('input[name="Edit[city]"]').val(result.city_id);
          }else if($type==2){
            $('#Del_id').val($id);
            $('#Del_title').text(result.title);
            $('#Del_mobile').text(result.mobile);
            $('#Del_name').text(result.name);
            $('#Del_zip').text(result.zip);
            $('#Del_address').text(result.address);
            $('#Del_city').text(result.city);            
          }
        },
        error: function(xhr,textStatus,errorThrown){
          console.log(textStatus);
          console.log(errorThrown);
        }
      });
    }
  </script>
  @stop
@stop