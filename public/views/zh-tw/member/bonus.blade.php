@extends('template')


@section('bodySetting', 'id="member" class="member member_bonus"')

@section('content')

@section('css')
    
    <link rel="stylesheet" href="/assets/css/member.css">
@stop
@include($locale.'.include.header')
  <main>

    <article class="m_box">

      <!--標題-->
      <section class="_top">
        
        <div class="_title">
          <h2>BONUS POINTS</h2>
          <p>紅利點數管理</p>
        </div>
    
      </section>

      <!--麵包屑-->
      <article class="detail_nav">
        
        <!--麵包屑-->
        <div class="series">
            <span><a href="{{ ItemMaker::url('member/guide') }}">會員中心</a></span>
            <span>紅利點數管理</span>
        </div>

        <!--返回鍵-->
        <div class="_back">
          <a href="{{ ItemMaker::url('member/guide') }}">
            <span></span>
            <p>BACK</p>
          </a>
        </div>

      </article>

      <!---->
      <article class="box_1">

        <section class="b_point">
          <p class="big_point">{{ number_format($tempusedbonuscount) }} BP</p>
          <p class="change">1BP＝1元</p>
          <p class="info">YOUR TOTAL POINTS</p>
        </section>

        <section class="use_information">
          <p class="date">使用期限 : {{ date('Y / m / d', strtotime('+3 year',strtotime($Member['last_get']))) }}</p>
          <p class="info">若紅利點數在使用期限前未使用完畢，將作歸零動作，為了避免您的權益受損，請記得期限前使用完畢，如對於使用規則有疑問，可以瀏覽紅利點數使用說明</p>
          <!--使用說明按鈕-->
          <div class="form_btn">
            <a href="javascript:void(0)" class="black one open_btn">
              <div>
                <p>紅利點數使用說明</p>
              </div>
            </a>
          </div>
        </section>

      </article>

      <!---->
      <article class="box_2">

        <!--會員生日禮 配 color_1--><!--購買回饋 配 color_2--><!--訂單折抵 配 color_3-->
        <!--好友推薦   配 color_4--><!--首次消費 配 color_5--><!--會員註冊 配 color_6--><!--使用期限到期 配 color_7-->
        <ul class="frame">
        @foreach($Bonus as $key => $value)
          <li>
            <div class="out_circle point color_{{ $value['sel_type'] }}">
              <div class="in_circle">
                <p @if($value['sel_type']!=7) class="@if($value['sel_type']==3) minus @else plus @endif" @endif>{{ abs($value['bonus']) }}<span>BP</span></p>
              </div>
            </div>
            <div class="block">
              <p class="date">{{ date('Y/m/d', strtotime($value['created_at'])) }}</p>
              <p class="title">{{ $bonusType[$value['sel_type']] }}</p>
              <p class="info">{{ $value['detail'] }}</p>
            </div>
          </li>
        @endforeach
        </ul>
        
      </article>

      <!--紅利點數使用說明 bonus rule-->
      <article class="use_box">

        <section class="box">

          <!--關閉按鈕-->
          <div class="_close close_btn">
            <a href="javascript:void(0)">
              <p>CLOSE</p>
            </a>
          </div>

          <!--標題-->
          <section class="_top">
              
            <div class="_title">
              <h2>BONUS RULE</h2>
              <p>紅利點數使用說明</p>
            </div>
          
          </section>

          <!--說明清單-->
          <ul class="frame">
            @foreach($BonusRule as $key => $value)
            <li>
              <a href="javascript:void(0)">
                <div class="title">
                  <p class="num">{{ sprintf("%02d",$key+1) }}.</p>
                  <p class="text">{{ $value['title'] }}</p>
                </div>
                <div class="content">
                  <p>{!! str_replace("\r\n", "<br>", $value['content']) !!}</p>
                </div>
              </a>
            </li>
            @endforeach
          </ul>

        </section>

      </article>
      
    </article>

  </main>
@include($locale.'.include.footer')
  @section('script')

  <script src="/assets/js/member.js"></script>
  <script type="text/javascript">

  </script>
  @stop

@stop