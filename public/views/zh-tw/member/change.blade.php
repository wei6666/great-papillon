@extends('template')

@if($type==1)
  @section('bodySetting', 'id="member" class="member memberJoin_frame"')
@else
  @section('bodySetting', 'id="member" class="member memberPassword_frame"')
@endif
@section('content')

@section('css')

    <link rel="stylesheet" href="/assets/css/member.css">
@stop

  <main>

    <!--登入會員與加入會員 member login and join box-->
    <article class="m_box" style="background-image: url(/upload/f2e/member/member-bg2.jpg);">

      <div class="m_box_inner">

        <!--關閉按鈕-->
        <div class="_close">
          <!--如果是從member_account過來的 這個close要返回去member_account的頁面-->
          <a href="javascript:void(0)" onclick="history.back();">
            <p>CLOSE</p>
          </a>
        </div>
        <!--會員帳號設定 從member_account的 變更郵件及手機號碼 按鈕連接過來時 請後端將此區塊顯示-->
        <section class="member_login frame_box @if(Session::get('getcode')=='true' || Session::get('success')=='true' ) not_show @endif" @if($type!=1) style="display:none" @endif>
            
          <!--標題-->
          <section class="_top">
              
            <div class="_title">
              <h2>ACCOUNT</h2>
              <p>帳號設定</p>
            </div>

          </section>

          <!--提醒訊息-->
          <div class="information">
            <p>請重新輸入您的會員資料電話以及者電子郵件帳號，我們將為您調整設定。</p>
          </div>

          <!--會員註冊表單-->
          <form class="member_form" action="{{ ItemMaker::url('member/change_submit') }}" method="get" id="changeForm">
            <ul class="login_member">
              <li>
                <label for="">MOBILE</label>
                <input type="number" name="Change[mobile]" value="" id="mobilecheck" placeholder="請填寫您的手機號碼">
              </li>
              <li>
                <label for="">EMAIL</label>
                <input type="email" name="Change[email]" value="" id="mailcheck" placeholder="請填寫您的電子郵件">
              </li>
              <li class="member_verification">
                @if($type==1) 
                <input type="text" name="captcha" value="" placeholder="請輸入驗證碼">
                <div class="f_code_box">
                  <div id="changeCap" style="width: 70px; height: 35px">{!! Captcha::img() !!}</div>
                </div>
                @endif
              </li>
            </ul>
          </form>

          <!--form_btn 送出修改-->
          <div class="form_btn">
            <a href="javascript:void(0)" class="next_content_btn black one" id="mbChange">
              <div>
                <p>確認送出</p>
              </div>
            </a>
          </div>

        </section>

        <!--會員密碼設定 從member_account的 變更密碼 按鈕連接過來時 請後端將此區塊顯示-->
        <section class="member_login frame_box @if(Session::get('getcode')=='true' || Session::get('success')=='true' ) not_show @endif" @if($type!=2) style="display:none" @endif>
            
          <!--標題-->
          <section class="_top">
              
            <div class="_title">
              <h2>ACCOUNT</h2>
              <p>密碼設定</p>
            </div>

          </section>

          <!--提醒訊息-->
          <div class="information">
            <p>請重新輸入您的會員密碼，我們將重新驗證您的會員身份並更新您的設定。</p>
          </div>

          <!--會員登入表單-->
          <form class="member_form" action="{{ ItemMaker::url('member/change_pass') }}" method="get" id="changePass">
            <ul class="login_member">
              <li>
                <label for="">PASSWORD</label>
                <input type="password" name="Pass[now]" value="" placeholder="請輸入您目前使用的舊密碼">
              </li>
              <li>
                <label for="">NEW PASSWORD</label>
                <input type="password" name="Pass[new]" value="" id="passcheck" placeholder="請輸入英文及數字組成之8個字元新密碼">
              </li>
              <li>
                <label for="">CHECK</label>
                <input type="password" name="Pass[new1]" value="" placeholder="請再次輸入您的新密碼">
              </li>
              <li class="member_verification">
                @if($type==2) 
                <input type="text" name="captcha" value="" placeholder="請輸入驗證碼">
                <div class="f_code_box" style="width: 20%; position: absolute; right: 0px;">
                    <div id="changeCap" >{!! Captcha::img() !!}</div>
                </div>
                @endif
              </li>
            </ul>
          </form>

          <!--form_btn 送出修改-->
          <div class="form_btn">
            <a href="javascript:void(0)" class="next_content_btn black one" id="change_pass">
              <div>
                <p>送出修改</p>
              </div>
            </a>
          </div>

        </section>

        <!--身分驗證區塊-->
        <section class="member_identity frame_box  @if(Session::get('getcode')!='true') not_show @endif">

          <!--標題-->
          <section class="_top">
              
            <div class="_title">
              <h2>IDENTITY VERIFICATION</h2>
              <p>會員身分驗證</p>
            </div>
        
          </section>

          <!--提醒訊息-->
          <div class="information">
            <p>親愛的會員您好，為了避免您的身分被盜用，我們會通過簡訊發送五位數驗證碼到您的電話{{ Session::get('mobile') }}，請於五分鐘內完成驗證，若您尚未收到驗證簡訊，請重新發送驗證碼，或者回到註冊畫面確認您輸入的資料是否正確。</p>
          </div>

          <!--驗證碼區塊-->
          <form class="member_form" style="margin-bottom:20px;" action="{{ ItemMaker::url('member/code_submit')}}" method='get' id='codeForm'>
            <ul id="vcode" class="enter_verification">
              <li>
                <input type="text" maxlength="1" size="1" min="0" max="9" name="Vcode[no1]" pattern="[0-9]{1}">
              </li>
              <li>
                <input type="text" maxlength="1" size="1" min="0" max="9" name="Vcode[no2]" pattern="[0-9]{1}">
              </li>
              <li>
                <input type="text" maxlength="1" size="1" min="0" max="9" name="Vcode[no3]" pattern="[0-9]{1}">
              </li>
              <li>
                <input type="text" maxlength="1" size="1" min="0" max="9" name="Vcode[no4]" pattern="[0-9]{1}">
              </li>
              <li>
                <input type="text" maxlength="1" size="1" min="0" max="9" name="Vcode[no5]" pattern="[0-9]{1}" onblur="chkCode('{{ ItemMaker::url('member/chk_code')}}')">
              </li>
            </ul>
          </form>

          <!--驗證碼勾勾 與 倒數計時器-->
          <div class="tick_countdown">

            <!--一開始都不顯示-->
            <div class="tick">

              <!--驗證碼輸入正確 加上show(class) 顯示icon-check-->
              <span class="icon-check"></span>

              <!--驗證碼輸入錯誤 加上show(class) 顯示icon-x-->
              <span class="icon-x"></span>

            </div>

            <div class="countdown">
              <p data-m="5"></p>
              <p data-s="00"></p>
            </div>

          </div>

          <!--icon_btn_group 確認會員資料 與 重新發送-->
          <div class="icon_btn_group">
            <a href="javascript:void(0)" class="back_content_btn">
              <div>
                <span class="icon-slick-arrow-left black"></span>
                <p>確認會員資料</p>
              </div>
            </a>
            <a href="{{ ItemMaker::url('member/change_submit?resend=true')}}" class="resend">
              <div>
                <span class="icon-refresh gray"></span>
                <p>重新發送</p>
              </div>
            </a>
          </div>

          <!--form_btn 完成認證-->
          <div class="form_btn">
            <a href="javascript:void(0)" class="open_success black one disabled">
              <div>
                <p>完成認證</p>
              </div>
            </a>
          </div>

        </section>

        <!--驗證成功區塊-->
        <section class="member_success  @if(Session::get('success')!='true') not_show @endif">

          <div class="box" style="background-image: url(/upload/f2e/member/success-bg.jpg);">

            <!--標題-->
            <section class="_top">
                
              <div class="_title">
                <h2>SUCCESSFUL</h2>
                <p>會員驗證成功</p>
              </div>
          
            </section>

            <!--提醒訊息-->
            <div class="information">
              @if($type==1)
              <p>親愛的會員您好，恭喜您的身分驗證成功，您的手機號碼以及電子郵件已經完成更新，要請您以新的帳號密碼重新登入會員中心。</p>
              @elseif($type==2)
              <p>親愛的會員您好，恭喜您的身分驗證成功，您的密碼已經完成更新，要請您以新的帳號密碼重新登入會員中心。</p>
              @endif
              {{-- <p>親愛的會員您好，恭喜您的身分驗證成功，我們已經將您的登入密碼重置，並將新的密碼發送簡訊到您的手機，建議您登入會員之後更改您的登入密碼，祝您購物於快。</p> --}}
            </div>

            <!--form_btn 立即登入 跳頁-->
            <div class="form_btn">
              <a href="javascript:void(0)" class="black one" onClick="reg_success('{{ ItemMaker::url('member/success') }}','{{ ItemMaker::url('member') }}')">
                <div>
                  <p>立即登入</p>
                </div>
              </a>
            </div>

          </div>

        </section>
      </div>

    </article>

  </main>

@stop
@section('script')

  <script src="/assets/js/member.js"></script>
  <style type="text/css">
    a.disabled {
       pointer-events: none;
    }
  </style>
  <script type="text/javascript">
    $(document).ready(function(){
      /*驗證碼點擊更換*/
      var cap = document.querySelector('div[id="changeCap"]');

      cap.addEventListener("click", function () {
          var d = new Date();
              this.innerHTML=this.innerHTML.replace("{{ $locale }}" ,"");
              this.innerHTML=this.innerHTML.replace("/1" ,"");
              this.innerHTML=this.innerHTML.replace("/member/change" ,"");
              img = this.childNodes[0];

          img.src = img.src.split('?')[0] + '?t=' + d.getMilliseconds();

      });
      $('#changeCap').click();
      $('#changeCap').click();
      $('#changeCap').click();

      if($('.member_identity').hasClass('not_show')){

      }else{
        var time=<?php echo (strtotime(date('Y-m-d H:i:s'))-strtotime(Session::get('submitTime')))?>;
        console.log(time);
        var min=5-Math.floor(time/60);
        var sec=60-time%60;
        console.log(min);
        console.log(sec);
        if(min<0){
          $.ajax({
              type :  'get',
              url :  window.location.href+'/timeUp' ,
              async :false,
              success :  function(result) {
                alert('驗證碼已過期,請重新輸入資料!');
                window.location.reload();
              },
              error : function(xhr,textStatus,errorThrown){
                console.log(textStatus);
                console.log(errorThrown);
              }
            });
        }else{
          $('.countdown p').eq(0).attr('data-m', min);
          $('.countdown p').eq(1).attr('data-s', sec);
          strat_countdown();
        }
      }

  });
  /*******送出手機email********/
  $('#mbChange').on('click',function(){
        var mail = /^\w+((-\w+)|(.\w+))\@[A-Za-z0-9]+((.|-)[A-Za-z0-9]+).[A-Za-z0-9]+$/;
        var cellphone = /^09[0-9]{8}$/;

        if(!cellphone.test($("#mobilecheck").val())){
          alert('手機號碼格式錯誤');
        }else if (!mail.test($("#mailcheck").val())){
          alert('電子郵件格式錯誤');
        }else{
          $('#changeForm').submit();
        }
  });
  /*******送出密碼*******/
  $('#change_pass').on('click',function(){
    var passCheck = /^([a-zA-Z]+\d+|\d+[a-zA-Z]+)[a-zA-Z0-9]*$/;
      if(!$('input[name="Pass[now]"]').val()){
        alert('請輸入你目前使用的舊密碼');
      }else if(!$('input[name="Pass[new]"]').val()){
        alert('請輸入新密碼');
      }else if(!$('input[name="Pass[new1]"]').val()){
        alert('請確認新密碼');
      }else if(!$('input[name="captcha"]').val()){
        alert('請輸入驗證碼');
      }else if(!passCheck.test($("#passcheck").val())){
        alert('請輸入英文及數字組成之8個字元新密碼');
      }else{
        $('#changePass').submit();
      }
  });
  /*****輸完驗證碼移開滑鼠判斷*****/
  function chkCode(s_url){
    $.ajax({
      type :  'get',
      url :  s_url ,
      async :false,
      data :{
        Vcode1:$('input[name="Vcode[no1]"]').val(),
        Vcode2:$('input[name="Vcode[no2]"]').val(),
        Vcode3:$('input[name="Vcode[no3]"]').val(),
        Vcode4:$('input[name="Vcode[no4]"]').val(),
        Vcode5:$('input[name="Vcode[no5]"]').val(),
      },
      success :  function(result) {
        if(result!='false'){
          $('.icon-x').removeClass('show');
          $('.icon-check').addClass('show');
          $('.open_success').removeClass('disabled');
          $('.member_success .information p ').text(result);

        }else{
          $('.icon-x').addClass('show');
          $('.icon-check').removeClass('show');
          $('.open_success').addClass('disabled');
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown){
          console.log(textStatus);
          console.log(errorThrown);
      }
    });
  }
  /*****點選完成驗證*****/
  $('.open_success').on('click',function(){
      if($('.open_success').hasClass('disabled')){
        alert('身分驗證碼錯誤');
      }else{
        $('#codeForm').submit();
      }
  });
  /*******傳送成功刪session*******/
    function reg_success($url,$go){
        $.ajax({
            url: $url,
            success: function(result){
                window.location.href=$go;
            },
            error: function(xhr,textStatus,errorThrown){
                console.log(xhr);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    }
  </script>
@stop