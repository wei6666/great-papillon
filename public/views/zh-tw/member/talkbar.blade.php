@foreach($talkdata as $value)
<li class="talk_list @if($value->speaker=="member") client @else 'official' @endif">
                
              <div class="identity">

                <p class="who">@if($value->speaker=="member") 會員 @else 小幫手 @endif</p>
                <p class="date">
				<?php
					$date = new DateTime($value->time);
				 ?>

                	{{ $date->format('Y/m/d') }}</p>
              </div>

              <div class="text">
                <p class="hello">{!! $value->content !!}</p>
              </div>
</li>
@endforeach