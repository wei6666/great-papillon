
@extends('template')

    <!--RWD Setting-->
@section('css')
    
    
    <link rel="stylesheet" href="/assets/css/member.css">
@stop
@section('content')
@include($locale.'.include.header')

  <main>

    <article class="m_box">

      <!--標題-->
      <section class="_top">
        
        <div class="_title">
          <h2>ORDER HISTORY</h2>
          <p>購買紀錄</p>
        </div>
    
      </section>

      <!--麵包屑-->
      <article class="detail_nav">

        <!--麵包屑-->
        <div class="series">
            <span><a href="{{ ItemMaker::url('member/guide') }}">會員中心</a></span>
            <span>購買紀錄</span>
        </div>

        <!--返回鍵-->
        <div class="_back">
          <a href="{{ ItemMaker::url('member/guide') }}">
            <span></span>
            <p>BACK</p>
          </a>
        </div>

      </article>

      <!--訂單框-->
      <article class="work_1">

        <!--訂單表-->
        <section class="orderReturn_box">

          <!--共有8種狀態 1:完成訂單  2:退換貨審核中  3:取消訂單  4:已到貨  5:已出貨  6:訂單成立  7:未完成付款  8:出貨準備中-->
          <ul class="orderReturn_table">
            <?php $allc=count($Member_OrderList);?>
            @foreach($Member_OrderList as $key => $value) 

            <li class="orderReturn_list">

              <!--紀錄順序號碼 三種顏色狀態 紅:red 黑:(預設顏色) 灰:grizzle-->
              <?
              
              $color=[];
              $color[0]='red';
              $color[1]='red';
              $color[3]='black';
              $color[4]='black';
              $color[2]='black';
              $color[5]='grizzle';
              $color[7]='grizzle';
              $color[11]='grizzle';

              ?>
              <div class="big_num">
                <p class="@if(!empty($color[$value->order_list_type])) {{ $color[$value->order_list_type]}} @endif">@if($allc<10) {{ "0".$allc }}.</p> @else {{ $allc}}.</p> @endif
              </div>
              <?php $allc--;?>

              <!---->
              <ul class="box_1">
                <li>
                  <p class="title">訂單編號</p>
                  <p class="number">{{ $value->num_for_user }}</p>
                </li>
                <li>
                  <p class="title">訂購日期</p>
                  <p class="number">{{ $value->created_at }} </p>
                </li>
                <li>
                  <p class="title">交易序號</p>
                  <p class="number">{{ $value->TradeNo }}</p>
                </li>
                @if($value->order_list_type == 1)
                <li>
                  
                  <p class="warning @if($value->order_list_type==1) red @endif">未完成付款，1小時候將自動取消訂單，要立即付款嗎?</p>
                  
                </li>
                @endif

                @if($value->order_list_type == 1)
                <li>
                
                  <!--立即付款 按鈕-->
                @if((date('Y-m-d H:i:s', strtotime($value->created_at)+3600) > date("Y-m-d H:i:s")))
                <div class="form_btn">
                  <a href="{{ ItemMaker::url("topay/1/".$value->id) }}" class="black one">
                    <div>
                      <p>立即付款</p>
                    </div>
                  </a>
                </div>
                @endif
                 
                </li>
                @endif
                 @if($value->order_list_type !=1)
                <li>
                  <p class="title">配送狀態-貨號查詢</p>

                  @if(!empty($value->arrive_num ))
                  <p class="title">{{ $value->arrive_num }}22</p>
                  @else
                  <p>-</p>
                  @endif
                  @if(!empty($value->send_company ))
                  <p class="state">{{ $value->send_company }}11</p>
                  @else
                  <p>-</p>
                  @endif

                  
                </li>


                <li>
                  <p class="title">預定到貨日</p>
                  @if(!empty($value->arrive_date ))
                  <p class="title @if($value->RtnCode!=1) red @endif">{{ $value->arrive_date }}</p>
                  @else
                  <p>-</p>
                  @endif

                  @if(!empty($value->send_company ))
                  <p class="state @if($value->RtnCode!=1) red @endif">{{ $value->send_company}}</p>
                  @else
                  <p>-</p>
                  @endif
                </li>
                @endif
               
                <li>
                  <p class="title">訂單狀態</p>
                  <p class="state">{{ $order_type_arr[$value->order_list_type] }}</p>
                </li>
                
              </ul>

              <!---->
              <section class="box_2">

                <!---->
                <div class="payment">
                  @if($value->RtnCode != 1)
                  <p class="title">未付款</p>
                  @else
                  <p class="title">已付款</p>
                  @endif
                  <p class="money">NT${{ $value->TotalAmount }}</p>
                </div>
                
                <!--編輯 移除 按鈕-->
                <div class="form_btn">
                  @if(in_array($value->order_list_type , $cancancelarr))
                  <a href="{{ ItemMaker::url("member/cancel_list/".$value->id) }}" class="gray two">
                    <div>
                      <p>取消訂單</p>
                    </div>
                  </a>
                  @endif
                  <a href="{{ ItemMaker::url("member/orderdetail/".$value->id) }}" class="black two">
                    <div>
                      <p>訂單明細</p>
                    </div>
                  </a>
                </div>

              </section>

            </li>

            @endforeach


          </ul>

        </section>

      </article>
      
    </article>

  </main>

@include($locale.'.include.footer')
@section('script')  
<script src="/assets/js/member.js"></script>


@stop


@stop
