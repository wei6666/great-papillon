
@extends('template')

    <!--RWD Setting-->
@section('css')
    <link rel="stylesheet" href="/assets/css/member.css">
@stop

@section('content')
  <main>

    <article class="m_box">

      <!--關閉按鈕 返回member_order-->
      <div class="_close">
        <a href="{{ ItemMaker::url("member/guide") }}">
          <p>CLOSE</p>
        </a>
      </div>

      <!--標題-->
      <section class="_top">
          
          <div class="_title">
            <h2>ORDER DETAIL</h2>
            <p>訂單明細</p>
          </div>
      
      </section>
  
      <!--訂單nav-->
      <section class="order_nav">

        <p class="serial_number">訂單編號 : 201711270001</p>

        <!--按鈕-->
        <div class="form_btn">
          <a href="javascript:void(0)" class="black">
            <div>
              <p>購買明細</p>
            </div>
          </a>
          <a href="javascript:void(0)" class="gray">
            <div>
              <p>收件人 / 宅配 / 發票資訊</p>
            </div>
          </a>
          <a href="javascript:void(0)" class="gray">
            <div>
              <p>備註留言</p>
            </div>
          </a>
        </div>

      </section>
  
      <!--購買明細(預設show)-->
      <article class="order_nav_frame detail_content show">

        <!--商品明細-->
        <section class="order_nav_detail order_navBox">

          <!--title-->
          <div class="order_li_title">
            <p class="title">Product Details</p>
            <p class="vice">商品明細</p>
          </div>

          <!--列表-->
          <div class="order_list_box">
            <ul class="order_ul">
              
                @foreach($productdata as $value)
                
                <li class="order_li">
      
                  <!--圖 140*140-->
                  <div class="img box_1">
                    <picture>
                      <img class="b-lazy" src="/assets/img/blazy_blank.svg" data-src="/upload/f2e/member/detail-1.jpg" alt="">
                    </picture>
                  </div>
                  <!--資訊-->
                  <div class="info box_2">
                    <p class="s_number">{{$value->complete_num}}</p>
                    <p class="name">{{$value->en_title}}</p>
                    <p class="vice">{{$value->title}}</p>
                    <p class="size">{{$value->size}}</p>
                  </div>

                  <!--數量-->
                  <div class="quantity box_3">
                    <p>數量 : {{$value->count}}</p>
                  </div>

                  <!--金額-->
                  <div class="num_word box_4">
                    <p>NT$ {{$value->price * $value->count}}</p>
                  </div>                

                </li>
                @endforeach

              </ul>
          </div>

        </section>

        <!--優惠折抵-->
        <section class="order_nav_discount order_navBox">

          <!--title-->
          <div class="order_li_title">
            <p class="title">Discount</p>
            <p class="vice">優惠折抵</p>
          </div>

          <!--列表-->
          <div class="order_list_box">
            <ul class="order_ul">
              
              @if(!empty($Member_OrderList->DisCountPercentRecord))  
              <li class="order_li">
    
                <!--title-->
                <div class="title box_1">
                  <?php $DisCountPercentRecord=json_decode( $Member_OrderList->DisCountPercentRecord );
                  ?>
                  <p>{!! $DisCountPercentRecord->title !!}</p>
                </div>

                <!--notice-->
                <div class="notice box_2">
                  <p>@if(!empty($DisCountPercentRecord->content )){!! $DisCountPercentRecord->content !!} @endif</p>
                </div>

                <!--數量-->

                <div class="num_word box_3">
                  <p>-{!! ((1-$DisCountPercentRecord->percent) *100 )."%" !!}</p>
                </div>

                <!--金額-->
                <div class="num_word minus box_4">
                  <p>NT$ {{ json_decode($Member_OrderList->discount)->PercentDisCount }}</p>
                </div>                

              </li>
              @endif

              @if(!empty($Member_OrderList->DisCountFullRecord))  
              <li class="order_li">
              <?php $DisCountFullRecord=json_decode( $Member_OrderList->DisCountFullRecord );?>    
                <!--title-->
                <div class="title box_1">
                  <p>{!! $DisCountFullRecord->title !!}</p>
                </div>

                <!--notice-->
                <div class="notice box_2">
                  <p>@if(!empty( $DisCountFullRecord->content )){!! $DisCountFullRecord->content !!} @endif</p>
                </div>

                <!--數量-->
                <div class="num_word box_3">
                  <p>滿 {!! $DisCountFullRecord->full !!} 送 {!! $DisCountFullRecord->discount !!}</p>
                </div>

                <!--金額-->
                <div class="num_word minus box_4">
                  <p>NT$ {{ json_decode($Member_OrderList->discount)->FullDisCount }}</p>
                </div>                

              </li>
              @endif


              @if(!empty($Member_OrderList->DisCountCodeRecord))  
              <?php $DisCountCodeRecord=json_decode( $Member_OrderList->DisCountCodeRecord );?>    
              <li class="order_li">
                  
                <!--title-->
                <div class="title box_1">
                  <p>{{ $DisCountCodeRecord->title }}</p>
                </div>

                <!--notice-->
                <div class="notice box_2">
                  <p>{!! $DisCountCodeRecord->content !!}</p>
                </div>

                <!--數量-->
                <div class="num_word box_3">
                  <p>{{ $DisCountCodeRecord->code }}</p>
                </div>

                <!--金額-->
                <div class="num_word minus box_4">
                  <p>NT$ {{ json_decode($Member_OrderList->discount)->CodeDisCount }}</p>
                </div>                

              </li>
              @endif

              @if(!empty($Member_OrderList->DisCountBonusRecord))  
              <?php $DisCountBonusRecord=json_decode( $Member_OrderList->DisCountBonusRecord );?>    
              <li class="order_li">
                  
                <!--title-->
                <div class="title box_1">
                  <p>{{ $DisCountBonusRecord->title }}</p>
                </div>

                <!--notice-->
                <div class="notice box_2">
                  <p>{!! $DisCountBonusRecord->content !!}</p>
                </div>

                <!--數量-->
                <div class="num_word box_3">
                  <p>- {{ json_decode($Member_OrderList->discount)->BonusDisCount }} BP</p>
                </div>

                <!--金額-->
                <div class="num_word minus box_4">
                  <p>NT$ {{ json_decode($Member_OrderList->discount)->BonusDisCount }}</p>
                </div>                

              </li>
              @endif

              </ul>
          </div>

        </section>

        <!--紅利回饋-->
        <section class="order_nav_bonus order_navBox">

          <!--title-->
          <div class="order_li_title">
            <p class="title">Bonus</p>
            <p class="vice">紅利回饋</p>
          </div>

          <!--列表-->
          <div class="order_list_box">
            <ul class="order_ul">
              @if(!empty($friendfeedback))
              <?php $friendfeedback=json_decode( $Member_OrderList->friendfeedback );?>    
              <li class="order_li">
    
                <!--title-->
                <div class="title box_1">
                  <p>@if(!empty($friendfeedback))
                    {{ $friendfeedback->title }}</p>
                    @endif
                </div>

                <!--notice-->
                <div class="notice box_2">
                  <p>@if(!empty($friendfeedback))
                    {{ $friendfeedback->content }}</p>
                    @endif
                </div>

                <!--數量-->
                <div class="num_word box_3">
                  <p>@if(!empty($friendfeedback))
                    {{ $friendfeedback->percent }} %</p>
                    @endif
                </div>

                <!--金額-->
                <div class="num_word box_4">
                  <p>@if(!empty($friendfeedback))
                    {{ $friendfeedback->friendBP }} BP</p>
                    @endif
                </div>                

              </li>
              @endif

              <li class="order_li">
              <?php $feedback=json_decode( $Member_OrderList->feedback );?>     
                <!--title-->
                <div class="title box_1">
                  <p>{{ $feedback->title }}</p>
                </div>

                <!--notice-->
                <div class="notice box_2">
                  <p>{!! $feedback->content !!}</p>
                </div>

                <!--數量-->
                <div class="num_word box_3">
                  <p>{{ $feedback->percent }} %</p>
                </div>

                <!--金額-->
                <div class="num_word box_4">
                  <p>{{ $feedback->feedbackBP }} BP</p>
                </div>                

              </li>

              </ul>
          </div>

        </section>



        <section class="order_nav_bonus order_navBox">

          <!--title-->
          <div class="order_li_title">
            <p class="title">Shipping</p>
            <p class="vice">商品運送</p>
          </div>

          <!--列表-->
          <div class="order_list_box">
            <ul class="order_ul">
              
              <?php $friendfeedback=json_decode( $Member_OrderList->friendfeedback );
              $ShippingRecord=json_decode($Member_OrderList->ShippingRecord);
              ?>    
              <li class="order_li">
    
                <!--title-->
                <div class="title box_1">
                  <p>@if(!empty($ShippingRecord->title))
                    {{ $ShippingRecord->title }}</p>
                    @endif
                </div>

                <!--notice-->
                <div class="notice box_2">
                  <p>@if(!empty($ShippingRecord->content))
                    {!! $ShippingRecord->content !!}</p>
                    @endif
                </div>

                <!--數量-->
                <div class="num_word box_4">
                 
                </div>    

                <!--金額-->
                <div class="num_word box_4">
                  <p>@if(!empty($ShippingRecord->price))
                    {{ $ShippingRecord->price }} </p>
                    @endif
                </div>                

              </li>
              

          

              </ul>
          </div>

        </section>

        <!--商品總金額-->
        <section class="total">
          <div class="box">
            <p class="title">商品總金額</p>
            <p class="number">NT$ {{ $Member_OrderList->TotalAmount }}</p>
          </div>
        </section>

      </article>

      <!--收件人/宅配/發票資訊-->
      <article class="order_nav_info detail_content">

        <!--商品運送-->
        <section class="order_nav_shipping order_navBox">
          
          <!--title-->
          <?php  
            $shop_receiver=json_decode($Member_OrderList->shop_receiver);

            $ticket_receiver=json_decode($Member_OrderList->ticket_receiver);

            $ticket=json_decode($Member_OrderList->ticket);



          ?>
          <div class="order_li_title">
            <p class="title">Shipping</p>
            <p class="vice">商品運送</p>
          </div>

          <!--列表-->
          <div class="order_list_box">
            <ul class="order_ul">
                
                <li class="order_li">
      
                  <!--title-->
                  <div class="title box_1">
                    <p>宅配資訊</p>
                  </div>
  
                  <!--notice-->
                  <div class="notice box_2">
                    <p>宅配貨號 :     {{ $Member_OrderList->arrive_num }}</p>
                    <p>宅配公司 :     {{ $Member_OrderList->send_company }}</p>
                  </div>                

                </li>

                <li class="order_li">
                    
                  <!--title-->
                  <div class="title box_1">
                    <p>預定到貨日</p>
                  </div>
  
                  <!--notice-->
                  <div class="notice box_2">
                    <p>日期 :     {{ $Member_OrderList->arrive_date }}</p>
                    <p>時段 :     {{ $Member_OrderList->arrive_time_zone }}</p>
                  </div>                

                </li>

                <li class="order_li">
                    
                  <!--title-->
                  <div class="title box_1">
                    <p>收件人資訊</p>
                  </div>
  
                  <!--notice-->
                  @if(!empty($shop_receiver->name))
                  <div class="notice box_2">
                    <p>收件人姓名 :     {{ $shop_receiver->name }}</p>
                    <p>收件人住址 :     {{ $shop_receiver->zip }} {{ $shop_receiver->address }}</p>
                    <p>收件人電話 :     {{ $shop_receiver->mobile }}</p>
                    
                  </div>    
                  @endif            

                </li>

              </ul>
          </div>
          
        </section>

        <!--發票資訊-->
        <section class="order_nav_shipping order_navBox">
            
          <!--title-->
          <div class="order_li_title">
            <p class="title">Invoice</p>
            <p class="vice">發票資訊</p>
          </div>

          <!--列表-->
          <div class="order_list_box">
            <ul class="order_ul">
                
                <li class="order_li">
      
                  <!--title-->
                  <div class="title box_1">
                    <p>發票內容</p>
                  </div>
  
                  <!--notice-->
                  <div class="notice box_2">
                    <p>發票 :     {{ $ticket[2] }}</p>
                    <p>發票抬頭 :     {{ $ticket[1] }}</p>
                    <p>統一編號 :     {{ $ticket[0] }}</p>
                  </div>                

                </li>

                <li class="order_li">
                    
                  <!--title-->
                  <div class="title box_1">
                    <p>發票收件人</p>
                  </div>
  
                  <!--notice-->
                  @if(!empty($ticket_receiver->name))
                  <div class="notice box_2">
                    <p>收件人姓名 :     {{ $ticket_receiver->name }}</p>
                    <p>收件人住址 :     {{ $ticket_receiver->zip }} {{ $ticket_receiver->address }}</p>
                    <p>收件人電話 :     {{ $ticket_receiver->mobile }}</p>
                  </div>
                  @endif                

                </li>

              </ul>
          </div>
          
        </section>

      </article>

      <!--備註留言-->
      <article class="order_nav_message detail_content">

        <!---->
        <section class="order_nav_remarks order_navBox">

          <form>
            <textarea rows="" id='tosay' cols="" placeholder="這裡可以輸入您取消訂單的備註說明"></textarea>
            <div class="btn" id="sent" order_list_id="{{ $Member_OrderList->id }}">
              <span class="icon-enter_arrow"></span>
              <input type="button" value="" class="send">
            </div>
          </form>

        </section>

        <!--"小幫手"就在li上加official ， "會員"就在li上加client-->
        <section class="order_nav_talk order_navBox">

          <ul class="talk_box">

          </ul>
            
        </section>

      </article>
      
    </article>

  </main>

@section('script')

  <script src="/assets/js/member.js"></script>

@stop
@stop


