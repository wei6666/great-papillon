@extends('template')


@section('bodySetting', 'id="member" class="member memberLogin_frame"')

@section('content')

@section('css')
    
    <link rel="stylesheet" href="/assets/css/member.css">
@stop
@include($locale.'.include.header')

  <main>

    <article class="m_box" style="background-image: url(/upload/f2e/member/member-bg1.jpg);">

      <!--標題-->
      <section class="_top">
        
        <div class="_title">
          <h2>MEMBER CENTER</h2>
          <p>會員中心</p>
        </div>
    
      </section>

      <!--登入會員與加入會員 member login and join box-->
      <article class="memberLogin">
          
        <!--會員登入區塊-->
        <section class="member_login frame_box">

          <!--標題-->
          <article class="title">
            <h2 class="">會員登入</h2>
            <!-- <h3 class="cn_vice">會員服務</h3> -->
          </article>
        
          <!--會員登入表單-->
          <form class="member_form" action="{{ ItemMaker::url('member/login') }}" method="POST" id="loginForm">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <ul class="login_member">
              <li>
                <label for="">ACCOUNT</label>
                <input type="text" name="Login[account]" value="" placeholder="請輸入電子郵件">
              </li>
              <li>
                <label for="">PASSWORD</label>
                <input type="password" name="Login[password]" value="" placeholder="請輸入登入密碼">
              </li>
            </ul>
          </form>
        
          <!--form_btn 登入按鈕 跳頁-->
          <div class="form_btn">
            <a href="javascript:void(0)" class="black one" id="mbLogin">
              <div>
                <p>登入</p>
              </div>
            </a>
          </div>
        
          <!--會員認證按鈕-->
          <ul class="member_authen">
            <li>
              <a href="{{ ItemMaker::url('member/forget') }}">
                <p>忘記密碼?</p>
              </a>
            </li>
          </ul>

        </section>

        <!--會員註冊區塊-->
        <section class="member_join frame_box">

          <!--標題-->
          <article class="title">
            <h2 class="">您尚未成為會員?</h2>
          </article>

          <!--訊息-->
          <div class="message">
            
            <p>現在註冊成為法蝶珠寶會員，享有更多購物優惠。</p>

            <p>註冊入會即贈500元紅利金，消費再贈500紅利金！紅利金可全額折抵</p>

          </div>

          <!--form_btn 註冊 跳頁-->
          <div class="form_btn">
            <a href="{{ ItemMaker::url('member/register') }}" class="gray one">
              <div>
                <p>註冊</p>
              </div>
            </a>
          </div>

        </section>

      </article>
      
    </article>

  </main>

@include($locale.'.include.footer')
  @section('script')

  <script src="/assets/js/menu.js"></script>
  <script src="/assets/js/member.js"></script>
  <script type="text/javascript">
    $('#mbLogin').on('click',function(){
      if($('input[name="Login[account]"]').val()==''){
        alert('請輸入電子郵件');
        return;
      }else if($('input[name="Login[password]"]').val()==''){
        alert('請輸入登入密碼');
        return;
      }else{
        $('#loginForm').submit();
      }
      
    });
  </script>
  @stop

@stop