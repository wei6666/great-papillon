
@extends('template')

    <!--RWD Setting-->
@section('css')
     <link rel="stylesheet" href="/assets/css/member.css">
@stop
@section('content')
@include($locale.'.include.header')

  <main>

    <article class="m_box">

      <!--標題-->
      <section class="_top">
        
        <div class="_title">
          <h2>RETURN</h2>
          <p>退換貨紀錄</p>
        </div>
    
      </section>

      <!--麵包屑-->
      <article class="detail_nav">

        <!--麵包屑-->
        <div class="series">
          <span><a href="../member/member_guide.html">會員中心</a></span>
          <span>退換貨記錄</span>
        </div>

        <!--返回鍵-->
        <div class="_back">
          <a href="../member/member_guide.html">
            <span></span>
            <p>BACK</p>
          </a>
        </div>

      </article>

      <!--訂單框-->
      <article class="work_1">

        <!--訂單表-->
        <section class="orderReturn_box">

          <!--共有5種狀態 5:退貨進行中  4:換貨進行中  3:已換貨  2:退換貨審核中  1:已退貨-->
          <ul class="orderReturn_table">
            
            <!--5:退貨進行中-->
            <?php 

              $allc=count($Member_OrderList);
              $color=[];             
              
              $color[8]='black';
              $color[9]='black';
              $color[11]='black';
              $color[5]='grizzle';
              $color[7]='grizzle';
              

              ?>
            @foreach($Member_OrderList as $value)
            <li class="orderReturn_list">

              <!--紀錄順序號碼 三種顏色狀態 紅:red 黑:(預設顏色) 灰:grizzle-->
               <div class="big_num">
                <p class="@if(!empty($color[$value->order_list_type])) {{ $color[$value->order_list_type]}} @endif">@if($allc<10) {{ "0".$allc }}.</p> @else {{ $allc }}.</p> @endif
              </div>
              <?php $allc--;?>

              <!---->
              <ul class="box_1">
                
                <li>
                  <p class="title">訂單編號</p>
                  <p class="number">{{ $value->num_for_user }}</p>
                </li>
                <li>
                  <p class="title">訂購日期</p>
                  <p class="number">{{ $value->created_at }}</p>
                </li>
                <li>
                  <p class="title">交易序號</p>
                  <p class="none">{{ $value->TradeNo }}</p>
                </li>
                <li>
                  <p class="title">配送狀態-貨號查詢</p>
                  <p class="number">{{ $value->arrive_num }}</p>
                  <p class="state">{{ $value->send_company }}</p>
                </li>
                <li>
                  <p class="title">預定到貨日</p>
                  <p class="number">{{ $value->arrive_date }}</p>
                  <p class="state">{{ $value->arrive_time_zone }}</p>
                </li>
                <li>
                  <p class="title">訂單狀態</p>
                  <p class="state red">{{ $order_type_arr[$value->order_list_type] }}</p>
                </li>
               
              </ul>

              <!---->
              <section class="box_2">

                <!---->
                <div class="payment">
                  <p class="title">已付款</p>
                  <p class="money">NT {{ $value->TotalAmount}}</p>
                </div>
                
                <!--編輯 移除 按鈕-->
                <div class="form_btn">
                  <a href="{{ ItemMaker::url("member/orderdetail/".$value->id) }}" class="black two">
                    <div>
                      <p>訂單明細</p>
                    </div>
                  </a>
                </div>

              </section>

            </li>
            @endforeach
          </ul>

        </section>

      </article>
      
    </article>

  </main>

@include($locale.'.include.footer')
@section('script')  
<script src="/assets/js/member.js"></script>


@stop


@stop

