
@extends('template')

    <!--RWD Setting-->
@section('css')
    
    
    <link rel="stylesheet" href="/assets/css/member.css">
@stop
@section('content')
@include($locale.'.include.header')
  <main>

    <article class="m_box">

      <!--標題-->
      <section class="_top">
        
        <div class="_title">
          <h2>FAVORITE</h2>
          <p>我的最愛收藏</p>
        </div>
    
      </section>

      <!--麵包屑-->
      <article class="detail_nav">
        
        <!--麵包屑-->
        <div class="series">
            <span><a href="{{ ItemMaker::url('member/guide') }}">會員中心</a></span>
            <span>我的最愛收藏</span>
        </div>

        <!--返回鍵-->
        <div class="_back">
          <a href="{{ ItemMaker::url('member/guide') }}">
            <span></span>
            <p>BACK</p>
          </a>
        </div>

      </article>

      <!---->
      <article class="box_1">

        <!--img 280*280-->
        <ul class="frame">
          @foreach($ProductData as $value)
          
          <li class="list">
            <div class="block">

              <!--info 點擊連結至該商品產品頁-->
              <div class="info">
                <a href="{{ ItemMaker::url('product/'.$value['id']) }}">
                  <div class="_img">
                    <img class="b-lazy" src="{{$value['out_img']}}" alt="">
                  </div>
                  <div class="text">
                    <h2>{{ $value['en_title']}}</h2>
                    <h3>{{$value['title'] }}</h3>
                  </div>
                </a>
              </div>

              <!--link 瀏覽商品連結至該商品產品頁-->
              <div class="link">
                <a href="{{ ItemMaker::url('product/'.$value['id']) }}">
                  <p>瀏覽商品</p>
                </a>
                <a href="javascript:;" class="delete open_btn" en_title="{{ $value['en_title']}}" title="{{ $value['title']}}" link="{{ItemMaker::url('addfavorite/'.$value['id'].'?R=1')}}" img="{{ $value['out_img'] }}" price="NT$  {{ $value['price'] }}">
                  <p>移除收藏</p>
                </a>
              </div>

            </div>
          </li>
          @endforeach
        </ul>

      </article>

      <!--紅利點數使用說明 bonus rule-->
      <article class="delete_box">

        <section class="box">

          <!--關閉按鈕-->
          <div class="_close close_btn">
            <a href="javascript:void(0)">
              <p>CLOSE</p>
            </a>
          </div>

          <!--標題-->
          <section class="_top">
              
            <div class="_title">
              <h2>REMOVE</h2>
              <p>移除最愛</p>
            </div>
          
          </section>

          <!--ITEM-->
          <div class="block">

            <div class="info">
              <div class="_img">
                <img id='askimg' class="b-lazy" src="../../upload/f2e/product/product_series/product_img1_280_280.jpg" alt="">
              </div>
              <div class="text">
                <h2 id='askentitle'>LOVE BARCELONA</h2>
                <h3 id='asktitle' >愛在巴塞隆納系列</h3>
                <p id="askprice" class="price">NT$ 32,000</p>
              </div>
            </div>

          </div>


          <!--繼續 付款 按鈕-->
          <div class="form_btn">
            <a id="asklink" href="javascript:void(0)" class="black two close_btn">
              <div>
                <p>確定</p>
              </div>
            </a>
            <a href="javascript:void(0)" class="gray two close_btn">
              <div>
                <p>我再想想</p>
              </div>
            </a>
          </div>

        </section>

      </article>
      
    </article>

  </main>
@include($locale.'.include.footer') 
@section('script')  
  

<!-- 共用end -->

<!-- 非共用 -->
<!-- 非共用end-->

  <script src="/assets/js/member.js"></script>
  
@stop

@stop


