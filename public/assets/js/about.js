var _Blazy = {
    aboutW: function() {

        if ($("body").hasClass("index")) {

            // 螢幕寬度700px以上
            if ($(window).width() > 700) {
                var bLazy = new Blazy({
                    offset: 1,
                    success: function(ele) {
                        $(ele).parent().parent().find(".row_img").addClass("show");
                        $(ele).parent().parent().find(".row_text").addClass("show");
                    }
                });
            }

            // 螢幕寬度小於700px
            if ($(window).width() < 701) {
                var bLazy = new Blazy({
                    offset: 1,
                    success: function(ele) {
                        $(ele).parent().parent().find(".row_img").addClass("show");
                        Waypoint.refreshAll();
                    }
                });
            }
        }


        // story內頁的
        if ($("body").hasClass("story")) {

            var bLazy = new Blazy({
                offset: 100,
                success: function(ele) {
                    Waypoint.refreshAll();
                }
            });
        }
    }
}







var _waypoint = {
    aboutW: function() {

        // 主頁
        if ($("body").hasClass("index")) {

            if ($(window).width() < 701) {
                $('.row>*').each(function(index, element) {
                    $(element).waypoint(function() {
                        $(element).addClass('show');
                    }, { offset: '70%' });
                });
            }
        }

        // story內頁
        if ($("body").hasClass("story")) {
            $('.origin>*').each(function(index, element) {
                $(element).waypoint(function() {
                    $(element).addClass('show');
                }, { offset: '70%' });
            });

            $('.protector>*').each(function(index, element) {
                $(element).waypoint(function() {
                    $(element).addClass('show');
                }, { offset: '70%' });
            });

            $('._sotry>*').each(function(index, element) {
                $(element).waypoint(function() {
                    $(element).addClass('show');
                }, { offset: '70%' });
            });

            $('.bookNow .logo').waypoint(function() {
                $('.bookNow .logo').addClass('show');
            }, { offset: '70%' });

            $('.bookNow .countup').waypoint(function() {
                $('.bookNow .countup').addClass('show');
                about.countup();
            }, { offset: '70%' });

            $('.bookNow ._btn').waypoint(function() {
                $('.bookNow ._btn').addClass('show');
            }, { offset: '80%' });
        }

        // design內頁
        if ($("body").hasClass("design")) {

            if ($(window).width() > 900) {
                $('.content_boxs>*').each(function(index, element) {
                    $(element).waypoint(function() {
                        $(element).addClass('show');
                    }, { offset: '50%' });
                });
            }

            if ($(window).width() < 901) {
                $('.content_box:nth-child(1)>*').each(function(index, element) {
                    $(element).waypoint(function() {
                        $(element).addClass('show');
                    }, { offset: '70%' });
                });

                $('.content_box:nth-child(2)>*').each(function(index, element) {
                    $(element).waypoint(function() {
                        $(element).addClass('show');
                    }, { offset: '70%' });
                });

                $('.content_box:nth-child(3)>*').each(function(index, element) {
                    $(element).waypoint(function() {
                        $(element).addClass('show');
                    }, { offset: '70%' });
                });
            }

            $('.designer').waypoint(function() {
                $('.designer').addClass('show');
            }, { offset: '60%' });

            $('.design ._slick').waypoint(function() {
                $('.design ._slick').addClass('show');
            }, { offset: '80%' });
        }

        // service內頁
        if ($("body").hasClass("service")) {

        	if ($(window).width() > 700) {
        		$('.service_content>*').each(function(index, element) {
                    $(element).waypoint(function() {
                        $(element).addClass('show');
                    }, { offset: '70%' });
                });
        	}

            if ($(window).width() < 701) {
                $('.content_box>*').each(function(index, element) {
                    $(element).waypoint(function() {
                        $(element).addClass('show');
                    }, { offset: '70%' });
                });
            }
        }

        // charity內頁
        if ($("body").hasClass("charity")) {

        	$('.box1').waypoint(function() {
                $('.box1').addClass('show');
            }, { offset: '60%' });

            $('.box2 img').waypoint(function() {
                $('.box2 img').addClass('show');
            }, { offset: '60%' });

            $('.box2 p').waypoint(function() {
                $('.box2 p').addClass('show');
            }, { offset: '80%' });
        }
    }
}


var about = {
    bannerScrollDown: function() {
        $("._banner_srcollDown").click(function() {
            $("html,body").animate({
                scrollTop: $("._banner").height() - $("header").height()
            }, 1000, "easeOutQuad")
        });
    },

    // 出現預約鑑賞
    scrollback: function() {
        var lastscrollY = 0;

        $(window).scroll(function() {

            var nowscrollY = Math.round($(this).scrollTop())

            var scrollBottom = $("body").prop("scrollHeight") - $(window).height();

            // 向上滑動時出現
            if (nowscrollY < lastscrollY) {
                $("._reservation").addClass("_hideUp");
            }

            // 到底部時出現
            else if (nowscrollY === scrollBottom) {
                $("._reservation").addClass("_hideUp");
            }

            // 向下滑動時消失
            else {
                $("._reservation").removeClass("_hideUp");
            }

            lastscrollY = nowscrollY;

        });
    },

    countup: function() {
        // 動畫設定值
        var options = {  
            useEasing: true,
              useGrouping: true,
              separator: ',',
              decimal: '.',
        };

        // 使用說明new CountUp("myTargetElement",startNumber,endNumber,小數點後幾位,time,選擇功能)

        var numb = 500

        var demo = new CountUp('counter', 0, numb, 0, 3.5, options);
        if (!demo.error) {  
            demo.start();
        } else {  
            console.error(demo.error);
        }
    },

    // 幸福好評內頁 GO TOP
    gotop: function() {
        $("._goTop").click(function() {
            $("html,body").animate({
                scrollTop: 0,
            }, 800, 'easeInOutCubic');
        });
    },

    all: function() {
        // 主頁的js	
        if ($("body").hasClass("index")) {
            about.bannerScrollDown();
        }

        // 內頁共用js
        if ($("body").hasClass("detail")) {
            about.scrollback();
            about.gotop();
        }

        // story內頁的js 
        if ($("body").hasClass("story")) {

        }
    }
}


$(document).ready(function() {

    var page = $('body').attr('id');


    if (page == "about") {

        if ($("body").hasClass("index")) {
            /*ajax header*/
            header();

            /*ajax footer*/
            footer();
        }

        /*slick*/
        slick("aboutSlick");

        /*waypoint*/
        _waypoint.aboutW();

        /* news*/
        about.all();

        /* b-lazy */
        _Blazy.aboutW();
    }
});