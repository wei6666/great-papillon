(function() {

	var handleContact = function() {

		var _this = this;
			_this._token = document.querySelector("input[name='_token']").value;
			_this.submitBtn = document.querySelector('.submitBtn');
			_this.clearBtn = document.querySelector('.clearBtn');
			_this.contactUsForm = document.getElementById('contactUsForm');
			_this.selects = $('select.required');
			_this.inputs = document.querySelectorAll('#contactUsForm input.required');
			_this.textarea = document.querySelector('#contactUsForm textarea.required');
			_this.alertMessage = document.querySelector('input.alertMessage').value;
	

		handleContact.prototype.init = function() {

			_this.submitAndCheckInputs();
			_this.changeCaptcha();
			_this.clearForm();

		};

		/*檢查填入的資料*/
		this.submitAndCheckInputs = function() {

			_this.submitBtn.addEventListener('click', function() {

	            /*避免重複一直按*/
	            if( this.className.indexOf('clicked') > -1 )
	            {
	              return false;
	            }


	            var notFilled = [],//未填寫的項目
	                radioName = '';


	                /*先處理inputs*/
	                  console.log(1);
	                [].slice.call( _this.inputs ).forEach( function(input) {
	                    if( input.getAttribute('type') == 'text' && input.value == '' ) //input[text]
	                    {
	                    	console.log(2);
	                    	console.log(  _this.inputs);
	                        var item = input.parentElement.parentElement.querySelector('h3').textContent.replace(/　/g, "");
	                        console.log(item);
	                        window.event.returnValue=true;
	                        notFilled.push( item );
	                    }
	                    else if( input.getAttribute('type') == 'radio' ) //input[radio]
	                    {
	                    	console.log(3);
	                        var checked = false,
	                            name = input.getAttribute('name'),
	                            radios = contactUsForm.querySelectorAll('input[name="'+name+'"]'),
	                            item = '';

	                            [].slice.call( radios ).forEach( function(radio) {
	                                if( radio.checked === true )
	                                {
	                                    checked = true;
	                                }
	                                else
	                                {
	                                    item = radio.parentElement.parentElement.querySelector('h3').textContent;
	                                }
	                            });

	                            if( checked === false && radioName != name )
	                            {
	                                  notFilled.push( item );
	                                  radioName = name;
	                            }
	                         console.log(item);   
	                    }
	                });


	                /*先處理select*/
	                _this.selects.each(function(index, select) {

	                	var val = $(this).val();

	                	if( val.length == 0 )
	                	{
	                		notFilled.push( $(this).data('info') );
	                	}

	                });


	                /*再處理textarea*/
	                if( _this.textarea.value == '' )
	                {
	                  	notFilled.push( _this.textarea.getAttribute('data-info') );
	                }

	            if( notFilled.length != 0 )
	            {	/*回傳訊息 或 顯示未填選的項目提示字*/
	            	alert( _this.alertMessage + notFilled);
	            }
	            else
	            {	/*送出表單*/
	            	_this.submitForm();

	            }

			});

		};





		/*表單送出*/
		_this.submitForm = function() {


    	  var btnOriginText = _this.submitBtn.textContent;
    	  this.submitBtn.classList.add('clicked');//避免重複點擊發送
    	  this.submitBtn.textContent = "LOADING...";

    	  var formWithData = _this.contactUsForm;
          var ajaxUrl = formWithData.getAttribute('action');
          console.log(ajaxUrl);
          //AJAX送出表單
          var xhttp = new XMLHttpRequest();

			xhttp.onreadystatechange = function() {
				if( xhttp.readyState == 4 && xhttp.status == 200 )
				{
					var data = JSON.parse( xhttp.responseText );

					//回傳訊息
					alert( data.message );

					//表單清除
					if( data.formPass === true )
					{
						formWithData.reset();
						_this.submitBtn.textContent = btnOriginText;
					}
					else
					{
						//重填驗證碼
						document.getElementById('changeCap').click();
						_this.submitBtn.classList.remove('clicked');
						_this.submitBtn.textContent = btnOriginText;
						inputCapcha = document.querySelector('input[name="captcha"]');
						inputCapcha.value = '';
						inputCapcha.focus();
					}

				}
			}

			xhttp.open("POST", ajaxUrl);
			xhttp.send( new FormData(formWithData) );



		};



		/*驗證碼點擊更換*/
		_this.changeCaptcha = function() {

			var cap = document.querySelector('div[id="changeCap"]');
			cap.addEventListener("click", function () {

			    var d = new Date(),
			        img = this.childNodes[0];

			    img.src = img.src.split('?')[0] + '?t=' + d.getMilliseconds();

			});

		};



		/*清除表單*/
		_this.clearForm = function() {

			_this.clearBtn.addEventListener('click', function() {
				_this.contactUsForm.reset();
			});

		};



	}





	var handleContactInstance = new handleContact();
	handleContactInstance.init();



})();