//照片lightBox Slick
	
	$('.laborSlid li').on( 'click' , function(){  
		$.swpmodal({
			type: 'ajax',
			url: '../../views/load/photo.html',  
			afterLoadingOnShow: function (){  
				var $slickElement = $('.photoSlide');
				$slickElement.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide) {
				    //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
				    var i = (currentSlide ? currentSlide : 0) + 1;
				    $('.pageCircle .top').text(i);
				    $('.pageCircle .bottom').text(slick.slideCount);
				});
			 	$('.photoSlide').attr('data-slick','true').slick({
					pauseOnFocus: false,
					pauseOnHover: false,
					slidesToShow: 1,
  					slidesToScroll: 1,
					infinite: true,
					autoplay: true,
					autoplaySpeed: 3600,
					speed: 500,
					arrows: true,
					dots: false, 
					appendArrows: $('.photo_ar'),
					prevArrow: $('.photo_arL'),
					nextArrow: $('.photo_arR'),	 				
				});	
			},
			afterClose: function(){ 
				$('.photoSlide').slick('unslick');
			},
		}); 
	});