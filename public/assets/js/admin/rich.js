var base_url = $('base').attr('href');

function fadeIn(el) {

	el.style.opacity = 0;

	var tick = function() {
	    el.style.opacity = +el.style.opacity + 0.02;

	    if (+el.style.opacity < 1) {
	      (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 3)
	    }
	};

	tick();
}


var detectBrowserVersion = function() {

	var ua = navigator.userAgent;

	if( ua.indexOf("MSIE") > -1 )
	{
		var rvIndex = ua.indexOf("MSIE");
		var versionString = ua.substring( rvIndex+5, rvIndex+8 );
		var bVersion = Number( versionString.split(".")[0] );
	}
	else
	{
		var bVersion = "NOT IE 9";
	}

	return bVersion;
}



var revertUrlToTitle = function(url) {

    var replace1 = url.replace('+', ' ');
    var replace2 = replace1.replace('^', '/');
    var replace3 = replace2.replace('`', '.');

    return replace3;
}


function htmlDecode(input){
    var e = document.createElement('div');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}


function unique_keys(array) {
  var values = {};

  for(var i = 0; i < array.length; i++) {
    values[array[i]] = null;
  }
  return Object.keys(values);
}


function unique_reduce(array) {
  return array.reduce(function(ret, cur) {
    if(ret.indexOf(cur) === -1) ret.push(cur);
    return ret;
  }, []);
}


//不要直接連結進入內頁，而是另外開一個Div載入內頁
var loadPageInLightBox = function ( loadLink , newLinkState, afterLoadFunc, afterLoadParam, closePageFunc )
{

    var $loadDiv = $('.lightSide'),
        originLink = location.href,
        webTitle = document.title;

    var closePageInLightBox = function( webTitle, originLink ) {
        $loadDiv.empty().fadeOut();
        $('body').css('overflow','auto');
        window.history.pushState('' , webTitle, originLink);
    }


    $loadDiv.load( loadLink , function() { //載入頁面內容

        //load完畢後顯示
        $(this).fadeIn('fast');
        $(this).css('display','inline-block');
        $('body').css('overflow','hidden');
        window.history.pushState('' , webTitle, newLinkState);

        $(window).resize(); //避免一些slick套件的小問題

        //綁定一個關掉lightBox的事件
        $('.closePage, .closeXX').on('click', function() {
            closePageInLightBox( webTitle, originLink );
        });

        //萬一使用者按了瀏覽器的上一頁
        $(window).on('popstate', function() {
            closePageInLightBox( webTitle, originLink );
        });

        //afterLoadFunc
        afterLoadFunc();

        //可以捲動
        $('.lightSide').css('overflow-y','auto');
        $('.closePage').on('click', function() {
            $('.lightSide').css('overflow-y','hidden');
            closePageFunc();
        });


    });
}



function loadJS(url)
{
   var script = document.createElement('script');
   script.type = 'text/javascript';
   script.src = url;
   document.body.appendChild(script);
}