    function shareFacebook(title, link, caption, description, picture) {
        // var params = {
        //     method: 'feed',
        //     name: title,
        //     link: link,
        //     caption: caption,
        //     description: description,
        //     picture: picture
        // };

        // if(params.picture === '') {
        //     delete params.picture;
        // }

        // // console.log(params);
        // FB.ui(params);

        window.open( 'https://www.facebook.com/dialog/feed?app_id=1548581555432064&link='+link+'&picture='+ picture +'&name=Facebook%20Dialogs&caption='+ caption +'&description=' + description + '&redirect_uri=https://mighty-lowlands-6381.herokuapp.com/' );
    }

    function shareFB(link)
    {
        window.open(  'http://www.facebook.com/share.php?u='.concat(encodeURIComponent(link)) );
    }

    function shareTwitter(text, link) {

        text = stripsString(text);

        if( link == '' )
        {
            window.open("https://twitter.com/share?url=" + encodeURIComponent(location.href) + '&text=' + text );
        }
        else
        {
            window.open("https://twitter.com/share?url=" + encodeURIComponent(link) + '&text=' + text );
        }
    }

    function shareGoogle(link) {

        window.open("https://plus.google.com/share?url=".concat(encodeURIComponent(link)), "", "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600");
    }

    function sharePlurk() {
        window.open("http://www.plurk.com/?qualifier=shares&status=".concat(encodeURIComponent(location.href)).concat(" ").concat("&#40;").concat(encodeURIComponent(document.title)).concat("&#41;"));
    }

    function shareWechat( title, link, desc, imgLink ) {
        if (typeof WeixinJSBridge == "undefined")
                alert("請先通過微信搜索添加分享組件提供商友推為好友，通過微信分享文章");
        else
            WeixinJSBridge.invoke('shareTimeline', {    "title": title,    "link": link,    "desc": desc,    "img_url": imgLink    });
    }


    function shareWeibo(title, imgLink) {
        window.open(  "http://service.weibo.com/share/share.php?appkey=&title=" + title + "&url=" + encodeURIComponent(location.href) + "&pic=" + imgLink + "&searchPic=false&style=simple"  );
    }


    function shareLine(link)
    {
        window.open(  "http://line.naver.jp/R/msg/text/?".concat(encodeURIComponent(link))  );
    }


    function sharePinterest()
    {
        window.open(  'http://pinterest.com/pin/create/button/?url='.concat(encodeURIComponent(location.href))  );
    }


    function shareTumblr()
    {
        //如果是首頁的話
        if( location.pathname.split('/')[3] == 'undefined' || location.pathname == '' )
        {   //首頁的大倉動態分享，因為網址狀態不會改變，所以加這一段
            window.open(  'http://tumblr.com/widgets/share/tool?canonicalUrl='.concat(encodeURIComponent(link)) );
        }
        else
        {
            window.open(  "http://tumblr.com/widgets/share/tool?canonicalUrl=".concat(encodeURIComponent(location.href))  );
        }
    }




    function stripsString(s) {
    var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】『；：」「'。，、？]")
    var rs = "";
    for (var i = 0; i < s.length; i++) {
    rs = rs + s.substr(i, 1).replace(pattern, '');
    }
    return rs;
    }
