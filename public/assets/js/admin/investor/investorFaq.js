//顯示更多AJAX
var loadmoreAjax = function( Total , dataId , nowhas){
	$.ajax({
		url: $('base').attr('href') + '/Investor/ajax/moreFaq',
		type: 'GET',
		dataType: 'html',
		data:
		{
            _token: $('input[name="_token"]').val(),
            dataId: dataId,
            nowhas: nowhas
        }
	})
	.done(function(data) {
		//console.log(data);
		$('tbody#faqtable').append(data);

		var dataId = [];//目前頁面上的ID

        $('tbody#faqtable').children('tr#table').each(function() {
        	dataId.push( $(this).data('id') );
        });

        var dataNowHas = dataId.length;//現在擁有幾筆資料
        if(Total <= dataNowHas)
        {
            $('a#moreBtn').hide();
        }
        $('a#moreBtn').removeClass('clicked');
        $('a#moreBtn').text("Load More");
	})
	.fail(function() {
		console.log("error");
	})
}

//關於顯示更多事件
$('a#moreBtn').on( 'click' , function(){
    $('a#moreBtn').prop("disabled", true);
	var Total = $('tbody#faqtable tr').data('total');
    var dataId = [];//目前頁面上之ID

    $('tbody#faqtable').children('tr#table').each(function() {
        dataId.push( $(this).data('id') );
    });
    var nowhas = dataId.length;
    if( $('a#moreBtn').hasClass('clicked') )
    {
        return false;
    }
    $('a#moreBtn').addClass('clicked');
    $('a#moreBtn').text("Loading...");
    
    loadmoreAjax(Total,dataId,nowhas);
});

//顯示或隱藏顯示更多按鈕
function showOrHideLoadMoreBtn () {
    var Total = $('tbody#faqtable tr').data('total');
    //console.log( Total );
     if(Total > 5)
     {
        $('a#moreBtn').fadeIn('fast');
     }
     else
     {
        $('a#moreBtn').hide();
     }
}

showOrHideLoadMoreBtn();