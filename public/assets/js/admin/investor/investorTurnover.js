//更改年份AJAX
var changeYear = function(yearNow){
	$.ajax({
		url: $('base').attr('href') + '/Investor/ajax/ChangeTurnoverYear',
		type: 'GET',
		dataType: 'html',
		data:
		{
            _token: $('input[name="_token"]').val(),
            yearNow: yearNow
        }
	})
	.done(function(data) {
		$('table.tb_style1').empty().html(data);
	})
	.fail(function() {
		console.log("error");
	})
}
//更改年份
$('a.yearSelect_arR').on( 'click' , function(){
	setTimeout(function(){ 
		var yearNow = $('ul.yearSelect li.slick-active').data('year');
		//console.log(yearNow);
		changeYear(yearNow);
	},100);
});
//更改年份
$('a.yearSelect_arL').on( 'click' , function(){
    setTimeout(function(){ 
		var yearNow = $('ul.yearSelect li.slick-active').data('year');
		//console.log(yearNow);
		changeYear(yearNow);
	},100);
});
