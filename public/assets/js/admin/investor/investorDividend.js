//顯示更多AJAX
var loadmoreDividendAjax = function( Total , dividendId , yearNow){
	$.ajax({
		url: $('base').attr('href') + '/Investor/moreDividend',
		type: 'GET',
		dataType: 'html',
		data:
		{
            _token: $('input[name="_token"]').val(),
            Total: Total,
            dividendId: dividendId,
            yearNow: yearNow
        }
	})
	.done(function(data) {
		//console.log(data);
		$('tbody#dividendtable').append(data);

		var dividendId = [];//目前頁面上的ID

        $('tbody#dividendtable').children('tr#dividendtable').each(function() {
        	dividendId.push( $(this).data('id') );
        });
        var AuthNowHas = dividendId.length;//現在擁有幾筆資料
        if(Total <= AuthNowHas)
        {
            $('a#moreDividend').hide();
        }
        $('a#moreDividend').removeClass('clicked');
        $('a#moreDividend').text("Load More");
	})
	.fail(function() {
		console.log("error");
	})
}
//更改年份AJAX
var changeDividendYearAjax = function(yearNow){
	$.ajax({
		url: $('base').attr('href') + '/Investor/changeDividendYear',
		type: 'GET',
		dataType: 'html',
		data:
		{
            _token: $('input[name="_token"]').val(),
            yearNow: yearNow
        }
	})
	.done(function(data) {
		$('table.tb_style3').empty().html(data);
		showOrHideDivddendLoadMoreBtn();
	})
	.fail(function() {
		console.log("error");
	})
}
//更改年份
$('a.yearSelect_arR').on( 'click' , function(){
	setTimeout(function(){ 
		var yearNow = $('ul.yearSelect li.slick-active').data('year');//取得目前
		//console.log(yearNow);
		changeDividendYearAjax(yearNow);
	},100);
});
//更改年份
$('a.yearSelect_arL').on( 'click' , function(){
    setTimeout(function(){ 
		var yearNow = $('ul.yearSelect li.slick-active').data('year');//取得目前
		//console.log(yearNow);
		changeDividendYearAjax(yearNow);
	},100);
});

//關於顯示更多事件
$('a#moreDividend').on( 'click' , function(){
    var Total = $('tbody#dividendtable tr').data('total');
    var dividendId = [];//目前頁面上之ID
    var yearNow = $('tbody#dividendtable tr').data('year')+1911;//目前為哪一年的分頁
    
    $('tbody#dividendtable').children('tr#dividendtable').each(function() {
        dividendId.push( $(this).data('id') );
    });
    //console.log(dividendId,Total,yearNow);
    if( $('a#moreDividend').hasClass('clicked') )
    {
        return false;
    }
    $('a#moreDividend').addClass('clicked');
    $('a#moreDividend').text("Loading...");
    loadmoreDividendAjax( Total , dividendId , yearNow );
});

//顯示或隱藏股利分派顯示更多按鈕
function showOrHideDivddendLoadMoreBtn () {
    var Total = $('tbody#dividendtable tr').data('total');
    //console.log( Total );
     if(Total > 8)
     {
        $('a#moreDividend').fadeIn('fast');
     }
     else
     {
        $('a#moreDividend').hide();
     }
}

showOrHideDivddendLoadMoreBtn();