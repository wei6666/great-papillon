//顯示更多AJAX
var loadmoreAjax = function( Total , dataId , yearNow , typeId , nowhas){
	$.ajax({
		url: $('base').attr('href') + '/Investor/ajax/moreReport',
		type: 'GET',
		dataType: 'html',
		data:
		{
            _token: $('input[name="_token"]').val(),
            dataId: dataId,
            yearNow: yearNow,
            typeId: typeId,
            nowhas: nowhas
        }
	})
	.done(function(data) {
		//console.log(data);
		$('tbody#table').append(data);

		var dataId = [];//目前頁面上的ID

        $('tbody#table').children('tr#table').each(function() {
        	dataId.push( $(this).data('id') );
        });
        var dataNowHas = dataId.length;//現在擁有幾筆資料
        $('#lbmsg').hide();
        $('a#moreBtn').fadeIn('fast');
        if(Total <= dataNowHas)
        {
            $('a#moreBtn').hide();
        }
        $('a#moreBtn').removeClass('clicked');
        $('a#moreBtn').text("Load More");
	})
	.fail(function() {
		console.log("error");
	})
}
//更改股東會年份AJAX
var changeYear = function(yearNow , typeId){
	$.ajax({
		url: $('base').attr('href') + '/Investor/ajax/ChangeYear',
		type: 'GET',
		dataType: 'html',
		data:
		{
            _token: $('input[name="_token"]').val(),
            yearNow: yearNow,
            typeId: typeId
        }
	})
	.done(function(data) {
		$('table.tb_style1').empty().html(data);
		showOrHideLoadMoreBtn();
	})
	.fail(function() {
		console.log("error");
	})
}
//更改年份
$('a.yearSelect_arR').on( 'click' , function(){
	setTimeout(function(){ 
		var yearNow = $('ul.yearSelect li.slick-active').data('year');//取得目前
		var typeId = $('tbody#table tr').data('type');
		//console.log(yearNow);
		changeYear(yearNow,typeId);
	},100);
});
//更改年份
$('a.yearSelect_arL').on( 'click' , function(){
    setTimeout(function(){ 
		var yearNow = $('ul.yearSelect li.slick-active').data('year');//取得目前
		var typeId = $('tbody#table tr').data('type');
		//console.log(yearNow);
		changeYear(yearNow,typeId);
	},100);
});

//關於顯示更多事件
$('a#moreBtn').on( 'click' , function(){
	var Total = $('tbody#table tr').data('total');
    var dataId = [];//目前頁面上之ID
    var yearNow = $('tbody#table tr').data('year');//目前為哪一年的分頁
    var typeId = $('tbody#table tr').data('type');

    $('tbody#table').children('tr#table').each(function() {
        dataId.push( $(this).data('id') );
    });
    var nowhas = dataId.length;
    if( $('a#moreBtn').hasClass('clicked') )
    {
        return false;
    }
    $('a#moreBtn').addClass('clicked');
    $('a#moreBtn').text("Loading...");
    loadmoreAjax(Total,dataId,yearNow,typeId,nowhas);
});

//顯示或隱藏顯示更多按鈕
function showOrHideLoadMoreBtn () {
    var Total = $('tbody#table tr').data('total');
    //console.log( Total );
     if(Total > 8)
     {
        $('a#moreBtn').fadeIn('fast');
     }
     else
     {
        $('a#moreBtn').hide();
     }
}

showOrHideLoadMoreBtn();