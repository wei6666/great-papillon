//顯示更多AJAX
var loadmoreAjax = function( Total , dataId , typeId , nowhas){
	$.ajax({
		url: $('base').attr('href') + '/Investor/ajax/moreContact',
		type: 'GET',
		dataType: 'html',
		data:
		{
            _token: $('input[name="_token"]').val(),
            dataId: dataId,
            typeId: typeId,
            nowhas: nowhas
        }
	})
	.done(function(data) {
		//console.log(data);
		$('tbody#contacttable').append(data);

		var dataId = [];//目前頁面上的ID

        $('tbody#contacttable').children('tr#table').each(function() {
        	dataId.push( $(this).data('id') );
        });
        var dataNowHas = dataId.length;//現在擁有幾筆資料
        if(Total <= dataNowHas)
        {
            $('a#moreBtn').hide();
        }
        $('a#moreBtn').removeClass('clicked');
        $('a#moreBtn').text("Load More");
	})
	.fail(function() {
		console.log("error");
	})
}

var loadmorecsrAjax = function( Total , dataId , typeId , nowhas){
    $.ajax({
        url: $('base').attr('href') + '/Investor/ajax/moreCsr',
        type: 'GET',
        dataType: 'html',
        data:
        {
            _token: $('input[name="_token"]').val(),
            dataId: dataId,
            typeId: typeId,
            nowhas: nowhas
        }
    })
    .done(function(data) {
        //console.log(data);
        $('tbody#csrtable').append(data);

        var dataId = [];//目前頁面上的ID

        $('tbody#csrtable').children('tr').each(function() {
            dataId.push( $(this).data('id') );
        });
        var dataNowHas = dataId.length;//現在擁有幾筆資料
        if(Total <= dataNowHas)
        {
            $('a#morecsrBtn').hide();
        }
        $('a#morecsrBtn').removeClass('clicked');
        $('a#morecsrBtn').text("Load More");
    })
    .fail(function() {
        console.log("error");
    })
}


//關於顯示更多事件
$('a#moreBtn').on( 'click' , function(){
	var Total = $('tbody#contacttable tr').data('total');
    var dataId = [];//目前頁面上之ID
    var typeId = $('tbody#contacttable tr').data('type');

    $('tbody#contacttable').children('tr#table').each(function() {
        dataId.push( $(this).data('id') );
    });
    var nowhas = dataId.length;
    if( $('a#moreBtn').hasClass('clicked') )
    {
        return false;
    }
    $('a#moreBtn').addClass('clicked');
    $('a#moreBtn').text("Loading...");
    loadmoreAjax(Total,dataId,typeId,nowhas);
});

$('a#morecsrBtn').on( 'click' , function(){
    var Total = $('tbody#csrtable tr').data('total');
    var dataId = [];//目前頁面上之ID
    var typeId = $('tbody#csrtable tr').data('type');

    $('tbody#csrtable').children('tr').each(function() {
        dataId.push( $(this).data('id') );
    });
    var nowhas = dataId.length;
    if( $('a#morecsrBtn').hasClass('clicked') )
    {
        return false;
    }
    $('a#morecsrBtn').addClass('clicked');
    $('a#morecsrBtn').text("Loading...");
    loadmorecsrAjax(Total,dataId,typeId,nowhas);
});

//顯示或隱藏顯示更多按鈕
function showOrHideLoadMoreBtn () {
    var Total = $('tbody#contacttable tr').data('total');
    //console.log( Total );
     if(Total > 8)
     {
        $('a#moreBtn').fadeIn('fast');
     }
     else
     {
        $('a#moreBtn').hide();
     }
}


//顯示或隱藏顯示更多按鈕
function showOrHideLoadMoreCsrBtn () {
    var Total = $('tbody#csrtable tr').data('total');
    //console.log( Total );
     if(Total > 8)
     {
        $('a#morecsrBtn').fadeIn('fast');
     }
     else
     {
        $('a#morecsrBtn').hide();
     }
}
showOrHideLoadMoreCsrBtn();
showOrHideLoadMoreBtn();