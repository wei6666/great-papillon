var NewsShare = function() {

	NewsShare.prototype.init = function() {

		$('.shareFbBtn').off().on('click', function() {

			var shareLink = $(this).data('link');
			shareFB( shareLink );

		});

		$('.shareLineBtn').off().on('click', function() {

			var shareLink = $(this).data('link');
			shareLine( shareLink );

		});


		$('.shareTwitterBtn').off().on('click', function() {

			var shareText = $(this).parents('.newsDetail').find('h4').text();
			var shareLink = $(this).data('link');

			shareTwitter( shareText,shareLink );

		});

	}

}


var NewsShareInstance = new NewsShare();
NewsShareInstance.init();