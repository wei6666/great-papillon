	$('ul.newsList li').off('click').on('click', function(e) {

		e.preventDefault();

		var newLinkState = $(this).find('a').attr('href'), //改變網址的連結狀態
			loadLink = newLinkState + " .detailLight-box", //載入頁面的網址
			afterLoadParam = {};

			afterLoadFunc = function() { //load完之後要特別做的事情

				$('.closePage').attr('href', 'javascript:;');

				$.getScript('assets/js/admin/clickToShare.js');

			};

			closePageFunc = function() { //當按下close關閉頁面的時候 特別要做的事情

			};

		loadPageInLightBox( loadLink , newLinkState , afterLoadFunc, afterLoadParam, closePageFunc );
	});