//讀取更多認證AJAX
var loadmoreTeammatesAjax = function( Total , IosId)
{
    $.ajax({
        url: $('base').attr('href') + '/About/iosajax',
        type: 'GET',
        dataType: 'html',
        data:
        {
            _token: $('input[name="_token"]').val(),
            IosId: IosId
        },
    })
    .done(function( data ) {
        $('ul.authList').append(data);

        var AuthId = [];//目前頁面上認證的ID

        $('div.front').children('div.frontInn').each(function() {
        	AuthId.push( $(this).data('id') );
        });
        var AuthNowHas = AuthId.length;//現在擁有幾筆資料
        if(Total <= AuthNowHas)
        {
            $('a#AuthMore').hide();
        }
        $('a#AuthMore').removeClass('clicked');
        $('a#AuthMore').text("Load More");
    })
    .fail(function() {
        console.log("error");
    });
}

//讀取更多成員AJAX
var loadmoreTeammatesAjax = function( Total , TeammatesId)
{
    $.ajax({
        url: $('base').attr('href') + '/About/teamajax',
        type: 'GET',
        dataType: 'html',
        data:
        {
            _token: $('input[name="_token"]').val(),
            TeammatesId: TeammatesId
        },
    })
    .done(function( data ) {
        $('ul.doctorList.fadeIn').append(data);

        var MatesId = [];//目前頁面上成員的ID

        $('div.doctorMask').children('div.circle').each(function() {
            MatesId.push( $(this).data('id') );
        });
        var MatesNowHas = MatesId.length;//現在擁有幾筆資料


        if(Total <= MatesNowHas)
        {
            $('a#teamMore').hide();
        }
        $('a#teamMore').removeClass('clicked');
        $('a#teamMore').text("Load More");
    })
    .fail(function() {
        console.log("error");
    });
}

//關於認證顯示更多事件
$('a#AuthMore').on( 'click' , function(){
    var Total = $('ul.authList li').data('total');
    var IosId = [];//目前頁面上成員的ID

    $('div.front').children('div.frontInn').each(function() {
        IosId.push( $(this).data('id') );
    });

    if( $('a#AuthMore').hasClass('clicked') )
	{
		return false;
	}
	$('a#AuthMore').addClass('clicked');
	$('a#AuthMore').text("Loading...");

    loadmoreTeammatesAjax( Total , IosId);

    //console.log( TeammatesId );

});
//認證照片lightBox Slick
$('.authList li').on( 'click' , function(){  

		var arrayId=[];//取得目前頁面上ID陣列
		var typeId = 3;//認證相簿類別
		var nowId = $(this).find('div.front div.frontInn').data('id');//取得目前ID

		$('div.front').children('div.frontInn').each(function() {
			//取得目前頁面上ID陣列
        	arrayId.push( $(this).data('id') );
    	});
    	//取得總數量
    	var Nowhas = arrayId.length;
    	//取得其順位
    	var count = 0;
    	for (var i = 0; i < Nowhas; i++) {
    		if (arrayId[i] == nowId) {
    			count = i;
    		} 
    	}
		$.swpmodal({
			type: 'ajax',
			url: $('base').attr('href') + '/About/photo/'+ typeId,  
			afterLoadingOnShow: function ( data ){
				// setTimeout(function () {

					//console.log( index );
					var $slickElement = $('.photoSlide');
					$slickElement.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide) {
					    //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
					    var i = (currentSlide ? currentSlide : 0) + 1;
					    $('.pageCircle .top').text(i);
					    $('.pageCircle .bottom').text(slick.slideCount);
					});
				 	$('.photoSlide').attr('data-slick','true').slick({
						pauseOnFocus: false,
						pauseOnHover: false,
						slidesToShow: 1,
	  					slidesToScroll: 1,
						infinite: true,
						autoplay: true,
						autoplaySpeed: 3600,
						speed: 500,
						arrows: true,
						dots: false, 
						appendArrows: $('.photo_ar'),
						prevArrow: $('.photo_arL'),
						nextArrow: $('.photo_arR'),	

					});	
					//跳至相簿相對應順位頁
					$('.photoSlide').slick('slickGoTo',count);
				// }, 1000);
			},

			beforeClose: function(){ 
				$('.photoSlide').slick('unslick');
			},
		}); 
	});

//獎項照片lightBox Slick	
	$('.cupBox').on( 'click' , function(){  
		var typeId = 2;//獎項類別
		var nowNo = $(this).index();//取得目前順位
		//console.log( nowNo );
		$.swpmodal({
			type: 'ajax',
			url: $('base').attr('href') + '/About/photo/'+ typeId,  
			afterLoadingOnShow: function (){  
				var $slickElement = $('.photoSlide');
				$slickElement.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide) {
				    //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
				    var i = (currentSlide ? currentSlide : 0) + 1;
				    $('.pageCircle .top').text(i);
				    $('.pageCircle .bottom').text(slick.slideCount);
				});
			 	$('.photoSlide').attr('data-slick','true').slick({
					pauseOnFocus: false,
					pauseOnHover: false,
					slidesToShow: 1,
  					slidesToScroll: 1,
					infinite: true,
					autoplay: true,
					autoplaySpeed: 3600,
					speed: 500,
					arrows: true,
					dots: false, 
					appendArrows: $('.photo_ar'),
					prevArrow: $('.photo_arL'),
					nextArrow: $('.photo_arR'),	 				
				});	
				//跳至相簿相對應順位頁
				$('.photoSlide').slick('slickGoTo',nowNo);
			},
			afterClose: function(){ 
				$('.photoSlide').slick('unslick');
			},
		}); 
	});

//檢測優勢照片lightBox Slick
$('.advanList li').on( 'click' , function(){  
	var typeId = 1;//檢測類別
	var nowNo = $(this).index();//取得目前順位
	$.swpmodal({
		type: 'ajax',
		url: $('base').attr('href') + '/About/photo/'+ typeId,  
		afterLoadingOnShow: function (){  
			var $slickElement = $('.photoSlide');
			$slickElement.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide) {
				    //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
				var i = (currentSlide ? currentSlide : 0) + 1;
				$('.pageCircle .top').text(i);
				$('.pageCircle .bottom').text(slick.slideCount);
			});
			 $('.photoSlide').attr('data-slick','true').slick({
				pauseOnFocus: false,
				pauseOnHover: false,
				slidesToShow: 1,
  				slidesToScroll: 1,
				infinite: true,
				autoplay: true,
				autoplaySpeed: 3600,
				speed: 500,
				arrows: true,
				dots: false, 
				appendArrows: $('.photo_ar'),
				prevArrow: $('.photo_arL'),
				nextArrow: $('.photo_arR'),	 				
			});	
			//跳至相簿相對應順位頁
			$('.photoSlide').slick('slickGoTo',nowNo);
		},
		afterClose: function(){ 
			$('.photoSlide').slick('unslick');
		},
	}); 
});



function showOrHideIosLoadMoreBtn () {
    var Total = $('ul.authList li').data('total');
    //console.log( Total );
     if(Total > 8)
     {
        $('a#AuthMore').fadeIn('fast');
     }
     else
     {
        $('a#AuthMore').hide();
     }
}


//關於我們的成員顯示更多事件
$('a#teamMore').on( 'click' , function(){
    var Total = $('ul.doctorList.fadeIn li').data('total');
    var TeammatesId = [];//目前頁面上成員的ID

    $('div.doctorMask').children('div.circle').each(function() {
        TeammatesId.push( $(this).data('id') );
    });
    if( $('a#teamMore').hasClass('clicked') )
	{
		return false;
	}
	$('a#teamMore').addClass('clicked');
	$('a#teamMore').text("Loading...");
    loadmoreTeammatesAjax( Total , TeammatesId);

    //console.log( TeammatesId );

});
$('a#historytop').on('click',function(){
    $('.historyBtn.upBtn').click();
});



//關於我們的成員顯示更多按鈕顯示與隱藏
function showOrHideTeamLoadMoreBtn () {
    var Total = $('ul.doctorList.fadeIn li').data('total');
    //console.log( Total );
     if(Total > 6)
     {
        $('a#teamMore').fadeIn('fast');
     }
     else
     {
        $('a#teamMore').hide();
     }
}
showOrHideTeamLoadMoreBtn();
showOrHideIosLoadMoreBtn();
