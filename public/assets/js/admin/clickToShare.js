var ClickToShare = function() {

	ClickToShare.prototype.init = function() {

		$('.shareFbBtn').off().on('click', function() {

			var shareLink = $(this).data('link');
			shareFB( shareLink );

		});

		$('.shareLineBtn').off().on('click', function() {

			var shareLink = $(this).data('link');
			shareLine( shareLink );

		});


		$('.shareGoogleBtn').off().on('click', function() {

			var shareLink = $(this).data('link');
			shareGoogle( shareLink );

		});



		$('.shareTwitterBtn').off().on('click', function() {

			var shareText = $(this).parents('.newsDetail').find('h4').text();
			var shareLink = $(this).data('link');

			shareTwitter( shareText,shareLink );

		});

	}

}


var ClickToShareInstance = new ClickToShare();
ClickToShareInstance.init();