//照片lightBox Slick
var EquipPhoto = function() {

	EquipPhoto.prototype.init = function() {

		var EquipPhotoInstance = new EquipPhoto();
		EquipPhotoInstance.showOrHideLoadMoreBtn();
		EquipPhotoInstance.bindClickToShowPhoto();
		EquipPhotoInstance.loadMoreAjaxGetPhotos();

	},


	EquipPhoto.prototype.showOrHideLoadMoreBtn = function() {

		var nowHas = $('article.advGrid').length;
		var equipPhotosTotal = Number( document.querySelector('input[name="equipPhotosTotal"]').value );
		var loadMoreBtn = document.querySelector('.loadMoreBtn');


		if( nowHas == equipPhotosTotal )
		{
			loadMoreBtn.style.display = 'none';
		}
		else
		{
			loadMoreBtn.style.display = 'block';
		}

	},


	EquipPhoto.prototype.loadMoreAjaxGetPhotos = function() {

		var loadMoreBtn = document.querySelector('.loadMoreBtn');

		loadMoreBtn.addEventListener('click', function() {

			this.classList.add('clicked');
			var loadMoreBtnOriginText = this.textContent;
			this.textContent = 'Loading...';

			//蒐集目前頁面上的equipPhoto id
			var photoIds = [];

			$('.equipPhotos .advGrid').each(function(index, el) {
				photoIds.push( $(el).data('id') );
			});

			//取得此實驗室id
			var laboratoryId = document.querySelector('input[name="laboratoryId"]').value;

				// ajax取得html資料
				var xhttp = new XMLHttpRequest(),
				token = document.querySelector('input[name="_token"]').value,
				param = "photoIds=" + photoIds + "&laboratoryId=" + laboratoryId;

				xhttp.onreadystatechange = function() {
					if( xhttp.readyState == 4 && xhttp.status == 200 )
					{
						var data = xhttp.responseText;

						//塞入html
						$('.photoChunk:last').after( data );


						//重做特效
						var $grid = $('.advs').packery({
									        itemSelector: '.advGrid',
									        gutter: '.gutter',
									   	});

						    $grid.packery('reloadItems');

						    $grid.packery('layout');
						    	$(window).load(function() {
						        $grid.packery('layout');
						    });

						//解除loadMoreBtn鎖定
					    loadMoreBtn.classList.remove('clicked');
					    loadMoreBtn.textContent = loadMoreBtnOriginText;

					    //綁定事件
					    var EquipPhotoInstance = new EquipPhoto();
					    EquipPhotoInstance.bindClickToShowPhoto();
					    EquipPhotoInstance.showOrHideLoadMoreBtn();
					}
				}

				xhttp.open("POST", base_url + "/Laboratory/ajax/getEquipPhoto");
				xhttp.setRequestHeader("X-CSRF-Token", token);
				xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

				xhttp.send( param );

		});

	},



	EquipPhoto.prototype.bindClickToShowPhoto = function() {

		$('.equipPhotos .advGrid').off('click').on( 'click' , function(){

			var equipPhotoLink = document.querySelector('input[name="equipPhotoLink"]').value;

			var chunkIndex = $(this).parents('.photoChunk').index();
			var photoIndexInChunk = $(this).index();
			var photoIndex = chunkIndex*photoIndexInChunk;


			$.swpmodal({
				type: 'ajax',
				url: equipPhotoLink,
				afterLoadingOnShow: function (){
					var $slickElement = $('.photoSlide');
					$slickElement.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide) {
					    //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
					    var i = (currentSlide ? currentSlide : 0) + 1;
					    $('.pageCircle .top').text(i);
					    $('.pageCircle .bottom').text(slick.slideCount);
					});
				 	$('.photoSlide').attr('data-slick','true').slick({
						pauseOnFocus: false,
						pauseOnHover: false,
						slidesToShow: 1,
	  					slidesToScroll: 1,
						infinite: true,
						autoplay: true,
						autoplaySpeed: 3600,
						speed: 500,
						arrows: true,
						dots: false, 
						appendArrows: $('.photo_ar'),
						prevArrow: $('.photo_arL'),
						nextArrow: $('.photo_arR'),
					});

					$('.photoSlide').slick('slickGoTo', photoIndex-1);
				},
				afterClose: function(){
					$('.photoSlide').slick('unslick');
				},
			});


		});



	}

}


var EquipPhotoInstance = new EquipPhoto();
EquipPhotoInstance.init();