var loadMoreItems = function() {

	loadMoreItems.prototype.init = function() {

		var loadMoreItemsInstance = new loadMoreItems();
		loadMoreItemsInstance.checkShowOrHideLoadMoreBtn();
		loadMoreItemsInstance.bindLoadMoreToBtn();

	},

	loadMoreItems.prototype.bindLoadMoreToBtn = function() {

		var loadMoreItemsBtn = document.querySelector('.loadMoreItemsBtn');

		loadMoreItemsBtn.addEventListener('click', function() {

			/*蒐集目前有的ids*/
			var itemList = document.querySelectorAll('.productItemList li');
			var nowHas = [];

			[].forEach.call( itemList, function(li) {

				var itemId = li.getAttribute('data-id');
				nowHas.push( itemId );

			});


			/* ajax 取得資料 */
			var loadMoreItemsInstance = new loadMoreItems();
			loadMoreItemsInstance.ajaxGetItems( nowHas );

		});
	},

	loadMoreItems.prototype.ajaxGetItems = function( nowHas ) {

		var categoryId = Number(document.querySelector('input[name="categoryId"]').value);
		var param = '?itemIds='+nowHas+"&categoryId="+categoryId;
		var getItemsLink = document.querySelector('input[name="getItemsLink"]').value + param;


		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if( xhttp.readyState == 4 && xhttp.status == 200 )
			{
				var data = xhttp.responseText;

				$('ul.productItemList').append( data );

				var loadMoreItemsInstance = new loadMoreItems();
				loadMoreItemsInstance.checkShowOrHideLoadMoreBtn();
			}
		}

		xhttp.open("GET", getItemsLink, true );
		xhttp.send();

	},

	loadMoreItems.prototype.checkShowOrHideLoadMoreBtn = function() {

		var loadMoreItemsBtn = document.querySelector('.loadMoreItemsBtn');
		var totalItemInCategory = Number(document.querySelector('input[name=totalItemInCategory]').value);
		var nowHasTotal = document.querySelectorAll('.productItemList li').length;

		if( nowHasTotal == totalItemInCategory )
		{
			loadMoreItemsBtn.style.display = 'none';
		}
		else
		{
			loadMoreItemsBtn.style.display = 'block';
		}

	}

}

var loadMoreItemsInstance = new loadMoreItems();
loadMoreItemsInstance.init();