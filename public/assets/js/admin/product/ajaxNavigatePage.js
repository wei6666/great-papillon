var goBackLinks = [];
var ajaxNavigatePage = function() {


	/* 點擊 頁碼 */
	ajaxNavigatePage.prototype.clickPageNumber = function() {

		$('ul.pageNumberList li').off('click').on('click', function(e) {

			e.preventDefault();

			/*將一般的連結，換成取資料的連結*/
			var goBackLink = decodeURI(location.href);
			var newsLink = $(this).find('a').attr('href');
			var getNewsLink = newsLink.replace('\/article\/', '\/getArticles\/');

			var changePageNumbers = $(this).hasClass('morePages');

			/* ajax取資料 */
			var ajaxNavigatePageInstance = new ajaxNavigatePage();
				ajaxNavigatePageInstance.getData( $(this), newsLink, getNewsLink, goBackLink, changePageNumbers );

		});

	},


	/* ajax取得新聞資料 */
	ajaxNavigatePage.prototype.getData = function( $this, newsLink, getNewsLink, goBackLink, changePageNumbers ) {

			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
				if( xhttp.readyState == 4 && xhttp.status == 200 )
				{
					var data = JSON.parse(xhttp.responseText);

					/*更換html*/
					var ajaxNavigatePageInstance = new ajaxNavigatePage();
					ajaxNavigatePageInstance.changeNewsListDom( changePageNumbers, data );

					/*改變網址*/
					window.history.pushState('' , document.title, newsLink);

					/*紀錄上一頁的路徑*/
					if( $.inArray(goBackLink, goBackLinks) == -1 )
					{
						goBackLinks.push( goBackLink );
						ajaxNavigatePageInstance.goBackPage();
					}

					/*改變頁碼的狀態*/
					if( changePageNumbers == false )
					{
						$('ul.pageNumberList li').removeClass('on');
						$this.addClass('on');
					}
				}
			}

			xhttp.open("GET", getNewsLink, true );
			xhttp.send();
	},



	/* 更換html內容 */
	ajaxNavigatePage.prototype.changeNewsListDom = function( changePageNumbers, data ) {

			/*取得資料*/
			var domList = htmlDecode(data.domList),
				pageNation = htmlDecode(data.pageNation),
				pageNavigation = data.pageNavigation;

			/*更換新聞列表html*/
			$('.pageNavigateWrap').stop().fadeTo(0, 0).empty().html( domList ).fadeTo(500, 1);


			/*更換頁碼html*/
			if( changePageNumbers == true )
			{
				$('ul.pageNumberList').stop().fadeTo(0, 0).empty().html( pageNation ).fadeTo(500, 1);
			}

			/* 更換頁碼下拉選單的表頭html */
			// selectTile = 'TOTAL '+ pageNavigation['totalPage'] + ' PAGE ' + pageNavigation['currentPage'];
			// $('.pageList').find('span.selected').text( selectTile );
			// $('.pageList').find('ul li').removeClass('active');


			/*滾動到上面*/
			$('html, body').animate({
		        scrollTop: $('.pageNavigateTop').offset().top
		    }, 300);


			/*重新綁定，點擊頁碼ajax事件*/
			var ajaxNavigatePageInstance = new ajaxNavigatePage();
			ajaxNavigatePageInstance.clickPageNumber();
			ajaxNavigatePageInstance.loadPageInDiv();

	},


	/* 點擊上一頁 */
	ajaxNavigatePage.prototype.goBackPage = function() {

		//萬一使用者按了瀏覽器的上一頁
        $(window).on('popstate', function() {
        	var latestGoBackLink = goBackLinks[goBackLinks.length-1];
            window.location.href = latestGoBackLink;
        });

	},


	/* 選擇 頁碼下拉選單 */
	ajaxNavigatePage.prototype.selectPageList = function() {

			$('select.pageListSelect').easyDropDown({
				cutOff: 10,
				onChange: function(selected){

					/*將一般的連結，換成取資料的連結*/
					var goBackLink = decodeURI(location.href);
					var newsLink = selected.value;
					var getNewsLink = newsLink.replace('\/article\/', '\/getArticles\/');

					var changePageNumbers = true;

					/* ajax取資料 */
					var ajaxNavigatePageInstance = new ajaxNavigatePage();
						ajaxNavigatePageInstance.getData( $(this), newsLink, getNewsLink, goBackLink, changePageNumbers );

				}
			});

	},



	ajaxNavigatePage.prototype.loadPageInDiv = function() {

		$('ul.essayList li').off('click').on('click', function(e) {

			e.preventDefault();


			var newLinkState = $(this).find('a').attr('href'), //改變網址的連結狀態
				loadLink = newLinkState + " .detailLight-box", //載入頁面的網址
				afterLoadParam = {};

				afterLoadFunc = function() { //load完之後要特別做的事情

					$('.closePage').attr('href', 'javascript:;');

					$.getScript('assets/js/admin/clickToShare.js');

				};

				closePageFunc = function() { //當按下close關閉頁面的時候 特別要做的事情

				};

			loadPageInLightBox( loadLink , newLinkState , afterLoadFunc, afterLoadParam, closePageFunc );

		});
	}



}


var ajaxNavigatePageInstance = new ajaxNavigatePage();
ajaxNavigatePageInstance.clickPageNumber();
ajaxNavigatePageInstance.selectPageList();
ajaxNavigatePageInstance.loadPageInDiv();