(function() {

	var $selects = $('.dropdownLink');

	$selects.easyDropDown({
		cutOff: 10,
		onChange: function(selected){

			var goToLink = selected.value;
			window.location.href = goToLink;

		}
	});

})();
