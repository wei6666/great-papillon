$(document).ready( function(){

  //產品類別下拉黏在上
  $(window).scroll(function() {
    var productDetailSticky = $('.product-d-menu'),
        scroll = $(window).scrollTop();
    if (scroll >= 900) {
    productDetailSticky.addClass('menuFixed headerFadeIn');
    }else {
    productDetailSticky.removeClass('menuFixed headerFadeIn');
    }
  });

  //slick幻燈片
  $('.product-banner>.bannerIND').slick({
    slide: '.xsBGshow',
    dots: true,
    arrows: false,
    autoplay: true,
    infinite: true,
    autoplaySpeed: 5000,
    fade: true,
    cssEase: 'linear'
  });
  $('.product-d-banner').slick({
    slide: '.product-d-slick',
    dots: true,
    arrows: false,
    autoplay: true,
    infinite: true,
    autoplaySpeed: 5000,
    fade: true,
    cssEase: 'linear'
  });
  $('.product-d-vedio').slick({
    slide: '.vedio',
    centerMode: true,
    centerPadding: '0',
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    autoplay: true,
    infinite: true,
    autoplaySpeed: 4000,
    centerPadding: '20%',
    responsive: [
    {
      breakpoint: 1200,
      settings: {
        centerPadding: 0
      }
    }
    ]
  });
  $('.circle-slick').slick({
    slide: '.circle-slick>li',
    dots: false,
    arrows: true,
    autoplay: true,
    infinite: true,
    autoplaySpeed: 5000,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3
      }
    },
    {
      breakpoint: 640,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 450,
      settings: {
        slidesToShow: 1
      }
    },
  ]
  });

  //產品底圖位置
  $('.product-all>li:odd').find('.bg-gray').addClass('even-bg-gray');

  // select
  var _theselectbox=1;
  var _selectBtn=$(".jq-selectBtn");
  var _selectBox=$(".jq-selectBox");

  _selectBtn.on('click', function() {
    if(_theselectbox==1){
      _selectBox.fadeIn(300);
      _theselectbox=0;
    }else if(_theselectbox!=1){
      _selectBox.fadeOut(300);
      _theselectbox=1;
    }
  });

  // 設定顯示文字
  var _selectLi = $(".jq-selectBox li");
  _selectLi.on('click', function() {
      //抓取文字→設定顯示名稱
      var str = $(this).text();
      $(".jq-selectBtn").html(str);
  });

  // 初始預設
  var _strDefaul = _selectLi.first().text();
  $(".jq-selectBtn").html(_strDefaul);

  //區塊回收
  $(document).click(function(e){
    if(!_selectBox.is(e.target) && !_selectBtn.is(e.target) && _selectBox.has(e.target).length === 0){
      _selectBox.fadeOut(300);
      _theselectbox=1;
    }
  });

  

});
