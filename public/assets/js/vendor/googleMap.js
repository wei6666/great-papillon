$(window).load(function() {



    // Using GeoLocation API
    var cleancut = [{
        "stylers": [{
            "hue": "#ff1a00"
        }, {
            "invert_lightness": true
        }, {
            "saturation": -100
        }, {
            "lightness": 33
        }, {
            "gamma": 0.5
        }]
    }, {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [{
            "color": "#2D333C"
        }]
    }];



    $('#locaMap').tinyMap({


        'center': '台北市大安區新生南路三段2號',
        //'center': '台北車站',
        //'center': 'N48°45.5952  E20°59.976' , // WGS84 format

        'zoom': 18,
        //zoomControlOptions: {
        //    'position': 'RIGHT_TOP' // 將縮放拉桿放到右邊
        //},

        //'autoLocation': true,
        'styles': 'greyscale', // 'LARGE',
        //'styles': cleancut,
        'scrollwheel': false,

        marker: [{
            addr: '台北市大安區新生南路三段2號',
            text: '台北市大安區新生南路三段2號',
            zoom: 15,
            //label: '請按我',
            //css: 'labels',
            // 自訂 marker click 事件
            event: function(e) {
                alert(e.latLng);
            },

            // 自訂圖示
            icon: {
                url: '../../img/icon-map01.png',
                //size: [128, 128]
            },
            // 動畫效果BOUNCE
            animation: 'DROP'
        }, ]

    });



});