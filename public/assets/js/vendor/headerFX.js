var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight =500;

$(window).scroll(function(event){
    didScroll = true;
});

setInterval(function() {
    if (didScroll) {

        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();

    if(Math.abs(lastScrollTop - st) <= delta)
        return;

    if (st > lastScrollTop && st > navbarHeight){
        // Scroll Down
        $('.headerFx').removeClass('headerFxdown').addClass('headerFxup');//選單隱藏

        $('.middleMenu').removeClass('headerFadeInUp').addClass('menuFixed headerFadeIn');
        $('.middleMenu-Fin').removeClass('headerFadeInUp').addClass('menuFixed01 headerFadeIn');
    } else {
        // Scroll Up
        if(st + $(window).height() < $(document).height()) {
            $('.headerFx').removeClass('headerFxup').addClass('headerFxdown');//選單出現

            $('.middleMenu').removeClass('menuFixed headerFadeIn').addClass('headerFadeInUp');
            $('.middleMenu-Fin').removeClass('menuFixed01 headerFadeIn').addClass('headerFadeInUp');
        }
        if ( st == 0 ){
            $('.headerFx').addClass('disNone ');
            // console.log('st = 0')
        }else{
            $('.headerFx').removeClass('disNone ');
        }
    }
    
    lastScrollTop = st;
}