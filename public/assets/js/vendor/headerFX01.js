var lastScrollTop = pageYOffset || document.documentElement.scrollTop;

window.addEventListener("scroll", function() {
  var st = window.pageYOffset || document.documentElement.scrollTop;
  if (st > lastScrollTop) {


    // Scroll Down
    $('.headerFx').removeClass('headerFxdown headerFadeIn').addClass('headerFxup');//選單隱藏

    $('.middleMenu').removeClass('headerFadeInUp').addClass('menuFixed headerFadeIn');//選單出現
    $('.middleMenu-Fin').removeClass('headerFadeInUp').addClass('menuFixed01 headerFadeIn');


  } else {
    // Scroll Up
    $('.headerFx').removeClass('headerFxup').addClass('headerFxdown headerFadeIn');//選單出現

    $('.middleMenu').removeClass('menuFixed headerFadeIn').addClass('headerFadeInUp');//選單隱藏
    $('.middleMenu-Fin').removeClass('menuFixed01 headerFadeIn').addClass('headerFadeInUp');
  }

  lastScrollTop = st;
}, false);