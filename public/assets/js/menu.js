var menu = {

    open_click: function() {
        $(".menu_switch").click(function() {
            menu.open();
            menu.mCustomScrollbar();
            // 電腦版才啟動
            if ($(window).width() > 1024) {
                menu.slick();
                menu.slickPageCount();
            }
        });
    },

    open: function() {

        $("._menu").addClass("open");
        $("body").css("overflow-y", "hidden");
    },

    close_click: function() {
        $(".menu_close").click(function() {
            menu.close();
        });
    },

    close: function() {
        $("._menu").removeClass("open");
        $("._menu").addClass("close");
        $("body").css("overflow-y", "visible");
        setTimeout(function() {
            $("._menu").removeClass("close");
            // 關閉已開啟的子選單
            $("._menu .menu_content").removeClass("on");
            // $("._menu .child_list").removeClass("on");

            // 選單關閉時同時停止slick避免有bug
            $('._menu ._slick ul').slick('unslick');
        }, 400);
    },

    mCustomScrollbar: function() {
        $(".main_list").mCustomScrollbar({
            scrollInertia: 300,
            scrollEasing: "ease",
            mouseWheel: { preventDefault: true }
        });

        $(".child_list_box").mCustomScrollbar({
            scrollInertia: 300,
            scrollEasing: "ease",
            mouseWheel: { preventDefault: true }
        });
    },

    slick: function() {
        $("._menu ._slick ul").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 1000,
            autoplay: true,
            // infinite: false,
            autoplaySpeed: 5000,
            lazyLoad: 'ondemand',
            swipe: false,

        });

        // 顯示頁數
        $('._menu ._slick ul').on('afterChange', function(event, slick, currentSlide, nextSlide) {
            var i = $("._menu ._slick ul .slick-active").prevAll().length;
            $("._menu ._slick ._pageCount span:first-child").text(i);
        });

    },

    slickPageCount: function() {
        var ele = document.querySelectorAll("._menu ._slick ul li")

        var eletotalnumb = ele.length - 2

        $("._menu ._slick ._pageCount span:last-child").text(eletotalnumb);

    },

    // 子選單開啟
    childListOpen: function() {
        $("._menu ._product ul li").click(function() {
        	$("._menu .menu_content").removeClass("chang");
        	menu.childListChang();
            $("._menu .menu_content").addClass("on");
            // $("._menu .child_list").addClass("on");
            setTimeout(function() {
                $("._menu .child_list").css("overflow", "auto");
            }, 800);

        });
    },

    // 子選單關閉
    childListClose: function() {
        $("._menu ._back_main_list").click(function() {
            $("._menu .menu_content").removeClass("on");
        });
    },

    // 選擇其他子選單
    childListChang: function() {
        if ($(window).width() > 1024) {
            if ($(".menu_content").hasClass("on")) {
                $("._menu .menu_content").addClass("chang");
            }
        }
    },

    all: function() {
        menu.open_click();
        menu.close_click();
        menu.childListOpen();
   
        // 手機板才啟動
        if ($(window).width() < 701) {
            menu.childListClose();
        }
    }
}