//  Adds data that is used to translate
i18n.translator.add({
    values:{
        "You must entry the required field data (with pink border)": "You must entry the required field data (with pink border)",
        "Your message has been served": "Your message has been served",
        "Invalid Captcha Code": "Invalid Captcha Code",
        "Invalid email format": "Invalid email format"
    }
})
