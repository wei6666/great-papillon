//  Adds data that is used to translate
i18n.translator.add({
    values:{
        "You must entry the required field data (with pink border)": "您必需填寫欄位資料 (被標記粉紅色框)",
        "Your message has been served": "你的訊息已送達",
        "Invalid Captcha Code": "無效的圖型驗證碼",
        "Invalid email format": "無效的電子信箱格式"
    }
})
