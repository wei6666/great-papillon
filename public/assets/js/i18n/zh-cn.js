//  Adds data that is used to translate
i18n.translator.add({
    values:{
        "You must entry the required field data (with pink border)": "您必须填写栏位资料(被标记粉红色框)",
        "Your message has been served": "你的讯息已送达",
        "Invalid Captcha Code": "无效的图形验证码",
        "Invalid email format": "无效的电子信箱格式"
    }
})
