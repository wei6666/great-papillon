$(document).ready( function(){

    /*memberPassword_frame 會員密碼修改*/
    if ( $('body').hasClass('memberPassword_frame') == true ) {

        /*會員內頁內容切換*/
        member_switch();

        /*會員驗證碼填寫*/
        vcode();

        /*倒數計時器設定*/
        countdown_timer();

    }


    /*memberJoin_frame 會員註冊*/
    if ( $('body').hasClass('memberJoin_frame') == true ) {

        /*會員內頁內容切換*/
        member_switch();

        /*會員驗證碼填寫*/
        vcode();

        /*倒數計時器設定*/
        countdown_timer();

        /*會員條款同意按鈕*/
        agree_btn();

        /*會員服務條款 scroll bar*/
        member_benefits_scroll();

        /*下拉選單*/
        dropMenu();

    }


    /*member_guide 會員總覽*/
    // if ( $('body').hasClass('member_guide') == true ) {

    //     bonus_height();
        
    // }


    /*memberAccount_frame 會員帳號管理*/
    if ( $('body').hasClass('memberAccount_frame') == true ) {

        /*月曆日曆*/
        $('.calender input').dateDropper({});

        /*設定大頭貼*/
        set_picture();

        /*會員下拉選單*/
        dropMenu();
        
    }


    /*orderDetail_frame 訂購單*/
    if ( $('body').hasClass('orderDetail_frame') == true ) {
        
        /*blazy*/
        work_blazy();
        
        /*明細內容切換*/
        orderDetail_switch();
        
    }

    /*shopping_cart 購物車*/
    if ( $('body').hasClass('shopping_cart') == true ) {
        
        /*blazy*/
        work_blazy();

        /*刪除item*/
        delete_item();

        /*付款方式切換*/
        payWay_switch();

        /*付款內容切換*/
        payMent_switch();

        /*發票內容切換*/
        invoce_switch();

        /*開關shipping_block*/
        shipping_switch();

        /*下拉選單*/
        dropMenu();

        /*shipping內容切換*/
        shipping_content_switch();

        /*svg動畫*/
        // var $svg = $('svg').drawsvg();

        // console.log($svg);
        
        // $svg.drawsvg('animate');
        
    }

    /*member_bonus*/
    if ( $('body').hasClass('member_bonus') == true ) {

        //紅利點數使用說明 bonus rule
        bonus_rule();

    }

    /*member_favorite*/
    if ( $('body').hasClass('member_favorite') == true ) {
        
        //remove item
        remove_item();

    }


    /*member_address*/
    if ( $('body').hasClass('member_address') == true ) {
        
        //address_box
        address_box();

        /*下拉選單*/
        dropMenu();

    }

    /*member_guide*/
    if ( $('body').hasClass('member_guide') == true ) {

        /*blazy*/
        work_blazy();

    }

});




/***************************************************************************************************************************/
///////////////////////////////////////
//           member_switch           //
//////////////////////////////////////
/*註冊頁 與 查詢密碼頁 的 內容切換*/
function member_switch(){

    var frame_box        = $('.frame_box'),  //內容
        next_content_btn = $('.next_content_btn'),  //切換內容按鈕
        back_content_btn = $('.back_content_btn'),  //切換內容按鈕
        open_success_btn = $('.open_success')  //打開成功畫面按鈕
        member_success   = $('.member_success');  //成功畫面

    //依照內容數量 給予每個內容一個 id
    for ( var index = 0; index < frame_box.length; index++) {

        frame_box.eq(index).attr('box-id',index);

    }

    //往下內容按鈕 當切換內容的按鈕 被按下 把當前的內容隱藏 下個內容打開
    next_content_btn.click('click',function(){

        var now_box_id  = parseInt($(this).closest('.frame_box').attr('box-id')),
            next_box_id = now_box_id + 1;

        frame_box.eq(now_box_id).addClass('not_show');

        frame_box.eq(next_box_id).removeClass('not_show');

    });

    //往回內容按鈕
    back_content_btn.on('click',function(){

        var now_box_id  = parseInt($(this).closest('.frame_box').attr('box-id')),
            back_box_id = now_box_id - 1;

        frame_box.eq(now_box_id).addClass('not_show');
        
        frame_box.eq(back_box_id).removeClass('not_show');

    });

    //成功畫面呼叫
    open_success_btn.on('click',function(){

        if ( member_success.hasClass('not_show') == true ) {

            member_success.removeClass('not_show');
            
        }

        // $('.member_success').niceScroll();

    });

}
/***************************************************************************************************************************/



/***************************************************************************************************************************/
////////////////////////////////////////////////
//               會員驗證碼填寫               ///
///////////////////////////////////////////////
function vcode(){
    //cache dom
    var $inputs = $("#vcode").find("input");

    //bind events
    $inputs.on('keyup', processInput);
  
    //define methods
    function processInput(e) {
        var x = e.charCode || e.keyCode;

        if( (x == 8 || x == 46) && this.value.length == 0) {

            var indexNum = $inputs.index(this);

            if(indexNum != 0) {
                $inputs.eq($inputs.index(this) - 1).focus();
            }

        }
    
        if( ignoreChar(e) ) 
            return false;
        else if (this.value.length == this.maxLength) {
            // $(this).next('input').focus();
            $(this).closest('li').next('li').find('input').focus();
        }
    }
    function ignoreChar(e) {
        var x = e.charCode || e.keyCode;
        if (x == 37 || x == 38 || x == 39 || x == 40 )
            return true;
        else 
            return false
    }
}
/***************************************************************************************************************************/



/***************************************************************************************************************************/
////////////////////////////////////////////////
//              會員條款同意按鈕              ///
///////////////////////////////////////////////
function agree_btn(){

    var icon_btn_group = $('.icon_btn_group'),
        tick_btn       = $('.icon_btn_group .tick');

    tick_btn.on('click',function(){

        if ( $(this).hasClass('agree') == false ) {
            
            $(this).addClass('agree');

        }else {

            $(this).removeClass('agree');

        }

    });

}
/***************************************************************************************************************************/



/***************************************************************************************************************************/
////////////////////////////////////////////////
//                會員服務條款                ///
///////////////////////////////////////////////
function member_benefits_scroll(){

    var service_term = $('.service_term'),
        box          = $('.service_term .box'),
        close_btn    = $('.service_term ._close'),
        open_btn     = $('.icon_btn_group .terms');

    //會員條款scroll bar
    box.niceScroll();

    //打開按鈕
    open_btn.on('click',function(){

        if ( service_term.hasClass('open') == false ) {

            service_term.addClass('open');


            
        }

    });

    //關閉按鈕
    close_btn.on('click',function(){

        if ( service_term.hasClass('open') == true ) {
            
            service_term.removeClass('open');
            
        }

    });

    //點擊box以外區域 也關閉
    service_term.on('click',function(e){

        if (e.target !== this) { 
            return; 
        }else {
            service_term.removeClass('open');
        }

    });

}
/***************************************************************************************************************************/



/***************************************************************************************************************************/
////////////////////////////////////////////////
//                 下拉選單                  ///
///////////////////////////////////////////////
function dropMenu(){

    var selectMenu    = $('.selectMenu'),
        dropMenu_list = $('.dropMenu .dropMenu_box li'),
        dropMenu_box  = $('.dropMenu .dropMenu_box');


    //
    selectMenu.click(function() {

        var ele = $(this)

        $(ele).toggleClass("open");

        // 開啟選單

        $(ele).closest('.dropMenu').find("ul").slideToggle(400);

        // 點選其他物件 關閉選單
        $(document).click(function(e) {

            if ($(ele).parent().has(e.target).length === 0) {

                $(ele).closest('.dropMenu').find('.dropMenu_box').slideUp(400);
                $(ele).removeClass("open");

            }

        });

    });

    // 選擇li後關閉選單
    dropMenu_list.click(function() {

        // 作用對象.selectMenu
        selectMenu.removeClass("open");

        // 作用對象.menulist ul li
        $(this).addClass("checked");

        // 作用對象其他.menulist ul li
        $(this).siblings().removeClass("checked");

        // 作用對象.menulist ul 
        $(this).parent().slideUp(400);

        // .selectMenu p 文字更改
        $(this).closest('.dropMenu').find(".selectMenu p").text($(this).find("p").text());

    });

    //下拉選單scroll bar
    dropMenu_box.niceScroll({
        cursorcolor: '#fff',
        cursorborder: 'none',
        cursorwidth: 4,
    });

}
/***************************************************************************************************************************/



/***************************************************************************************************************************/
////////////////////////////////////////////////
//                 倒數計時器                 ///
///////////////////////////////////////////////
function countdown_timer(){

    var count_text = $('.countdown p');
        minutes    = $('.countdown p').eq(0).attr('data-m') / 1,
        seconds    = $('.countdown p').eq(1).attr('data-s') / 1;


    //
    if ( seconds < 10 ) {

        seconds = '0' + seconds;

    }


    count_text.eq(0).text(minutes);

    count_text.eq(1).text(seconds);

}

//開始計時
function strat_countdown(){

    var count_text = $('.countdown p');
        minutes    = $('.countdown p').eq(0).attr('data-m') / 1,
        seconds    = $('.countdown p').eq(1).attr('data-s') / 1;


    /***** 起始值 *****/
    //秒數
    var ippp = 60;

    //
    minutes = minutes - 1;

    count_text.eq(0).text(minutes);

    //
    ippp = ippp - 1;
    
    count_text.eq(1).text(ippp);

    //啟動計時器
    setDelay();
    /***** 起始值 *****/
    

    //計時器功能
    function setDelay() {

        if ( minutes > 0 && ippp > 0 ) {
            //分 秒 皆大於0 (ex: 2:35, 2:39之類的)

            setTimeout(function(){
                
                ippp = ippp - 1;

                //
                if ( ippp < 10 ) {
                    
                    ippp = '0' + ippp;
            
                }

                //
                count_text.eq(1).text(ippp);
    
                //
                setDelay();
    
            }, 1000);
            
        }else if ( minutes > 0 && ippp == 0 ) {
            //分大於0 秒數剛好為0 (ex: 2:00, 5:00之類的)

            ippp = 60;

            //
            minutes = minutes - 1;

            count_text.eq(0).text(minutes);

            //
            ippp = ippp - 1;

            count_text.eq(1).text(ippp);

            //
            setDelay();

        }else if ( minutes == 0 && ippp > 0 ) {
            //分為0 秒數還大於0 (ex: 0:09, 0:36之類的)

            setTimeout(function(){
                
                ippp = ippp - 1;

                //
                if ( ippp < 10 ) {
                    
                    ippp = '0' + ippp;
            
                }

                //
                count_text.eq(1).text(ippp);
    
                //
                setDelay();
    
            }, 1000);

        }

    }

}
/***************************************************************************************************************************/



/***************************************************************************************************************************/
////////////////////////////////////////////////
//                大頭貼開光箱                ///
///////////////////////////////////////////////
function set_picture(){

    var set_pic_btn   = $('.set_pic'),
        close_pic_btn = $('.close_pic'),
        set_picture   = $('.set_picture'),
        body          = $('body'),
        header        = $('header'),
        set_pic_frame = false;

    var rwd = window.screen.width == $(window).width();

    //打開
    set_pic_btn.on('click',function(){

        if ( set_pic_frame == false ) {

            set_picture.addClass('show');

            if (rwd == false) {

                body.css({
                    'overflow':'hidden',
                    'padding-right':'17px',
                });
    
                header.css({
                    'padding-right':'17px',
                });

            }else {

                body.css({
                    'overflow':'hidden',
                });

            }

            set_pic_frame = true;
            
        }

    });

    //關閉
    close_pic_btn.on('click',function(){
        
        if ( set_pic_frame == true ) {

            set_picture.removeClass('show');

            set_picture.addClass('no_show');

            setTimeout(function() {

                body.css({
                    'overflow':'scroll',
                    'padding-right':'',
                });
    
                header.css({
                    'padding-right':'0px',
                });

                set_picture.removeClass('no_show');

                set_pic_frame = false;

            }, 1000);
            
        }

    });

    //關閉
    set_picture.on('click',function(e){

        if (e.target !== this) {

            return;

        } else {

            set_picture.removeClass('show');
            
            set_picture.addClass('no_show');

            setTimeout(function() {

                body.css({
                    'overflow':'scroll',
                    'padding-right':'',
                });
    
                header.css({
                    'padding-right':'0px',
                });

                set_picture.removeClass('no_show');

                set_pic_frame = false;

            }, 1000);

        }

    });

    //scroll bar
    set_picture.niceScroll();

}
/***************************************************************************************************************************/



/***************************************************************************************************************************/
////////////////////////////////////////////////
//                   b-lazy                  ///
///////////////////////////////////////////////
function work_blazy(){

    //member_orderDetail
    if ( $('body').hasClass('orderDetail_frame') == true ) {
        
        var bLazy = new Blazy({
            offset: 100,
            success: function(ele) {
            }
        });
        
    }

    //shopping_cart
    if ( $('body').hasClass('shopping_cart') == true ) {
        
        var bLazy = new Blazy({
            offset: 100,
            success: function(ele) {
            }
        });
        
    }

    //member_guide
    if ( $('body').hasClass('member_guide') == true ) {
        
        var bLazy = new Blazy({
            offset: 70,
            success: function(ele) {

                $(ele).closest('.list').addClass('show');

            }
        });
        
    }

}
/***************************************************************************************************************************/



/***************************************************************************************************************************/
////////////////////////////////////////////////
//                明細內容切換                ///
///////////////////////////////////////////////
/*註冊頁 與 查詢密碼頁 的 內容切換*/
function orderDetail_switch(){
    
    var frame_box        = $('.detail_content'),  //內容
        switch_btn       = $('.order_nav .form_btn a');  //切換內容按鈕

    //依照內容數量 給予每個內容一個 id
    for ( var index = 0; index < frame_box.length; index++) {

        frame_box.eq(index).attr('box-id',index);

    }
    //依照內容數量 給予每個'切換按鈕'一個 id
    for ( var index = 0; index < frame_box.length; index++) {
        
        switch_btn.eq(index).attr('btn-id',index);

    }

    //按鈕切換
    switch_btn.on('click',function(){

        var a = $(this).attr('btn-id');

        if ( frame_box.eq(a).hasClass('show') == false ) {

            //按鈕樣式切換
            if ( $(this).hasClass('black') == false ) {

                switch_btn.removeClass('black').addClass('gray');

                switch_btn.eq(a).removeClass('gray').addClass('black');
                
            }

            //內容切換
            frame_box.removeClass('show');
            
            frame_box.eq(a).addClass('show');
        }

    });

}
/***************************************************************************************************************************/



/***************************************************************************************************************************/
////////////////////////////////////////////////
//                付款方式切換                ///
///////////////////////////////////////////////
function payWay_switch(){

    var list = $('.pay_way_list');

    list.on('click',function(){

        if ( $(this).hasClass('clicked') == false ) {

            list.removeClass('clicked');
            
            $(this).addClass('clicked');
            
        }

    });

}
/***************************************************************************************************************************/



/***************************************************************************************************************************/
///////////////////////////////////////
//            付款內容切換            //
//////////////////////////////////////
function payMent_switch(){

    var frame_box          = $('.payment .box'),  //內容
        payment            = $('.payment');        //內容內框
        payment_block      = $('.payment_block'),  //內容外框
        next_content_btn   = $('.next_content_btn'),  //切換內容按鈕
        cancel_content_btn = $('.cancel_content_btn');  //切換內容按鈕

    var rwd = window.screen.width == $(window).width();

    
    //依照內容數量 給予每個內容一個 id
    for ( var index = 0; index < frame_box.length; index++) {
        
        frame_box.eq(index).attr('payment-id',index);

    }
    for ( var index = 0; index < next_content_btn.length; index++) {
        
        next_content_btn.eq(index).attr('btn-id',index);

    }
    
    //打開
    next_content_btn.on('click',function(){

        var a = $(this).attr('btn-id');

        if ( payment_block.hasClass('show') == false ) {

            payment_block.addClass('show');

            frame_box.eq(a).addClass('show');

            if ( rwd == false ) {

                $('body').css({ 'overflow': 'hidden', 'padding-right': '17px', });
                
            }else {

                $('body').css({ 'overflow': 'hidden', });

            }
            
        }else {

            frame_box.eq(a - 1).removeClass('show').addClass('no_show');

            setTimeout(function(){

                frame_box.eq(a - 1).removeClass('no_show');
                
                frame_box.eq(a).removeClass('no_show').addClass('show');
                
            }, 1000);

        }

    });

    //關閉
    cancel_content_btn.on('click',function(){

        $(this).closest('.box').removeClass('show').addClass('no_show');

        payment_block.removeClass('show').addClass('no_show');

        setTimeout(function(){

            frame_box.removeClass('no_show');

            payment_block.removeClass('no_show');

            if ( rwd == false ) {

                $('body').css({ 'overflow': '', 'padding-right': '' });
                
            }else {

                $('body').css({ 'overflow': '', });

            }
            
        }, 1000);

    });

    //scroll bar
    // $('.payment').niceScroll();

}
/***************************************************************************************************************************/



/***************************************************************************************************************************/
///////////////////////////////////////
//            發票內容切換            //
//////////////////////////////////////
function invoce_switch(){

    var check_box = $('.invoce .check_box');
    

    check_box.on('click',function(){

        if ( $(this).hasClass('show') == false ) {
            
            check_box.removeClass('show');
    
            $(this).addClass('show');
            
        }

    });

}
/***************************************************************************************************************************/


/***************************************************************************************************************************/
///////////////////////////////////////
//             刪除item              //
//////////////////////////////////////
function delete_item(){

    var delete_btn = $('.shopping_cart .delete_btn');

    delete_btn.on('click',function(){

        var item = $(this).closest('.order_li');

        if ( item.hasClass('close') == false ) {

            item.addClass('close');

            setTimeout(function(){

                item.remove();
                
            }, 1000);
            
        }

    });

}
/***************************************************************************************************************************/



/***************************************************************************************************************************/
///////////////////////////////////////
//        開關shipping_block         //
//////////////////////////////////////
function shipping_switch(){

    var open_btn     = $('.shipping_btn'),
        hidden_block = $('.shipping_block'),
        cancel_btn   = $('.cancel_btn'),
        close_btn    = $('.close_btn'),
        save_btn     = $('.save_btn'),
        box          = $('.shipping_list .box');

    var rwd = window.screen.width == $(window).width();

    //打開鍵
    open_btn.on('click',function(){

        box.eq(0).addClass('open_show');

        if ( hidden_block.hasClass('show') == false ) {

            hidden_block.addClass('show');

            if ( rwd == false ) {
                
                $('body').css({ 'overflow': 'hidden', 'padding-right': '17px', });
                
            }else {

                $('body').css({ 'overflow': 'hidden', });

            }
            
        }

    });

    //取消鍵
    cancel_btn.on('click',function(){
        
        if ( hidden_block.hasClass('show') == true ) {

            hidden_block.removeClass('show');

            hidden_block.addClass('no_show');

            setTimeout(function(){

                box.removeClass('show').removeClass('open_show');

                hidden_block.removeClass('no_show');

                if ( rwd == false ) {
                    
                    $('body').css({ 'overflow': '', 'padding-right': '' });
                    
                }else {
    
                    $('body').css({ 'overflow': '', });
    
                }
                
            }, 1000);
            
        }

    });

    //關閉鍵
    close_btn.on('click',function(){

        if ( hidden_block.hasClass('show') == true ) {

            hidden_block.removeClass('show');
            
            hidden_block.addClass('no_show');

            setTimeout(function(){

                box.removeClass('show').removeClass('open_show');

                hidden_block.removeClass('no_show');

                if ( rwd == false ) {
                    
                    $('body').css({ 'overflow': '', 'padding-right': '' });
                    
                }else {
    
                    $('body').css({ 'overflow': '', });
    
                }
                
            }, 1000);
            
        }

    });

    //儲存鍵
    // save_btn.on('click',function(){
        
    //     if ( hidden_block.hasClass('show') == true ) {

    //         hidden_block.removeClass('show');
            
    //         hidden_block.addClass('no_show');

    //         setTimeout(function(){

    //             box.removeClass('show').removeClass('open_show');

    //             hidden_block.removeClass('no_show');

    //             if ( rwd == false ) {
                    
    //                 $('body').css({ 'overflow': '', 'padding-right': '' });
                    
    //             }else {
    
    //                 $('body').css({ 'overflow': '', });
    
    //             }
                
    //         }, 1000);
            
    //     }

    // });

}
/***************************************************************************************************************************/



/***************************************************************************************************************************/
///////////////////////////////////////
//          shipping內容切換         //
//////////////////////////////////////
function shipping_content_switch(){

    var content_btn = $('.content_btn'),
        content     = $('.shipping_list .box');

    //依照內容數量 給予每個內容一個 id
    for ( var index = 0; index < content_btn.length; index++) {
        
        content_btn.eq(index).attr('btn-id',index);

    }
    //依照內容數量 給予每個內容一個 id
    for ( var index = 0; index < content.length; index++) {
        
        content.eq(index).attr('content-id',index);

    }

    content_btn.on('click',function(){

        content.eq(0).removeClass('open_show');

        //
        if ( $(this).hasClass('black') == false ) {

            content_btn.removeClass('black').addClass('gray');

            $(this).removeClass('gray').addClass('black');
            
        }


        //
        var a = $(this).attr('btn-id');

        content.removeClass('show');

        content.eq(a).addClass('show');

    });

}
/***************************************************************************************************************************/



/***************************************************************************************************************************/
///////////////////////////////////////
//             bonus rule            //
//////////////////////////////////////
function bonus_rule(){

    var list = $('.use_box .frame li');

    var open_btn   = $('.use_information .open_btn'),
        close_btn  = $('.use_box .close_btn'),
        frame      = $('.use_box'),
        frame_box  = $('.use_box .box');

    var rwd = window.screen.width == $(window).width();
    var body   = $('body'),
        header = $('header');


    //打開
    open_btn.on('click',function(){

        if ( frame.hasClass('open') == false ) {

            frame.addClass('open');

            if (rwd == false) {
                
                body.css({
                    'overflow':'hidden',
                    'padding-right':'',
                });
    
                header.css({
                    'padding-right':'17px',
                });

            }else {

                body.css({
                    'overflow':'hidden',
                });

            }
            
        }

    });


    //關閉
    close_btn.on('click',function(){

        frame.removeClass('open').addClass('close');

        setTimeout(function(){
            
            frame.removeClass('close');

            body.css({
                'overflow':'',
                'padding-right':'',
            });

            header.css({
                'padding-right':'0px',
            });

            
        }, 1000);

    });


    //點旁邊關閉
    frame.on('click',function(e){

        if (e.target !== this) { 
            
            return; 

        }else {

            if ( frame.hasClass('open') == true ) {
                
                frame.removeClass('open').addClass('close');
    
                setTimeout(function(){
    
                    frame.removeClass('close');

                    body.css({
                        'overflow':'',
                        'padding-right':'',
                    });
        
                    header.css({
                        'padding-right':'0px',
                    });
                    
                }, 1000);
                
            }

        }

    });


    //點擊事件
    list.on('click',function(){
        
        if ( $(this).hasClass('open') == false ) {

            var list_opened = $('.use_box .frame li.open');

            list_opened.removeClass('open').find('.content').slideUp();

            $(this).addClass('open');

            $(this).find('.content').slideToggle(function() {

                //scroll bar resize
                frame_box.getNiceScroll().resize();

            });
            
        }else {

            $(this).removeClass('open');            

            $(this).find('.content').slideToggle();

        }
        
    });


    //active scroll bar
    frame_box.niceScroll({horizrailenabled:false});

}
/***************************************************************************************************************************/



/***************************************************************************************************************************/
///////////////////////////////////////
//            remove_item            //
//////////////////////////////////////
function remove_item(){

    var open_btn   = $('.box_1 .open_btn'),
        close_btn  = $('.delete_box .close_btn'),
        frame      = $('.delete_box'),
        frame_box  = $('.delete_box .box');

    var rwd = window.screen.width == $(window).width();
    var body   = $('body'),
        header = $('header');


    //打開
    open_btn.on('click',function(){

        if ( frame.hasClass('open') == false ) {
            
            frame.addClass('open');

            if (rwd == false) {
                
                body.css({
                    'overflow':'hidden',
                    'padding-right':'',
                });
    
                header.css({
                    'padding-right':'17px',
                });

            }else {

                body.css({
                    'overflow':'hidden',
                });

            }
            
        }

    });


    //關閉
    close_btn.on('click',function(){

        frame.removeClass('open').addClass('close');
        
        setTimeout(function(){
            
            frame.removeClass('close');

            body.css({
                'overflow':'',
                'padding-right':'',
            });

            header.css({
                'padding-right':'0px',
            });

            
        }, 1300);

    });

}
/***************************************************************************************************************************/



/***************************************************************************************************************************/
///////////////////////////////////////
//            address_box            //
//////////////////////////////////////
function address_box(){
    
    var open_btn     = $('.member_address .frame .open_btn'),
        close_btn    = $('.address_box .close_btn'),
        frame        = $('.address_box'),
        frame_box    = $('.address_box .box'),
        frame_adbox  = $('.address_box .adbox');

    var rwd = window.screen.width == $(window).width();
    var body   = $('body'),
        header = $('header');

    
    //打開
    //判斷開啟的adbox  
    open_btn.on('click',function(){

        // frame_box.getNiceScroll().resize();
        
        var e = $(this);

        if ( frame.hasClass('open') == false && e.hasClass('btn_add') == true ) {

            frame.addClass('open');

            $('.adbox_1').addClass('open');
            
            if (rwd == false) {
                
                body.css({
                    'overflow':'hidden',
                    'padding-right':'',
                });
    
                header.css({
                    'padding-right':'17px',
                });

            }else {

                body.css({
                    'overflow':'hidden',
                });

            }
            
        }
        if ( frame.hasClass('open') == false && e.hasClass('btn_edit') == true ) {
            
            frame.addClass('open');

            $('.adbox_2').addClass('open');
            
            if (rwd == false) {
                
                body.css({
                    'overflow':'hidden',
                    'padding-right':'',
                });
    
                header.css({
                    'padding-right':'17px',
                });

            }else {

                body.css({
                    'overflow':'hidden',
                });

            }
            
        }
        if ( frame.hasClass('open') == false && e.hasClass('btn_delete') == true ) {
            
            frame.addClass('open');

            $('.adbox_3').addClass('open');
            
            if (rwd == false) {
                
                body.css({
                    'overflow':'hidden',
                    'padding-right':'',
                });
    
                header.css({
                    'padding-right':'17px',
                });

            }else {

                body.css({
                    'overflow':'hidden',
                });

            }
            
        }

    });


    //關閉
    close_btn.on('click',function(){
        
        frame.removeClass('open').addClass('close');

        frame_adbox.removeClass('open');

        setTimeout(function(){
            
            frame.removeClass('close');

            body.css({
                'overflow':'',
                'padding-right':'',
            });

            header.css({
                'padding-right':'0px',
            });

            
        }, 1000);

    });


    //點旁邊關閉
    frame.on('click',function(e){
        
        if (e.target !== this) { 
            
            return; 

        }else {

            if ( frame.hasClass('open') == true ) {
                
                frame.removeClass('open').addClass('close');

                frame_adbox.removeClass('open');
    
                setTimeout(function(){
    
                    frame.removeClass('close');

                    body.css({
                        'overflow':'',
                        'padding-right':'',
                    });
        
                    header.css({
                        'padding-right':'0px',
                    });
                    
                }, 1000);
                
            }

        }

    });


    //active scroll bar
    // frame.niceScroll({horizrailenabled:false});

}
/***************************************************************************************************************************/